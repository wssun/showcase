package com.tecace.showcase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.WindowManager;

import com.tecace.showcase.util.AppConst;
import com.tecace.showcase.util.GMailSender;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class CrashActivity extends Activity {

    private final String TAG = this.getClass().getCanonicalName();

    private Context mContext;

    private WaveMessageView mWaveMessageView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash);

        mContext = this.getApplicationContext();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mWaveMessageView = (WaveMessageView) findViewById(R.id.main_message_view);

        Bundle bundle = getIntent().getExtras();
        String report = bundle.getString("error");
        String msg = bundle.getString("msg");

        mWaveMessageView.showCrashErrorMessageWithAnimation(msg);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String date = sdf.format(new Date());
        String title = new StringBuilder()
                .append("AppTable Exception Report ")
                .append(date).toString();
        new SendMailTask().execute("iheartappcollection@gmail.com",
                "Samsung#1",
                title,
                report.toString(),
                null);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        sendBroadcast(new Intent(AppConst.ACTION_APP_CRASHED));

        super.onDestroy();
    }

    private class SendMailTask extends AsyncTask<String, Integer, Boolean> {
        String attachedFile = null;
        @Override
        protected Boolean doInBackground(String... params) {
            String user = params[0];
            String password = params[1];
            String title = params[2];
            String message = params[3];
            attachedFile = params[4];
            String recipient = "sTableSupport@tecace.com";
            //String recipient = "sun0519@tecace.com";
            boolean success;

            try {
                if (recipient == null || recipient.length() <= 0) return false;

                GMailSender sender = new GMailSender(user, password);

                if (attachedFile != null && attachedFile.length() > 0)
                    sender.addAttachment(attachedFile);

                success = sender.sendMail(title,
                        message,
                        user,
                        recipient);

                //sendBroadcast(new Intent(AppConst.ACTION_APP_CRASHED));
                //defaultUEH.uncaughtException(mThread, mThrowable);
                return success;
            } catch (Exception e) {
                Log.e(TAG, Arrays.toString(e.getStackTrace()));
                return false;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Boolean isSuccess) {
            if (isSuccess) {
            } else {
            }
        }
    }
}
