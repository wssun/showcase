package com.tecace.showcase.navigation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tecace.showcase.R;
//import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.util.BuildUtil;
import com.tecace.showcase.util.FontTypeface;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class AttractorLoopView extends LinearLayout {

    private static final String TAG = "AttractorLoopView";

    private String[] mPassionText;

    // FIXED VALUES
//    private static final int HEIGHT_SMALLEST_PASSION = 24;  // DEFAULT HEIGHT OF PASSION LABEL
//    private static final int HEIGHT_MEDIUM_PASSION = 41;    // DEFAULT HEIGHT OF PASSION LABEL
//    private static final int HEIGHT_BIGGEST_PASSION = 58;   // DEFAULT HEIGHT OF PASSION LABEL
//    private static final float SCALE_BIGGEST_SCALE = (float) HEIGHT_BIGGEST_PASSION / (float) HEIGHT_SMALLEST_PASSION;
//    private static final float SCALE_MEDIUM_SCALE = (float) HEIGHT_MEDIUM_PASSION / (float) HEIGHT_SMALLEST_PASSION;
//    private int HEIGHT_SMALLEST_PASSION = 24;  // DEFAULT HEIGHT OF PASSION LABEL
//    private int HEIGHT_MEDIUM_PASSION = 41;    // DEFAULT HEIGHT OF PASSION LABEL
//    private int HEIGHT_BIGGEST_PASSION = 58;   // DEFAULT HEIGHT OF PASSION LABEL
//    private float SCALE_BIGGEST_SCALE = (float) HEIGHT_BIGGEST_PASSION / (float) HEIGHT_SMALLEST_PASSION;
//    private float SCALE_MEDIUM_SCALE = (float) HEIGHT_MEDIUM_PASSION / (float) HEIGHT_SMALLEST_PASSION;
    private static final int SIZE_COLOR_BAR = 30;           // DEFAULT HEIGHT OF COLOR BAR
    private static final int MARGIN_BOTTOM_BAR = 28;        // MARGIN OF THIS VIEW
    //private static final int MARGIN_BOTTOM_PASSION = 24;    // MARGIN OF THIS VIEW
    private static final int MARGIN_BOTTOM_PASSION = 16;    // MARGIN OF THIS VIEW

    private static final int ALPHA_GRADIENT_PADDING = 30;
    private static final int ALPHA_GRADIENT_WIDTH = 75;

    private static final int MARGIN_NORMAL = 90;            // LEFT&RIGHT MARGIN OF PASSION (LEVEL 2,3)

    private static final int FONT_S = 20;
    private static final int FONT_M = 45;
    private static final int FONT_L = 60;

    private int nPassion = 10;                       // DEFAULT NUMBER OF PASSION : IT'S AUTOMATICALLY UPDATED AFTER CALL 'setPassions(String[])'
    private int mCenterIndex = nPassion / 2;                   // DEFAULT CENTER INDEX OF PASSION


    // SMALL SIZE OF PASSIONS (MUST UPDATE IF NUMBER OF PASSION IS CHANGED)
    // private Point[] mPassionSizes = new Point[nPassion];
    private float[] mAccumulatedWidth = new float[nPassion];
    private PassionLocInformation[] mPassionLocInformation = new PassionLocInformation[nPassion];

    // PROPERTIES OF ATTRACTOR LOOP
    private Point mScreenSize = new Point();
    private int mScreenCenter = 0;

    // LAYOUT PARAMS OF PARENT(THIS)
    private LinearLayout mPassionContainer;
    // CALLBACK
    OnAttractorLoopEventListener mOnAttractorEventListener;  // CALLBACK LISTENER (SERVICE TABLE)

    public AttractorLoopView(Context context) {
        super(context);
        init();
    }

    public AttractorLoopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AttractorLoopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setMotionEventSplittingEnabled(false);

        mPassionContainer = new LinearLayout(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.bottomMargin = SIZE_COLOR_BAR + MARGIN_BOTTOM_BAR + MARGIN_BOTTOM_PASSION;
        mPassionContainer.setGravity(Gravity.BOTTOM);
        mPassionContainer.setMotionEventSplittingEnabled(false);
        this.addView(mPassionContainer, params);
        mFlingHandler = new FlingHandler(this);
        mFinishFlingHandler = new FinishFlingHandler(this);

        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);
        mPaint.setDither(true);

        mColorBarPaint.setAntiAlias(true);
        mCenterPointPaint.setAntiAlias(true);
    }

    public int getMarginBottom() {
        return SIZE_COLOR_BAR + MARGIN_BOTTOM_BAR + MARGIN_BOTTOM_PASSION;
    }

    public int getFontL() {
        return FONT_L;
    }

    ArrayList<Bitmap> mSmallestPassionBitmap = new ArrayList<>();
    ArrayList<Bitmap> mMediumPassionBitmap = new ArrayList<>();
    ArrayList<Bitmap> mBiggestPassionBitmap = new ArrayList<>();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void generateEachPassionBitmaps() {
        int heights[] = {FONT_S, FONT_M, FONT_L};

        Typeface type = FontTypeface.getInstance().getFontThin();

        for (String passion : mPassionText) {
            for (int passionHeight : heights) {

                TextView textView = new TextView(getContext());
                textView.setText(passion);

                //837-APK-review-IX Table-20151007.pdf
                //page 6 Primary Navigation
                if (BuildUtil.hasLollipop()) {
                    TypedValue typedValue = new TypedValue();
                    if (passionHeight == FONT_S)
                        getResources().getValue(R.dimen.attractloopview_font_s_letter_spacing, typedValue, true);
                    else
                        getResources().getValue(R.dimen.attractloopview_font_letter_spacing, typedValue, true);
                    textView.setLetterSpacing(typedValue.getFloat());
                }

                textView.setTextColor(Color.WHITE);
                textView.setBackgroundColor(Color.TRANSPARENT);

                if (type == null)
                    Log.e(TAG, "type is null");
                textView.setTypeface(type);
                textView.setPaintFlags(textView.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, passionHeight);
                textView.setGravity(Gravity.BOTTOM);

                textView.setDrawingCacheEnabled(true);

                textView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                        MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
                textView.buildDrawingCache(true);

                int width = textView.getMeasuredWidth();
                int height = textView.getMeasuredHeight();


                Bitmap bitmap = Bitmap.createBitmap(textView.getDrawingCache());
                textView.setDrawingCacheEnabled(false);
                switch ((int) passionHeight) {
                    case FONT_S:
                        mSmallestPassionBitmap.add(bitmap);
                        break;
                    case FONT_M:
                        mMediumPassionBitmap.add(bitmap);
                        break;
                    case FONT_L:
                        mBiggestPassionBitmap.add(bitmap);
                        break;
                }

            }
        }
    }

    //========================================
    // PASSIONS
    //========================================
    public void onCreateAttractorLoopView() {
        // GET SCREEN SIZE
        mScreenSize.x = getResources().getDimensionPixelSize(R.dimen.screen_width);
        mScreenSize.y = getResources().getDimensionPixelSize(R.dimen.screen_height);
        mScreenCenter = mScreenSize.x / 2;

        // SET EVENT
        this.setOnTouchListener(new AttractorLoopTouchListener());

        float realAccumulatedSize = 0.0f;
        if (mSmallestPassionBitmap != null) {
            while (mSmallestPassionBitmap.size() > 0) {
                mSmallestPassionBitmap.get(0).recycle();
                mSmallestPassionBitmap.set(0, null);
                mSmallestPassionBitmap.remove(mSmallestPassionBitmap.get(0));
            }

            while (mMediumPassionBitmap.size() > 0) {
                mMediumPassionBitmap.get(0).recycle();
                mMediumPassionBitmap.set(0, null);
                mMediumPassionBitmap.remove(mMediumPassionBitmap.get(0));
            }

            while (mBiggestPassionBitmap.size() > 0) {
                mBiggestPassionBitmap.get(0).recycle();
                mBiggestPassionBitmap.set(0, null);
                mBiggestPassionBitmap.remove(mBiggestPassionBitmap.get(0));
            }
        }

        mSmallestPassionBitmap = new ArrayList<>();
        mMediumPassionBitmap = new ArrayList<>();
        mBiggestPassionBitmap = new ArrayList<>();
        generateEachPassionBitmaps();

        // GENERATE PASSIONS
        for (int index = 0; index < nPassion; index++) {
            ImageView imageView = new ImageView(getContext());
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            int margin = MARGIN_NORMAL;
            // SET SIZE OF PASSION VIEW
            float realWidth, realHeight;
            if (index == mCenterIndex) {            // CENTER PASSION
                //realWidth = (float) mPassionSizes[index].x * SCALE_BIGGEST_SCALE;
                //realHeight = HEIGHT_BIGGEST_PASSION;
                realWidth = mBiggestPassionBitmap.get(index).getWidth();
                realHeight = mBiggestPassionBitmap.get(index).getHeight();
                margin = MARGIN_NORMAL;
                imageView.setImageBitmap(mBiggestPassionBitmap.get(index));
                imageView.setTag(R.string.size_key, FONT_L);
            } else if (index == mCenterIndex - 1 || index == nPassion + mCenterIndex - 1 || index == mCenterIndex + 1) {   // NEIGHBOUR PASSION
                realWidth = mMediumPassionBitmap.get(index).getWidth();
                realHeight = mMediumPassionBitmap.get(index).getHeight();
                imageView.setImageBitmap(mMediumPassionBitmap.get(index));
                imageView.setTag(R.string.size_key, FONT_M);
            } else {                                // ELSE
                realWidth = mSmallestPassionBitmap.get(index).getWidth();
                realHeight = mSmallestPassionBitmap.get(index).getHeight();
                imageView.setImageBitmap(mSmallestPassionBitmap.get(index));
                imageView.setTag(R.string.size_key, FONT_S);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) realWidth, (int) realHeight);
            params.leftMargin = margin;
            params.rightMargin = margin;
            // COLLECT PASSION SIZE
            realAccumulatedSize += realWidth + (margin * 2);
            mAccumulatedWidth[index] = realAccumulatedSize;
            mPassionContainer.addView(imageView, params);
        }
        calculateLevelDistance();
    }

    // CALCULATE DISTANCE OF EACH LEVEL FROM SCREEN CENTER
    private void calculateLevelDistance() {
        for (int index = 0; index < nPassion; index++) {
            PassionLocInformation levelDistance = new PassionLocInformation();

            //#231 Category name 떨리는 현상 수정함
//            levelDistance.leftSmallDistance = (mSmallestPassionBitmap.get(index).getWidth() / 2 + MARGIN_NORMAL)
//                    + (mMediumPassionBitmap.get(((index + 1) % nPassion)).getWidth() + MARGIN_NORMAL * 2)
//                    + (mBiggestPassionBitmap.get(((index + 2) % nPassion)).getHeight() / 2 + MARGIN_NORMAL);
            levelDistance.leftSmallDistance = (mSmallestPassionBitmap.get(index).getWidth() / 2 + MARGIN_NORMAL)
                    + (mMediumPassionBitmap.get(((index + 1) % nPassion)).getWidth() + MARGIN_NORMAL * 2)
                    + (mBiggestPassionBitmap.get(((index + 2) % nPassion)).getWidth() / 2 + MARGIN_NORMAL);

            levelDistance.leftMediumDistance = (mMediumPassionBitmap.get(index).getWidth() / 2 + MARGIN_NORMAL)
                    + (mBiggestPassionBitmap.get(((index + 1) % nPassion)).getWidth() / 2 + MARGIN_NORMAL);
            levelDistance.rightMediumDistance
                    = (mBiggestPassionBitmap.get(((index - 1) >= 0) ? (index - 1) : (nPassion - 1)).getWidth() / 2 + MARGIN_NORMAL)
                    + (mMediumPassionBitmap.get(index).getWidth() / 2 + MARGIN_NORMAL);
            levelDistance.rightSmallDistance
                    = (mSmallestPassionBitmap.get(index).getWidth() / 2 + MARGIN_NORMAL)
                    + (mBiggestPassionBitmap.get(((index - 2) >= 0) ? (index - 2) : (nPassion - 2)).getWidth() / 2 + MARGIN_NORMAL)
                    + (mMediumPassionBitmap.get(((index - 1) >= 0) ? (index - 1) : (nPassion - 1)).getWidth() + MARGIN_NORMAL * 2);
            levelDistance.index = index;
            mPassionLocInformation[index] = new PassionLocInformation();
            mPassionLocInformation[index].index = index;
            mPassionLocInformation[index].leftSmallDistance = levelDistance.leftSmallDistance;
            mPassionLocInformation[index].leftMediumDistance = levelDistance.leftMediumDistance;
            mPassionLocInformation[index].rightMediumDistance = levelDistance.rightMediumDistance;
            mPassionLocInformation[index].rightSmallDistance = levelDistance.rightSmallDistance;
            mPassionContainer.getChildAt(index).setTag(levelDistance);
        }
    }

    public void setPassionData(AppDataManager appDataManager, int centerIndex) {
        nPassion = appDataManager.getPassionsLabels().length;
        mCenterIndex = centerIndex;
        if (this.getLayoutParams() != null) {
            this.getLayoutParams().width = RelativeLayout.LayoutParams.WRAP_CONTENT;
            this.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin = 0;
            this.requestLayout();
        }
        this.setPassions(appDataManager.getPassionsLabels());
        int[] colors = new int[nPassion];
        for (int i = 0; i < nPassion; i++)
            colors[i] = appDataManager.getPassionDataAt(i).getPassionColor();
        this.setColors(colors);
        onCreateAttractorLoopView();

        // SET CONTAINER SIZE
        ViewTreeObserver observer = this.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new InitObserver(this));
    }

    // INITIALIZATION OBSERVER
    class InitObserver implements ViewTreeObserver.OnGlobalLayoutListener {
        private ViewGroup parent;

        public InitObserver(ViewGroup parent) {
            this.parent = parent;
        }

        @Override
        public void onGlobalLayout() {
            initManageLoop();
            parent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    private void initManageLoop() {
        float parentWidth = mAccumulatedWidth[mAccumulatedWidth.length - 1];
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin = 0;
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).width = (int) parentWidth;
        int centerLocation;
        if (mCenterIndex == 0)
            centerLocation = (int) (mAccumulatedWidth[mCenterIndex] / 2);
        else
            centerLocation = (int) (mAccumulatedWidth[mCenterIndex - 1] + (mAccumulatedWidth[mCenterIndex] - mAccumulatedWidth[mCenterIndex - 1]) / 2);
        float leftMargin = (mScreenCenter - centerLocation);
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += leftMargin;
        // REMOVE EXCEEDED PASSION ON LEFT SIDE
        for (int i = 0; i < 10; i++) {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(0).getTag();
            int index = levelDistance.index;
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin + (mSmallestPassionBitmap.get(index).getWidth()
                    + (MARGIN_NORMAL * 2)) > -AVAILABLE_SPACE)
                break;

            ImageView imageView = (ImageView) mPassionContainer.getChildAt(0);
            mPassionContainer.removeView(imageView);

            ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
        }
        // REMOVE EXCEEDED PASSION ON RIGHT SIDE
        for (int i = 0; i < 10; i++) {
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).width
                    + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin < mScreenSize.x + AVAILABLE_SPACE)
                break;
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag();
            int index = levelDistance.index;
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));

            ImageView imageView = (ImageView) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1);
            //((BitmapDrawable) imageView.getDrawable()).getBitmap().recycle();
            mPassionContainer.removeView(imageView);
            imageView = null;
        }
        // ADD PASSION ON LEFT SIDE
        for (int i = 0; i < 10; i++) {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(0).getTag();
            int index = levelDistance.index - 1;
            if (index < 0) index = nPassion + index;
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin < -AVAILABLE_SPACE)
                break;
            ImageView imageView = generateNewPassion(index);
            if (levelDistance.index == mCenterIndex) {
                imageView.getLayoutParams().width = mMediumPassionBitmap.get(index).getWidth();
                imageView.getLayoutParams().height = mMediumPassionBitmap.get(index).getWidth();
                ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
                ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;

                ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin -= imageView.getLayoutParams().width + (MARGIN_NORMAL * 2);
                ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += imageView.getLayoutParams().width + (MARGIN_NORMAL * 2);
            } else {
                ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
                ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;

                ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
                ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            }
            PassionLocInformation locInformation = new PassionLocInformation();
            locInformation.rightSmallDistance = mPassionLocInformation[index].rightSmallDistance;
            locInformation.rightMediumDistance = mPassionLocInformation[index].rightMediumDistance;
            locInformation.leftSmallDistance = mPassionLocInformation[index].leftSmallDistance;
            locInformation.leftMediumDistance = mPassionLocInformation[index].leftMediumDistance;
            locInformation.index = index;
            imageView.setTag(locInformation);
            ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
            ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;
            mPassionContainer.addView(imageView, 0);
            imageView.requestLayout();
        }
        // ADD PASSION ON RIGHT SIDE
        for (int i = 0; i < 10; i++) {
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).width
                    + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin > mScreenSize.x + AVAILABLE_SPACE)
                break;
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag();
            int index = (levelDistance.index + 1) % nPassion;
            ImageView imageView = generateNewPassion(index);
            if (levelDistance.index == mCenterIndex) {
                imageView.getLayoutParams().width = mMediumPassionBitmap.get(index).getWidth();
                imageView.getLayoutParams().height = mMediumPassionBitmap.get(index).getHeight();
                ((LayoutParams) imageView.getLayoutParams()).leftMargin = (int) MARGIN_NORMAL;
                ((LayoutParams) imageView.getLayoutParams()).rightMargin = (int) MARGIN_NORMAL;
                ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += imageView.getLayoutParams().width + (MARGIN_NORMAL * 2);
            } else {
                ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
                ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;
                ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            }
            PassionLocInformation locInformation = new PassionLocInformation();
            locInformation.rightSmallDistance = mPassionLocInformation[index].rightSmallDistance;
            locInformation.rightMediumDistance = mPassionLocInformation[index].rightMediumDistance;
            locInformation.leftSmallDistance = mPassionLocInformation[index].leftSmallDistance;
            locInformation.leftMediumDistance = mPassionLocInformation[index].leftMediumDistance;
            locInformation.index = index;
            imageView.setTag(locInformation);
            mPassionContainer.addView(imageView);
        }
    }

    // GET CENTER OF VIEW
    private int getViewLeft(View view) {
        int[] locOnScreen = new int[2];
        view.getLocationOnScreen(locOnScreen);
        return locOnScreen[0];
    }

    public void setPassions(String[] passions) {
        mPassionText = passions;
        nPassion = mPassionText.length;
        mAccumulatedWidth = new float[nPassion];
        mPassionLocInformation = new PassionLocInformation[nPassion];
    }

    // GET CENTER OF VIEW
    private int getViewCenter(View view) {
        int[] locOnScreen = new int[2];
        view.getLocationOnScreen(locOnScreen);
        int viewWidth = view.getMeasuredWidth();
        return (locOnScreen[0] + (viewWidth / 2));
    }


    // IS VIEW TOUCHED
    private boolean isContains(View view, float x, float y) {
        int[] locOnScreen = new int[2];
        view.getLocationOnScreen(locOnScreen);
        int margin = ((LayoutParams) view.getLayoutParams()).leftMargin;
        if (x > locOnScreen[0] - margin && x < (locOnScreen[0] + view.getMeasuredWidth() + margin))
            return true;
        return false;
    }

    // UPDATE PASSION SIZE
    private void updatePassionSizes(boolean isTouch) {
        float realParentWidth = 0.0f;
        int nChild = mPassionContainer.getChildCount();
        int minDistance = Integer.MAX_VALUE;
        int diffLeftMargin = 0;
        for (int i = 0; i < nChild; i++) {
            View view = mPassionContainer.getChildAt(i);
            PassionLocInformation levelDistance = (PassionLocInformation) view.getTag();
            int realIndex = levelDistance.index;
            int viewCenter = getViewCenter(view);

            int distance = (viewCenter - mScreenCenter);
            int absDistance = Math.abs(distance);
            minDistance = Math.min(minDistance, absDistance);
            float width, height;
            if (distance < 0) {       // LEFT SIDE
                if (levelDistance.leftMediumDistance == absDistance) {
                    // MEDIUM
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_M);
                } else if (levelDistance.leftMediumDistance > absDistance) {
                    // BIG - MEDIUM
                    float scale = ((float) (levelDistance.leftMediumDistance - absDistance) / (float) levelDistance.leftMediumDistance);
                    height = (float) mMediumPassionBitmap.get(realIndex).getHeight()
                            + ((float) (mBiggestPassionBitmap.get(realIndex).getHeight() - mMediumPassionBitmap.get(realIndex).getHeight()) * scale);
                    width = (float) mMediumPassionBitmap.get(realIndex).getWidth()
                            + ((float) (mBiggestPassionBitmap.get(realIndex).getWidth() - mMediumPassionBitmap.get(realIndex).getWidth()) * scale);
                    setPassionBitmap(view, FONT_L);
                } else if (levelDistance.leftSmallDistance == absDistance) {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_S);
                } else if (levelDistance.leftSmallDistance > absDistance) {
                    // MEDIUM - SMALL
                    float scale = ((float) (levelDistance.leftSmallDistance - absDistance)
                            / (float) (levelDistance.leftSmallDistance - levelDistance.leftMediumDistance));
                    height = (float) mSmallestPassionBitmap.get(realIndex).getHeight()
                            + ((float) (mMediumPassionBitmap.get(realIndex).getHeight() - mSmallestPassionBitmap.get(realIndex).getHeight()) * scale);
                    width = (float) mSmallestPassionBitmap.get(realIndex).getWidth()
                            + ((float) (mMediumPassionBitmap.get(realIndex).getWidth() - mSmallestPassionBitmap.get(realIndex).getWidth()) * scale);
                    setPassionBitmap(view, FONT_M);
                } else {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(realIndex).getWidth();
                    height = (float) mSmallestPassionBitmap.get(realIndex).getHeight();
                    setPassionBitmap(view, FONT_S);
                }
            } else if (distance > 0) {   //RIGHT SIDE
                if (levelDistance.rightMediumDistance == absDistance) {
                    // MEDIUM
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_M);
                } else if ((int) levelDistance.rightMediumDistance > absDistance) {
                    // BIG - MEDIUM
                    float scale = ((float) (levelDistance.rightMediumDistance - absDistance) / (float) levelDistance.rightMediumDistance);
                    height = (float) mMediumPassionBitmap.get(realIndex).getHeight()
                            + ((float) (mBiggestPassionBitmap.get(realIndex).getHeight() - mMediumPassionBitmap.get(realIndex).getHeight()) * scale);
                    width = (float) mMediumPassionBitmap.get(realIndex).getWidth()
                            + ((float) (mBiggestPassionBitmap.get(realIndex).getWidth() - mMediumPassionBitmap.get(realIndex).getWidth()) * scale);
                    setPassionBitmap(view, FONT_L);
                } else if (levelDistance.rightSmallDistance == absDistance) {
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_S);
                } else if (levelDistance.rightSmallDistance > absDistance) {
                    // MEDIUM - SMALL
                    float scale = ((float) (levelDistance.rightSmallDistance - absDistance)
                            / (float) (levelDistance.rightSmallDistance - levelDistance.rightMediumDistance));
                    height = (float) mSmallestPassionBitmap.get(realIndex).getHeight()
                            + ((float) (mMediumPassionBitmap.get(realIndex).getHeight() - mSmallestPassionBitmap.get(realIndex).getHeight()) * scale);
                    width = (float) mSmallestPassionBitmap.get(realIndex).getWidth()
                            + ((float) (mMediumPassionBitmap.get(realIndex).getWidth() - mSmallestPassionBitmap.get(realIndex).getWidth()) * scale);
                    setPassionBitmap(view, FONT_M);
                } else {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(realIndex).getWidth();
                    height = (float) mSmallestPassionBitmap.get(realIndex).getHeight();
                    setPassionBitmap(view, FONT_S);
                }
            } else {  // CENTER
                // SMALL
                width = (float) mBiggestPassionBitmap.get(realIndex).getWidth();
                height = (float) mBiggestPassionBitmap.get(realIndex).getHeight();
                setPassionBitmap(view, FONT_L);
            }

            if (getViewCenter(view) < mScreenCenter) {
                diffLeftMargin += (((LayoutParams) view.getLayoutParams()).width - (int) width);
            }

            ((LayoutParams) view.getLayoutParams()).width = (int) width;
            ((LayoutParams) view.getLayoutParams()).height = (int) height;
            ((LayoutParams) view.getLayoutParams()).leftMargin = MARGIN_NORMAL;
            ((LayoutParams) view.getLayoutParams()).rightMargin = MARGIN_NORMAL;
            view.requestLayout();

            realParentWidth += (width + ((float) MARGIN_NORMAL * 2f));
        }
        if (!isTouch)
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += diffLeftMargin;
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).width = (int) realParentWidth;
        //mCenterIndex = centerIndex;
    }

    // GENERATE NEW PASSION FOR ROTATION
    private ImageView generateNewPassion(int index) {
        ImageView imageView = new ImageView(getContext());
        Bitmap bitmap = mSmallestPassionBitmap.get(index);
        imageView.setTag(R.string.size_key, FONT_S);
        imageView.setImageBitmap(bitmap);
        //imageView.setBackgroundColor(Color.GRAY);
        LinearLayout.LayoutParams params
                = new LinearLayout.LayoutParams(mSmallestPassionBitmap.get(index).getWidth(), mSmallestPassionBitmap.get(index).getHeight());
        params.leftMargin = MARGIN_NORMAL;
        params.rightMargin = MARGIN_NORMAL;
        imageView.setLayoutParams(params);
        return imageView;
    }

    private void setLandedPassionSizes() {
        int nChild = mPassionContainer.getChildCount();
        for (int i = 0; i < nChild; i++) {
            if (((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index == mCenterIndex) {
                for (int j = 0; j < nChild; j++) {
                    if (j == i) {
                        setPassionBitmap(mPassionContainer.getChildAt(j), FONT_L);
                        mPassionContainer.getChildAt(j).getLayoutParams().width
                                = mBiggestPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getWidth();
                        mPassionContainer.getChildAt(j).getLayoutParams().height
                                = mBiggestPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getHeight();
                        mPassionContainer.getChildAt(j).requestLayout();
                    } else if (j == i - 1 ||  j  == nChild + i - 1 || j == i + 1) {
                        setPassionBitmap(mPassionContainer.getChildAt(j), FONT_M);
                        mPassionContainer.getChildAt(j).getLayoutParams().width
                                = mMediumPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getWidth();
                        mPassionContainer.getChildAt(j).getLayoutParams().height
                                = mMediumPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getHeight();
                        mPassionContainer.getChildAt(j).requestLayout();
                    } else {
                        setPassionBitmap(mPassionContainer.getChildAt(j), FONT_S);
                        mPassionContainer.getChildAt(j).getLayoutParams().width
                                = mSmallestPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getWidth();
                        mPassionContainer.getChildAt(j).getLayoutParams().height
                                = mSmallestPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(j).getTag()).index).getHeight();
                        mPassionContainer.getChildAt(j).requestLayout();
                    }
                }
                break;
            }
        }
    }

    private void setPassionBitmap(View view, int size) {
        if (view.getTag(R.string.size_key) != null && (int)view.getTag(R.string.size_key) == size)
            return;
        view.setTag(R.string.size_key, size);
        switch (size) {
            case FONT_L:
                ((ImageView) view).setImageBitmap(mBiggestPassionBitmap.get(((PassionLocInformation) view.getTag()).index));
                //Log.d(TAG, ((PassionLocInformation) view.getTag()).index + "th(" + mPassionText[((PassionLocInformation) view.getTag()).index] + ") set big");
                break;
            case FONT_M:
                ((ImageView) view).setImageBitmap(mMediumPassionBitmap.get(((PassionLocInformation) view.getTag()).index));
                // Log.d(TAG, ((PassionLocInformation) view.getTag()).index + "th(" + mPassionText[((PassionLocInformation) view.getTag()).index] + ") set medium");
                break;
            case FONT_S:
                ((ImageView) view).setImageBitmap(mSmallestPassionBitmap.get(((PassionLocInformation) view.getTag()).index));
                // Log.d(TAG, ((PassionLocInformation) view.getTag()).index + "th(" + mPassionText[((PassionLocInformation) view.getTag()).index] + ") set small");
                break;
            default:
                Log.e(TAG, "wrong size");
                break;
        }
    }

    // MANAGE CONTAINER FOR ROTATING
    private static final int AVAILABLE_SPACE = 300;
    private static final int LOOP_FLAG_REMOVE_LEFT = 1;
    private static final int LOOP_FLAG_REMOVE_RIGHT = 2;
    private static final int LOOP_FLAG_REMOVE_ALL = 0;

    private void manageRotateContainer(int flag) {
        // REMOVE EXCEEDED PASSION ON LEFT SIDE
        for (int i = 0; i < 5 && (flag == LOOP_FLAG_REMOVE_LEFT || flag == LOOP_FLAG_REMOVE_ALL); i++) {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(0).getTag();
            int index = levelDistance.index;
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin
                    + (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2)) > -AVAILABLE_SPACE)
                break;
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            mOldMargin += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            ImageView imageView = (ImageView) mPassionContainer.getChildAt(0);
            //((BitmapDrawable) imageView.getDrawable()).getBitmap().recycle();
            mPassionContainer.removeView(imageView);
            imageView = null;
        }
        // REMOVE EXCEEDED PASSION ON RIGHT SIDE
        for (int i = 0; i < 5 && (flag == LOOP_FLAG_REMOVE_RIGHT || flag == LOOP_FLAG_REMOVE_ALL); i++) {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag();
            int index = levelDistance.index;
            if ((((RelativeLayout.LayoutParams) this.getLayoutParams()).width + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin)
                    - (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2)) < mScreenSize.x + AVAILABLE_SPACE)
                break;
            ImageView imageView = (ImageView) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1);
            //((BitmapDrawable) imageView.getDrawable()).getBitmap().recycle();
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width -=
                    (imageView.getMeasuredWidth() + (((LinearLayout.LayoutParams) imageView.getLayoutParams()).leftMargin * 2));
            mPassionContainer.removeView(imageView);
            imageView = null;
        }
        // ADD PASSION ON LEFT SIDE
        for (int i = 0; i < 5; i++) {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(0).getTag();
            int index = levelDistance.index - 1;
            if (index < 0) index = nPassion + index;
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin < -AVAILABLE_SPACE)
                break;
            ImageView imageView = generateNewPassion(index);
            PassionLocInformation locInformation = new PassionLocInformation();
            locInformation.rightSmallDistance = mPassionLocInformation[index].rightSmallDistance;
            locInformation.rightMediumDistance = mPassionLocInformation[index].rightMediumDistance;
            locInformation.leftSmallDistance = mPassionLocInformation[index].leftSmallDistance;
            locInformation.leftMediumDistance = mPassionLocInformation[index].leftMediumDistance;
            locInformation.index = index;
            imageView.setTag(locInformation);
            ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
            ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;
            mPassionContainer.addView(imageView, 0);
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
            mOldMargin -= (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
        }
        // ADD PASSION ON RIGHT SIDE
        for (int i = 0; i < 5; i++) {
            if (((RelativeLayout.LayoutParams) this.getLayoutParams()).width + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin > mScreenSize.x + AVAILABLE_SPACE)
                break;
            PassionLocInformation nextLevelDistance = (PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag();
            int index = (nextLevelDistance.index + 1) % nPassion;
            ImageView imageView = generateNewPassion(index);
            PassionLocInformation locInformation = new PassionLocInformation();
            locInformation.rightSmallDistance = mPassionLocInformation[index].rightSmallDistance;
            locInformation.rightMediumDistance = mPassionLocInformation[index].rightMediumDistance;
            locInformation.leftSmallDistance = mPassionLocInformation[index].leftSmallDistance;
            locInformation.leftMediumDistance = mPassionLocInformation[index].leftMediumDistance;
            locInformation.index = index;
            imageView.setTag(locInformation);
            mPassionContainer.addView(imageView);
            ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(index).getWidth() + (MARGIN_NORMAL * 2));
        }
    }

    private View getPassionViewAt(float x, float y) {
        int nChild = mPassionContainer.getChildCount();
        for (int i = 0; i < nChild; i++) {
            if (isContains(mPassionContainer.getChildAt(i), x, y)) {
                return mPassionContainer.getChildAt(i);
            }
        }
        return null;
    }

    private int getPassionIndexOfView(View view) {
        if (view == null)
            return -1;
        return ((PassionLocInformation) view.getTag()).index;
    }

    private int getPassionIndexOfChildAt(int index) {
        View loc = mPassionContainer.getChildAt(index);
        if (loc == null)
            return 0;

        return ((PassionLocInformation) loc.getTag()).index;
    }

    private static final int MOVEMENT_LEFT = 1;
    private static final int MOVEMENT_RIGHT = 2;
    public static final int DIRECTION_STOPPED = 0;

    public void stopAttractorLoop() {
        if (mSmoothLoopAnimator != null && (mSmoothLoopAnimator.isRunning() || mSmoothLoopAnimator.isStarted())) {
            cancelAllAnimatorListener(mSmoothLoopAnimator);
        }
        mFlingHandler.removeCallbacksAndMessages(null);
        mFinishFlingHandler.removeCallbacksAndMessages(null);
        isHandlerRunning = false;
    }

    private void cancelAllAnimatorListener(AnimatorSet animatorSet) {
        for (Animator childrenAnimator : mSmoothLoopAnimator.getChildAnimations()) {
            if (childrenAnimator.getClass().equals(AnimatorSet.class))
                cancelAllAnimatorListener((AnimatorSet) childrenAnimator);
            childrenAnimator.removeAllListeners();
            childrenAnimator.cancel();
        }
        animatorSet.removeAllListeners();
        animatorSet.cancel();
    }

    //========================================
    // FLING ANIMATION HANDLER
    //========================================
    private FlingHandler mFlingHandler;
    private boolean isHandlerRunning = false;

    private static class FlingHandler extends Handler {
        private final WeakReference<AttractorLoopView> mActivity;

        public FlingHandler(AttractorLoopView parent) {
            mActivity = new WeakReference<>(parent);
        }

        @Override
        public void handleMessage(Message msg) {
            AttractorLoopView activity = mActivity.get();
            if (activity != null) {
                activity.handleFlingMessage(msg);
            }
        }
    }

    private void handleFlingMessage(Message msg) {
        isLanded = false;
        isHandlerRunning = true;
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += (int) mVelocity;
        manageRotateContainer(LOOP_FLAG_REMOVE_ALL);
        updatePassionSizes(true);
        this.requestLayout();
        sendArrivingInformation(false, false);
        if (Math.abs(mVelocity * 0.9) < 5f) {
            int nChild = mPassionContainer.getChildCount();
            int minDistance = Integer.MAX_VALUE;
            View targetView = null;
            for (int i = 0; i < nChild; i++) {
                View view = mPassionContainer.getChildAt(i);
                int viewCenter = getViewCenter(view);
                int distance = Math.abs(mScreenCenter - viewCenter);
                minDistance = Math.min(minDistance, distance);
                if (minDistance == distance)
                    targetView = view;
            }
            assert targetView != null;
            startSmoothLoop(targetView, ((PassionLocInformation) targetView.getTag()).index, 0, 0, ARRIVED_MODE_FLING);
            return;
        } else if (Math.abs(mVelocity * 0.9) < 30f) {
            Message finishMsg = mFinishFlingHandler.obtainMessage();
            finishMsg.arg1 = msg.arg1;  // DIRECTION
            finishMsg.arg2 = Integer.MAX_VALUE;
            mFinishFlingHandler.sendMessage(finishMsg);
            return;
        }
        mVelocity *= 0.8f;
        Message newMsg = mFlingHandler.obtainMessage();
        newMsg.arg1 = msg.arg1;
        mFlingHandler.sendMessageDelayed(newMsg, 1);
    }

    //========================================
    // FINISH FLING ANIMATION HANDLER
    //========================================
    private FinishFlingHandler mFinishFlingHandler;

    private static class FinishFlingHandler extends Handler {
        private final WeakReference<AttractorLoopView> mActivity;

        public FinishFlingHandler(AttractorLoopView parent) {
            mActivity = new WeakReference<>(parent);
        }

        @Override
        public void handleMessage(Message msg) {
            AttractorLoopView activity = mActivity.get();
            if (activity != null) {
                activity.handleFinishFlingMessage(msg);
            }
        }
    }

    private void handleFinishFlingMessage(Message msg) {
        int nChild = mPassionContainer.getChildCount();
        int absMinDistance = Integer.MAX_VALUE; // NEAREST DISTANCE
        int direction = msg.arg1;
        float referenceVelocity = mVelocity * 0.98f;
        int prvMinDistance = msg.arg2;
        int minDistance = Integer.MAX_VALUE;

        for (int i = 0; i < nChild; i++) {
            int distance = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(i));
            if ((mVelocity < 0 && distance > 0)
                    || (mVelocity > 0 && distance < 0)) {
                continue;
            }
            absMinDistance = Math.min(Math.abs(distance), absMinDistance);
            if (absMinDistance == Math.abs(distance)) {
                minDistance = (distance > 0) ? absMinDistance : -absMinDistance;
            }
        }

        if (prvMinDistance != Integer.MAX_VALUE && (Math.abs(prvMinDistance) < Math.abs(minDistance))) {
            // OVERSHOT
            mCenteringHandler = new CenteringHandler(this);
            mCenteringHandler.sendEmptyMessage(0);
            return;
        }

        int movement = (absMinDistance > Math.abs((int) referenceVelocity)) ? (int) referenceVelocity : minDistance;
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += movement;
        manageRotateContainer(LOOP_FLAG_REMOVE_ALL);
        updatePassionSizes(true);
        this.requestLayout();
        sendArrivingInformation(true, false);
        if (Math.abs(movement) == Math.abs((int) referenceVelocity)) {
            isLanded = false;
            // KEEP FLING
            Message newMsg = mFinishFlingHandler.obtainMessage();
            newMsg.arg1 = direction;
            newMsg.arg2 = minDistance;
            mFinishFlingHandler.sendMessageDelayed(newMsg, 1);
            if (Math.abs(referenceVelocity) < 10f)
                mVelocity = (mVelocity > 0) ? 10f : -10f;
            else
                mVelocity = referenceVelocity;
        } else {
            // END OF FLING
            mCenteringHandler = new CenteringHandler(this);
            mCenteringHandler.sendEmptyMessage(0);
        }
    }

    //========================================
    // FINISH FLING ANIMATION HANDLER
    //========================================
    private CenteringHandler mCenteringHandler;

    private static class CenteringHandler extends Handler {
        private final WeakReference<AttractorLoopView> mActivity;

        public CenteringHandler(AttractorLoopView parent) {
            mActivity = new WeakReference<>(parent);
        }

        @Override
        public void handleMessage(Message msg) {
            AttractorLoopView activity = mActivity.get();
            if (activity != null) {
                activity.handleCenteringMessage(msg);
            }
        }
    }

    private void handleCenteringMessage(Message msg) {
        View centerView = getPassionViewAt((float) mScreenCenter, 0f);
        if (centerView == null) {
            centerView = mPassionContainer.getChildAt(mPassionContainer.getChildCount() / 2);
        }
        mCenterIndex = ((PassionLocInformation) centerView.getTag()).index;
        int diff = mScreenCenter - getViewCenter(centerView);
//        Log.d(TAG, "[before]centering : " + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin + "//diff : " + diff
//                        +"// view center : " + getViewCenter(centerView) + "/ screen : " + mScreenCenter);
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += diff;
        this.requestLayout();
        isHandlerRunning = false;
        isLanded = true;
        //setLandedPassionSizes();
        if (mOnAttractorEventListener != null) {
            mOnAttractorEventListener.onArrivedFromFling(mCenterIndex);
        }
//        Log.d(TAG, "[after]centering : " + ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin
//                + "//diff=" + (mScreenCenter - getViewCenter(centerView)));
//        Log.d(TAG, "arrived " + mPassionText[((PassionLocInformation)centerView.getTag()).index]);
    }

    private void sendArrivingInformation(boolean isLandArriving, boolean withTouch) {
        int nChild = mPassionContainer.getChildCount();
        // SEND ARRIVING & UPDATE CENTER INDEX
        int targetViewIndex;
        for (targetViewIndex = 0; targetViewIndex < nChild; targetViewIndex++) {
            if (getPassionIndexOfChildAt(targetViewIndex) == mCenterIndex)
                break;
        }
        if (targetViewIndex == nChild)
            return;
        int distanceCenter = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(targetViewIndex));
        if (distanceCenter < 0) {
            // CENTER TO RIGHT
            if (targetViewIndex - 1 < 0) return;
            int viewDistance = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(targetViewIndex - 1));
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(targetViewIndex - 1).getTag();
            float ratio = ((levelDistance.leftMediumDistance - (float) Math.abs(viewDistance)) / levelDistance.leftMediumDistance);

            if (ratio >= 1f || viewDistance < 0) {
                mCenterIndex = levelDistance.index;
                if (mOnAttractorEventListener != null)
                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, 1f, withTouch);
            } else {
                if (ratio < 0f) ratio = 0f;
                else if (ratio > 1f) ratio = 1f;
                if (mOnAttractorEventListener != null)
                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, ratio, isLandArriving);
            }
        } else if (distanceCenter > 0) {
            // CENTER TO LEFT
            if (targetViewIndex + 1 >= nChild) return;
            int viewDistance = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(targetViewIndex + 1));
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(targetViewIndex + 1).getTag();
            float ratio = ((levelDistance.rightMediumDistance - (float) Math.abs(viewDistance)) / levelDistance.rightMediumDistance);

            if (ratio >= 1f || viewDistance > 0) {
                mCenterIndex = levelDistance.index;

                if (mOnAttractorEventListener != null) {
                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, 1f, withTouch);
                }
            } else {
                if (ratio < 0f) ratio = 0f;
                if (mOnAttractorEventListener != null)
                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, ratio, isLandArriving);
            }
        } else {
            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(targetViewIndex).getTag();
            mOnAttractorEventListener.onArriving(mCenterIndex, mCenterIndex, 1f, true);
        }
    }

    //========================================
    // ON TOUCH LISTENER
    //========================================
    private float mOldDragX = 0f, mPrevDragX = 0f, mVelocity = 0f;
    private int mOldMargin = 0;
    private boolean isScrollable = false;
    private boolean isLanded = true;
    private ArrayList<Float> mPointAccumulator = new ArrayList<>();

    //private float
    private class AttractorLoopTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (event.getPointerCount() > 1) return true;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mOnAttractorEventListener != null)
                        mOnAttractorEventListener.onFirstTouch((AttractorLoopView) mPassionContainer.getParent());
                    if (mOnAttractorEventListener != null)
                        mOnAttractorEventListener.onAttactTouchDuringYoutubeLoading();
                    stopAttractorLoop();
                    mOldDragX = mPrevDragX = event.getRawX();
                    mOldMargin = ((RelativeLayout.LayoutParams) view.getLayoutParams()).leftMargin;
                    isScrollable = false;
                    //mPrevTargetIndex = Integer.MIN_VALUE;
                    break;
                case MotionEvent.ACTION_MOVE:
//                    ((MainActivity) view.getContext()).stopAutoLoopHandler();
                    float dx = event.getRawX() - mOldDragX;
                    mVelocity = (event.getRawX() - mPrevDragX);
                    if (Math.abs(mVelocity) > 120f)
                        mVelocity = (mVelocity < 0f) ? -120f : 120f;
                    mPrevDragX = event.getRawX();
                    if (!isScrollable) {
                        // IF dx IS SMALLER THAN 30f, NAVIGATION NOT MOVE
                        if (Math.abs(dx) > 30f) {
                            mOldDragX = event.getRawX();
                            isScrollable = true;
                        }
                        return true;
                    }
//                  // Simple way
                    if (Math.abs(mVelocity) < 0.9f)
                        return true;
                    ((RelativeLayout.LayoutParams) view.getLayoutParams()).leftMargin = (int) (mOldMargin + dx);
                    sendArrivingInformation(true, true);
                    updatePassionSizes(true);
                    manageRotateContainer(LOOP_FLAG_REMOVE_ALL);
                    view.requestLayout();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_POINTER_UP:
                    float moveDistance = Math.abs(mOldDragX - event.getRawX());

                    //#309 Navigation Bar 오동작 수정함.
//                    if (!isScrollable && moveDistance < 30f || isScrollable && moveDistance <= 0f) {
                    if (moveDistance < 30f) {
                        View targetView = getPassionViewAt(event.getRawX(), event.getRawY());
                        if (targetView != null && targetView instanceof ImageView &&
                                targetView.getTag() != null && targetView.getTag().getClass().equals(PassionLocInformation.class)) {
                            mOnAttractorEventListener.onTap(targetView,
                                    ((PassionLocInformation) targetView.getTag()).index);
                        }
                    }
                    else {
                        if (mOnAttractorEventListener != null)
                            mOnAttractorEventListener.onStartOfFling();

                        Message msg = mFlingHandler.obtainMessage();
                        msg.arg1 = ((mOldDragX - event.getRawX()) < 0f) ? MOVEMENT_RIGHT : MOVEMENT_LEFT;
                        mFlingHandler.sendMessage(msg);
                    }

//                    //#284 App does not navigate to selected passion after swiping left or right 수정함.
//                    if (((!isScrollable && moveDistance < 30f)
//                            || (isScrollable && moveDistance <= 0f)) && isLanded) {
////                        mPassionContainer.playSoundEffect(SoundEffectConstants.CLICK);
//                        View targetView = getPassionViewAt(event.getRawX(), event.getRawY());
//                        if (targetView != null && targetView instanceof ImageView &&
//                                targetView.getTag() != null && targetView.getTag().getClass().equals(PassionLocInformation.class)) {
//
//                            View centerView = getViewFromPassionIndex(mCenterIndex);
//
//                            if (centerView != null && Math.abs(getViewCenter(centerView) - mScreenCenter) > 10) {
//                                Message msg = mFlingHandler.obtainMessage();
//                                if (getViewCenter(centerView) - mScreenCenter < 0) {
//                                    msg.arg1 = MOVEMENT_RIGHT;
//                                } else {
//                                    msg.arg1 = MOVEMENT_LEFT;
//                                }
//                                Log.d(TAG, "##### FLING...");
//                                mFlingHandler.sendMessage(msg);
//                            } else {
//                                Log.d(TAG, "##### TAP...");
//                                mOnAttractorEventListener.onTap(targetView,
//                                        ((PassionLocInformation) targetView.getTag()).index);
//                            }
//                        }
//                    } else {
//                        Message msg = mFlingHandler.obtainMessage();
//                        msg.arg1 = ((mOldDragX - event.getRawX()) < 0f) ? MOVEMENT_RIGHT : MOVEMENT_LEFT;
//                        mFlingHandler.sendMessage(msg);
//                    }
                    break;
            }
            return true;
        }
    }

    private AnimatorSet mSmoothLoopAnimator = null;

    public boolean isLoopAnimationRunning() {

        //icanmobile - 20160407 - after fling, mSmoothLoopAnimator.isRunning() value always true.
//        if ((mSmoothLoopAnimator != null && (mSmoothLoopAnimator.isRunning() || mSmoothLoopAnimator.isStarted())))
//            return true;
        if ((mSmoothLoopAnimator != null && (mSmoothLoopAnimator.isRunning() && mSmoothLoopAnimator.isStarted())))
            return true;
        //end - icanmobile


        return isHandlerRunning;
    }

    public void startAutoLoopAnimation(final int passionIndex) {
        startSmoothLoop(null, passionIndex, 0, 0, ARRIVED_MODE_NONE);
    }

    public void startSmoothLoopWithDisplayAnimation(final int passionIndex, final int subcategoryIndex, final int appIndex) {
        startSmoothLoop(null, passionIndex, subcategoryIndex, appIndex, ARRIVED_MODE_DISPLAY);
    }


    private static final int ARRIVED_MODE_NONE = 0;
    private static final int ARRIVED_MODE_PREVIEW = 1;
    private static final int ARRIVED_MODE_DISPLAY = 2;
    private static final int ARRIVED_MODE_FLING = 3;
    private static final int DIRECTION_TO_LEFT = 1;
    private static final int DIRECTION_TO_RIGHT = 2;

    private void startSmoothLoop(final View view, final int passionIndex, final int subcategoryIndex, final int appIndex, final int mode) {
        final int finalPassionIndex;
        if (passionIndex >= nPassion) {
            finalPassionIndex = passionIndex % nPassion;
        } else if (passionIndex < 0) {
            finalPassionIndex = nPassion + passionIndex;
        } else {
            finalPassionIndex = passionIndex;
        }
        View foundView = null;
        if (view == null || !view.getClass().equals(ImageView.class)) {
            foundView = findViewFromIndex(finalPassionIndex);
            if (foundView == null) {
                Log.e(TAG, "[startSmoothLoop:view,index] receive target view is null");
                isLanded = true;
                return;
            }
        }
        if ((mSmoothLoopAnimator != null && (mSmoothLoopAnimator.isStarted()))) {
            Log.e(TAG, "[startSmoothLoop:view,index] animation is running");
            isLanded = true;
            return;
        }
        int flag = LOOP_FLAG_REMOVE_ALL;
        if (foundView != null)
            flag = (getViewCenter(foundView) < mScreenCenter) ? LOOP_FLAG_REMOVE_RIGHT : LOOP_FLAG_REMOVE_LEFT;
        final int finalFlag = flag;
        final View finalFoundView = (view == null) ? foundView : view;
        mSmoothLoopAnimator = new AnimatorSet();
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0f, 1f);
        final int direction = ((mScreenCenter - getViewCenter(finalFoundView)) > 0) ? DIRECTION_TO_RIGHT : DIRECTION_TO_LEFT;

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            int prevTargetIndex = Integer.MIN_VALUE;

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                updatePassionSizes(false);
                manageRotateContainer(finalFlag);
                mPassionContainer.getParent().requestLayout();
                int distance = mScreenCenter - getViewCenter(finalFoundView);
                if (distance < 0) {
                    // TARGET VIEW ON RIGHT FROM CENTER
                    int nChild = mPassionContainer.getChildCount();
                    if (((PassionLocInformation) finalFoundView.getTag()).index == mCenterIndex) {
                        // BACK TO CENTER
                        //int viewDistance = mScreenCenter - getViewCenter(finalFoundView);
                        PassionLocInformation levelDistance = (PassionLocInformation) finalFoundView.getTag();
                        float ratio = ((levelDistance.leftMediumDistance - (float) Math.abs(distance)) / levelDistance.leftMediumDistance);
                        if (ratio < 0f) {
                            ratio = 0f;
                            distance = -(int) levelDistance.leftMediumDistance;
                        } else if (ratio > 1f) {
                            ratio = 1f;
                            distance = 0;
                        }
                        int targetIndex = mCenterIndex - 1;
                        if (targetIndex < 0)
                            targetIndex = nPassion + targetIndex;
                        if (mOnAttractorEventListener != null) {
                            mOnAttractorEventListener.onArriving(mCenterIndex, targetIndex, 1f - ratio,
                                    (finalPassionIndex == targetIndex || finalPassionIndex == mCenterIndex));
                        }
                    } else {
                        for (int i = 0; i < nChild; i++) {
                            // PASS PASSIONS
                            int viewDistance = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(i));
                            if (viewDistance > 0 || ((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index == mCenterIndex)
                                continue;
                            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(i).getTag();
                            if (viewDistance > -levelDistance.rightMediumDistance && levelDistance.index != mCenterIndex) {
                                float ratio = ((levelDistance.rightMediumDistance - (float) Math.abs(viewDistance)) / levelDistance.rightMediumDistance);
                                if (ratio < 0f) {
                                    ratio = 0f;
                                    distance = -(int) levelDistance.rightMediumDistance;
                                } else if (ratio > 1f) {
                                    ratio = 1f;
                                    distance = 0;
                                }
                                if (prevTargetIndex != levelDistance.index && prevTargetIndex != Integer.MIN_VALUE) {
                                    if (mOnAttractorEventListener != null)
                                        mOnAttractorEventListener.onArrived(prevTargetIndex, false);
                                    mCenterIndex = prevTargetIndex;
                                }
                                prevTargetIndex = levelDistance.index;
                                if (mOnAttractorEventListener != null)
                                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, ratio,
                                            (finalPassionIndex == prevTargetIndex || finalPassionIndex == mCenterIndex));
                                break;
                            }
                        }
                    }
                } else if (distance > 0) {
                    // TARGET VIEW ON LEFT FROM CENTER
                    if (((PassionLocInformation) finalFoundView.getTag()).index == mCenterIndex) {
                        // BACK TO CENTER
                        PassionLocInformation levelDistance = (PassionLocInformation) finalFoundView.getTag();
                        float ratio = ((levelDistance.leftMediumDistance - (float) Math.abs(distance)) / levelDistance.leftMediumDistance);
                        if (ratio < 0f) {
                            ratio = 0f;
                            distance = (int) levelDistance.leftMediumDistance;
                        } else if (ratio > 1f) {
                            ratio = 1f;
                            distance = 0;
                        }
                        int targetIndex = mCenterIndex + 1;
                        if (targetIndex >= nPassion)
                            targetIndex = targetIndex % nPassion;
                        if (mOnAttractorEventListener != null)
                            mOnAttractorEventListener.onArriving(mCenterIndex, targetIndex, 1f - ratio, (targetIndex == finalPassionIndex));
                    } else {
                        int nChild = mPassionContainer.getChildCount();
                        for (int i = 0; i < nChild; i++) {
                            // PASS PASSIONS
                            int viewDistance = mScreenCenter - getViewCenter(mPassionContainer.getChildAt(i));
                            if (viewDistance < 0 || ((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index == mCenterIndex)
                                continue;
                            PassionLocInformation levelDistance = (PassionLocInformation) mPassionContainer.getChildAt(i).getTag();
                            if (viewDistance < levelDistance.leftMediumDistance) {
                                float ratio = ((levelDistance.leftMediumDistance - (float) Math.abs(viewDistance)) / levelDistance.leftMediumDistance);
                                if (ratio < 0f) {
                                    ratio = 0f;
                                    distance = (int) levelDistance.leftMediumDistance;
                                } else if (ratio > 1f) {
                                    ratio = 1f;
                                    distance = 0;
                                }
                                if (prevTargetIndex != levelDistance.index && prevTargetIndex != Integer.MIN_VALUE) {
                                    mCenterIndex = prevTargetIndex;
                                    if (mOnAttractorEventListener != null)
                                        mOnAttractorEventListener.onArrived(prevTargetIndex, false);
                                }
                                prevTargetIndex = levelDistance.index;
                                if (mOnAttractorEventListener != null)
                                    mOnAttractorEventListener.onArriving(mCenterIndex, levelDistance.index, ratio, (prevTargetIndex == finalPassionIndex));
                                break;
                            }
                        }
                    }
                }

                if (direction == DIRECTION_TO_LEFT && distance > 0) {
                    ((RelativeLayout.LayoutParams) ((AttractorLoopView) mPassionContainer.getParent()).getLayoutParams()).leftMargin += distance;
                    mPassionContainer.getParent().requestLayout();
                    endOfLoopAnimator(mode, finalPassionIndex, subcategoryIndex, appIndex);
                } else if (direction == DIRECTION_TO_RIGHT && distance < 0) {
                    ((RelativeLayout.LayoutParams) ((AttractorLoopView) mPassionContainer.getParent()).getLayoutParams()).leftMargin += distance;
                    mPassionContainer.getParent().requestLayout();
                    endOfLoopAnimator(mode, finalPassionIndex, subcategoryIndex, appIndex);
                } else if (Math.abs(distance) <= 1) {
                    ((RelativeLayout.LayoutParams) ((AttractorLoopView) mPassionContainer.getParent()).getLayoutParams()).leftMargin += distance;
                    mPassionContainer.getParent().requestLayout();
                    endOfLoopAnimator(mode, finalPassionIndex, subcategoryIndex, appIndex);
                } else {
                    int movement = Math.round((float) distance * (float) animation.getAnimatedValue());
                    if (direction == DIRECTION_TO_LEFT && movement > 0) {
                        movement = 0;
                    } else if (direction == DIRECTION_TO_RIGHT && movement < 0) {
                        movement = 0;
                    }
                    ((RelativeLayout.LayoutParams) ((AttractorLoopView) mPassionContainer.getParent()).getLayoutParams()).leftMargin += movement;
                }
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                isLanded = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                endOfLoopAnimator(mode, finalPassionIndex, subcategoryIndex, appIndex);
            }
        });
        mSmoothLoopAnimator.play(valueAnimator);
        int startViewIndex = 0, endViewIndex = 0;
        for (int i = 0; i < mPassionContainer.getChildCount(); i++) {
            if (getPassionIndexOfChildAt(i) == mCenterIndex)
                startViewIndex = i;
            if (getPassionIndexOfChildAt(i) == finalPassionIndex)
                endViewIndex = i;
        }
        int step = Math.abs(startViewIndex - endViewIndex);
        if (step > 5) step = 5;
        else if (step <= 0) step = 1;
        mSmoothLoopAnimator.setDuration(step * 1500);
        mSmoothLoopAnimator.start();
    }

    private void endOfLoopAnimator(int mode, int finalPassionIndex, int subcategoryIndex, int appIndex) {
        isHandlerRunning = false;
        isLanded = true;
        mCenterIndex = finalPassionIndex;
        //Attract loop passion 변경시, effect 부드럽게 진행되도록 수정함
//        setLandedPassionSizes();
        if (mOnAttractorEventListener != null) {
            switch (mode) {
                case ARRIVED_MODE_DISPLAY:
                    mOnAttractorEventListener.onArrivedForAppDetail(finalPassionIndex, subcategoryIndex, appIndex);
                    break;
                case ARRIVED_MODE_PREVIEW:
                    mOnAttractorEventListener.onArrived(finalPassionIndex, true);
                    break;
                case ARRIVED_MODE_NONE:
                    mOnAttractorEventListener.onAutoLoopArrived(finalPassionIndex);
                    break;
                case ARRIVED_MODE_FLING:
                    mOnAttractorEventListener.onArrivedFromFling(finalPassionIndex);
                    break;
            }
        }
        stopAttractorLoop();
    }

    public void startSmoothLoop(final View view, final int passionIndex) {
        startSmoothLoop(view, passionIndex, 0, 0, ARRIVED_MODE_PREVIEW);
    }

    public void loopWithBackground(int passionIndex, float ratio) {
        //Attract loop passion 변경시, effect 부드럽게 진행되도록 수정함
        if (ratio >= 1.0f) {
            mCenterIndex = passionIndex;
            manageRotateContainer(LOOP_FLAG_REMOVE_ALL);
            return;
        }

        if (ratio > 1.0f) ratio = 1.0f;
        if (ratio < 0f) ratio = 0f;
        if (isLoopAnimationRunning()) {
            stopAttractorLoop();
        }
        int nChild = mPassionContainer.getChildCount();
        int targetViewIndex;
        for (targetViewIndex = 0; targetViewIndex < nChild; targetViewIndex++) {
            if (getPassionIndexOfChildAt(targetViewIndex) == passionIndex) {
                break;
            }
        }
        if (targetViewIndex == nChild) {
            return;
        }

        int distanceFromCenter = this.getViewCenter(mPassionContainer.getChildAt(targetViewIndex)) - mScreenCenter;
        float destinationDistanceFromCenter;
        if (distanceFromCenter < 0) { // LEFT FROM CENTER
            float referenceDistance = ((PassionLocInformation) mPassionContainer.getChildAt(targetViewIndex).getTag()).leftMediumDistance;
            destinationDistanceFromCenter = -referenceDistance * (1f - ratio);
        } else if (distanceFromCenter > 0) {
            float referenceDistance = ((PassionLocInformation) mPassionContainer.getChildAt(targetViewIndex).getTag()).rightMediumDistance;
            destinationDistanceFromCenter = referenceDistance * (1f - ratio);
        } else {
            return;
        }

        // SET PARAMETER OF TARGET VIEW
        float height = (float) mMediumPassionBitmap.get(passionIndex).getHeight()
                + ((float) (mBiggestPassionBitmap.get(passionIndex).getHeight() - mMediumPassionBitmap.get(passionIndex).getHeight()) * ratio);
        float width = (float) mMediumPassionBitmap.get(passionIndex).getWidth()
                + ((float) (mBiggestPassionBitmap.get(passionIndex).getWidth() - mMediumPassionBitmap.get(passionIndex).getWidth()) * ratio);
        ((LayoutParams) mPassionContainer.getChildAt(targetViewIndex).getLayoutParams()).width = (int) width;
        ((LayoutParams) mPassionContainer.getChildAt(targetViewIndex).getLayoutParams()).height = (int) height;
        ((LayoutParams) mPassionContainer.getChildAt(targetViewIndex).getLayoutParams()).leftMargin = MARGIN_NORMAL;
        ((LayoutParams) mPassionContainer.getChildAt(targetViewIndex).getLayoutParams()).rightMargin = MARGIN_NORMAL;
        mPassionContainer.getChildAt(targetViewIndex).requestLayout();

        // SET CENTER TO TARGET RATIO
        int currentDistance = getViewCenter(mPassionContainer.getChildAt(targetViewIndex)) - mScreenCenter;
        int diffCenter = (int) (destinationDistanceFromCenter - currentDistance);
        ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin += diffCenter;

        // NEIGHBOURS WHERE [RIGHT] POSITION OF TARGET VIEW
        int diffWidth = (int) (width + MARGIN_NORMAL * 2);
        int diffLeft = 0;
        for (int i = 0; i < nChild; i++) {
            if (i == targetViewIndex)
                continue;
            View view = mPassionContainer.getChildAt(i);
            int diff = (getViewCenter(view) - mScreenCenter);
            int absDiff = Math.abs(diff);
            PassionLocInformation levelDistance = (PassionLocInformation) view.getTag();
            if (diff < 0) {       // LEFT SIDE
                if (levelDistance.leftMediumDistance == absDiff) {
                    // MEDIUM
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_M);
                } else if (levelDistance.leftMediumDistance > absDiff) {
                    // BIG - MEDIUM
                    float scale = ((float) (levelDistance.leftMediumDistance - absDiff) / (float) levelDistance.leftMediumDistance);
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight()
                            + ((float) (mBiggestPassionBitmap.get(levelDistance.index).getHeight() - mMediumPassionBitmap.get(levelDistance.index).getHeight()) * scale);
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth()
                            + ((float) (mBiggestPassionBitmap.get(levelDistance.index).getWidth() - mMediumPassionBitmap.get(levelDistance.index).getWidth()) * scale);
                    setPassionBitmap(view, FONT_L);
                } else if (levelDistance.leftSmallDistance == absDiff) {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                } else if (levelDistance.leftSmallDistance > absDiff) {
                    // MEDIUM - SMALL
                    float scale = ((float) (levelDistance.leftSmallDistance - absDiff)
                            / (float) (levelDistance.leftSmallDistance - levelDistance.leftMediumDistance));
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight()
                            + ((float) (mMediumPassionBitmap.get(levelDistance.index).getHeight() - mSmallestPassionBitmap.get(levelDistance.index).getHeight()) * scale);
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth()
                            + ((float) (mMediumPassionBitmap.get(levelDistance.index).getWidth() - mSmallestPassionBitmap.get(levelDistance.index).getWidth()) * scale);
                    setPassionBitmap(view, FONT_M);
                } else {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_S);
                }
            } else if (diff > 0) {   //RIGHT SIDE
                if (levelDistance.rightMediumDistance == absDiff) {
                    // MEDIUM
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_M);
                } else if ((int) levelDistance.rightMediumDistance > absDiff) {
                    // BIG - MEDIUM
                    float scale = ((float) (levelDistance.rightMediumDistance - absDiff) / (float) levelDistance.rightMediumDistance);
                    height = (float) mMediumPassionBitmap.get(levelDistance.index).getHeight()
                            + ((float) (mBiggestPassionBitmap.get(levelDistance.index).getHeight() - mMediumPassionBitmap.get(levelDistance.index).getHeight()) * scale);
                    width = (float) mMediumPassionBitmap.get(levelDistance.index).getWidth()
                            + ((float) (mBiggestPassionBitmap.get(levelDistance.index).getWidth() - mMediumPassionBitmap.get(levelDistance.index).getWidth()) * scale);
                    setPassionBitmap(view, FONT_L);
                } else if (levelDistance.rightSmallDistance == absDiff) {
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_S);
                } else if (levelDistance.rightSmallDistance > absDiff) {
                    // MEDIUM - SMALL
                    float scale = ((float) (levelDistance.rightSmallDistance - absDiff)
                            / (float) (levelDistance.rightSmallDistance - levelDistance.rightMediumDistance));
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight()
                            + ((float) (mMediumPassionBitmap.get(levelDistance.index).getHeight()
                            - mSmallestPassionBitmap.get(levelDistance.index).getHeight()) * scale);
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth()
                            + ((float) (mMediumPassionBitmap.get(levelDistance.index).getWidth()
                            - mSmallestPassionBitmap.get(levelDistance.index).getWidth()) * scale);
                    setPassionBitmap(view, FONT_M);
                } else {
                    // SMALL
                    width = (float) mSmallestPassionBitmap.get(levelDistance.index).getWidth();
                    height = (float) mSmallestPassionBitmap.get(levelDistance.index).getHeight();
                    setPassionBitmap(view, FONT_S);
                }
            } else {  // CENTER
                height = (float) mBiggestPassionBitmap.get(levelDistance.index).getHeight();
                width = (float) mBiggestPassionBitmap.get(levelDistance.index).getWidth();
                setPassionBitmap(view, FONT_L);
            }
            diffWidth += Math.round(width + (MARGIN_NORMAL * 2f));
            if (i < targetViewIndex) {
                diffLeft = Math.round(width - ((LayoutParams) view.getLayoutParams()).width)
                        + ((MARGIN_NORMAL - ((LayoutParams) view.getLayoutParams()).leftMargin) * 2);
                ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin -= diffLeft;
            }
            ((LayoutParams) view.getLayoutParams()).width = Math.round(width);
            ((LayoutParams) view.getLayoutParams()).height = Math.round(height);
            ((LayoutParams) view.getLayoutParams()).leftMargin = MARGIN_NORMAL;
            ((LayoutParams) view.getLayoutParams()).rightMargin = MARGIN_NORMAL;
            view.requestLayout();
        }

        ((RelativeLayout.LayoutParams) this.getLayoutParams()).width = diffWidth;
        this.requestLayout();

        if (ratio == 1f) {
            mCenterIndex = passionIndex;
            //Attract loop passion 변경시, effect 부드럽게 진행되도록 수정함
//            setLandedPassionSizes();
        } else if (ratio == 0f) {
            //Attract loop passion 변경시, effect 부드럽게 진행되도록 수정함
//            setLandedPassionSizes();
        }
        manageRotateContainer(LOOP_FLAG_REMOVE_ALL);
    }

    private View findViewFromIndex(int passionIndex) {
        View view = getViewFromPassionIndex(passionIndex);
        if (view == null) {
            Log.e(TAG, "[startSmoothLoop] found target view is null");
            int leftDistance;
            for (leftDistance = 0; leftDistance < nPassion; leftDistance++) {
                int leftIndex = ((PassionLocInformation) mPassionContainer.getChildAt(0).getTag()).index - leftDistance;
                if (leftIndex < 0)
                    leftIndex = nPassion + leftIndex;
                if (leftIndex == passionIndex)
                    break;
            }
            int rightDistance;
            for (rightDistance = 0; rightDistance < nPassion; rightDistance++) {
                int rightIndex = ((PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag()).index + rightDistance;
                if (rightIndex >= nPassion)
                    rightIndex = rightIndex % nPassion;
                if (rightIndex == passionIndex)
                    break;
            }

            if (rightDistance < leftDistance) {
                int rightEndIndex = ((PassionLocInformation) mPassionContainer.getChildAt(mPassionContainer.getChildCount() - 1).getTag()).index;
                for (int i = 1; i <= rightDistance; i++) {
                    int rightIndex = rightEndIndex + i;
                    if (rightIndex >= nPassion)
                        rightIndex = rightIndex % nPassion;
                    ImageView imageView = generateNewPassion(rightIndex);
                    PassionLocInformation locInformation = new PassionLocInformation();
                    locInformation.rightSmallDistance = mPassionLocInformation[rightIndex].rightSmallDistance;
                    locInformation.rightMediumDistance = mPassionLocInformation[rightIndex].rightMediumDistance;
                    locInformation.leftSmallDistance = mPassionLocInformation[rightIndex].leftSmallDistance;
                    locInformation.leftMediumDistance = mPassionLocInformation[rightIndex].leftMediumDistance;
                    locInformation.index = rightIndex;
                    imageView.setTag(locInformation);
                    mPassionContainer.addView(imageView);
                    ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(rightIndex).getWidth() + (MARGIN_NORMAL * 2));
                    imageView.requestLayout();
                }
            } else {
                int leftEndIndex = ((PassionLocInformation) mPassionContainer.getChildAt(0).getTag()).index;
                for (int i = 1; i <= leftDistance; i++) {
                    int leftIndex = leftEndIndex - i;
                    if (leftIndex < 0)
                        leftIndex = nPassion + leftIndex;
                    ImageView imageView = generateNewPassion(leftIndex);
                    PassionLocInformation locInformation = new PassionLocInformation();
                    locInformation.rightSmallDistance = mPassionLocInformation[leftIndex].rightSmallDistance;
                    locInformation.rightMediumDistance = mPassionLocInformation[leftIndex].rightMediumDistance;
                    locInformation.leftSmallDistance = mPassionLocInformation[leftIndex].leftSmallDistance;
                    locInformation.leftMediumDistance = mPassionLocInformation[leftIndex].leftMediumDistance;
                    locInformation.index = leftIndex;
                    imageView.setTag(locInformation);
                    ((LayoutParams) imageView.getLayoutParams()).leftMargin = MARGIN_NORMAL;
                    ((LayoutParams) imageView.getLayoutParams()).rightMargin = MARGIN_NORMAL;
                    mPassionContainer.addView(imageView, 0);
                    ((RelativeLayout.LayoutParams) this.getLayoutParams()).leftMargin -= (mSmallestPassionBitmap.get(leftIndex).getWidth() + (MARGIN_NORMAL * 2));
                    ((RelativeLayout.LayoutParams) this.getLayoutParams()).width += (mSmallestPassionBitmap.get(leftIndex).getWidth() + (MARGIN_NORMAL * 2));
                    imageView.requestLayout();
                }
            }
            view = getViewFromPassionIndex(passionIndex);
        }
        return view;
    }

    public void startSmoothLoop(final int passionIndex) {
        if ((mSmoothLoopAnimator != null && (mSmoothLoopAnimator.isRunning() || mSmoothLoopAnimator.isStarted()))) {
            Log.e(TAG, "[startSmoothLoop:passionIndex] animation is running");
            return;
        }
        startSmoothLoop(null, passionIndex);
    }

    public int getCenterPassionIndex() {
        return mCenterIndex;
    }

    public void addOnLoopListener(OnAttractorLoopEventListener listener) {
        mOnAttractorEventListener = listener;
    }

    private View getViewFromPassionIndex(int index) {
        int nChild = mPassionContainer.getChildCount();
        for (int i = 0; i < nChild; i++) {
            if (((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index == index) {
                return mPassionContainer.getChildAt(i);
            }
        }
        return null;
    }

    public void onDestroy() {
        for (int i = 0; i < mSmallestPassionBitmap.size(); i++) {
            if (mSmallestPassionBitmap.get(i) != null && !mSmallestPassionBitmap.get(i).isRecycled())
            mSmallestPassionBitmap.get(i).recycle();
            mSmallestPassionBitmap.remove(mSmallestPassionBitmap.get(i));
        }
        mSmallestPassionBitmap = null;

        for (int i = 0; i < mMediumPassionBitmap.size(); i++) {
            if (mMediumPassionBitmap.get(i) != null && !mMediumPassionBitmap.get(i).isRecycled())
                mMediumPassionBitmap.get(i).recycle();
            mMediumPassionBitmap.remove(mMediumPassionBitmap.get(i));
        }
        mMediumPassionBitmap = null;

        for (int i = 0; i < mBiggestPassionBitmap.size(); i++) {
            if (mBiggestPassionBitmap.get(i) != null && !mBiggestPassionBitmap.get(i).isRecycled())
                mBiggestPassionBitmap.get(i).recycle();
            mBiggestPassionBitmap.remove(mBiggestPassionBitmap.get(i));
        }
        mBiggestPassionBitmap = null;

        if (attractorBitmap != null) {
            attractorBitmap.recycle();
            attractorBitmap = null;
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    //========================================
    // Passion Location Information
    //========================================
    class PassionLocInformation {
        int index;
        int leftSmallDistance;
        int leftMediumDistance;
        int rightMediumDistance;
        int rightSmallDistance;
    }

    //========================================
    // COLOR BAR
    //========================================
    private int[] mColors;
    private Paint mColorBarPaint = new Paint();
    private Paint mCenterPointPaint = new Paint();
    private Paint mRightPaddingPaint = new Paint();
    private Paint mLeftPaddingPaint = new Paint();
    private Paint mRightAlphaPaint = new Paint();
    private Paint mLeftAlphaPaint = new Paint();
    private Paint mPaint = new Paint();
    private Canvas temporalCanvas = new Canvas();
    private PorterDuffXfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);
    private Bitmap attractorBitmap;
    private int[] viewLocation = new int[2];

    public void setColors(int[] colors) {
        mColors = colors;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mColors == null) {
            Log.e(TAG, "color null");
            return;
        }
        if (attractorBitmap != null) {
            attractorBitmap.recycle();
            attractorBitmap = null;
        }

        int smallestHeight = mSmallestPassionBitmap.get(0).getHeight();
        this.getLocationInWindow(viewLocation);
        attractorBitmap = Bitmap.createBitmap(this.getMeasuredWidth(),
                SIZE_COLOR_BAR + smallestHeight + MARGIN_BOTTOM_PASSION, Bitmap.Config.ARGB_8888);
        int nChild = mPassionContainer.getChildCount();
        int[] normLocation = new int[nChild];
        float[] locRatio = new float[nChild];
        int[] colors = new int[nChild];
        for (int i = 0; i < nChild; i++) {
            normLocation[i] = getViewCenter(mPassionContainer.getChildAt(i)) - getViewCenter(mPassionContainer.getChildAt(0));
            colors[i] = mColors[((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index];
        }
        for (int i = 0; i < nChild; i++) {
            locRatio[i] = (float) normLocation[i] / (float) normLocation[nChild - 1];
        }
        temporalCanvas.setBitmap(attractorBitmap);
        for (int i = 0; i < nChild; i++) {
            int height = mPassionContainer.getChildAt(i).getLayoutParams().height;
            int width = mPassionContainer.getChildAt(i).getLayoutParams().width;
            int[] loc = new int[2];
            mPassionContainer.getChildAt(i).getLocationOnScreen(loc);
            if (((loc[0] + width) <= 0) || (loc[0] >= mScreenSize.x)) {
                if (mPassionContainer.getChildAt(i).isShown())
                    mPassionContainer.getChildAt(i).setVisibility(INVISIBLE);
            } else if ((loc[0] < (ALPHA_GRADIENT_WIDTH + ALPHA_GRADIENT_PADDING))
                    || (loc[0] + width >= mScreenSize.x - (ALPHA_GRADIENT_WIDTH + ALPHA_GRADIENT_PADDING))) {
                if (mPassionContainer.getChildAt(i).isShown())
                    mPassionContainer.getChildAt(i).setVisibility(INVISIBLE);
                //int y = HEIGHT_SMALLEST_PASSION - height;
                int y = 0;
                temporalCanvas.drawBitmap(mSmallestPassionBitmap.get(((PassionLocInformation) mPassionContainer.getChildAt(i).getTag()).index)
                        , mPassionContainer.getChildAt(i).getLeft(), y, null);
            } else {
                if (!mPassionContainer.getChildAt(i).isShown())
                    mPassionContainer.getChildAt(i).setVisibility(VISIBLE);
            }
        }

        // DRAW ATTRACTOR LOOP
        mColorBarPaint.setShader(new LinearGradient(0, 0, normLocation[nChild - 1], 0, colors, locRatio, Shader.TileMode.CLAMP));
        temporalCanvas.drawRect(0, smallestHeight + MARGIN_BOTTOM_PASSION, attractorBitmap.getWidth(),
                SIZE_COLOR_BAR + MARGIN_BOTTOM_PASSION + smallestHeight, mColorBarPaint);

        //mColorBarPaint.setDither(true);
//        mCenterPointPaint.setColor(0xFFFFFFFF);
//        temporalCanvas.drawCircle((mScreenCenter - viewLocation[0] - getViewLeft((View) this.getParent())),
//                SIZE_COLOR_BAR / 2 + smallestHeight + MARGIN_BOTTOM_PASSION, 5, mCenterPointPaint);


        // DRAW LEFT ALPHA GRADIENT
        mLeftAlphaPaint.setShader(new LinearGradient(-viewLocation[0] + ALPHA_GRADIENT_PADDING,
                0, -viewLocation[0] + ALPHA_GRADIENT_PADDING + ALPHA_GRADIENT_WIDTH, 0,
                0x00000000, 0xFFFFFFFF, Shader.TileMode.CLAMP));
        mLeftAlphaPaint.setXfermode(xfermode);
        mLeftAlphaPaint.setAntiAlias(true);
        temporalCanvas.drawRect(-viewLocation[0] + ALPHA_GRADIENT_PADDING, 0,
                -viewLocation[0] + ALPHA_GRADIENT_PADDING + ALPHA_GRADIENT_WIDTH, SIZE_COLOR_BAR + smallestHeight + MARGIN_BOTTOM_PASSION, mLeftAlphaPaint);
        mLeftPaddingPaint.setColor(0x00000000);
        mLeftPaddingPaint.setXfermode(xfermode);
        temporalCanvas.drawRect(-viewLocation[0], 0,
                -viewLocation[0] + ALPHA_GRADIENT_PADDING, SIZE_COLOR_BAR + smallestHeight + MARGIN_BOTTOM_PASSION, mLeftPaddingPaint);
        // DRAW RIGHT ALPHA GRADIENT
        mRightAlphaPaint.setShader(
                new LinearGradient(-viewLocation[0] + mScreenSize.x - (ALPHA_GRADIENT_WIDTH + ALPHA_GRADIENT_PADDING), 0,
                        -viewLocation[0] + mScreenSize.x - ALPHA_GRADIENT_PADDING, 0,
                        0xFFFFFFFF, 0x00000000, Shader.TileMode.CLAMP));
        mRightAlphaPaint.setXfermode(xfermode);
        mRightAlphaPaint.setAntiAlias(true);
        temporalCanvas.drawRect(-viewLocation[0] + mScreenSize.x - (ALPHA_GRADIENT_WIDTH + ALPHA_GRADIENT_PADDING),
                0, -viewLocation[0] + mScreenSize.x - ALPHA_GRADIENT_PADDING, SIZE_COLOR_BAR + smallestHeight + MARGIN_BOTTOM_PASSION, mRightAlphaPaint);
        mRightPaddingPaint.setColor(0x00000000);
        mRightPaddingPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        temporalCanvas.drawRect(-viewLocation[0] + mScreenSize.x - ALPHA_GRADIENT_PADDING,
                0, -viewLocation[0] + mScreenSize.x + 1, SIZE_COLOR_BAR + smallestHeight + MARGIN_BOTTOM_PASSION, mRightPaddingPaint);


        // DRAW RESULT ON CANVAS
        canvas.drawBitmap(attractorBitmap, 0, (getMeasuredHeight() - SIZE_COLOR_BAR - smallestHeight - MARGIN_BOTTOM_BAR - MARGIN_BOTTOM_PASSION), mPaint);
    }

    public interface OnAttractorLoopEventListener {
        void onArriving(int oldPassionIndex, int newPassionIndex, float ratio, boolean isLandArriving);

        void onFirstTouch(View view);

        void onTap(View view, int passionIndex);

        void onArrived(int passionIndex, boolean isLand);

        void onArrivedFromFling(int passionIndex);

        void onStartOfFling();

        void onArrivedForAppDetail(int passionIndex, int subcategoryIndex, int appIndex);

        void onAutoLoopArrived(int passionIndex);

        void onAttactTouchDuringYoutubeLoading();
    }
}