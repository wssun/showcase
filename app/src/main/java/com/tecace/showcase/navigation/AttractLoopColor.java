package com.tecace.showcase.navigation;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by swkim on 2015-03-19.
 */
public class AttractLoopColor {
    private static final String TAG = AttractLoopColor.class.getName();

    private ArrayList<Integer> colors;

    public AttractLoopColor(int nPassion, int samsungIndex){

        colors = new ArrayList<Integer>();

        // http://krazydad.com/tutorials/makecolors.php
        float frequency = 0.4f;
        int samsungColorIndex = makeColorGradient(frequency, frequency, frequency, 0, (float)(2.0f*Math.PI/3.0f), (float)(4.0f*Math.PI/3.0f), 127, 128, nPassion);

        if (samsungIndex != 0) {
            Collections.rotate(colors, samsungIndex - samsungColorIndex);
        }
    }

    private int makeColorGradient(float frequency1, float frequency2, float frequency3,
                               float phase1, float phase2, float phase3,
                               int center, int width, int len)
    {
        if (center == -1)   center = 128;
        if (width == -1)    width = 127;
        if (len == -1)      len = 18;

        int blueMax = 0;
        int blueMaxIndex = 0;

        for (int i = 0; i < len; ++i)
        {
            int red = (int)(Math.sin(frequency1*i + phase1) * width) + center;
            int grn = (int)(Math.sin(frequency2*i + phase2) * width) + center;
            int blu = (int)(Math.sin(frequency3*i + phase3) * width) + center;

            if (blu > blueMax) {
                blueMaxIndex = i;
                blueMax = blu;
            }

            //Log.d(TAG, String.valueOf(red) + "," + String.valueOf(grn) + "," + String.valueOf(blu));
            colors.add(Color.rgb(red, grn, blu));
        }

        return blueMaxIndex;
    }

    public int getColorAt(int index){
        //return colors.get(index*(colors.size()/nPassion));
        return colors.get(index);
    }
}
