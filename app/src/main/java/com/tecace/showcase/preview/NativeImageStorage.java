package com.tecace.showcase.preview;

import android.content.Context;
import android.graphics.Bitmap;

import com.tecace.showcase.R;

/**
 * Created by swkim on 2015-06-12.
 */
public class NativeImageStorage {

    public static final int STATE_NEW = 0;
    public static final int STATE_OLD = 1;
    public static final int MODE_CLEAR = 0;
    public static final int MODE_BLUR = 1;

    private static final String TAG = "NativeImageStorage";
    static {
        System.loadLibrary("NativeImageStorage");
    }

    public NativeImageStorage(Context context) {
        init(context.getResources().getDimensionPixelSize(R.dimen.screen_width),
                context.getResources().getDimensionPixelSize(R.dimen.screen_height));
    }

    public native void init(int width, int height);

    public native void storeImageInNativeHeap(Bitmap bitmap, int passionIndex, int mode);

    public native Bitmap pullImageFromNativeHeap(int passionIndex, int state, int mode);

    public native void releaseImageStorage();
}
