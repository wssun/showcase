package com.tecace.showcase.preview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tecace.showcase.R;
import com.tecace.showcase.data.AppDataManager;

import java.lang.ref.WeakReference;

public class TransitionBackgroundView extends RelativeLayout {

    private static final String TAG = "TransitionBackground";

    private int mCenterIndex = 12;

    private RelativeLayout mRootView = null;
    private ImageView mNewBlurBackgroundView = null;
    private ImageView mOldBlurBackgroundView = null;
    private ImageView mOldClearBackgroundView = null;
    private ImageView mNewClearBackgroundView = null;

    private OnBackgroundGestureListener mOnBackgroundTouchListener = null;
    private TransitionBackgroundView mParent = null;

    private AppDataManager mAppDataManager = null;
    private NativeImageStorage mNativeImageStorage = null;

    public TransitionBackgroundView(Context context) {
        super(context);
        init();
    }

    public TransitionBackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TransitionBackgroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mParent = this;
        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layoutInflater != null)
            mRootView = (RelativeLayout) layoutInflater.inflate(R.layout.transition_background_layout, null);
        mOldBlurBackgroundView = (ImageView) mRootView.findViewById(R.id.preview_old_blur_background);
        mNewBlurBackgroundView = (ImageView) mRootView.findViewById(R.id.preview_new_blur_background);
        mOldClearBackgroundView = (ImageView) mRootView.findViewById(R.id.preview_old_clear_background);
        mNewClearBackgroundView = (ImageView) mRootView.findViewById(R.id.preview_new_clear_background);

        this.setOnTouchListener(new TransitionTouchListener());
        this.addView(mRootView);

    }

    private int mTransitioningNewIndex = Integer.MIN_VALUE;
    private float mRunningRatio = 0f;

    public void setTransitionRatio(int passionIndex, float ratio, boolean isLandArriving, boolean isPreviewShown) {
        if (ratio > 1.0f) ratio = 1.0f;
        if (ratio < 0f) ratio = 0f;
        mRunningRatio = ratio;
        if (mTransitioningNewIndex != passionIndex) {
            // SET TARGET BLUR IMAGE
            mNewBlurBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(passionIndex,
                    NativeImageStorage.STATE_NEW,
                    NativeImageStorage.MODE_BLUR));
            mNewBlurBackgroundView.setTag(passionIndex);
            mNewClearBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(passionIndex,
                    NativeImageStorage.STATE_NEW,
                    NativeImageStorage.MODE_CLEAR));
            // SET VISIBILITY
            if (!isPreviewShown) {
                if (mNewClearBackgroundView.getAlpha() != 0f)
                    mNewClearBackgroundView.setAlpha(0f);
                if (!isLandArriving && isTransitionRunning) {
                    mNewClearBackgroundView.setVisibility(GONE);
                    mNewClearBackgroundView.setImageResource(android.R.color.transparent);
                    mOldClearBackgroundView.setVisibility(GONE);
                    mOldClearBackgroundView.setImageResource(android.R.color.transparent);
                } else {
                    mNewClearBackgroundView.setVisibility(VISIBLE);
                    mOldClearBackgroundView.setVisibility(VISIBLE);
                }
            } else {
                mNewClearBackgroundView.setVisibility(GONE);
                mNewClearBackgroundView.setImageResource(android.R.color.transparent);
                mOldClearBackgroundView.setVisibility(GONE);
                mOldClearBackgroundView.setImageResource(android.R.color.transparent);
            }
            mOldBlurBackgroundView.setVisibility(VISIBLE);
            mNewBlurBackgroundView.setVisibility(VISIBLE);
            // REMEMBER CURRENT INDEX
            mTransitioningNewIndex = passionIndex;
        }
        mNewBlurBackgroundView.setAlpha(ratio);
        if (!isPreviewShown) {
            if (ratio < 0.5f) {
                float clearRatio = 1f - ratio * 2f;
                if (clearRatio < 0f)
                    clearRatio = 0f;
                else if (clearRatio > 1f)
                    clearRatio = 1f;
                mOldClearBackgroundView.setAlpha(clearRatio);
                if (!mOldClearBackgroundView.isShown() && isLandArriving)
                    mOldClearBackgroundView.setVisibility(VISIBLE);
                if (mNewClearBackgroundView.getAlpha() != 0f)
                    mNewClearBackgroundView.setAlpha(0f);
            } else if (isLandArriving && isTransitionRunning) {
                float clearRatio = (ratio * 2f) - 1f;
                if (clearRatio < 0f)
                    clearRatio = 0f;
                else if (clearRatio > 1f)
                    clearRatio = 1f;
                mNewClearBackgroundView.setAlpha(clearRatio);
                if (!mNewClearBackgroundView.isShown())
                    mNewClearBackgroundView.setVisibility(VISIBLE);
                if (!mOldClearBackgroundView.isShown())
                    mOldClearBackgroundView.setVisibility(VISIBLE);
            }
        }
        if (ratio == 1f) {
            // SET BLUR IMAGE
            mOldBlurBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(passionIndex,
                    NativeImageStorage.STATE_OLD,
                    NativeImageStorage.MODE_BLUR));
            // SET CLEAR IMAGE
            if (isLandArriving) {
                mOldClearBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(passionIndex,
                        NativeImageStorage.STATE_OLD,
                        NativeImageStorage.MODE_CLEAR));

                if (mOldClearBackgroundView.getAlpha() != 1f)
                    mOldClearBackgroundView.setAlpha(1f);

                if (!isPreviewShown) {
                    mOldClearBackgroundView.setVisibility(VISIBLE);
                    //mOldBlurBackgroundView.setVisibility(GONE);
                } else {
                    mOldClearBackgroundView.setVisibility(GONE);
                    mOldBlurBackgroundView.setVisibility(VISIBLE);
                }
            } else {
                mOldClearBackgroundView.setAlpha(0f);
                mOldClearBackgroundView.setImageResource(android.R.color.transparent);
                mOldClearBackgroundView.setVisibility(GONE);
            }

            mNewClearBackgroundView.setImageResource(android.R.color.transparent);
            mNewClearBackgroundView.setVisibility(GONE);
            mNewBlurBackgroundView.setImageResource(android.R.color.transparent);
            mNewBlurBackgroundView.setVisibility(GONE);

            mCenterIndex = passionIndex;
            mTransitioningNewIndex = Integer.MIN_VALUE;
            if (isLandArriving)
                isTransitionRunning = false;
        } else if (ratio == 0f) {
            mNewClearBackgroundView.setImageResource(android.R.color.transparent);
            mNewClearBackgroundView.setVisibility(GONE);
            mNewBlurBackgroundView.setImageResource(android.R.color.transparent);
            mNewBlurBackgroundView.setVisibility(GONE);
            if (!isPreviewShown) {
                if (isLandArriving)
                    mOldClearBackgroundView.setVisibility(VISIBLE);
                //mOldBlurBackgroundView.setVisibility(GONE);
            } else {
                mOldClearBackgroundView.setVisibility(GONE);
                mOldBlurBackgroundView.setVisibility(VISIBLE);
            }
            mTransitioningNewIndex = Integer.MIN_VALUE;
            if (isLandArriving)
                isTransitionRunning = false;
        } else {
            isTransitionRunning = true;
        }
    }

    public Animator generateBlurBackgroundAnimator() {
        blurAnimator = ObjectAnimator.ofFloat(mOldClearBackgroundView, "alpha", 1f, 0f);
        blurAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mOldClearBackgroundView.setVisibility(VISIBLE);
                mOldBlurBackgroundView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mOldClearBackgroundView.setVisibility(GONE);
            }
        });
        return blurAnimator;
    }

    ValueAnimator blurAnimator;
    ValueAnimator clearAnimator;

    public Animator generateClearBackgroundAnimator() {
        clearAnimator = ObjectAnimator.ofFloat(mOldClearBackgroundView, "alpha", 0f, 1f);
        clearAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mOldClearBackgroundView.setVisibility(VISIBLE);
                mOldBlurBackgroundView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //mOldBlurBackgroundView.setVisibility(GONE);
                mOldClearBackgroundView.setAlpha(1f);
            }
        });
        return clearAnimator;
    }

    public void stopAnimators() {
        if (clearAnimator != null && (clearAnimator.isRunning() || clearAnimator.isStarted())) {
            clearAnimator.removeAllListeners();
            clearAnimator.cancel();
        }
        if (blurAnimator != null && (blurAnimator.isRunning() || blurAnimator.isStarted())) {
            blurAnimator.removeAllListeners();
            blurAnimator.cancel();
        }
    }

    public void onDestroy() {
        mNativeImageStorage.releaseImageStorage();
    }

    public void setAppDataManager(AppDataManager appDataManager, int centerIndex) {
        mAppDataManager = appDataManager;
        mCenterIndex = centerIndex;

        mNativeImageStorage = new NativeImageStorage(this.getContext());
        for (int i = 0; i < mAppDataManager.getNumberOfPassions(); i++) {
            Bitmap clearBitmap = BitmapFactory.decodeFile(mAppDataManager.getPassionDataAt(i).getClearBgImage());
            if (clearBitmap == null) {
                Log.e(TAG, "Cannot read background(clear) file : " + mAppDataManager.getPassionDataAt(i).getClearBgImage());
                clearBitmap = Bitmap.createBitmap(getResources().getDimensionPixelSize(R.dimen.screen_width),
                        getResources().getDimensionPixelSize(R.dimen.screen_height),
                        Bitmap.Config.ARGB_8888);
            }
            mNativeImageStorage.storeImageInNativeHeap(clearBitmap, i, NativeImageStorage.MODE_CLEAR);
            clearBitmap.recycle();
            Bitmap blurBitmap = BitmapFactory.decodeFile(mAppDataManager.getPassionDataAt(i).getBlurBgImage());
            if (blurBitmap == null) {
                Log.e(TAG, "Cannot read background(blur) file : " + mAppDataManager.getPassionDataAt(i).getBlurBgImage());
                blurBitmap = Bitmap.createBitmap(getResources().getDimensionPixelSize(R.dimen.screen_width),
                        getResources().getDimensionPixelSize(R.dimen.screen_height),
                        Bitmap.Config.ARGB_8888);
            }
            mNativeImageStorage.storeImageInNativeHeap(blurBitmap, i, NativeImageStorage.MODE_BLUR);
            blurBitmap.recycle();
        }
    }

    public void setDefaultImageSet() {
        mOldClearBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(mCenterIndex,
                NativeImageStorage.STATE_OLD,
                NativeImageStorage.MODE_CLEAR));
        mOldBlurBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(mCenterIndex,
                NativeImageStorage.STATE_OLD,
                NativeImageStorage.MODE_BLUR));
    }

    public void setOnBackgroundGestureListener(OnBackgroundGestureListener listener) {
        mOnBackgroundTouchListener = listener;
    }

    private AnimatorSet generateBackgroundClearAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator alpha = ObjectAnimator.ofFloat(mOldClearBackgroundView, "alpha", 1f);
        alpha.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mOldClearBackgroundView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }
        });
        animatorSet.play(alpha);
        return animatorSet;
    }

    private AnimatorSet generateBackgroundBlurAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator alpha = ObjectAnimator.ofFloat(mOldClearBackgroundView, "alpha", 0f);
        alpha.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mOldClearBackgroundView.setVisibility(View.GONE);
            }
        });
        animatorSet.play(alpha);
        return animatorSet;
    }

    private float mOldDragX = 0f, mPrevDragX = 0f, mVelocity = 0f, mInitY = 0f;
    private boolean isScrollable = false;

    public void setBlurBackground(boolean blurBackground) {
        stopAnimators();
        if (!blurBackground) {
            if (mOldClearBackgroundView.getAlpha() != 1f)
                mOldClearBackgroundView.setAlpha(1f);
            mOldClearBackgroundView.setImageBitmap(mNativeImageStorage.pullImageFromNativeHeap(mCenterIndex,
                    NativeImageStorage.STATE_OLD,
                    NativeImageStorage.MODE_CLEAR));
            mOldClearBackgroundView.setVisibility(VISIBLE);
            //mOldBlurBackgroundView.setVisibility(GONE);
        } else {
            if (mOldClearBackgroundView.getAlpha() != 0f)
                mOldClearBackgroundView.setAlpha(0f);
            mOldClearBackgroundView.setVisibility(GONE);
            mOldBlurBackgroundView.setVisibility(VISIBLE);

        }
        mNewBlurBackgroundView.setVisibility(GONE);
        mNewClearBackgroundView.setVisibility(GONE);
    }

    public boolean isTranslateAnimationRunning() {
        return ((mFlingHandler != null && mFlingHandler.isRunning()));
    }

    public void stopTranslateAnimation() {
        if (mFlingHandler.isRunning()) {
            mFlingHandler.removeCallbacksAndMessages(null);
            mFlingHandler.stop();
        }

    }

    private boolean isTransitionRunning = false;
    private class TransitionTouchListener implements OnTouchListener {
        private int targetIndex = Integer.MIN_VALUE, prevTargetIndex = Integer.MIN_VALUE;
        private static final float SPEED_LIMIT = 20f;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mVelocity = 0f;
                    mOldDragX = mPrevDragX = event.getRawX();
                    mInitY = event.getRawX();
                    isScrollable = false;
                    isTappedDuringAnimation = false;
                    if (isTranslateAnimationRunning() || isTransitionRunning) {
                        float interruptedValue;
                        if (!isTransitionRunning && (mFlingHandler != null && mFlingHandler.isRunning()))
                            interruptedValue = mFlingHandler.getRunningRatio();
                        else interruptedValue = mRunningRatio;
                        isTransitionRunning = true;
                        int prevIndex = mCenterIndex - 1;
                        int nextIndex = mCenterIndex + 1;
                        if (prevIndex < 0)
                            prevIndex = mAppDataManager.getNumberOfPassions() + prevIndex;
                        if (nextIndex >= mAppDataManager.getNumberOfPassions())
                            nextIndex = nextIndex % mAppDataManager.getNumberOfPassions();
                        if (targetIndex == nextIndex || mTransitioningNewIndex == nextIndex)
                            mOldDragX += interruptedValue * (float) view.getMeasuredWidth();
                        else if (targetIndex == prevIndex || mTransitioningNewIndex == prevIndex)
                            mOldDragX -= interruptedValue * (float) view.getMeasuredWidth();
                        mPrevDragX = mOldDragX;
                    }
                    if (mOnBackgroundTouchListener != null) {
                        mOnBackgroundTouchListener.onFirstTouch(mParent);
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    float dx = event.getRawX() - mOldDragX;
                    mVelocity = (event.getRawX() - mPrevDragX);
                    if (Math.abs(mVelocity) > 80f)
                        mVelocity = (mVelocity < 0f) ? -80f : 80f;
                    mPrevDragX = event.getRawX();
                    if (!isScrollable && !isTransitionRunning) {
                        // IF dx IS SMALLER THAN 30f, NAVIGATION NOT MOVE
                        if (Math.abs(dx) > 30f) {
                            mOldDragX = event.getRawX();
                            isScrollable = true;
                        }
                        return true;
                    }
                    if (dx < 0f)
                        targetIndex = mCenterIndex + 1;
                    else if (dx > 0f)
                        targetIndex = mCenterIndex - 1;
                    if (targetIndex >= mAppDataManager.getNumberOfPassions())
                        targetIndex = targetIndex % mAppDataManager.getNumberOfPassions();
                    else if (targetIndex < 0)
                        targetIndex = mAppDataManager.getNumberOfPassions() + targetIndex;
                    if (targetIndex < 0 || targetIndex > mAppDataManager.getNumberOfPassions()) break;

                    if (prevTargetIndex != Integer.MIN_VALUE && targetIndex != prevTargetIndex)
                        setTransitionRatio(mCenterIndex, 0f, true, false);
                    float ratio = Math.abs(dx) / (float) view.getMeasuredWidth();
                    setTransitionRatio(targetIndex, ratio, true, false);
                    if (mOnBackgroundTouchListener != null)
                        mOnBackgroundTouchListener.onBackgroundArriving(targetIndex, ratio);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_POINTER_UP:

                    float moveDistance = Math.abs(mOldDragX - event.getRawX());

                    //#294 Tapping background to open passion does nothing 수정함.
//                    if (!isTransitionRunning &&
//                            ((!isScrollable && moveDistance < 30f) || (isScrollable && moveDistance <= 0f)) && moveDistance <= 30f) {
                    if (((!isScrollable && moveDistance < 30f) || (isScrollable && moveDistance <= 0f)) && moveDistance <= 30f) {
                        // TAP EVENT
                        if (mOnBackgroundTouchListener != null) {
                            mOnBackgroundTouchListener.onBackgroundTap();
                        }
                        isTransitionRunning = false;
                    } else if (isTransitionRunning && !isScrollable && Math.abs(mInitY - event.getRawX()) < 30f) {
                        // TAP EVENT
                        if (mNewBlurBackgroundView.getAlpha() < 0.5f)
                            startCrossFadeAnimator((int) mNewBlurBackgroundView.getTag(), mNewBlurBackgroundView.getAlpha(), 0f, true);
                        else
                            startCrossFadeAnimator((int) mNewBlurBackgroundView.getTag(), mNewBlurBackgroundView.getAlpha(), 1f, true);
                    } else {
                        // FLING EVENT
                        float releaseRatio = moveDistance / (float) view.getMeasuredWidth();

                        // BACK TO CURRENT PAGE
                        if (targetIndex == Integer.MIN_VALUE) {
                            //#280 Main window display is not displayed correctly if sub-category is tapped quickly multiple times 수정함.
//                            startCrossFadeAnimator(mCenterIndex, releaseRatio, 0f);
                        } else if (Math.abs(mVelocity) < SPEED_LIMIT && releaseRatio < 0.5f) {
                            startCrossFadeAnimator(targetIndex, releaseRatio, 0f);
                        } else {
                            startCrossFadeAnimator(targetIndex, releaseRatio, 1f);
                        }
                        isTransitionRunning = true;
                        targetIndex = prevTargetIndex = Integer.MIN_VALUE;
                    }
                    isScrollable = false;
                    break;
            }
            return true;
        }
    }

    private void startCrossFadeAnimator(final int targetIndex, final float fromRatio, float toRatio) {
        if (targetIndex < 0 || targetIndex >= mAppDataManager.getNumberOfPassions()) return;
        if (mFlingHandler == null)
            mFlingHandler = new FlingHandler(this);
        mFlingHandler.removeCallbacksAndMessages(null);
        mFlingHandler.reset();
        Message msg = mFlingHandler.obtainMessage();
        msg.arg1 = targetIndex;
        msg.arg2 = (int) toRatio;
        msg.obj = fromRatio;
        mFlingHandler.sendMessage(msg);
    }

    private boolean isTappedDuringAnimation = false;

    private void startCrossFadeAnimator(final int targetIndex, final float fromRatio, float toRatio, boolean isTapped) {
        if (mFlingHandler == null)
            mFlingHandler = new FlingHandler(this);
        isTappedDuringAnimation = isTapped;
        startCrossFadeAnimator(targetIndex, fromRatio, toRatio);
    }

    private FlingHandler mFlingHandler = null;

    static private class FlingHandler extends Handler {
        private final WeakReference<TransitionBackgroundView> parent;
        private float runningRatio = 0f;
        private boolean stopFlag = false;

        public FlingHandler(TransitionBackgroundView parent) {
            this.parent = new WeakReference<>(parent);
        }

        @Override
        public void handleMessage(Message msg) {
            TransitionBackgroundView parent = this.parent.get();
            if (parent != null && !stopFlag) {
                runningRatio = parent.handleFlingMessage(msg);
            } else {
                stopFlag = false;
            }
        }

        public void reset() {
            stopFlag = false;
        }

        public void stop() {
            stopFlag = true;
        }

        public boolean isRunning() {
            return !(runningRatio == 0f || runningRatio == 1f);
        }

        public float getRunningRatio() {
            return runningRatio;
        }
    }

    public float handleFlingMessage(Message msg) {
        int passionIndex = msg.arg1;
        float toRatio = (float) msg.arg2;
        float ratio = (toRatio == 0f) ? ((float) msg.obj - 0.1f) : ((float) msg.obj + 0.1f);
        if (toRatio == 0f && ratio < 0f) {
            setTransitionRatio(passionIndex, 0f, true, false);
            if (mOnBackgroundTouchListener != null) {
                mOnBackgroundTouchListener.onBackgroundArriving(passionIndex, 0f);
                mOnBackgroundTouchListener.onBackgroundArrived(passionIndex);
                if (isTappedDuringAnimation)
                    mOnBackgroundTouchListener.onBackgroundTap();
            }
            isTransitionRunning = false;
            return 0f;
        } else if (toRatio == 1f && ratio > 1f) {
            setTransitionRatio(passionIndex, 1f, true, false);
            if (mOnBackgroundTouchListener != null) {
                mOnBackgroundTouchListener.onBackgroundArriving(passionIndex, 1f);
                mOnBackgroundTouchListener.onBackgroundArrived(passionIndex);
                if (isTappedDuringAnimation)
                    mOnBackgroundTouchListener.onBackgroundTap();
            }
            isTransitionRunning = false;
            return 1f;
        }

        setTransitionRatio(passionIndex, ratio, true, false);
        if (mOnBackgroundTouchListener != null)
            mOnBackgroundTouchListener.onBackgroundArriving(passionIndex, ratio);
        Message newMsg = mFlingHandler.obtainMessage();
        if (mFlingHandler != null) {
            newMsg.arg1 = passionIndex;
            newMsg.arg2 = (int) toRatio;
            newMsg.obj = ratio;
            mFlingHandler.sendMessageDelayed(newMsg, 1);
        }
        return ratio;
    }

    public interface OnBackgroundGestureListener {
        void onBackgroundTap();

        void onFirstTouch(View view);

        void onBackgroundArriving(int targetIndex, float ratio);

        void onBackgroundArrived(int targetIndex);
    }
}
