package com.tecace.showcase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tecace.showcase.util.FontTypeface;

public class ControllersLayout extends RelativeLayout {
    private String TAG = this.getClass().getCanonicalName();

    private View mAvailability;
    private View mDeviceType;
    private View mEmail;
    private View mHome;
    private View mBackTitleLayout;
    private ImageView mBackBtn;

    private TextView mStaticMessage;
    private TextView mAppTitle;
    private TextView mBackTitle;

    ImageView viewEmailSelection;
    ImageView viewDeviceTypeSelection;
    ImageView viewAvailabilitySelection;

    private OnControllersLayoutInteractionListener mListener = null;

    public ControllersLayout(Context context) {
        super(context);
        init(context);
    }

    public ControllersLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ControllersLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        if (!isInEditMode()) {
            mListener = (OnControllersLayoutInteractionListener) (Activity) (super.getContext());
        }

        RelativeLayout parent = (RelativeLayout) inflate(getContext(), R.layout.controller_layout, this);

        mStaticMessage = (TextView) parent.findViewById(R.id.static_message);

        mAppTitle = (TextView) parent.findViewById(R.id.apps);
        Typeface tfUThin = FontTypeface.getInstance().getFontUltrathin();
        mAppTitle.setTypeface(tfUThin);

        mBackTitleLayout = parent.findViewById(R.id.back_button_n_title_layout);
        mBackTitleLayout.setVisibility(View.GONE);

        mBackTitle = (TextView) parent.findViewById(R.id.backTitle);
        mBackTitle.setTypeface(tfUThin);

        initButton(parent);
    }

   private void initButton(final RelativeLayout parent) {
        Typeface tfEtextBold = FontTypeface.getInstance().getEtextFontBold();

        viewAvailabilitySelection = (ImageView) parent.findViewById(R.id.availabilityButton);
        mAvailability = parent.findViewById(R.id.availability_button_layout);
        ((TextView)mAvailability.findViewById(R.id.availability_button_text)).setTypeface(tfEtextBold);
        mAvailability.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((FullscreenActivity) view.getContext()).isSwitchingAnimationRunning())
                    return;

                if (mListener != null)
                    mListener.onAvailabilityPressed(false);

                viewAvailabilitySelection.setImageResource(R.drawable.availability_selected);
                viewDeviceTypeSelection.setImageResource(R.drawable.type_normal);
                viewEmailSelection.setImageResource(R.drawable.email_normal);
            }
        });

       viewDeviceTypeSelection = (ImageView) parent.findViewById(R.id.devicetypeButton);
       mDeviceType = parent.findViewById(R.id.devicetype_button_layout);
        ((TextView)mDeviceType.findViewById(R.id.devicetype_button_text)).setTypeface(tfEtextBold);
       mDeviceType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((FullscreenActivity) view.getContext()).isSwitchingAnimationRunning())
                    return;

                if (mListener != null)
                    mListener.onDeviceTypePressed(false);

                viewAvailabilitySelection.setImageResource(R.drawable.availability_normal);
                viewDeviceTypeSelection.setImageResource(R.drawable.type_selected);
                viewEmailSelection.setImageResource(R.drawable.email_normal);


            }
        });

       viewEmailSelection = (ImageView) parent.findViewById(R.id.emailButton);
       mEmail = parent.findViewById(R.id.email_button_layout);
        ((TextView)mEmail.findViewById(R.id.email_button_text)).setTypeface(tfEtextBold);
       mEmail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((FullscreenActivity) view.getContext()).isSwitchingAnimationRunning())
                    return;

                view.setClickable(false);

                if (mListener != null)
                    mListener.onEmailTypePressed(false);

                viewAvailabilitySelection.setImageResource(R.drawable.availability_normal);
                viewDeviceTypeSelection.setImageResource(R.drawable.type_normal);
                viewEmailSelection.setImageResource(R.drawable.email_selected);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mEmail.setClickable(true);
                    }
                }, 100);
            }
        });

       mHome = parent.findViewById(R.id.home_button_layout);
       mHome.setOnClickListener(new OnClickListener() {
           @Override
           public void onClick(View view) {
               if (((FullscreenActivity) view.getContext()).isSwitchingAnimationRunning())
                   return;

               view.setClickable(false);

               if (mListener != null)
                   mListener.onHomePressed(false);

               final Handler handler = new Handler();
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       mHome.setClickable(true);
                   }
               }, 100);
           }
       });

       mBackBtn = (ImageView) parent.findViewById(R.id.backButton);
       mBackBtn.setOnClickListener(new OnClickListener() {
           @Override
           public void onClick(View view) {
               if (((FullscreenActivity) view.getContext()).isSwitchingAnimationRunning())
                   return;

               view.setClickable(false);

               if (mListener != null)
                    mListener.onBackPressed(false);

               final Handler handler = new Handler();
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       mBackBtn.setClickable(true);
                   }
               }, 100);
           }
       });
    }

//    public Animator getStaticMessageDismissAnimator() {
//        return ObjectAnimator.ofFloat(mStaticMessage, "alpha", 1f, 0f);
//    }
//
//    public Animator getStaticMessageAttractorLoop() {
//        ValueAnimator animator = ObjectAnimator.ofFloat(mStaticMessage, "alpha", 0f, 1f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                mStaticMessage.setText(R.string.static_message_attractor_loop);
//            }
//        });
//        return animator;
//    }
//
//    public Animator getStaticMessageSubcategoryDisplay() {
//        ValueAnimator animator = ObjectAnimator.ofFloat(mStaticMessage, "alpha", 0f, 1f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                mStaticMessage.setText(R.string.static_message_subcategory_display);
//            }
//        });
//        return animator;
//    }
//
//    public Animator getStaticMessageSubcategoryPreview() {
//        ValueAnimator animator = ObjectAnimator.ofFloat(mStaticMessage, "alpha", 0f, 1f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                mStaticMessage.setText(R.string.static_message_subcategory_preview);
//            }
//        });
//        return animator;
//    }
//
//    public Animator getStaticMessageCheckOutDisplay() {
//        ValueAnimator animator = ObjectAnimator.ofFloat(mAppTitle, "alpha", 0f, 1f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                mAppTitle.setText(R.string.static_message_checkout_display);
//            }
//        });
//        return animator;
//    }

    public Animator getAppTitleDismissAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animator = ObjectAnimator.ofFloat(mAppTitle, "alpha", 1f, 0f);
        ValueAnimator animatorBtnHome = ObjectAnimator.ofFloat(mHome, "alpha", 1f, 0f);

        animatorSet.play(animator).with(animatorBtnHome);
        return animatorSet;
    }

    public Animator getAppsTitleAttractorLoop() {
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animator = ObjectAnimator.ofFloat(mAppTitle, "alpha", 0f, 1f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mAppTitle.setVisibility(View.GONE);
            }
        });

        ValueAnimator animatorBtnHome = ObjectAnimator.ofFloat(mHome, "alpha", 0f, 1f);
        animatorBtnHome.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mHome.setVisibility(View.GONE);
            }
        });

        animatorSet.play(animator).with(animatorBtnHome);
        return animatorSet;
    }

    public Animator getAppsTitleSubcategoryDisplay(final String passionName) {
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animator = ObjectAnimator.ofFloat(mAppTitle, "alpha", 0f, 1f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mAppTitle.setVisibility(View.VISIBLE);
                mAppTitle.setText(passionName);
                showAccessButtons(false);
            }
        });

        if (passionName.compareTo("Made for Samsung") == 0) {
            mHome.setVisibility(View.GONE);
            animatorSet.play(animator);
        } else {
            ValueAnimator animatorBtnHome = ObjectAnimator.ofFloat(mHome, "alpha", 0f, 1f);
            animatorBtnHome.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mHome.setVisibility(View.VISIBLE);
                }
            });
            animatorSet.play(animator).with(animatorBtnHome);
        }

        return animatorSet;
    }

    public Animator getM4STitleDisplay(final String passionName) {
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animator = ObjectAnimator.ofFloat(mBackTitleLayout, "alpha", 0f, 1f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mBackTitleLayout.setVisibility(View.VISIBLE);
                mBackTitle.setText(passionName);
            }
        });

        animatorSet.play(animator);
        return animatorSet;
    }

    public Animator getM4STitleDismissAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animator = ObjectAnimator.ofFloat(mBackTitleLayout, "alpha", 1f, 0f);

        animatorSet.play(animator);
        return animatorSet;
    }

    public void setAppTitle(String passionName) {
        mAppTitle.setText(passionName);
    }

    public void showHomeButton(boolean show) {
        mHome.setAlpha(1.0f);
        if (show)
            mHome.setVisibility(View.VISIBLE);
        else
            mHome.setVisibility(View.GONE);
    }

    public void showAccessButtons(boolean show) {
        int visibleMode = View.GONE;
        if (show)
            visibleMode = View.VISIBLE;

        mAvailability.setVisibility(visibleMode);
        mDeviceType.setVisibility(visibleMode);
        mEmail.setVisibility(visibleMode);
    }

    public void resetAccessButtons() {
        viewAvailabilitySelection.setImageResource(R.drawable.availability_normal);
        viewDeviceTypeSelection.setImageResource(R.drawable.type_normal);
        viewEmailSelection.setImageResource(R.drawable.email_normal);
    }

    public void hideTitleNHome() {
        mAppTitle.setVisibility(View.GONE);
        mHome.setVisibility(View.GONE);
    }

    public interface OnControllersLayoutInteractionListener {
        void onAvailabilityPressed(boolean withAnimation);

        void onDeviceTypePressed(boolean withAnimation);

        void onEmailTypePressed(boolean withAnimation);

        void onHomePressed(boolean withAnimation);

        void onBackPressed(boolean withAnimatino);
    }
}
