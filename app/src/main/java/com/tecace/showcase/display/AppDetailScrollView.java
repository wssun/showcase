package com.tecace.showcase.display;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by swkim on 2015-06-05.
 */
public class AppDetailScrollView extends ScrollView {

    public AppDetailScrollView(Context context) {
        super(context);
    }

    public AppDetailScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppDetailScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private OnScrollViewListener mOnScrollViewListener;

    public void setOnScrollViewListener(OnScrollViewListener l) {
        this.mOnScrollViewListener = l;
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (mOnScrollViewListener != null)
            mOnScrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
        super.onScrollChanged(l, t, oldl, oldt);
    }

    public interface OnScrollViewListener {
        void onScrollChanged(AppDetailScrollView view, int l, int t, int oldl, int oldt);
    }
}
