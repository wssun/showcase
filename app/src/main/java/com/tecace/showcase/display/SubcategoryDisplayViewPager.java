package com.tecace.showcase.display;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tecace.showcase.ShowcaseApplication;
import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.RecoveryData;
import com.tecace.showcase.data.SubcategoryData;
import com.tecace.showcase.util.BitmapUtil;
import com.tecace.showcase.util.CubicBezierInterpolator;

import java.lang.ref.WeakReference;

public class SubcategoryDisplayViewPager extends LinearLayout {
    private static final String TAG = "SubDisplayViewPager";

    public static final int N_PAGE = 5;
    //public int nSubcategory = 5;
    public int nPassion = 10;
    private LinearLayout mRootLayout = null;
    private AppDataManager mAppDataManager = null;
    private AppTableDataManager mAppTableDataManager = null;
    private RecoveryData mRecoveryData = null;

    private int mSubcategoryIndex = 0;
    private int mPassionIndex = 0;
    private OnDisplayPagerEventListener mOnDisplayPagerListener = null;

    public class IndexValues {
        public int mPassion;
        public int mSubcategory;

        public IndexValues(int passion, int subcategory) {
            this.mPassion = passion;
            this.mSubcategory = subcategory;
        }
    };

    public SubcategoryDisplayViewPager(Context context) {
        super(context);
        init();
    }

    public SubcategoryDisplayViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SubcategoryDisplayViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mAppDataManager = ((ShowcaseApplication) getContext().getApplicationContext()).getAppDataManager();

        mRootLayout = new LinearLayout(getContext());
        mRootLayout.setOrientation(LinearLayout.HORIZONTAL);
        mRootLayout.setClickable(true);
        int width = (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width) * N_PAGE)
                + (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * N_PAGE * 2)
                + (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_neighbour_width) * 2);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width,
                ViewGroup.LayoutParams.MATCH_PARENT);
        this.addView(mRootLayout, params);
    }

    public void destroyChildren() {
        for (int i = 0; i < mRootLayout.getChildCount(); i++) {
            if (mRootLayout.getChildAt(i) instanceof SubcategoryDisplayLayout)
                ((SubcategoryDisplayLayout) mRootLayout.getChildAt(i)).free();
        }
        mRootLayout.removeAllViews();
    }

    private SubcategoryDisplayLayout createDisplayLayout(int passionIndex, int subcategoryIndex) {

        Log.d(TAG, "SubcategoryDisplayLayout createDisplayLayout : " + passionIndex +", " + subcategoryIndex);

        SubcategoryData subcategoryData = mAppDataManager.getPassionDataAt(passionIndex).getSubcategoryDataAt(subcategoryIndex);
        int color = mAppDataManager.getPassionDataAt(passionIndex).getPassionColor();
        SubcategoryDisplayLayout subcategoryDisplayLayout = new SubcategoryDisplayLayout(this.getContext());
        subcategoryDisplayLayout.setDisplayData(subcategoryData, subcategoryIndex, color);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width),
                getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_height));
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_top);
        subcategoryDisplayLayout.setOnTouchListener(new OnTouchSubcategoryDisplayPager());
        subcategoryDisplayLayout.setLayoutParams(params);
        return subcategoryDisplayLayout;
    }

    public void setAppItem(int appIndex) {
        if (mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout) {
            ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).setAppItem(appIndex);
        }
    }

    private IndexValues getPassionNSubcategoryIndex(int curPassion, int curSubcategoryIndex) {
        IndexValues newIndex = new IndexValues(curPassion, curSubcategoryIndex);

//        Log.d(TAG, "getPassionNSubcategoryIndex : current (" + curPassion + "," + curSubcategoryIndex + ")");

        if (curSubcategoryIndex < 0) {
            while (curSubcategoryIndex < 0) {
                curPassion = (nPassion + curPassion - 1) % nPassion;

                curSubcategoryIndex += mAppDataManager.getPassionDataAt(curPassion).getSubcategories().size();
            }
        } else if (curSubcategoryIndex >= mAppDataManager.getPassionDataAt(curPassion).getSubcategories().size()) {
            do  {
                curSubcategoryIndex -= mAppDataManager.getPassionDataAt(curPassion).getSubcategories().size();
                curPassion = (curPassion + 1) % nPassion;
            } while  (curSubcategoryIndex >= mAppDataManager.getPassionDataAt(curPassion).getSubcategories().size());
        }

//        Log.d(TAG, "getPassionNSubcategoryIndex : new (" + curPassion + "," + curSubcategoryIndex + ")");
        newIndex.mPassion = curPassion;
        newIndex.mSubcategory = curSubcategoryIndex;

        return newIndex;
    }

    // ===========================================
    // INIT
    // ===========================================
    private void onInitializeLayout() {
        if (mRootLayout.getChildCount() > 0)
            destroyChildren();
        // INIT LEFT MARGIN
        ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin = getLeftMarginOfViewIndex(N_PAGE / 2);
        // ADD DISPLAY LAYOUT
        for (int viewIndex = -N_PAGE / 2; viewIndex <= N_PAGE / 2; viewIndex++) {
            int passionIndex = mPassionIndex;
            int subcategoryIndex = mSubcategoryIndex + viewIndex;
//            // ROTATION INDEX
//            if (subcategoryIndex < 0) {
//                passionIndex = passionIndex - 1;
//                if (passionIndex < 0)
//                    passionIndex = nPassion + passionIndex;
//                subcategoryIndex = mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() + subcategoryIndex;
//
//                if (subcategoryIndex < 0) {
//                    passionIndex = passionIndex - 1;
//                    if (passionIndex < 0)
//                        passionIndex = nPassion + passionIndex;
//                    subcategoryIndex = mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() + subcategoryIndex;
//
//                    if (subcategoryIndex < 0) {
//                        passionIndex = passionIndex - 1;
//                        if (passionIndex < 0)
//                            passionIndex = nPassion + passionIndex;
//                        subcategoryIndex = mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() + subcategoryIndex;
//                    }
//                }
//            } else if (subcategoryIndex >= mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size()) {
//                if (mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() == 1)
//                    subcategoryIndex = 0;
//                else
//                    subcategoryIndex = subcategoryIndex % mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size();
//                passionIndex = passionIndex + 1;
//                if (passionIndex >= nPassion)
//                    passionIndex = passionIndex % nPassion;
//
//                if (subcategoryIndex >= mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size()) {
//                    if (mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() == 1)
//                        subcategoryIndex = 0;
//                    else
//                        subcategoryIndex = subcategoryIndex % mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size();
//                    passionIndex = passionIndex + 1;
//                    if (passionIndex >= nPassion)
//                        passionIndex = passionIndex % nPassion;
//
//                    if (subcategoryIndex >= mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size()) {
//                        if (mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size() == 1)
//                            subcategoryIndex = 0;
//                        else
//                            subcategoryIndex = subcategoryIndex % mAppDataManager.getPassionDataAt(passionIndex).getSubcategories().size();
//                        passionIndex = passionIndex + 1;
//                        if (passionIndex >= nPassion)
//                            passionIndex = passionIndex % nPassion;
//                    }
//                }
//            }

            Log.d(TAG, "onInitializeLayout createDisplayLayout : " + passionIndex +", " + subcategoryIndex);

            IndexValues newIdx = getPassionNSubcategoryIndex(passionIndex, subcategoryIndex);
            passionIndex = newIdx.mPassion;
            subcategoryIndex = newIdx.mSubcategory;

            SubcategoryDisplayLayout subcategoryDisplayLayout = createDisplayLayout(passionIndex, subcategoryIndex);
            if (viewIndex == 0) {
                subcategoryDisplayLayout.setCover(false);
            } else {
                subcategoryDisplayLayout.setCover(true);
                if (viewIndex < 0) {
                    subcategoryDisplayLayout.setCoverShadow(R.drawable.cover_left_shadow);
                } else {
                    subcategoryDisplayLayout.setCoverShadow(R.drawable.cover_right_shadow);
                }
            }
            mRootLayout.addView(subcategoryDisplayLayout);
        }
        mOnTouchListener = new OnTouchSubcategoryDisplayPager();
        mRootLayout.setOnTouchListener(mOnTouchListener);
    }

    private OnTouchSubcategoryDisplayPager mOnTouchListener = null;

    private class CreateDisplayLayoutAsync extends AsyncTask<Integer, Void, SubcategoryDisplayLayout> {
        private WeakReference<LinearLayout> parentReference;

        public CreateDisplayLayoutAsync(LinearLayout parent) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            parentReference = new WeakReference<>(parent);
        }

        @Override
        protected SubcategoryDisplayLayout doInBackground(Integer... params) {
            SubcategoryDisplayLayout view = createDisplayLayout(params[1], params[2]);
            view.setTag(params[0]);
            view.setCover(true);
            if (params[0] < 0) {
                view.setCoverShadow(R.drawable.cover_left_shadow);
            } else {
                view.setCoverShadow(R.drawable.cover_right_shadow);
            }
            return view;
        }

        protected void onPostExecute(SubcategoryDisplayLayout view) {
            if (parentReference != null && view != null) {
                final LinearLayout parent = parentReference.get();
                int nChild = parent.getChildCount();
                for (int i = 0; i < nChild; i++) {
                    if (!(parent.getChildAt(i) instanceof SubcategoryDisplayLayout)) {
                        if (parent.getChildAt(i).getTag() == view.getTag()) {
                            // REMOVE FAKE VIEW
                            final RelativeLayout fakeView = (RelativeLayout) parent.getChildAt(i);
                            // RECYCLE BITMAP
                            while( fakeView.getChildCount() > 0) {
                                if (fakeView.getChildAt(0) instanceof ImageView) {
                                    BitmapUtil.recyleBitmap((ImageView) fakeView.getChildAt(0));
                                    fakeView.removeViewAt(0);
                                }
                            }
                            if (mRootLayout.getChildAt(N_PAGE / 2).equals(view)) {
                                view.setCover(false);
                            }
                            parent.removeView(parent.getChildAt(i));
                            parent.addView(view, i);
                            view.setAppItem(0);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void setOnDisplayPagerEventListener(OnDisplayPagerEventListener listener) {
        mOnDisplayPagerListener = listener;
    }

    public void setAppTableDataManager(AppTableDataManager appTableDataManager) {
        mAppTableDataManager = appTableDataManager;
        nPassion = mAppDataManager.getNumberOfPassions();
    }

    public void setRecoveryData(RecoveryData recoveryData) {
        mRecoveryData = recoveryData;
    }

    public RecoveryData getRecoveryData() {
        return mRecoveryData;
    }

    public void setCurrentPage(int passionIndex, int subcategoryIndex) {
        mPassionIndex = passionIndex;
        mSubcategoryIndex = subcategoryIndex;

        mRecoveryData.setRecoveryPassionIndex(passionIndex);
        mRecoveryData.setRecoverySubcategoryIndex(subcategoryIndex);
        onInitializeLayout();
    }

    public void setCurrentNeighbourPage(int passionIndex, int subcategoryIndex) {
        // ADD DISPLAY LAYOUT
        for (int viewIndex = -N_PAGE / 2; viewIndex <= N_PAGE / 2; viewIndex++) {
            if (viewIndex == 0) {
                continue;
            }
            int passion = mPassionIndex;
            int subcategory = mSubcategoryIndex + viewIndex;

//            Log.d(TAG, "setCurrentNeighbourPage ADD DISPLAY LAYOUT : " + viewIndex + "//" + passion +", " + subcategory);

//            // ROTATION INDEX
//            if (subcategory < 0) {
//                passion = passion - 1;
//                if (passion < 0)
//                    passion = nPassion + passion;
//
//                subcategory = mAppDataManager.getPassionDataAt(passion).getSubcategories().size() + subcategory;
//                if (subcategory < 0) {
//                    passion = passion - 1;
//                    if (passion < 0)
//                        passion = nPassion + passion;
//
//                    subcategory = mAppDataManager.getPassionDataAt(passion).getSubcategories().size() + subcategory;
//                    if (subcategory < 0) {
//                        passion = passion - 1;
//                        if (passion < 0)
//                            passion = nPassion + passion;
//
//                        subcategory = mAppDataManager.getPassionDataAt(passion).getSubcategories().size() + subcategory;
//                    }
//                }
//            } else if (subcategory >= mAppDataManager.getPassionDataAt(passion).getSubcategories().size()) {
//                subcategory = subcategory % mAppDataManager.getPassionDataAt(passion).getSubcategories().size();
//                passion = passion + 1;
//                if (passion >= nPassion)
//                    passion = passion % nPassion;
//
//                if (subcategory >= mAppDataManager.getPassionDataAt(passion).getSubcategories().size()) {
//                    subcategory = subcategory % mAppDataManager.getPassionDataAt(passion).getSubcategories().size();
//                    passion = passion + 1;
//                    if (passion >= nPassion)
//                        passion = passion % nPassion;
//
//                    if (subcategory >= mAppDataManager.getPassionDataAt(passion).getSubcategories().size()) {
//                        subcategory = subcategory % mAppDataManager.getPassionDataAt(passion).getSubcategories().size();
//                        passion = passion + 1;
//                        if (passion >= nPassion)
//                            passion = passion % nPassion;
//
//                    }
//                }
//            }
            IndexValues newIdx = getPassionNSubcategoryIndex(passion, subcategory);
            passion = newIdx.mPassion;
            subcategory = newIdx.mSubcategory;

//            Log.d(TAG, "ADD VIEWS : " + viewIndex + "//" + passion +", " + subcategory);
            CreateDisplayLayoutAsync task = new CreateDisplayLayoutAsync(mRootLayout);
            task.execute(viewIndex, passion, subcategory);
        }
    }


    public void setCurrentCenterPage(int passionIndex, int subcategoryIndex) {
        mPassionIndex = passionIndex;
        mSubcategoryIndex = subcategoryIndex;
        mRecoveryData.setRecoveryPassionIndex(passionIndex);
        mRecoveryData.setRecoverySubcategoryIndex(subcategoryIndex);
        //onInitializeLayout();
        mRootLayout.removeAllViews();
        ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin = getLeftMarginOfViewIndex(N_PAGE / 2);
        // ADD DISPLAY LAYOUT
        for (int viewIndex = -N_PAGE / 2; viewIndex <= N_PAGE / 2; viewIndex++) {
            int passion = mPassionIndex;
            int subcategory = mSubcategoryIndex + viewIndex;
//            // ROTATION INDEX
//            if (subcategory < 0) {
//                passion = passion - 1;
//                if (passion < 0)
//                    passion = nPassion + passion;
//                subcategory = mAppDataManager.getPassionDataAt(passion).getSubcategories().size() + subcategory;
//            } else if (subcategory >= mAppDataManager.getPassionDataAt(passion).getSubcategories().size()) {
//                if (mAppDataManager.getPassionDataAt(passion).getSubcategories().size() == 1)
//                    subcategory = 0;
//                else
//                    subcategory = subcategory % mAppDataManager.getPassionDataAt(passion).getSubcategories().size();
//                passion = passion + 1;
//                if (passion >= nPassion)
//                    passion = passion % nPassion;
//            }
            IndexValues newIdx = getPassionNSubcategoryIndex(passion, subcategory);
            passion = newIdx.mPassion;
            subcategory = newIdx.mSubcategory;

            if (viewIndex == 0) {
                SubcategoryDisplayLayout subcategoryDisplayLayout = createDisplayLayout(passion, subcategory);
                //subcategoryDisplayLayout.setAppItem(0);
                subcategoryDisplayLayout.setCover(false);
                mRootLayout.addView(subcategoryDisplayLayout);
            } else {
                final RelativeLayout fakeView = new RelativeLayout(this.getContext());
                fakeView.setTag(viewIndex);
                fakeView.setClickable(true);
                if (Math.abs(viewIndex) == 1) {
                    ImageView coverImage = new ImageView(this.getContext());
                    coverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    BitmapUtil.setBitmap(coverImage, mAppDataManager.getPassionDataAt(passion).getSubcategoryDataAt(subcategory).getPreviewImage2());
                    fakeView.addView(coverImage, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                    ImageView shadowImage = new ImageView(this.getContext());
                    if (viewIndex < 0)   // LEFT SIDE
                        shadowImage.setImageResource(R.drawable.cover_left_shadow);
                    else
                        shadowImage.setImageResource(R.drawable.cover_right_shadow);
                    fakeView.addView(shadowImage, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width),
                        getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_height)
                                - getResources().getDimensionPixelSize(R.dimen.subcategory_display_subtitle_height));
                params.leftMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
                params.topMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_top)
                        + getResources().getDimensionPixelSize(R.dimen.subcategory_display_subtitle_height);
                mRootLayout.addView(fakeView, params);

            }
        }
        mRootLayout.requestLayout();
        mRootLayout.setOnTouchListener(new OnTouchSubcategoryDisplayPager());
    }

    private void updateCoverOfChildren(int centerIndex, int targetIndex) {
        for (int i = 0; i < mRootLayout.getChildCount(); i++) {
            if (mRootLayout.getChildAt(i) instanceof SubcategoryDisplayLayout)
                ((SubcategoryDisplayLayout) mRootLayout.getChildAt(i)).updateCover(centerIndex, targetIndex);
        }
    }

    // ===========================================
    // ON TOUCH LISTENER
    // ===========================================
    private int mOldLeftMargin = 0;
    private float mOldDragX = 0, mOldDragY = 0, mPrevDragX = 0f, mVelocity;

    public int getLeftMargin() {
        return ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;
    }

    public void setManualActionDown(float x, float y) {
        mOldLeftMargin = ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;
        mOldDragX = mPrevDragX = x;
        mOldDragY = y;
    }

    public void forwardEvent2SubDispPager(MotionEvent event) {
        mOldDragX = event.getX();
        mOldDragY = event.getY();
        this.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !(mRootLayout.getChildCount() > 0);
    }

    private class OnTouchSubcategoryDisplayPager implements OnTouchListener {
        private static final float SPEED_LIMIT = 20f;
        private static final float CLICK_LIMIT = 10f;
        private boolean isCovering = false;
        private long animatorCancelTime = 0;
        private boolean isDownHere = false;

        private float dx = 0;
        private long currentTimeMillis = 0;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (!(mRootLayout.getChildCount() > 0)) return true;
            if (((FullscreenActivity) getContext()).isSwitchingAnimationRunning()) return true;
//            if (isMovingWithAnimation()) return true;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                    ((MainActivity) view.getContext()).stopAutoResetHandler();
                    ((FullscreenActivity) getContext()).startAutoResetHandler();
                    if (mTranslateAnimation != null && (mTranslateAnimation.isRunning() || mTranslateAnimation.isStarted())) {
//                        animatorCancelTime = mTranslateAnimation.getCurrentPlayTime();
//                        mTranslateAnimation.removeAllListeners();
//                        mTranslateAnimation.cancel();
                        return true;
                    }
                    mOldLeftMargin = ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;

                    isDownHere = true;
                    mOldDragX = mPrevDragX = event.getRawX();
                    mOldDragY = event.getRawY();
                    isCovering = false;

                    dx = event.getRawX() - ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mPrevDragX == event.getRawX()) return false;
                    if (!isDownHere) {
//                        ((MainActivity) view.getContext()).stopAutoResetHandler();
                        ((FullscreenActivity) getContext()).startAutoResetHandler();
                        if (mTranslateAnimation != null && (mTranslateAnimation.isRunning() || mTranslateAnimation.isStarted())) {
//                            animatorCancelTime = mTranslateAnimation.getCurrentPlayTime();
//                            mTranslateAnimation.removeAllListeners();
//                            mTranslateAnimation.cancel();
                            return true;
                        }
                        mOldLeftMargin = ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;

                        isDownHere = true;
                        isCovering = false;
                        mOldDragX = mPrevDragX = event.getRawX();
                        mOldDragY = event.getRawY();

                        dx = event.getRawX() - ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin;
                    }
                    mVelocity = (event.getRawX() - mPrevDragX);
//                    mPrevDragX = event.getRawX();

                    // MOVE VIEW
                    float dX = event.getRawX() - mOldDragX;
                    if (Math.abs(dX) >= 5f && !isCovering) {
                        mOldDragX = event.getRawX();
                        mOldDragY = event.getRawY();
                        dX = event.getRawX() - mOldDragX;
                        isCovering = true;
                        animatorCancelTime = 0;
                    }
//                    if (Math.abs(dX) >= 30f && !isCovering) {
//                        mOldDragX = event.getRawX();
//                        mOldDragY = event.getRawY();
//                        dX = event.getRawX() - mOldDragX;
//                        isCovering = true;
//                        animatorCancelTime = 0;
//                    }
//                    else if (Math.abs(dX) < 5f && !isCovering) {
//                        return false;
//                    }



//                    ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin = (mOldLeftMargin + (int) dX);
//                    mRootLayout.requestLayout();
                    final int startX = Math.round(mPrevDragX);
                    final int endX = Math.round(event.getRawX());
                    new Runnable() {
                        @Override
                        public void run() {
                            for(int i=0; i!=(endX-startX);) {
                                ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin = Math.round(startX+i-dx);
                                mRootLayout.requestLayout();
                                i = (Math.signum(endX-startX) == 1) ? i+1 : i-1;
                            }
                        }
                    }.run();
                    mPrevDragX = event.getRawX();


                    // MOVE PASSION
                    int referenceMargin = getLeftMarginOfViewIndex(N_PAGE / 2);
                    int targetIndex = mPassionIndex;
                    float ratio = 0f;

                    if (mSubcategoryIndex == 0 && (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin > referenceMargin)) {
                        int referenceWidth = (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                                + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2);
                        ratio =
                                (float) (Math.abs(referenceMargin - ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin))
                                        / (float) referenceWidth;
                        targetIndex = targetIndex - 1;
                    } else if (mSubcategoryIndex == (mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size() - 1)
                            && (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin < referenceMargin)) {
                        int referenceWidth = (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                                + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2);
                        ratio =
                                (float) (Math.abs(referenceMargin - ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin))
                                        / (float) referenceWidth;
                        targetIndex = targetIndex + 1;
                    }
                    if (targetIndex < 0)
                        targetIndex = mAppDataManager.getNumberOfPassions() + targetIndex;
                    else if (targetIndex >= mAppDataManager.getNumberOfPassions())
                        targetIndex = targetIndex % mAppDataManager.getNumberOfPassions();
                    if (mOnDisplayPagerListener != null && (mPassionIndex != targetIndex))
                        mOnDisplayPagerListener.onDisplayMovingNeighbourPassion(targetIndex, ratio);

                    // UPDATE COVER
                    int targetSubcategoryIndex = mSubcategoryIndex;
                    if (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin < referenceMargin)
                        targetSubcategoryIndex = targetSubcategoryIndex + 1;
                    else if (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin > referenceMargin)
                        targetSubcategoryIndex = targetSubcategoryIndex - 1;
                    if (targetSubcategoryIndex >= mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size()) {
                        if (mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size() == 1)
                            targetSubcategoryIndex = 0;
                        else
                            targetSubcategoryIndex = targetSubcategoryIndex % mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size();
                    } else if (targetSubcategoryIndex < 0)
                        targetSubcategoryIndex = mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size() + targetSubcategoryIndex;
                    updateCoverOfChildren(mSubcategoryIndex, targetSubcategoryIndex);
                    break;
                case MotionEvent.ACTION_UP:
                    isDownHere = false;
                    if (animatorCancelTime != 0) {
                        startNextPageAnimation(DIRECTION_NONE);
                    } else if (Math.abs(event.getRawX() - mOldDragX) < CLICK_LIMIT && Math.abs(event.getRawY() - mOldDragY) < CLICK_LIMIT && !isCovering) {
                        if (view.equals(mRootLayout.getChildAt(N_PAGE / 2 - 1))) {
                            startAnimation((N_PAGE / 2 - 1), getLeftMarginOfViewIndex((N_PAGE / 2 - 1)));
//                            mRootLayout.getChildAt(N_PAGE / 2 - 1).playSoundEffect(SoundEffectConstants.CLICK);
                        } else if (view.equals(mRootLayout.getChildAt(N_PAGE / 2 + 1))) {
                            startAnimation((N_PAGE / 2 + 1), getLeftMarginOfViewIndex((N_PAGE / 2 + 1)));
//                            mRootLayout.getChildAt(N_PAGE / 2 + 1).playSoundEffect(SoundEffectConstants.CLICK);
                        } else if (!(view instanceof SubcategoryDisplayLayout) && !(view instanceof RelativeLayout)) {
                            // IF VIEW IS NOT THE SUBCATEGORY DISPLAY LAYOUT AND FAKEVIEW
                            if (animatorCancelTime != 0) {
                                mTranslateAnimation.setCurrentPlayTime(animatorCancelTime);
                                mTranslateAnimation.start();
                            } else if (mOnDisplayPagerListener != null) {
                                mOnDisplayPagerListener.onDisplayBackgroundTapped();
                            }
                        }
                    } else if (isCovering) {
                        int moveX = ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin - mOldLeftMargin;
                        int destinationIndex = N_PAGE / 2;
                        if (mVelocity < -SPEED_LIMIT)
                            destinationIndex = destinationIndex + 1;
                        else if (mVelocity > SPEED_LIMIT)
                            destinationIndex = destinationIndex - 1;
                        else if (moveX < -getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width) / 3)
                            destinationIndex = destinationIndex + 1;
                        else if (moveX > getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width) / 3)
                            destinationIndex = destinationIndex - 1;
                        startAnimation(destinationIndex, getLeftMarginOfViewIndex(destinationIndex));
                        isCovering = false;
                    }
                    animatorCancelTime = 0;

                    break;
            }
            return true;
        }
    }

    private int getLeftMarginOfViewIndex(int viewIndex) {
        return -(viewIndex * (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2))
                + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_neighbour_width)
                + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
    }

    public boolean isCentered() {
        return !(isMovingWithAnimation()
                || ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin == getLeftMarginOfViewIndex((N_PAGE / 2)));
    }

    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_LEFT = 2;
    public static final int DIRECTION_NONE = 0;

    public void startNextPageAnimation(int direction) {
        switch (direction) {
            case DIRECTION_RIGHT:
                startAnimation((N_PAGE / 2 + 1), getLeftMarginOfViewIndex((N_PAGE / 2 + 1)));
                break;
            case DIRECTION_LEFT:
                startAnimation((N_PAGE / 2 - 1), getLeftMarginOfViewIndex((N_PAGE / 2 - 1)));
                break;
            case DIRECTION_NONE:
                startAnimation((N_PAGE / 2), getLeftMarginOfViewIndex((N_PAGE / 2)));
                break;
        }
    }

    // ===========================================
    // FLING ANIMATION
    // ===========================================
    ValueAnimator mTranslateAnimation = null;
    boolean mUpdateRootLeftMargin = false;
    private void startAnimation(final int destinationIndex, final int destination) {
        final int referenceMargin = getLeftMarginOfViewIndex(N_PAGE / 2);
        final int movedIndex = N_PAGE / 2 - destinationIndex;   // INDEX FOR CHILD VIEW
        int targetSubcategoryIndex = mSubcategoryIndex - movedIndex;
        int targetPassionIndex = mPassionIndex;
//        if (targetSubcategoryIndex < 0) {
//            // PREVIOUS PASSION
//            targetPassionIndex = targetPassionIndex - 1;
//            if (targetPassionIndex < 0)
//                targetPassionIndex = nPassion + targetPassionIndex;
//            targetSubcategoryIndex = mAppDataManager.getPassionDataAt(targetPassionIndex).getSubcategories().size() - 1;
//
//        } else if (targetSubcategoryIndex >= mAppDataManager.getPassionDataAt(targetPassionIndex).getSubcategories().size()) {
//            // NEXT PASSION
////            if (mAppDataManager.getPassionDataAt(targetPassionIndex).getSubcategories().size() == 1)
////                targetSubcategoryIndex = 0;
////            else
////                targetSubcategoryIndex = targetSubcategoryIndex % mAppDataManager.getPassionDataAt(targetPassionIndex).getSubcategories().size();
//            targetSubcategoryIndex = 0;
//            targetPassionIndex = targetPassionIndex + 1;
//            if (targetPassionIndex >= nPassion)
//                targetPassionIndex = targetPassionIndex % nPassion;
//        }
        IndexValues newIdx = getPassionNSubcategoryIndex(targetPassionIndex, targetSubcategoryIndex);
        targetPassionIndex = newIdx.mPassion;
        targetSubcategoryIndex = newIdx.mSubcategory;

        final int finalPassionIndex = targetPassionIndex;
        final int finalSubcategoryIndex = targetSubcategoryIndex;
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mRootLayout.getLayoutParams();

//        mTranslateAnimation = ValueAnimator.ofInt(params.leftMargin, destination);
        mTranslateAnimation = ValueAnimator.ofInt(params.leftMargin, destination);
        mTranslateAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                mRootLayout.requestLayout();
                updateCoverOfChildren(mSubcategoryIndex, finalSubcategoryIndex);
                // SEND ARRIVING INFORMATION WHEN CHANGE PASSION INDEX
                int referenceWidth = (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                        + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2);
                float ratio =
                        (float) (Math.abs(referenceMargin - ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin))
                                / (float) referenceWidth;
                if (finalPassionIndex == mPassionIndex) // BACK TO CENTER
                    ratio = 1f - ratio;

                //#198 App detail view를 좌우로 이동시킬때 passion bar가 움직이는 현상 수정함
                if (mOnDisplayPagerListener != null && (mPassionIndex != finalPassionIndex)) {
                    if (mSubcategoryIndex == 0 && (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin > referenceMargin)) {
                        mOnDisplayPagerListener.onDisplayMovingNeighbourPassion(finalPassionIndex, ratio);
                    } else if (mSubcategoryIndex == (mAppDataManager.getPassionDataAt(mPassionIndex).getSubcategories().size() - 1)
                            && (((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin < referenceMargin)) {
                        mOnDisplayPagerListener.onDisplayMovingNeighbourPassion(finalPassionIndex, ratio);
                    }
                }
//                if (mOnDisplayPagerListener != null && (mPassionIndex != finalPassionIndex || destinationIndex == (N_PAGE / 2))) {
//                        mOnDisplayPagerListener.onDisplayMovingNeighbourPassion(finalPassionIndex, ratio);
//                }
            }
        });

        mTranslateAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                // M4s intro anim
                if (mPassionIndex == mAppDataManager.getSamsungIndex()) {
                    Animator displayM4SAnimator = ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateM4SDismissAnimator();
                    displayM4SAnimator.setDuration(300);

                    displayM4SAnimator.start();
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mRootLayout.getChildCount() < N_PAGE)
                    return;

                if (movedIndex < 0) {
                    // REMOVE LEFT END VIEW
                    if (mRootLayout.getChildAt(0) instanceof SubcategoryDisplayLayout)
                        ((SubcategoryDisplayLayout) mRootLayout.getChildAt(0)).free();
                    mRootLayout.removeView(mRootLayout.getChildAt(0));

                    // ADD RIGHT END VIEW
                    int addingSubcategoryIndex = mSubcategoryIndex + (N_PAGE / 2 + 1);
                    int addingPassionIndex = mPassionIndex;
//                    if (addingSubcategoryIndex >= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size()) {
//                        addingSubcategoryIndex -= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//                        addingPassionIndex = addingPassionIndex + 1;
//                        if (addingPassionIndex >= nPassion)
//                            addingPassionIndex = addingPassionIndex % nPassion;
//
//                        if (addingSubcategoryIndex >= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size()) {
//                            addingSubcategoryIndex -= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//                            addingPassionIndex = addingPassionIndex + 1;
//                            if (addingPassionIndex >= nPassion)
//                                addingPassionIndex = addingPassionIndex % nPassion;
//
//                            if (addingSubcategoryIndex >= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size()) {
//                                addingSubcategoryIndex -= mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//                                addingPassionIndex = addingPassionIndex + 1;
//                                if (addingPassionIndex >= nPassion)
//                                    addingPassionIndex = addingPassionIndex % nPassion;
//                            }
//                        }
//                    }
                    //Log.d(TAG, "ADD RIGHT : " + addingPassionIndex + ", " + addingSubcategoryIndex);

                    IndexValues newIdx = getPassionNSubcategoryIndex(addingPassionIndex, addingSubcategoryIndex);
                    addingPassionIndex = newIdx.mPassion;
                    addingSubcategoryIndex = newIdx.mSubcategory;

                    SubcategoryDisplayLayout subcategoryDisplayLayout = createDisplayLayout(addingPassionIndex, addingSubcategoryIndex);
                    mRootLayout.addView(subcategoryDisplayLayout);
                    subcategoryDisplayLayout.setAppItem(0);
                    subcategoryDisplayLayout.setCover(true);
                    subcategoryDisplayLayout.setCoverShadow(R.drawable.cover_right_shadow);

                    ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin +=
                            (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                                    + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2);
                    ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin += 1;

                    mSubcategoryIndex = finalSubcategoryIndex;
                    mPassionIndex = finalPassionIndex;
                } else if (movedIndex > 0) {
                    // REMOVE RIGHT END VIEW
                    if (mRootLayout.getChildAt(mRootLayout.getChildCount() - 1) instanceof SubcategoryDisplayLayout)
                        ((SubcategoryDisplayLayout) mRootLayout.getChildAt(mRootLayout.getChildCount() - 1)).free();
                    mRootLayout.removeView(mRootLayout.getChildAt(mRootLayout.getChildCount() - 1));

                    // ADD LEFT END VIEW
                    int addingSubcategoryIndex = mSubcategoryIndex - (N_PAGE / 2 + 1);
                    int addingPassionIndex = mPassionIndex;
//                    if (addingSubcategoryIndex < 0) {
//                        addingPassionIndex = addingPassionIndex - 1;
//                        if (addingPassionIndex < 0)
//                            addingPassionIndex = nPassion + addingPassionIndex;
//
//                        addingSubcategoryIndex += mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//
//                        if (addingSubcategoryIndex < 0) {
//                            addingPassionIndex = addingPassionIndex - 1;
//                            if (addingPassionIndex < 0)
//                                addingPassionIndex = nPassion + addingPassionIndex;
//
//                            addingSubcategoryIndex += mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//
//                            if (addingSubcategoryIndex < 0) {
//                                addingPassionIndex = addingPassionIndex - 1;
//                                if (addingPassionIndex < 0)
//                                    addingPassionIndex = nPassion + addingPassionIndex;
//
//                                addingSubcategoryIndex += mAppDataManager.getPassionDataAt(addingPassionIndex).getSubcategories().size();
//                            }
//                        }
//                    }
                    //Log.d(TAG, "ADD LEFT : " + addingPassionIndex + ", " + addingSubcategoryIndex);

                    IndexValues newIdx = getPassionNSubcategoryIndex(addingPassionIndex, addingSubcategoryIndex);
                    addingPassionIndex = newIdx.mPassion;
                    addingSubcategoryIndex = newIdx.mSubcategory;

                    SubcategoryDisplayLayout subcategoryDisplayLayout = createDisplayLayout(addingPassionIndex, addingSubcategoryIndex);
                    mRootLayout.addView(subcategoryDisplayLayout, 0);
                    subcategoryDisplayLayout.setCover(true);
                    subcategoryDisplayLayout.setCoverShadow(R.drawable.cover_left_shadow);
                    subcategoryDisplayLayout.setAppItem(0);

                    ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin -=
                            (getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width)
                                    + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2);
                    ((LinearLayout.LayoutParams) mRootLayout.getLayoutParams()).leftMargin -= 1;

                    mSubcategoryIndex = finalSubcategoryIndex;
                    mPassionIndex = finalPassionIndex;
                }
//                else {
//                    mOnDisplayPagerListener.onDisplayArrived(finalPassionIndex);
//                }

                if (mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout)
                    ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).setCover(false);

                // M4s intro anim
                if (mPassionIndex == mAppDataManager.getSamsungIndex()) {
                    Animator displayM4SAnimator = ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateM4SShowAnimator();
                    if (displayM4SAnimator != null) {
                        displayM4SAnimator.setDuration(300);

                        displayM4SAnimator.start();
                    }
                }

                mRecoveryData.setRecoveryPassionIndex(mPassionIndex);
                mRecoveryData.setRecoverySubcategoryIndex(mSubcategoryIndex);
                ((FullscreenActivity) mRootLayout.getContext()).startAutoResetHandler();
            }
        });

        if (params.leftMargin == getLeftMarginOfViewIndex(N_PAGE / 2))
            mTranslateAnimation.setDuration(1000);
        else {
            mTranslateAnimation.setInterpolator(new DecelerateInterpolator());
            mTranslateAnimation.setDuration(600);
        }
        mTranslateAnimation.start();
    }

    public boolean isMovingWithAnimation() {
        if (mTranslateAnimation == null)
            return false;
        else if (mTranslateAnimation.isRunning() || mTranslateAnimation.isStarted())
            return true;
        return false;
    }

//    public Animator generateTitleDismissAnimator() {
//        if (mRootLayout.getChildCount() > (N_PAGE / 2) && mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout)
//            return ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateTitleDismissAnimator();
//        return null;
//    }
//
//    public Animator generateTitleShowAnimator() {
//        if (mRootLayout.getChildCount() > (N_PAGE / 2) && mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout) {
//            return ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateTitleShowAnimator();
//        }
//        return null;
//    }

    public Animator generateM4SShowAnimator() {
        if (mRootLayout.getChildCount() > (N_PAGE / 2) && mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout) {
            return ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateM4SShowAnimator();
        }
        return null;
    }

    public Animator generateM4SDismissAnimator() {
        if (mRootLayout.getChildCount() > (N_PAGE / 2) && mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout) {
            return ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).generateM4SDismissAnimator();
        }
        return null;
    }

    public Animator generateDisplayToLoopAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        int previewMarginTop = getResources().getDimensionPixelSize(R.dimen.subcategory_preview_margin_top);
        int previewHeight = getResources().getDimensionPixelSize(R.dimen.subcategory_preview_image_height)
                + getResources().getDimensionPixelSize(R.dimen.subcategory_preview_bar_height);
        int displayHeight = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_height);
        int displayMarginTop = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_top);

        final SubcategoryDisplayViewPager pager = this;
        ObjectAnimator displayAlpha = ObjectAnimator.ofFloat(this, "alpha", 1f, 0f);

        displayAlpha.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout)
                    ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).setTitleAlpha(0f);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                pager.setVisibility(GONE);
                pager.destroyChildren();
            }
        });
        ObjectAnimator displayScale = ObjectAnimator.ofPropertyValuesHolder(this,
                PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.5f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.5f));
        displayScale.setInterpolator(dialogSally);

        ObjectAnimator displayMove = ObjectAnimator.ofPropertyValuesHolder(this,
                PropertyValuesHolder.ofFloat("Y", 0, ((previewMarginTop + previewHeight / 2) - (displayMarginTop + displayHeight / 2))));

        animatorSet.play(displayAlpha).with(displayScale).with(displayMove);

        return animatorSet;
    }

    public Animator generateLoopToDisplayAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();

        int previewMarginTop = getResources().getDimensionPixelSize(R.dimen.subcategory_preview_margin_top);
        int previewHeight = getResources().getDimensionPixelSize(R.dimen.subcategory_preview_image_height)
                + getResources().getDimensionPixelSize(R.dimen.subcategory_preview_bar_height);
        int displayHeight = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_height);
        int displayMarginTop = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_top);

        ValueAnimator displayAlpha = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
        final SubcategoryDisplayViewPager parent = this;
        displayAlpha.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                parent.setVisibility(VISIBLE);
            }
        });
        ObjectAnimator displayScale = ObjectAnimator.ofPropertyValuesHolder(this,
                PropertyValuesHolder.ofFloat("scaleX", 0.5f, 1f),
                PropertyValuesHolder.ofFloat("scaleY", 0.5f, 1f));
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        displayScale.setInterpolator(dialogSally);
        ObjectAnimator displayMove = ObjectAnimator.ofPropertyValuesHolder(this,
                PropertyValuesHolder.ofFloat("Y", ((previewMarginTop + previewHeight / 2) - (displayMarginTop + displayHeight / 2)), 0));
        displayMove.setInterpolator(dialogSally);
        animatorSet.play(displayAlpha).with(displayScale).with(displayMove);
        if (mRootLayout.getChildAt(N_PAGE / 2) instanceof SubcategoryDisplayLayout)
            ((SubcategoryDisplayLayout) mRootLayout.getChildAt(N_PAGE / 2)).setTitleAlpha(0f);
        return animatorSet;
    }

    public int getDisplayCount() {
        return mRootLayout.getChildCount();
    }

    public interface OnDisplayPagerEventListener {
        void onDisplayArrived(int finalPassionIndex);

        void onDisplayMovingNeighbourPassion(int toPassionIndex, float ratio);

        void onDisplayBackgroundTapped();

//        void onChangeDoingYoutubeDisplay(boolean value);
    }
}
