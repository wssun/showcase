package com.tecace.showcase.display;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/**
 * Created by icanmobile on 9/25/15.
 */
public class AppScreenshotHScrollView extends HorizontalScrollView {
    public AppScreenshotHScrollView(Context context) {
        super(context);
    }

    public AppScreenshotHScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppScreenshotHScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private OnHScrollViewListener mOnHScrollViewListener;

    public void setOnScrollViewListener(OnHScrollViewListener l) {
        this.mOnHScrollViewListener = l;
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (mOnHScrollViewListener != null)
            mOnHScrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
        super.onScrollChanged(l, t, oldl, oldt);
    }

    public interface OnHScrollViewListener {
        void onScrollChanged(AppScreenshotHScrollView view, int l, int t, int oldl, int oldt);
    }


    private int MAX_SCROLL_SPEED = 4000;
    @Override
    public void fling(int velocityX)
    {
        velocityX = (int) ((Math.min(Math.abs(velocityX), MAX_SCROLL_SPEED) ) * Math.signum(velocityX));
        super.fling(velocityX);
    }
}
