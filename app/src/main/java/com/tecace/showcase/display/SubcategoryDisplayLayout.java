package com.tecace.showcase.display;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;
import com.tecace.showcase.ShowcaseApplication;
import com.tecace.showcase.data.SubPageData;
import com.tecace.showcase.data.SubcategoryData;
import com.tecace.showcase.util.BitmapUtil;
import com.tecace.showcase.util.CustomTypeFaceSpan;
import com.tecace.showcase.util.FontTypeface;
import com.tecace.showcase.util.MixPanelUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.TimerTask;

public class SubcategoryDisplayLayout extends RelativeLayout {
    private final String TAG = "SubDisplayLayout";

    private Context mContext = null;
    private SubcategoryData mSubcategoryData = null;

    private RelativeLayout mRootView = null;
    private ImageView mCoverImageView = null;
    private ImageView mCoverShadowView = null;

    private TextView mSubcategoryTitle = null;
    private ImageView mPartnerIcon = null;

    // Right Pane show/hide
    private static final int DISPLAY_INTRODUCTION = 1004 ;
    private static final int DISPLAY_BENEFITS = 1005 ;
    private static final int DISPLAY_EXPERIENCE = 1006 ;

    private RelativeLayout mPane_Left = null;
    private RelativeLayout mPane_Introduction = null;
    private RelativeLayout mPane_Benefits = null;
    private RelativeLayout mPane_Experience = null;

    private TextView mBtnIntroduction = null;
    private TextView mBtnBenefits = null;
    private TextView mBtnExperience = null;

    private ImageView mDevicePhone = null;
    private ImageView mDeviceTablet = null;
    private ImageView mDeviceS3 = null;

    private ImageView mM4SPhone1 = null;
    private ImageView mM4SPhone2 = null;
    private ImageView mM4SPhone3 = null;
    private ImageView mM4SPhone4 = null;
    private ImageView mM4STitle = null;
    private ImageView mM4SIntro = null;

    private ImageView mIntro_image = null;
    private TextView mIntro_text  = null;
    private ImageView mBenefits_image = null;
    private TextView mBenefits_text  = null;

    private Point mScreenSize = new Point();
    private int mSubcategoryIndex = 0;

    private ValueAnimator mRealSmoothScrollAnim = null;

    // COVER ANIMATION VALUES
    int[] locOnScreen = new int[2];
    private static final int COVER_SHADOW_ANIMATION_REGION = 300;
    private static final int COVER_ANIMATION_REGION = 500;
    private static final int TITLE_ANIMATION_OFFSET = 600;
    private static final int TITLE_ANIMATION_REGION = 1000;
    private static final int COVER_ANIMATION_OPEN_OFFSET = 200;
    private static final int COVER_ANIMATION_CLOSE_OFFSET = 200;
    private static final int TITLE_ANIMATION_CLOSE_OFFSET = 600;
    private int neighbourWidth;
    private int pageWidth;
    private int leftSide;
    private int rightSide;
    private int centerLocation;
    private int leftDistance;
    private int rightDistance;
    private int mShadowResource = R.drawable.cover_right_shadow;

    private DisplayImageOptions options;

    private ArrayList<SubPageData> mSubPagePhoneDataList = new ArrayList<SubPageData>();
    private ArrayList<SubPageData> mSubPageTabDataList = new ArrayList<SubPageData>();
    private ArrayList<SubPageData> mSubPageGearDataList = new ArrayList<SubPageData>();
    private int mCurrentPageIndexPhone = 0;
    private int mCurrentPageIndexTab = 0;
    private int mCurrentPageIndexGear = 0;
    private ImageView mExperience_phone = null;
    private ImageView mExperience_tap = null;
    private ImageView mExperience_tap_portrait = null;
    private ImageView mExperience_gear = null;
    private ImageView mExperience_phone_image = null;
    private ImageView mExperience_tap_image = null;
    private ImageView mExperience_tap_image_portrait = null;
    private ImageView mExperience_gear_image = null;
    private int mCurrentPageIndex = 0;
    private static final int DISPLAY_PHONE = 1007 ;
    private static final int DISPLAY_TAB = 1008 ;
    private static final int DISPLAY_GEAR = 1009 ;

    private ImageView mGhostTouchView = null;

    // SCROLL SHADOW
    private static final int SCROLL_SHADOW_REGION = 100;

//?    private TimerHandler mTimerHandler;
    private static final int NO_INTERACTION_TIMEOUT = 2000;
    private Timer mTimer;
    private RelativeLayout mGhostAnimationContainner = null;
//?    private TextView layout_experience = null;

    public SubcategoryDisplayLayout(Context context) {
        super(context);

        mContext = context;
    }

    public SubcategoryDisplayLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
    }

    public SubcategoryDisplayLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
    }

    public void setDisplayData(SubcategoryData subcategoryData, int index, int color) {
        mSubcategoryData = subcategoryData;
        mSubcategoryIndex = index;

        if(mTimer == null) {
            mTimer = new Timer();
        }
/*
        if(mTimerHandler == null) {
            mTimerHandler = new TimerHandler();

            mTimerHandler.setTimeout(NO_INTERACTION_TIMEOUT);
            mTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
                @Override
                public void onTimeout() {
                    changeViewPage();
                }
            });
        }
*/

        onInitializeLayout();
    }

    class UpdateTimeTask extends TimerTask {
        public void run(){
            Handler timerHandler = mExperience_phone_image.getHandler();
            if (timerHandler != null) {
                timerHandler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(TAG, "UpdateTimeTask changeSubPage");
                        changeSubPage(null, null);

//?                        mBtnExperience.setClickable(true);
//?                        mDevicePhone.setClickable(true);
//?                        mDeviceTablet.setClickable(true);
//?                        mDeviceS3.setClickable(true);

/*
                        switch (mCurrentPageIndex) {
                            case DISPLAY_PHONE:
                                mDevicePhone.setClickable(true);
                                break;
                            case DISPLAY_TAB:
                                mDeviceTablet.setClickable(true);
                                break;
                            case DISPLAY_GEAR:
                                mDeviceS3.setClickable(true);
                                break;
                        }
*/
                    }

                });
            }
        }
    }

    private void refreshLayout (int mode) {
        mBtnIntroduction.setSelected(false);
        mBtnBenefits.setSelected(false);
        mBtnExperience.setSelected(false);

        mPane_Introduction.setVisibility(View.GONE);
        mPane_Benefits.setVisibility(View.GONE);
        mPane_Experience.setVisibility(View.GONE);

        switch (mode) {
            case DISPLAY_INTRODUCTION:
                mBtnIntroduction.setSelected(true);
                mPane_Introduction.setVisibility(View.VISIBLE);

                mDevicePhone.setSelected(false);
                mDeviceTablet.setSelected(false);
                mDeviceS3.setSelected(false);
                break;
            case DISPLAY_BENEFITS:
                mBtnBenefits.setSelected(true);
                mPane_Benefits.setVisibility(View.VISIBLE);

                mDevicePhone.setSelected(false);
                mDeviceTablet.setSelected(false);
                mDeviceS3.setSelected(false);
                break;
            case DISPLAY_EXPERIENCE:
                mBtnExperience.setSelected(true);
                mPane_Experience.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setMargins(View v, int l , int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    private void setOverlayLayoutParam(ArrayList<SubPageData> mImageViewDataList, int mCurrentPageNumber) {

        int supportActions = mImageViewDataList.get(mCurrentPageNumber).getnextAction();

        Log.d(TAG, "setOverlayLayoutParam : " + mCurrentPageNumber + " : " + mImageViewDataList.get(mCurrentPageNumber).getleftTopX() + " : " + supportActions + " : " + mImageViewDataList.size());

        if ((supportActions & SubcategoryData.ACTION_CLICK) > 0) {

//?            mTimerHandler.stop();
//?            mTimerHandler.setOnTimeoutListener(null);

            showTouchView();
            setTouchView(Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopX()), Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopY()));

            mBtnExperience.setClickable(true);
            mDevicePhone.setClickable(true);
            mDeviceTablet.setClickable(true);
            mDeviceS3.setClickable(true);
/*
            switch (mCurrentPageIndex) {
                case DISPLAY_PHONE:
                    mDevicePhone.setClickable(true);
                    break;
                case DISPLAY_TAB:
                    mDeviceTablet.setClickable(true);
                    break;
                case DISPLAY_GEAR:
                    mDeviceS3.setClickable(true);
                    break;
            }
*/
//?        startAutoLoopHandler(0, Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopX()), Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopY()));

        } else if ((supportActions & SubcategoryData.ACTION_TIMER) > 0) {
/*
            mTimerHandler.setTimeout(NO_INTERACTION_TIMEOUT);
            mTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
                @Override
                public void onTimeout() {

                    changeSubPage(null, null);
                }
            });
            mTimerHandler.start();
*/
            hideTouchView();

            if(mCurrentPageNumber < (mImageViewDataList.size() - 1)) {
                Log.d(TAG, "setOverlayLayoutParam1 : " + mCurrentPageNumber + " : " + mImageViewDataList.get(mCurrentPageNumber).getleftTopX() + " : " + supportActions + " : " + mImageViewDataList.size());
                mTimer.schedule(new UpdateTimeTask(), NO_INTERACTION_TIMEOUT);
            } else {
                Log.d(TAG, "setOverlayLayoutParam2 : " + mCurrentPageNumber + " : " + mImageViewDataList.get(mCurrentPageNumber).getleftTopX() + " : " + supportActions + " : " + mImageViewDataList.size());
            }

        } else {

        }


        if(mCurrentPageNumber < (mImageViewDataList.size() - 1)) {

//?            mGhostTouchView.setVisibility(View.VISIBLE);
            mGhostAnimationContainner.setVisibility(View.INVISIBLE);

        } else {

//?            mGhostTouchView.setVisibility(View.INVISIBLE);
            mGhostAnimationContainner.setVisibility(View.VISIBLE);

            mBtnExperience.setClickable(true);
            mDevicePhone.setClickable(true);
            mDeviceTablet.setClickable(true);
            mDeviceS3.setClickable(true);
        }

/*
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getrightBottomX()) - Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopX()),
                Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getrightBottomY()) - Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopY()));

        mOverlay.setLayoutParams(layoutParams);
        mOverlay.setBackgroundColor(0xFFFFFF00);
        setMargins(mOverlay,Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopX()),
                Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getleftTopY()),
                Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getrightBottomX()),
                Integer.parseInt(mImageViewDataList.get(mCurrentPageNumber).getrightBottomY()));
*/
    }

    private void changeSubPagePhone() {
        if(mSubPagePhoneDataList.isEmpty()) {
            return;
        }

        mCurrentPageIndexPhone++;

        Log.d(TAG, "onTouch1" + mCurrentPageIndexPhone + " : " + mSubPagePhoneDataList.size());

        if(mCurrentPageIndexPhone < mSubPagePhoneDataList.size()) {

            Log.d(TAG, "onTouch2" + mCurrentPageIndexPhone + " : " + mSubPagePhoneDataList.size());

//?            BitmapUtil.recyleBitmap(mExperience_phone_image);
            BitmapUtil.setBitmap(mExperience_phone_image, mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getpageView());
            Log.d(TAG, "onTouch3");


//?            mViewPageImage.setImageURI(getImageResourceFile(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getPageView()));

            setOverlayLayoutParam(mSubPagePhoneDataList, mCurrentPageIndexPhone);
//?            Log.d(TAG, "[TA]#### Showing page index " + mCurrentPageIndexPhone);
//?            mTimerHandler.stop();
//?            mTimerHandler.setTimeout(NO_INTERACTION_TIMEOUT);
//?            mTimerHandler.start();
        } else {
//?            Toast.makeText(mContext, " End of Page", Toast.LENGTH_LONG).show();
//?            mTimerHandler.stop();
//?            mTimerHandler.setOnTimeoutListener(null);
            return;
        }
    }

    private void changeSubPageTab() {
        if(mSubPageTabDataList.isEmpty()) {
            return;
        }

        mCurrentPageIndexTab++;

        Log.d(TAG, "onTouch1" + mCurrentPageIndexTab + " : " + mSubPageTabDataList.size());

        if(mCurrentPageIndexTab < mSubPageTabDataList.size()) {

            int supportActions = mSubPageTabDataList.get(mCurrentPageIndexTab).getnextAction();

            Log.d(TAG, "onTouch2" + mCurrentPageIndexTab + " : " + mSubPageTabDataList.size());

            if ((supportActions & SubcategoryData.ACTION_PHONE) > 0) {

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(VISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(VISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

//?                BitmapUtil.recyleBitmap(mExperience_tap_image_portrait);
                BitmapUtil.setBitmap(mExperience_tap_image_portrait, mSubPageTabDataList.get(mCurrentPageIndexTab).getpageView());

            } else {

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(VISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(VISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

//?                BitmapUtil.recyleBitmap(mExperience_tap_image);
                BitmapUtil.setBitmap(mExperience_tap_image, mSubPageTabDataList.get(mCurrentPageIndexTab).getpageView());
            }

                Log.d(TAG, "onTouch3");


//?            mViewPageImage.setImageURI(getImageResourceFile(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getPageView()));

            setOverlayLayoutParam(mSubPageTabDataList, mCurrentPageIndexTab);
//?            Log.d(TAG, "[TA]#### Showing page index " + mCurrentPageIndexPhone);
//?            mTimerHandler.stop();
//?            mTimerHandler.setTimeout(NO_INTERACTION_TIMEOUT);
//?            mTimerHandler.start();
        } else {

            mCurrentPageIndexTab = mSubPageTabDataList.size() - 1;

//?            Toast.makeText(mContext, " End of Page", Toast.LENGTH_LONG).show();
//?            mTimerHandler.stop();
//?            mTimerHandler.setOnTimeoutListener(null);
            return;
        }
    }

    private void changeSubPageGear() {
        if(mSubPageGearDataList.isEmpty()) {
            return;
        }

        mCurrentPageIndexGear++;

        Log.d(TAG, "onTouch1 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

        if(mCurrentPageIndexGear < mSubPageGearDataList.size()) {

            int supportActions = mSubPageGearDataList.get(mCurrentPageIndexGear).getnextAction();

            Log.d(TAG, "onTouch2 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

            if ((supportActions & SubcategoryData.ACTION_PHONE) > 0) {

                Log.d(TAG, "onTouch3 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

                mExperience_phone.setVisibility(VISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(VISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

//?                BitmapUtil.recyleBitmap(mExperience_phone_image);
                BitmapUtil.setBitmap(mExperience_phone_image, mSubPageGearDataList.get(mCurrentPageIndexGear).getpageView());

            } else if ((supportActions & SubcategoryData.ACTION_ALL) > 0) {



            } else {

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(VISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(VISIBLE);

//?                BitmapUtil.recyleBitmap(mExperience_gear_image);
                BitmapUtil.setBitmap(mExperience_gear_image, mSubPageGearDataList.get(mCurrentPageIndexGear).getpageView());

            }

//?            mViewPageImage.setImageURI(getImageResourceFile(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getPageView()));

            setOverlayLayoutParam(mSubPageGearDataList, mCurrentPageIndexGear);
//?            Log.d(TAG, "[TA]#### Showing page index " + mCurrentPageIndexPhone);
//?            mTimerHandler.stop();
//?            mTimerHandler.setTimeout(NO_INTERACTION_TIMEOUT);
//?            mTimerHandler.start();
        } else {

            mCurrentPageIndexGear = mSubPageGearDataList.size() - 1;

//?            Toast.makeText(mContext, " End of Page", Toast.LENGTH_LONG).show();
//?            mTimerHandler.stop();
//?            mTimerHandler.setOnTimeoutListener(null);
            return;
        }
    }

    private boolean isClickedPhone(View v, MotionEvent event) {
        if(mCurrentPageIndexPhone >= mSubPagePhoneDataList.size()) {
            return false;
        }

        Log.d(TAG, "onTouch13");
/*
        float xClickPos = event.getX();
        float YClickPos = event.getY();
        Log.d(TAG, "clicked xPos : " + xClickPos + " clicked yPos : " + YClickPos);

        int leftTopX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopX());
        int leftTopY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopY());
        int rightBottomX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomX());
        int rightBottomY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomY());
*/
        return true;

/*
        if(((xClickPos > leftTopX) && (xClickPos < rightBottomX)) &&
                ((YClickPos > leftTopY) && (xClickPos < rightBottomY))) {
            Log.d(TAG, "[TA]#### tap is inbound ");
            Toast.makeText(mContext, " Tap X: " + xClickPos + " Tap Y: " + YClickPos + "\n" +
                    " Bound X : " + leftTopX + "-" + rightBottomX + "\n" +
                    " Bound Y : " + leftTopY + "-" + rightBottomY, Toast.LENGTH_SHORT).show();

            return true;
        } else {
            return false;
        }
*/
    }

    private boolean isClickedTab(View v, MotionEvent event) {
        if(mCurrentPageIndexTab >= mSubPageTabDataList.size()) {
            return false;
        }

        Log.d(TAG, "onTouch13");
/*
        float xClickPos = event.getX();
        float YClickPos = event.getY();
        Log.d(TAG, "clicked xPos : " + xClickPos + " clicked yPos : " + YClickPos);

        int leftTopX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopX());
        int leftTopY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopY());
        int rightBottomX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomX());
        int rightBottomY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomY());
*/
        return true;

/*
        if(((xClickPos > leftTopX) && (xClickPos < rightBottomX)) &&
                ((YClickPos > leftTopY) && (xClickPos < rightBottomY))) {
            Log.d(TAG, "[TA]#### tap is inbound ");
            Toast.makeText(mContext, " Tap X: " + xClickPos + " Tap Y: " + YClickPos + "\n" +
                    " Bound X : " + leftTopX + "-" + rightBottomX + "\n" +
                    " Bound Y : " + leftTopY + "-" + rightBottomY, Toast.LENGTH_SHORT).show();

            return true;
        } else {
            return false;
        }
*/
    }

    private boolean isClickedGear(View v, MotionEvent event) {
        if(mCurrentPageIndexGear >= mSubPageGearDataList.size()) {
            return false;
        }

        Log.d(TAG, "onTouch13");
/*
        float xClickPos = event.getX();
        float YClickPos = event.getY();
        Log.d(TAG, "clicked xPos : " + xClickPos + " clicked yPos : " + YClickPos);

        int leftTopX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopX());
        int leftTopY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getleftTopY());
        int rightBottomX = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomX());
        int rightBottomY = Integer.parseInt(mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getrightBottomY());
*/
        return true;

/*
        if(((xClickPos > leftTopX) && (xClickPos < rightBottomX)) &&
                ((YClickPos > leftTopY) && (xClickPos < rightBottomY))) {
            Log.d(TAG, "[TA]#### tap is inbound ");
            Toast.makeText(mContext, " Tap X: " + xClickPos + " Tap Y: " + YClickPos + "\n" +
                    " Bound X : " + leftTopX + "-" + rightBottomX + "\n" +
                    " Bound Y : " + leftTopY + "-" + rightBottomY, Toast.LENGTH_SHORT).show();

            return true;
        } else {
            return false;
        }
*/
    }

    private void showFirstPagePhone(boolean firstLaunch) {
         if(mSubPagePhoneDataList.isEmpty()) {
             return;
         }

        mDevicePhone.setSelected(true);
        mDeviceTablet.setSelected(false);
        mDeviceS3.setSelected(false);

        mExperience_phone.setVisibility(VISIBLE);
        mExperience_tap.setVisibility(INVISIBLE);
        mExperience_tap_portrait.setVisibility(INVISIBLE);
        mExperience_gear.setVisibility(INVISIBLE);
        mExperience_phone_image.setVisibility(VISIBLE);
        mExperience_tap_image.setVisibility(INVISIBLE);
        mExperience_tap_image_portrait.setVisibility(INVISIBLE);
        mExperience_gear_image.setVisibility(INVISIBLE);

         if(firstLaunch) {

             mCurrentPageIndexPhone  = 0;

             BitmapUtil.setBitmap(mExperience_phone_image, mSubPagePhoneDataList.get(mCurrentPageIndexPhone).getpageView());

             refreshLayout(DISPLAY_EXPERIENCE);

             setOverlayLayoutParam(mSubPagePhoneDataList, mCurrentPageIndexPhone);

         } else {

         }
     }

    private void showFirstPageTab(boolean firstLaunch) {
        if(mSubPageTabDataList.isEmpty()) {
            return;
        }

        mDevicePhone.setSelected(false);
        mDeviceTablet.setSelected(true);
        mDeviceS3.setSelected(false);

        mExperience_phone.setVisibility(INVISIBLE);
        mExperience_tap.setVisibility(VISIBLE);
        mExperience_tap_portrait.setVisibility(INVISIBLE);
        mExperience_gear.setVisibility(INVISIBLE);
        mExperience_phone_image.setVisibility(INVISIBLE);
        mExperience_tap_image.setVisibility(VISIBLE);
        mExperience_tap_image_portrait.setVisibility(INVISIBLE);
        mExperience_gear_image.setVisibility(INVISIBLE);

        if(firstLaunch) {

            mCurrentPageIndexTab  = 0;

            int supportActions = mSubPageTabDataList.get(mCurrentPageIndexTab).getnextAction();

            if ((supportActions & SubcategoryData.ACTION_PHONE) > 0) {

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(VISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(VISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

                BitmapUtil.setBitmap(mExperience_tap_image_portrait, mSubPageTabDataList.get(mCurrentPageIndexTab).getpageView());

            } else {

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(VISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(VISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

                BitmapUtil.setBitmap(mExperience_tap_image, mSubPageTabDataList.get(mCurrentPageIndexTab).getpageView());

            }

            refreshLayout(DISPLAY_EXPERIENCE);

            setOverlayLayoutParam(mSubPageTabDataList, mCurrentPageIndexTab);

        } else {

        }
    }

    private void showFirstPageGear(boolean firstLaunch) {
        if(mSubPageGearDataList.isEmpty()) {
            return;
        }

        mDevicePhone.setSelected(false);
        mDeviceTablet.setSelected(false);
        mDeviceS3.setSelected(true);

        mExperience_phone.setVisibility(INVISIBLE);
        mExperience_tap.setVisibility(INVISIBLE);
        mExperience_tap_portrait.setVisibility(INVISIBLE);
        mExperience_gear.setVisibility(VISIBLE);
        mExperience_phone_image.setVisibility(INVISIBLE);
        mExperience_tap_image.setVisibility(INVISIBLE);
        mExperience_tap_image_portrait.setVisibility(INVISIBLE);
        mExperience_gear_image.setVisibility(VISIBLE);

        if(firstLaunch) {

            mCurrentPageIndexGear  = 0;

//?            BitmapUtil.setBitmap(mExperience_gear_image, mSubPageGearDataList.get(mCurrentPageIndexGear).getpageView());
            Log.d(TAG, "showFirstPageGear1 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

            int supportActions = mSubPageGearDataList.get(mCurrentPageIndexGear).getnextAction();

            if ((supportActions & SubcategoryData.ACTION_PHONE) > 0) {

                Log.d(TAG, "showFirstPageGear2 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

                mExperience_phone.setVisibility(VISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(INVISIBLE);
                mExperience_phone_image.setVisibility(VISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(INVISIBLE);

                BitmapUtil.setBitmap(mExperience_phone_image, mSubPageGearDataList.get(mCurrentPageIndexGear).getpageView());

            } else if ((supportActions & SubcategoryData.ACTION_ALL) > 0) {



            } else {

                Log.d(TAG, "showFirstPageGear3 : " + mCurrentPageIndexGear + " : " + mSubPageGearDataList.size());

                mExperience_phone.setVisibility(INVISIBLE);
                mExperience_tap.setVisibility(INVISIBLE);
                mExperience_tap_portrait.setVisibility(INVISIBLE);
                mExperience_gear.setVisibility(VISIBLE);
                mExperience_phone_image.setVisibility(INVISIBLE);
                mExperience_tap_image.setVisibility(INVISIBLE);
                mExperience_tap_image_portrait.setVisibility(INVISIBLE);
                mExperience_gear_image.setVisibility(VISIBLE);

                BitmapUtil.setBitmap(mExperience_gear_image, mSubPageGearDataList.get(mCurrentPageIndexGear).getpageView());

            }

            refreshLayout(DISPLAY_EXPERIENCE);

            setOverlayLayoutParam(mSubPageGearDataList, mCurrentPageIndexGear);

        } else {

        }
    }

    private boolean changeSubPage(View v, MotionEvent event) {

        mBtnExperience.setClickable(false);
        mDevicePhone.setClickable(false);
        mDeviceTablet.setClickable(false);
        mDeviceS3.setClickable(false);

        switch (mCurrentPageIndex) {
            case DISPLAY_PHONE:
//?                if(isClickedPhone(v, event)) {
                    Log.d(TAG, "onTouch12 PHONE");
                    changeSubPagePhone();
//?                }
                break;
            case DISPLAY_TAB:
//?                if(isClickedTab(v, event)) {
                    Log.d(TAG, "onTouch12 TAB");
                    changeSubPageTab();
//?                }
                break;
            case DISPLAY_GEAR:
//?                if(isClickedGear(v, event)) {
                    Log.d(TAG, "onTouch12 GEAR");
                    changeSubPageGear();
//?                }
                break;
        }

        refreshLayout(DISPLAY_EXPERIENCE);

        return true;

    }


 /*
 * Ghost Touch View
 */
    public void setTouchView(int x, int y) {
//?        if (!mParent.isShown()) return;

        showTouchView();
        mGhostTouchView.setScaleX(1f);
        mGhostTouchView.setScaleY(1f);

//?        ((RelativeLayout.LayoutParams) mGhostTouchView.getLayoutParams()).leftMargin = x - mGhostTouchView.getMeasuredWidth() / 2;
//?        ((RelativeLayout.LayoutParams) mGhostTouchView.getLayoutParams()).topMargin = y - mGhostTouchView.getMeasuredHeight() / 2;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mGhostTouchView.getLayoutParams();
        layoutParams.leftMargin = x - layoutParams.width / 2;
        layoutParams.topMargin = y - layoutParams.height / 2;

        mGhostTouchView.requestLayout();


        Log.d(TAG, "setTouchView : " + layoutParams.width + " : " + layoutParams.height + " : " + mGhostTouchView.getMeasuredHeight() + " : " + mGhostTouchView.getMeasuredWidth());

    }

    float mScaleX = 1f;
    float mScaleY = 1f;
    public void setTouchViewScale(float scaleX, float scaleY) {
//?        if (!mParent.isShown()) return;

        if (mScaleX != scaleX) {
            mGhostTouchView.setScaleX(scaleX);
            mScaleX = scaleX;
        }

        if (mScaleY != scaleY) {
            mGhostTouchView.setScaleY(scaleY);
            mScaleY = scaleY;
        }
    }

    public void setTouchViewScale(float scale) {
//?        if (!mParent.isShown()) return;

        if (mScaleX == scale || mScaleY == scale) return;

        mGhostTouchView.setScaleX(scale);
        mGhostTouchView.setScaleY(scale);
    }

    public void showTouchView() {
        mGhostTouchView.setVisibility(View.VISIBLE);
    }

    public void hideTouchView() {
        mGhostTouchView.setVisibility(View.GONE);
    }


    private AnimatorSet mAutoLoopAnimator = null;
    public void startAutoLoopHandler(int delay, int x, int y) {
        final int xPoint = x;
        final int yPoint = y;

//?        if (mAutoLoopAnimator != null && (mAutoLoopAnimator.isRunning() || mAutoLoopAnimator.isStarted())) {
//?            Log.e(TAG, "startAutoLoopHandler is running");
//?            return;
//?        }

        int stackedDelay = 1000;

//?        final int attractorLoopY = getResources().getDimensionPixelSize(R.dimen.screen_height)
//?                - (mAttractorLoopView.getMarginBottom() + mAttractorLoopView.getFontL());
//?        final int attractorLoopStartX = getResources().getDimensionPixelSize(R.dimen.screen_width) / 2;
//?        final int attractorLoopEndX = getResources().getDimensionPixelSize(R.dimen.screen_width) / 3;
        mAutoLoopAnimator = new AnimatorSet();

/*
        ValueAnimator stepMoveAnimator1 = ValueAnimator.ofInt(attractorLoopStartX, attractorLoopEndX);
        stepMoveAnimator1.setDuration(600);
        stepMoveAnimator1.setStartDelay(stackedDelay);
        stepMoveAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int moveX = (int) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchView(moveX, attractorLoopY);
            }
        });
        stepMoveAnimator1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.show();
                mGhostAnimationDefenceView.setTouchView(attractorLoopStartX, attractorLoopY);
                mAttractorLoopView.startAutoLoopAnimation(mAttractorLoopView.getCenterPassionIndex() + 1);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        // delay 1000ms
        stackedDelay += 2000;
        ValueAnimator stepMoveAnimator2 = ValueAnimator.ofInt(attractorLoopStartX, attractorLoopEndX);
        stepMoveAnimator2.setDuration(600);
        stepMoveAnimator2.setStartDelay(stackedDelay);
        stepMoveAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int moveX = (int) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchView(moveX, attractorLoopY);
            }
        });
        stepMoveAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(attractorLoopStartX, attractorLoopY);
                mAttractorLoopView.startAutoLoopAnimation(mAttractorLoopView.getCenterPassionIndex() + 1);
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });

        // delay 1000ms
        stackedDelay += 2000;
        ValueAnimator backgroundTouchAnimator = ValueAnimator.ofFloat(1f, 2f);
        backgroundTouchAnimator.setDuration(500);
        backgroundTouchAnimator.setStartDelay(stackedDelay);
        backgroundTouchAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        backgroundTouchAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mGhostAnimationDefenceView.show();
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) / 2,
                        getResources().getDimensionPixelSize(R.dimen.screen_height) / 2);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();

//                if (mSubcategoryPreviewPager.isShown() == false)
//                    startLoopToPreviewAnimation(0); // 1700ms
//                else
//                    startPreviewToLoopAnimation(true);
                if (mPartnerDisplayPager.isShown() == false)
                    startLoopToDisplayAnimation(mAttractorLoopView.getCenterPassionIndex(), 0, 0); // 1700ms
                else
                    startDisplayToLoopAnimation(true);

            }
        });

        stackedDelay += 3000;
        ValueAnimator rightMoveTouchAnimator = ValueAnimator.ofFloat(1f, 2f);
        rightMoveTouchAnimator.setDuration(500);
        rightMoveTouchAnimator.setStartDelay(stackedDelay);
        rightMoveTouchAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        rightMoveTouchAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) - 200, 400);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        ValueAnimator rightMoveAnimator = ValueAnimator.ofFloat(1f, 2f);
        rightMoveAnimator.setDuration(500);
        rightMoveAnimator.setStartDelay(stackedDelay);
        rightMoveAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.startNextPageAnimation(SubcategoryDisplayViewPager.DIRECTION_RIGHT);
            }
        });

        stackedDelay += 3000;
        ValueAnimator rightMoveTouchAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        rightMoveTouchAnimator2.setDuration(500);
        rightMoveTouchAnimator2.setStartDelay(stackedDelay);
        rightMoveTouchAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        rightMoveTouchAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) - 200, 400);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        ValueAnimator rightMoveAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        rightMoveAnimator2.setDuration(500);
        rightMoveAnimator2.setStartDelay(stackedDelay);
        rightMoveAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.startNextPageAnimation(SubcategoryDisplayViewPager.DIRECTION_RIGHT);
            }
        });
*/
        // delay 1000ms
//?        stackedDelay += 2000;
        ValueAnimator backgroundTouchAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        backgroundTouchAnimator2.setDuration(500);
        backgroundTouchAnimator2.setStartDelay(stackedDelay);
        backgroundTouchAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                setTouchViewScale(scale, scale);
            }
        });
        backgroundTouchAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) / 2,
//                        getResources().getDimensionPixelSize(R.dimen.screen_height) / 4);
                setTouchView(xPoint, yPoint); // position of home button
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hideTouchView();

//                if (mSubcategoryPreviewPager.isShown() == false)
//                    startLoopToPreviewAnimation(0); // 1700ms
//                else
//                    startPreviewToLoopAnimation(true);
//?                if (mPartnerDisplayPager.isShown() == false)
//?                    startLoopToDisplayAnimation(mAttractorLoopView.getCenterPassionIndex(), 0, 0); // 1700ms
//?                else
//?                    startDisplayToLoopAnimation(true);

            }
        });


        mAutoLoopAnimator.play(backgroundTouchAnimator2);
//        mAutoLoopAnimator.play(stepMoveAnimator1).with(stepMoveAnimator2).with(backgroundTouchAnimator)
//                .with(previewTouchAnimator).with(previewToDisplayAnimator)
//                .with(addAppTouchAnimator).with(rightMoveTouchAnimator).with(rightMoveAnimator)
//                .with(displayAppClickAnimator).with(addAllTouchAnimator).with(leftMoveTouchAnimator).with(leftMoveAnimator)
//                .with(toCheckoutClickAnimator).with(emailAnimator).with(sendTouchAnimator).with(sentAnimator);

        mAutoLoopAnimator.setStartDelay(delay);

        mAutoLoopAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(final Animator animation) {
                new Handler().postDelayed(new Runnable() {// 1 초 후에 실행
                    @Override
                    public void run() {
                        animation.start();
                    }
                }, 500);
            }
        });
        mAutoLoopAnimator.start();
    }


    public void onInitializeLayout() {
        mScreenSize.x = getResources().getDimensionPixelSize(R.dimen.screen_width);
        mScreenSize.y = getResources().getDimensionPixelSize(R.dimen.screen_height);

        options = ((ShowcaseApplication) (getContext().getApplicationContext())).getUILOptions();

        neighbourWidth = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_neighbour_width);
        pageWidth = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width);
        leftSide = neighbourWidth - pageWidth;
        rightSide = mScreenSize.x - neighbourWidth;
        centerLocation = neighbourWidth + getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side) * 2;
        leftDistance = centerLocation - leftSide;
        rightDistance = rightSide - centerLocation;

        Typeface tfEtextMedium = FontTypeface.getInstance().getEtextFontMedium();
        Typeface tfEtextBold = FontTypeface.getInstance().getEtextFontBold();
        Typeface tfEtextRoman = FontTypeface.getInstance().getEtextFontRoman();

        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layoutInflater != null)
            mRootView = (RelativeLayout) layoutInflater.inflate(R.layout.subcategory_display_layout, null);

        mPane_Left = (RelativeLayout) mRootView.findViewById(R.id.display_category_layout);
        mPane_Introduction = (RelativeLayout) mRootView.findViewById(R.id.right_pane_introduction);
        mIntro_image = (ImageView) mRootView.findViewById(R.id.intro_image);
        if (mSubcategoryData.getIntroImage().length() > 0)
            BitmapUtil.setBitmap(mIntro_image, mSubcategoryData.getIntroImage());
        mIntro_text = (TextView) mRootView.findViewById(R.id.intro_text);
        mIntro_text.setTypeface(tfEtextRoman);
        mPane_Benefits = (RelativeLayout) mRootView.findViewById(R.id.right_pane_benefits);
        mBenefits_image = (ImageView) mRootView.findViewById(R.id.benefits_image);
//        if (mSubcategoryData.getBenefitsImage().length() > 0)
//            BitmapUtil.setBitmap(mBenefits_image, mSubcategoryData.getBenefitsImage());
        mBenefits_text = (TextView) mRootView.findViewById(R.id.benefits_text);
        mBenefits_text.setTypeface(tfEtextRoman);
        mPane_Experience = (RelativeLayout) mRootView.findViewById(R.id.right_pane_experience);

        TextView partnerName = (TextView) mRootView.findViewById(R.id.partner_title);
        partnerName.setTypeface(tfEtextBold);
        String subTitle = mSubcategoryData.getSubcategoryName();
        if (subTitle != null && subTitle.length() > 0)
            partnerName.setText(subTitle.toUpperCase());
        else
            partnerName.setText("");

        mBtnIntroduction = (TextView) mRootView.findViewById(R.id.btn_introduction);
        mBtnBenefits = (TextView) mRootView.findViewById(R.id.btn_benefits);
        mBtnExperience = (TextView) mRootView.findViewById(R.id.btn_experience);
//?        layout_experience = (TextView) mRootView.findViewById(R.id.btn_experience);
        mBtnIntroduction.setSelected(true);

        mBtnIntroduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.isSelected())
                    return;

                refreshLayout(DISPLAY_INTRODUCTION);
            }
        });
        mBtnBenefits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.isSelected())
                    return;

                refreshLayout(DISPLAY_BENEFITS);

                if ( mBenefits_image.getDrawable() == null && mSubcategoryData.getBenefitsImage().length() > 0)
                    BitmapUtil.setBitmap(mBenefits_image, mSubcategoryData.getBenefitsImage());
            }
        });

        mSubPagePhoneDataList = (ArrayList) mSubcategoryData.getSubPagePhone();
        mSubPageTabDataList = (ArrayList) mSubcategoryData.getSubPageTab();
        mSubPageGearDataList = (ArrayList) mSubcategoryData.getSubPageGear();

        mExperience_phone = (ImageView) mRootView.findViewById(R.id.right_pane_experience_phone);
        mExperience_tap = (ImageView) mRootView.findViewById(R.id.right_pane_experience_tap);
        mExperience_tap_portrait = (ImageView) mRootView.findViewById(R.id.right_pane_experience_tap_portrait);
        mExperience_gear = (ImageView) mRootView.findViewById(R.id.right_pane_experience_gear);
        mExperience_phone_image = (ImageView) mRootView.findViewById(R.id.right_pane_experience_phone_image);
        mExperience_tap_image = (ImageView) mRootView.findViewById(R.id.right_pane_experience_tap_image);
        mExperience_tap_image_portrait = (ImageView) mRootView.findViewById(R.id.right_pane_experience_tap_image_portrait);
        mExperience_gear_image = (ImageView) mRootView.findViewById(R.id.right_pane_experience_gear_image);

        mGhostAnimationContainner = (RelativeLayout) mRootView.findViewById(R.id.ghost_animation_containner);
        mGhostAnimationContainner.setAlpha(0.5f);
        mGhostAnimationContainner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "mGhostAnimationContainner onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        switch (mCurrentPageIndex) {
                            case DISPLAY_PHONE:
                                Log.d(TAG, "mGhostAnimationContainner PHONE");
                                showFirstPagePhone(true);
                                break;
                            case DISPLAY_TAB:
                                Log.d(TAG, "mGhostAnimationContainner TAB");
                                showFirstPageTab(true);
                                break;
                            case DISPLAY_GEAR:
                                Log.d(TAG, "mGhostAnimationContainner GEAR");
                                showFirstPageGear(true);
                                break;
                        }

                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });
/*
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_width),
                getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_height));
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_side);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.subcategory_display_page_margin_top);
        params.width = 100;
        params.height = 100
        mExperience_phone_image.setLayoutParams(params);
*/
        mExperience_phone_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "mExperience_phone_image changeSubPage");
                        changeSubPage(v, event);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });

        mExperience_tap_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "mExperience_tap_image changeSubPage");
                        changeSubPage(v, event);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });

        mExperience_tap_image_portrait.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "mExperience_tap_image changeSubPage");
                        changeSubPage(v, event);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });

        mExperience_gear_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "mExperience_gear_image changeSubPage");
                        changeSubPage(v, event);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });

        mGhostTouchView = (ImageView) mRootView.findViewById(R.id.ghost_touch_view);

        mGhostTouchView.setVisibility((View.INVISIBLE));
        ((AnimationDrawable) mGhostTouchView.getBackground()).start();

        mGhostTouchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch");
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "mGhostTouchView changeSubPage");
                        changeSubPage(v, event);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                }

                return false;
            }
        });

        mBtnExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "onClick");

                mBtnExperience.setClickable(false);
                mDevicePhone.setClickable(false);
                mDeviceTablet.setClickable(false);
                mDeviceS3.setClickable(false);

                if ( mDevicePhone.isShown() ) {
                    mCurrentPageIndex = DISPLAY_PHONE;

//?                    mDevicePhone.setClickable(false);
                    showFirstPagePhone(true);
                } else if ( mDeviceTablet.isShown() ) {
                    mCurrentPageIndex = DISPLAY_TAB;

//?                    mDeviceTablet.setClickable(false);
                    showFirstPageTab(true);
                } else if ( mDeviceS3.isShown() ) {
                    mCurrentPageIndex = DISPLAY_GEAR;

//?                    mDeviceS3.setClickable(false);
                    showFirstPageGear(true);
                }

            }
        });

        // show/hide supported device
        mDevicePhone = (ImageView) mRootView.findViewById(R.id.device_s7);
        mDevicePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick PHONE");

                mCurrentPageIndex = DISPLAY_PHONE;

                mBtnExperience.setClickable(false);
                mDevicePhone.setClickable(false);
                mDeviceTablet.setClickable(false);
                mDeviceS3.setClickable(false);

                showFirstPagePhone(true);
            }
        });

        mDeviceTablet = (ImageView) mRootView.findViewById(R.id.device_tabs2);
        mDeviceTablet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick TAB");

                mCurrentPageIndex = DISPLAY_TAB;

//                mExperience_phone.setVisibility(INVISIBLE);
//                mExperience_tap.setVisibility(VISIBLE);
//                mExperience_gear.setVisibility(INVISIBLE);
//                mExperience_phone_image.setVisibility(INVISIBLE);
//                mExperience_tap_image.setVisibility(VISIBLE);
//                mExperience_gear_image.setVisibility(INVISIBLE);

                mBtnExperience.setClickable(false);
                mDevicePhone.setClickable(false);
                mDeviceTablet.setClickable(false);
                mDeviceS3.setClickable(false);

                showFirstPageTab(true);
            }
        });

        mDeviceS3 = (ImageView) mRootView.findViewById(R.id.device_gears3);
        mDeviceS3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick GEAR");

                mCurrentPageIndex = DISPLAY_GEAR;

//                mExperience_phone.setVisibility(INVISIBLE);
//                mExperience_tap.setVisibility(INVISIBLE);
//                mExperience_gear.setVisibility(VISIBLE);
//                mExperience_phone_image.setVisibility(INVISIBLE);
//                mExperience_tap_image.setVisibility(INVISIBLE);
//                mExperience_gear_image.setVisibility(VISIBLE);

                mBtnExperience.setClickable(false);
                mDevicePhone.setClickable(false);
                mDeviceTablet.setClickable(false);
                mDeviceS3.setClickable(false);

                showFirstPageGear(true);
            }
        });

        int supportDevices = mSubcategoryData.getDeviceInfo();
        if ((supportDevices & SubcategoryData.DEVICE_PHONE) > 0) {
            mDevicePhone.setVisibility(View.VISIBLE);
        } else
            mDevicePhone.setVisibility(View.GONE);
        if ((supportDevices & SubcategoryData.DEVICE_TABLET) > 0) {
            mDeviceTablet.setVisibility(View.VISIBLE);
        } else
            mDeviceTablet.setVisibility(View.GONE);
        if ((supportDevices & SubcategoryData.DEVICE_S3) > 0) {
            mDeviceS3.setVisibility(View.VISIBLE);
        } else
            mDeviceS3.setVisibility(View.GONE);

        RelativeLayout m4s_layout = (RelativeLayout) mRootView.findViewById(R.id.layout_for_m4s);
        mM4SPhone1 = (ImageView) mRootView.findViewById(R.id.image_m4s_phone1);
        mM4SPhone2 = (ImageView) mRootView.findViewById(R.id.image_m4s_phone2);
        mM4SPhone3 = (ImageView) mRootView.findViewById(R.id.image_m4s_phone3);
        mM4SPhone4 = (ImageView) mRootView.findViewById(R.id.image_m4s_phone4);
        mM4STitle = (ImageView) mRootView.findViewById(R.id.image_m4s_made_for_samsung);
        mM4SIntro = (ImageView) mRootView.findViewById(R.id.image_m4s_introduction);
        if (mSubcategoryData.getPassionName().compareTo("Made for Samsung") == 0) {
            m4s_layout.setVisibility(View.VISIBLE);

            mPane_Left.setVisibility(View.GONE);
            mPane_Introduction.setVisibility(View.GONE);
            mPane_Benefits.setVisibility(View.GONE);
            mPane_Experience.setVisibility(View.GONE);
        }

        // SUBCATEGORY DISPLAY COVER
        mCoverImageView = (ImageView) mRootView.findViewById(R.id.display_layout_cover);
        mCoverShadowView = (ImageView) mRootView.findViewById(R.id.display_layout_cover_shadow);

        BitmapUtil.setBitmap(mCoverImageView, mSubcategoryData.getPreviewImage2());

        mSubcategoryTitle = (TextView) mRootView.findViewById(R.id.display_category_subtitle);
        mSubcategoryTitle.setText("");
//        String subTitle = mSubcategoryData.getSubcategoryName();
//        if (subTitle != null && subTitle.length() > 0)
//            mSubcategoryTitle.setText(subTitle.substring(0, 1).toUpperCase() + subTitle.substring(1));
//        else
//            mSubcategoryTitle.setText("");

        mPartnerIcon = (ImageView) mRootView.findViewById(R.id.partner_icon);
        if (mSubcategoryData.getIcon().length() > 0)
            BitmapUtil.setBitmap(mPartnerIcon, mSubcategoryData.getIcon());

        this.addView(mRootView);
        this.requestLayout();
    }

    private ValueAnimator mTranslateAnimation = null;

    public SubcategoryData getSubcategoryData() {
        return mSubcategoryData;
    }

    private void setHelpMessage(TextView tv, Typeface tf, String original, String bold) {
        String upperOriginal = original.toUpperCase();
        String upperBold = (bold + " for Samsung").toUpperCase();

        int location = upperOriginal.indexOf(upperBold);
        if (location == -1) { // not found
            upperBold = bold.toUpperCase();
//            upperBold.toUpperCase();
            location = upperOriginal.indexOf(upperBold);
        } else
            bold += " for Samusng";

        if (location > -1) { // found
            SpannableStringBuilder SS = new SpannableStringBuilder(original);
            //SS.setSpan (new CustomTypeFaceSpan("", fontThin), 0, location-1,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            tv.setText(SS);
        } else
            tv.setText(original);
    }

    public void setAppItem(int index) {

        String description_intro = removeLinkTags(mSubcategoryData.getIntroduction());
        SpannableStringBuilder descriptionBuilder_intro = new SpannableStringBuilder(Html.fromHtml(description_intro));
        descriptionBuilder_intro = trimTrailingWhitespace(descriptionBuilder_intro);

        setHelpMessage (mIntro_text, FontTypeface.getInstance().getEtextFontBold(), descriptionBuilder_intro.toString(),
                mSubcategoryData.getSubcategoryName());
        //mIntro_text.setText(descriptionBuilder_intro.toString());

        String description_benefits = removeLinkTags(mSubcategoryData.getBenefits());
        SpannableStringBuilder descriptionBuilder_benefits = new SpannableStringBuilder(Html.fromHtml(description_benefits));
        descriptionBuilder_benefits = trimTrailingWhitespace(descriptionBuilder_benefits);
        mBenefits_text.setText(descriptionBuilder_benefits.toString());

        MixPanelUtil.page_view(MixPanelUtil.pageview_Feature_selected_app);
        ((FullscreenActivity) this.getContext()).startAutoResetHandler();
    }

    private String removeLinkTags(String text) {        String newString = text;
        int startIndex = text.indexOf("<a href=");
        if (startIndex != -1) {
            int tagEndIndex = text.substring(startIndex, text.length() - 1).indexOf(">") + startIndex;
            int linkEndIndex = text.indexOf("</a>");
            if (linkEndIndex == -1) {
//?                Log.e(TAG, "fail to find tag </a> : " + text);
                return text;
            }
            String link = text.substring(tagEndIndex + 1, linkEndIndex);
            newString = text.substring(0, startIndex) + link + text.substring(linkEndIndex + "</a>".length(), text.length());
            newString = removeLinkTags(newString);
        }
        return newString;
    }

    public SpannableStringBuilder trimTrailingWhitespace(
            SpannableStringBuilder spannableString) {

        if (spannableString == null)
            return new SpannableStringBuilder("");

        int i = spannableString.length();

        // loop back to the first non-whitespace character
        while (--i >= 0 && Character.isWhitespace(spannableString.charAt(i))) {
        }

        return new SpannableStringBuilder(spannableString.subSequence(0, i + 1));
    }

    private String makeDownloadFormattedString(String download) {
        int hyphen = download.indexOf("-");
        if (hyphen == -1) // not found
            return download;

        String from = download.substring(0, hyphen).trim();
        String to = download.substring(hyphen + 1).trim();

        return getFormattedString(from) + "-" + getFormattedString(to);
    }

    private String getFormattedString(String number) {
        String[] array;

        array = number.split(",");

        switch (array.length) {
            case 1:
            default:
                return number;
            case 2: // K
                return array[0] + "K";
            case 3: // M
                return array[0] + "M";
            case 4: // B
                return array[0] + "B";
            case 5: // T
                return array[0] + "T";
        }
    }

    private List<String> getVideoCode(List<String> mYoutubeUrls) {
        ArrayList<String> videoCodes = new ArrayList<String>();
        for (String url : mYoutubeUrls) {
            if (url != null && url.trim().length() > 0 && url.startsWith("http")) {
                String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
                CharSequence input = url;
                Pattern pattern = Pattern.compile(expression);
                Matcher matcher = pattern.matcher(input);
                if (matcher.matches()) {
                    String groupIndex1 = matcher.group(7);
                    if (groupIndex1 != null && groupIndex1.length() == 11) {
                        videoCodes.add(groupIndex1);
                    }
                }
            }
        }
        return videoCodes;
    }

    int[] loc = new int[2];

    public Point getCenterOfAddAppButton() {
        return new Point(100, 100);
    }

    public Point getCenterOfAddAllAppButton() {
        return new Point(200, 200);
    }

    public Point getCenterIconInGrid(int index) {
        return new Point(500, 500);
    }

    public boolean isCovered() {
        return ((mCoverImageView.isShown() && mCoverImageView.getAlpha() == 1f));
    }

    public void setCover(boolean visibility) {
        if (visibility) {
            //mHeartCheckBox.setVisibility(GONE);
//            mSubcategoryTitle.setVisibility(INVISIBLE);
            mCoverImageView.setVisibility(VISIBLE);
            mCoverImageView.setAlpha(1f);
            //mHeartCheckBox.setAlpha(0f);
//            mSubcategoryTitle.setAlpha(0f);
        } else {
            //mHeartCheckBox.setVisibility(VISIBLE);
//            mSubcategoryTitle.setVisibility(VISIBLE);
            mCoverImageView.setVisibility(GONE);
            mCoverImageView.setAlpha(0f);
            mCoverShadowView.setAlpha(0f);
            //mHeartCheckBox.setAlpha(1f);
//            mSubcategoryTitle.setAlpha(1f);
        }
    }

    // ======================================
    // TOUCH LISTENER OF GRIDVIEW & SCROLLVIEW
    // ======================================
    private boolean isHorizontalMove = false;
    private boolean checkDirection = false;
    private PointF mDownTouchPoint = new PointF();
    private PointF mMoveTouchPoint = new PointF(0,0);
    private boolean isForCanceling = false;
    private boolean isTouchCategoryAppGridView = false;
    private boolean isTouchButtonLayout = false;
    private int maxTouchAppMoreInfoLayoutDiff = 0;

    private float mAppScreenshotPivotX = 0;

    private boolean isTouchFullScreenshotHScroll(float x, float y){
        return false;
    }

    @Override
    public boolean
    onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isForCanceling = true;
//                cancelVideo();
                ((FullscreenActivity) getContext()).startAutoResetHandler();

                isHorizontalMove = false;
                checkDirection = true;
                mDownTouchPoint.x = event.getRawX();
                mDownTouchPoint.y = event.getRawY();
                mMoveTouchPoint.x = 0;
                mMoveTouchPoint.y = 0;

                maxTouchAppMoreInfoLayoutDiff = 0;

                isTouchButtonLayout = false;

//                if (mCoverImageView.isShown()) return true;
                if (isCovered()) return true;
                break;
            case MotionEvent.ACTION_MOVE:
                //touch button should be always return false;
                if (isTouchButtonLayout)return false;

                if (checkDirection) {
                    //same touch move position will be return false;
                    if (mDownTouchPoint.x == event.getRawX() && mDownTouchPoint.y == event.getRawY()) return false;
                    if (Math.abs(mDownTouchPoint.x - event.getRawX()) >= Math.abs(mDownTouchPoint.y - event.getRawY())) {
                        isHorizontalMove = true;
                    }
                    mMoveTouchPoint.x = event.getX();
                    mMoveTouchPoint.y = event.getY();
                    checkDirection = false;
                }

                if (isHorizontalMove) {
                    if (Math.abs(mDownTouchPoint.x - event.getRawX()) <= 2f)
                        return false;

                    //for click event of AppMoreInfoLayout and CategoryAppGridView
                    if (Math.abs(mDownTouchPoint.x - event.getRawX()) <= 4f)
                        return false;

                    //move subcategory display view pager
//                    ((SubcategoryDisplayViewPager) this.getParent().getParent()).requestDisallowInterceptTouchEvent(false);
                    ((SubcategoryDisplayViewPager) this.getParent().getParent()).setManualActionDown(event.getRawX(), event.getRawY());
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                ((FullscreenActivity) getContext()).startAutoResetHandler();

                isHorizontalMove = false;
                checkDirection = true;
                isForCanceling = false;
                isTouchCategoryAppGridView = false;
                isTouchButtonLayout = false;
                maxTouchAppMoreInfoLayoutDiff = 0;
                break;
        }
        return false;
    }


    private boolean isWillCompleteAnim = false;

    public void setCoverShadow(int resource) {
        if (mShadowResource != resource) {
            mCoverShadowView.setImageResource(resource);
            mShadowResource = resource;
        }
    }

    public void updateOpenCover() {
        // COVER
        if (locOnScreen[0] <= leftSide) {
            if (mCoverImageView.getAlpha() != 1f) {
                mCoverImageView.setAlpha(1f);
            }
        } else if (locOnScreen[0] >= rightSide) {
            if (mCoverImageView.getAlpha() != 1f) {
                mCoverImageView.setAlpha(1f);
            }
        } else if (locOnScreen[0] > leftSide + COVER_ANIMATION_OPEN_OFFSET && locOnScreen[0] <= leftSide + COVER_ANIMATION_REGION + COVER_ANIMATION_OPEN_OFFSET) {
            int distance = locOnScreen[0] - (leftSide + COVER_ANIMATION_OPEN_OFFSET);
            float alpha = 1f - ((float) distance / COVER_ANIMATION_REGION);
            if (alpha > 0.91f)
                alpha = 1.0f;
            else if (alpha < 0.09f)
                alpha = 0.0f;
            if (!mCoverImageView.isShown())
                mCoverImageView.setVisibility(VISIBLE);
            mCoverImageView.setAlpha(alpha);
//            Log.d (TAG, "updateOpenCover(left) : " + locOnScreen[0] + "," + leftSide + "," + alpha);
        } else if (locOnScreen[0] < rightSide - COVER_ANIMATION_OPEN_OFFSET && locOnScreen[0] >= rightSide - COVER_ANIMATION_REGION - COVER_ANIMATION_OPEN_OFFSET) {
            int distance = (rightSide - COVER_ANIMATION_OPEN_OFFSET) - locOnScreen[0];
            float alpha = 1f - ((float) distance / COVER_ANIMATION_REGION);
            if (alpha < 0.09f)
                alpha = 0.0f;
            else if (alpha > 0.91f)
                alpha = 1.0f;
            if (!mCoverImageView.isShown())
                mCoverImageView.setVisibility(VISIBLE);
            mCoverImageView.setAlpha(alpha);
//            Log.d (TAG, "updateOpenCover(right) : " + locOnScreen[0] + "," + rightSide + "," + alpha);
        } else if (locOnScreen[0] > leftSide + COVER_ANIMATION_REGION + COVER_ANIMATION_OPEN_OFFSET && locOnScreen[0] < rightSide - COVER_ANIMATION_REGION - COVER_ANIMATION_OPEN_OFFSET) {
            if (mCoverImageView.getAlpha() != 0f) {
                mCoverImageView.setAlpha(0f);
                mCoverImageView.setVisibility(GONE);
            }
        }

//        if (locOnScreen[0] <= (leftSide + TITLE_ANIMATION_OFFSET)) {
//            if (mSubcategoryTitle.getAlpha() != 0f) {
//                mSubcategoryTitle.setAlpha(0f);
//                if (mSubcategoryTitle.isShown())
//                    mSubcategoryTitle.setVisibility(INVISIBLE);
//            }
//            if (mSubcategoryTitle.getAlpha() != 0f) {
//                mSubcategoryTitle.setAlpha(0f);
//                if (mSubcategoryTitle.isShown())
//                    mSubcategoryTitle.setVisibility(INVISIBLE);
//            }
//        } else if (locOnScreen[0] > leftSide + TITLE_ANIMATION_OFFSET && locOnScreen[0] <= leftSide + TITLE_ANIMATION_REGION) {
//            int distance = locOnScreen[0] - (leftSide + TITLE_ANIMATION_OFFSET);
//            float alpha = 1f - ((float) distance / (TITLE_ANIMATION_REGION - TITLE_ANIMATION_OFFSET));
//            if (!mSubcategoryTitle.isShown())
//                mSubcategoryTitle.setVisibility(VISIBLE);
//            mSubcategoryTitle.setAlpha(1f - alpha);
//        } else if (locOnScreen[0] < (rightSide - TITLE_ANIMATION_OFFSET) && locOnScreen[0] >= rightSide - TITLE_ANIMATION_REGION) {
//            int distance = (rightSide - TITLE_ANIMATION_OFFSET) - locOnScreen[0];
//            float alpha = 1f - ((float) distance / (TITLE_ANIMATION_REGION - TITLE_ANIMATION_OFFSET));
//            if (!mSubcategoryTitle.isShown())
//                mSubcategoryTitle.setVisibility(VISIBLE);
//            mSubcategoryTitle.setAlpha(1f - alpha);
//        }
    }

    public void updateCloseCover() {
        // COVER
        if (locOnScreen[0] <= leftSide + COVER_ANIMATION_CLOSE_OFFSET) {
            if (mCoverImageView.getAlpha() != 1f) {
                mCoverImageView.setAlpha(1f);
            }
        } else if (locOnScreen[0] >= rightSide - COVER_ANIMATION_CLOSE_OFFSET) {
            if (mCoverImageView.getAlpha() != 1f) {
                mCoverImageView.setAlpha(1f);
            }
        } else if (locOnScreen[0] > leftSide && locOnScreen[0] <= centerLocation - COVER_ANIMATION_CLOSE_OFFSET) {
            int distance = locOnScreen[0] - (leftSide + COVER_ANIMATION_CLOSE_OFFSET);
//            float alpha = 1f - ((float) distance / (leftDistance - (COVER_ANIMATION_CLOSE_OFFSET*2)));
            float alpha = 1f - ((float) distance / COVER_ANIMATION_REGION);
            if (!mCoverImageView.isShown())
                mCoverImageView.setVisibility(VISIBLE);
            if (alpha > 1f) alpha = 1f;
            if (alpha < 0f) alpha = 0f;
            mCoverImageView.setAlpha(alpha);
        } else if (locOnScreen[0] < rightSide && locOnScreen[0] >= centerLocation + COVER_ANIMATION_CLOSE_OFFSET) {
            int distance = (rightSide - COVER_ANIMATION_CLOSE_OFFSET) - locOnScreen[0];
//            float alpha = 1f - ((float) distance / (rightDistance - (COVER_ANIMATION_CLOSE_OFFSET*2)));
            float alpha = 1f - ((float) distance / COVER_ANIMATION_REGION);
            if (alpha > 1f) alpha = 1f;
            if (alpha < 0f) alpha = 0f;
            if (!mCoverImageView.isShown())
                mCoverImageView.setVisibility(VISIBLE);
            mCoverImageView.setAlpha(alpha);
        } else if (locOnScreen[0] == centerLocation) {
            if (mCoverImageView.getAlpha() != 0f) {
                mCoverImageView.setAlpha(0f);
                mCoverImageView.setVisibility(GONE);
            }
        }

//        if (locOnScreen[0] <= leftSide + TITLE_ANIMATION_CLOSE_OFFSET) {
//            if (mSubcategoryTitle.getAlpha() != 0f) {
//                mSubcategoryTitle.setAlpha(0f);
//                if (mSubcategoryTitle.isShown())
//                    mSubcategoryTitle.setVisibility(INVISIBLE);
//            }
//        } else if (locOnScreen[0] >= rightSide - TITLE_ANIMATION_CLOSE_OFFSET) {
//            if (mSubcategoryTitle.getAlpha() != 0f) {
//                mSubcategoryTitle.setAlpha(0f);
//                if (mSubcategoryTitle.isShown())
//                    mSubcategoryTitle.setVisibility(INVISIBLE);
//            }
//        } else if (locOnScreen[0] > leftSide + TITLE_ANIMATION_CLOSE_OFFSET && locOnScreen[0] <= centerLocation - COVER_ANIMATION_CLOSE_OFFSET) {
//            int distance = locOnScreen[0] - (leftSide + TITLE_ANIMATION_CLOSE_OFFSET);
//            float alpha = 1f - ((float) distance / (leftDistance - TITLE_ANIMATION_CLOSE_OFFSET - COVER_ANIMATION_CLOSE_OFFSET));
//            if (!mSubcategoryTitle.isShown())
//                mSubcategoryTitle.setVisibility(VISIBLE);
//            if (alpha > 1f) alpha = 1f;
//            if (alpha < 0f) alpha = 0f;
//            mSubcategoryTitle.setAlpha(1f - alpha);
//        } else if (locOnScreen[0] < rightSide - TITLE_ANIMATION_CLOSE_OFFSET && locOnScreen[0] >= centerLocation + COVER_ANIMATION_CLOSE_OFFSET) {
//            int distance = (rightSide - TITLE_ANIMATION_CLOSE_OFFSET) - locOnScreen[0];
//            float alpha = 1f - ((float) distance / (rightDistance - TITLE_ANIMATION_CLOSE_OFFSET - COVER_ANIMATION_CLOSE_OFFSET));
//            if (alpha > 1f) alpha = 1f;
//            if (alpha < 0f) alpha = 0f;
//            if (!mSubcategoryTitle.isShown())
//                mSubcategoryTitle.setVisibility(VISIBLE);
//            mSubcategoryTitle.setAlpha(1f - alpha);
//        } else if (locOnScreen[0] == centerLocation) {
//            if (mSubcategoryTitle.getAlpha() != 1f) {
//                mSubcategoryTitle.setAlpha(1f);
//                if (!mSubcategoryTitle.isShown())
//                    mSubcategoryTitle.setVisibility(VISIBLE);
//            }
//        }
    }

    public void updateCover(int centerIndex, int targetIndex) {
        mCoverShadowView.getLocationOnScreen(locOnScreen);

//        Log.d(TAG, "updateCover : " + locOnScreen[0]);

        // COVER SHADOW
        if (locOnScreen[0] <= leftSide) {
            if (mCoverShadowView.getAlpha() != 1f) {
                mCoverShadowView.setAlpha(1f);
                if (mShadowResource != R.drawable.cover_left_shadow)
                    this.setCoverShadow(R.drawable.cover_left_shadow);
            }
        } else if (locOnScreen[0] >= rightSide) {
            if (mCoverShadowView.getAlpha() != 1f) {
                mCoverShadowView.setAlpha(1f);
                if (mShadowResource != R.drawable.cover_right_shadow)
                    this.setCoverShadow(R.drawable.cover_right_shadow);
            }
        } else if (locOnScreen[0] > leftSide && locOnScreen[0] <= leftSide + COVER_SHADOW_ANIMATION_REGION) {
            if (mShadowResource != R.drawable.cover_left_shadow)
                this.setCoverShadow(R.drawable.cover_left_shadow);
            int distance = locOnScreen[0] - leftSide;
            float alpha = 1f - ((float) distance / COVER_SHADOW_ANIMATION_REGION);
            mCoverShadowView.setAlpha(alpha);
        } else if (locOnScreen[0] < rightSide && locOnScreen[0] >= rightSide - COVER_SHADOW_ANIMATION_REGION) {
            if (mShadowResource != R.drawable.cover_right_shadow)
                this.setCoverShadow(R.drawable.cover_right_shadow);
            int distance = rightSide - locOnScreen[0];
            float alpha = 1f - ((float) distance / COVER_SHADOW_ANIMATION_REGION);
            mCoverShadowView.setAlpha(alpha);
        } else if (locOnScreen[0] > leftSide + COVER_SHADOW_ANIMATION_REGION && locOnScreen[0] < rightSide + COVER_SHADOW_ANIMATION_REGION) {
            if (mCoverShadowView.getAlpha() != 0f) {
                mCoverShadowView.setAlpha(0f);
                mCoverShadowView.setVisibility(GONE);
            }
        }

        if (centerIndex != targetIndex)
            mPrevTargetIndex = targetIndex;

        if ((mSubcategoryIndex == mPrevTargetIndex) && (centerIndex == targetIndex)) {
            updateOpenCover();
        } else if ((targetIndex == mSubcategoryIndex) && (centerIndex != targetIndex)) {
            updateOpenCover();
        } else {
            updateCloseCover();
        }
    }

    private int mPrevTargetIndex = Integer.MIN_VALUE;

    public void free() {
        BitmapUtil.recyleBitmap(mCoverImageView);
        BitmapUtil.recyleBitmap(mIntro_image);
        BitmapUtil.recyleBitmap(mBenefits_image);

        Log.d(TAG, "---------------------------------- free mTimer.cancel()");
        mTimer.cancel();
//?        mTimer.purge();
//?        mTimer = null;
    }

//    public Animator generateTitleDismissAnimator() {
//        ObjectAnimator animator = ObjectAnimator.ofFloat(mSubcategoryTitle, "alpha", 1f, 0f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mSubcategoryTitle.setAlpha(1f);
//                mSubcategoryTitle.setVisibility(View.INVISIBLE);
//            }
//        });
//        return animator;
//    }
//
//    public Animator generateTitleShowAnimator() {
//        ObjectAnimator animator = ObjectAnimator.ofFloat(mSubcategoryTitle, "alpha", 0f, 1f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                mSubcategoryTitle.setAlpha(0f);
//                mSubcategoryTitle.setVisibility(View.VISIBLE);
//            }
//        });
//        return animator;
//    }

    public Animator generateM4SShowAnimator() {
        if (mSubcategoryData.getPassionName().compareTo("Made for Samsung") != 0 )
            return null;

        AnimatorSet introAnim = new AnimatorSet();

        ObjectAnimator animatorAlpha1 = ObjectAnimator.ofFloat(mM4STitle, "alpha", 0f, 1f);
        animatorAlpha1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4STitle.setAlpha(0f);
                mM4STitle.setVisibility(View.VISIBLE);
            }
        });

        ObjectAnimator animatorAlpha2 = ObjectAnimator.ofFloat(mM4SIntro, "alpha", 0f, 1f);
        animatorAlpha2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4SIntro.setAlpha(0f);
                mM4SIntro.setVisibility(View.VISIBLE);
            }
        });

        ObjectAnimator animatorMoveLeft = ObjectAnimator.ofFloat(mM4SPhone4, "translationX", -100, 0f);
        ObjectAnimator animatorAlphaLeft = ObjectAnimator.ofFloat(mM4SPhone4, "alpha", 0f, 1f);
        animatorAlphaLeft.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4SPhone4.setAlpha(0f);
                mM4SPhone4.setVisibility(View.VISIBLE);
            }
        });

        ObjectAnimator animatorMoveRight1 = ObjectAnimator.ofFloat(mM4SPhone3, "translationX", -100, 0f);
        ObjectAnimator animatorAlphaRight1 = ObjectAnimator.ofFloat(mM4SPhone3, "alpha", 0f, 1f);
        animatorMoveRight1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4SPhone3.setAlpha(0f);
                mM4SPhone3.setVisibility(View.VISIBLE);
            }
        });
        animatorMoveRight1.setStartDelay(100);
        animatorAlphaRight1.setStartDelay(100);

        ObjectAnimator animatorMoveRight2 = ObjectAnimator.ofFloat(mM4SPhone2, "translationX", -100, 0f);
        ObjectAnimator animatorAlphaRight2 = ObjectAnimator.ofFloat(mM4SPhone2, "alpha", 0f, 1f);
        animatorMoveRight2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4SPhone2.setAlpha(0f);
                mM4SPhone2.setVisibility(View.VISIBLE);
            }
        });
        animatorMoveRight2.setStartDelay(200);
        animatorAlphaRight2.setStartDelay(200);

        ObjectAnimator animatorMoveRight3 = ObjectAnimator.ofFloat(mM4SPhone1, "translationX", -100, 0f);
        ObjectAnimator animatorAlphaRight3 = ObjectAnimator.ofFloat(mM4SPhone1, "alpha", 0f, 1f);
        animatorMoveRight2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mM4SPhone1.setAlpha(0f);
                mM4SPhone1.setVisibility(View.VISIBLE);
            }
        });
        animatorMoveRight3.setStartDelay(300);
        animatorAlphaRight3.setStartDelay(300);

        introAnim.play(animatorAlpha1).with(animatorAlpha2)
                .with(animatorMoveLeft).with(animatorAlphaLeft)
                .with(animatorMoveRight1).with(animatorAlphaRight1)
                .with(animatorMoveRight2).with(animatorAlphaRight2)
                .with(animatorMoveRight3).with(animatorAlphaRight3);

        return introAnim;
    }

    public Animator generateM4SDismissAnimator() {
        if (mSubcategoryData.getPassionName().compareTo("Made for Samsung") != 0 )
            return null;

        AnimatorSet introAnim = new AnimatorSet();

        ObjectAnimator animatorAlpha1 = ObjectAnimator.ofFloat(mM4STitle, "alpha", 1f, 0f);
        animatorAlpha1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4STitle.setAlpha(0f);
                mM4STitle.setVisibility(View.GONE);
            }
        });

        ObjectAnimator animatorAlpha2 = ObjectAnimator.ofFloat(mM4SIntro, "alpha", 1f, 0f);
        animatorAlpha2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4SIntro.setAlpha(0f);
                mM4SIntro.setVisibility(View.GONE);
            }
        });

        ObjectAnimator animatorMoveLeft = ObjectAnimator.ofFloat(mM4SPhone4, "translationX", 0, -100f);
        ObjectAnimator animatorAlphaLeft = ObjectAnimator.ofFloat(mM4SPhone4, "alpha", 1f, 0f);
        animatorAlphaLeft.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4SPhone4.setAlpha(0f);
                mM4SPhone4.setVisibility(View.GONE);
            }
        });

        ObjectAnimator animatorMoveRight1 = ObjectAnimator.ofFloat(mM4SPhone3, "translationX", 0, -100f);
        ObjectAnimator animatorAlphaRight1 = ObjectAnimator.ofFloat(mM4SPhone3, "alpha", 1f, 0f);
        animatorMoveRight1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4SPhone3.setAlpha(0f);
                mM4SPhone3.setVisibility(View.GONE);
            }
        });
        animatorMoveRight1.setStartDelay(100);
        animatorAlphaRight1.setStartDelay(100);

        ObjectAnimator animatorMoveRight2 = ObjectAnimator.ofFloat(mM4SPhone2, "translationX", 0, -100f);
        ObjectAnimator animatorAlphaRight2 = ObjectAnimator.ofFloat(mM4SPhone2, "alpha", 1f, 0f);
        animatorMoveRight2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4SPhone2.setAlpha(0f);
                mM4SPhone2.setVisibility(View.GONE);
            }
        });
        animatorMoveRight2.setStartDelay(200);
        animatorAlphaRight2.setStartDelay(200);

        ObjectAnimator animatorMoveRight3 = ObjectAnimator.ofFloat(mM4SPhone1, "translationX", 0, -100f);
        ObjectAnimator animatorAlphaRight3 = ObjectAnimator.ofFloat(mM4SPhone1, "alpha", 1f, 0f);
        animatorMoveRight2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mM4SPhone1.setAlpha(0f);
                mM4SPhone1.setVisibility(View.GONE);
            }
        });
        animatorMoveRight3.setStartDelay(300);
        animatorAlphaRight3.setStartDelay(300);

        introAnim.play(animatorAlpha1).with(animatorAlpha2)
                .with(animatorMoveLeft).with(animatorAlphaLeft)
                .with(animatorMoveRight1).with(animatorAlphaRight1)
                .with(animatorMoveRight2).with(animatorAlphaRight2)
                .with(animatorMoveRight3).with(animatorAlphaRight3);

//        ObjectAnimator animator = ObjectAnimator.ofFloat(mM4SIntro, "alpha", 1f, 0f);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mM4SIntro.setAlpha(0f);
//                mM4SIntro.setVisibility(View.GONE);
//            }
//        });
//
//        ObjectAnimator animatorMoveLeft = ObjectAnimator.ofFloat(mM4STab, "translationX", 0, -100);
//        ObjectAnimator animatorAlphaLeft = ObjectAnimator.ofFloat(mM4STab, "alpha", 1f, 0f);
//        animatorAlphaLeft.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mM4STab.setAlpha(0f);
//                mM4STab.setVisibility(View.GONE);
//            }
//        });
//
//        ObjectAnimator animatorMoveRight1 = ObjectAnimator.ofFloat(mM4SS7, "translationX", 0, 100);
//        ObjectAnimator animatorAlphaRight1 = ObjectAnimator.ofFloat(mM4SS7, "alpha", 1f, 0f);
//        animatorMoveRight1.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mM4SS7.setAlpha(0f);
//                mM4SS7.setVisibility(View.GONE);
//            }
//        });
//        animatorMoveRight1.setStartDelay(100);
//        animatorAlphaRight1.setStartDelay(100);
//
//        ObjectAnimator animatorMoveRight2 = ObjectAnimator.ofFloat(mM4SS3, "translationX", 0, 100);
//        ObjectAnimator animatorAlphaRight2 = ObjectAnimator.ofFloat(mM4SS3, "alpha", 1f, 0f);
//        animatorMoveRight2.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mM4SS3.setAlpha(0f);
//                mM4SS3.setVisibility(View.GONE);
//            }
//        });
//
//        introAnim.play(animator).with(animatorMoveLeft).with(animatorAlphaLeft).with(animatorMoveRight1).with(animatorAlphaRight1).with(animatorMoveRight2).with(animatorAlphaRight2);

        return introAnim;
    }

    public void setTitleAlpha(float alpha) {
        mSubcategoryTitle.setAlpha(alpha);
    }
}
