package com.tecace.showcase.checkout;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tecace.showcase.CustomGridView;
import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;
import com.tecace.showcase.ShowcaseApplication;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.ApplicationIndex;
import com.tecace.showcase.data.SubcategoryData;
import com.tecace.showcase.util.CubicBezierInterpolator;

//public class MapAvailabilityLayout extends FrameLayout implements CheckoutViewPager.OnCheckoutPageChangeListener {
public class MapAvailabilityLayout extends FrameLayout {
    private final String TAG = this.getClass().getSimpleName();
    //Todo: IS_DEBUG set false before git checkout.
    private final Boolean IS_DEBUG = false;

    private static final int MESSAGE_SHOW_SEC = 3000;      //3sec

    private AppTableDataManager mAppTableDataManager = null;
    private AppDataManager mAppDataManager = null;

    private FrameLayout parent;

    private SelectedAppAdapter mSelectedAppAdapter = null;
    private DisplayImageOptions options;

    TextView checkoutTitle;
    private final String DOWN = "DOWN";
    private final String UP = "UP";
    CustomGridView selectedAppGridView;
    ImageButton moreButton;

    ImageView viewMapSelection;

    View root;

    private OnMapAvailabilityLayoutInteractionListener mListener = null;

    private int iSelectedMapAvailability = SubcategoryData.SELECTED_REGION_AMERICA;

    ImageMap mImageMap;
    int iMapId = 0;

    public MapAvailabilityLayout(Context context) {
        super(context);
        init(context);
    }

    public MapAvailabilityLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MapAvailabilityLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        parent = (FrameLayout) inflate(getContext(), R.layout.activity_map_availability, this);
        mListener = (OnMapAvailabilityLayoutInteractionListener) (Activity) (super.getContext());
        mAppDataManager = ((ShowcaseApplication) getContext().getApplicationContext()).getAppDataManager();
    }

    public void initLayout(final Context context) {
        options = ((ShowcaseApplication) (getContext().getApplicationContext())).getUILOptions();

        RelativeLayout touchInterceptLayout = (RelativeLayout) parent.findViewById(R.id.mapavailability_touch_intercept_layout);

        checkoutTitle = (TextView) parent.findViewById(R.id.mapavailability_checkout_layout_title);

        selectedAppGridView = (CustomGridView) parent.findViewById(R.id.map_availability_grid);
        selectedAppGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView absListView, int scrollState) {
            }

            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {
                if (moreButton == null) return;
                ((FullscreenActivity) view.getContext()).startAutoResetHandler();

                int offsetY = selectedAppGridView.computeVerticalScrollOffset();
                int extentY = selectedAppGridView.computeVerticalScrollExtent();
                int rangeY = selectedAppGridView.computeVerticalScrollRange();
                rangeY += 32; //paddingbottom 40px (??)

                if (offsetY + extentY == rangeY) {
                    if (moreButton.getTag() != null && !moreButton.getTag().equals(UP)) {
                        moreButton.setTag(UP);
                        moreButton.setImageResource(R.drawable.ic_more_info_opend);
                    }
                    return;
                }
                if (moreButton.getTag() != null && !moreButton.getTag().equals(DOWN)) {
                    moreButton.setTag(DOWN);
                    moreButton.setImageResource(R.drawable.ic_more_info_closed);
                }
            }
        });
        moreButton = (ImageButton) parent.findViewById(R.id.map_availability_more_button);
        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();

                if (moreButton.getTag() != null && moreButton.getTag().equals(DOWN)) {
                    final int lastPosition = selectedAppGridView.getLastVisiblePosition();
                    selectedAppGridView.smoothScrollToPosition(lastPosition + 9);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(lastPosition);
                        }
                    }, 100);
                } else {
                    selectedAppGridView.smoothScrollToPositionFromTop(0, 0);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(0);
                        }
                    }, 100);
                }
            }
        });
        moreButton.setTag(DOWN);

        viewMapSelection = (ImageView) parent.findViewById(R.id.mapavailability_selection_view_all);
        viewMapSelection.setImageResource(R.drawable.map_america);

        // find the image map in the view
        mImageMap = (ImageMap)findViewById(R.id.map);
//?        mImageMap.setImageResource(R.drawable.map_all);

        // add a click handler to react when areas are tapped
        mImageMap.addOnImageMapClickedHandler(new ImageMap.OnImageMapClickedHandler()
        {
            @Override
            public void onImageMapClicked(int id, ImageMap imageMap)
            {
                String sImageMap = mImageMap.getName(id);
                Log.d(TAG, "mImageMap addOnImageMapClickedHandler onImageMapClicked ::: " + sImageMap + " : " + iMapId + " : " + id);

//?                if (id != iMapId) {
                    iMapId = id;

                    Log.d(TAG, "mImageMap addOnImageMapClickedHandler onImageMapClicked1 ::: " + sImageMap + " : " + iMapId);

                    switch ((String) sImageMap) {
                        case "america" :
                            viewMapSelection.setImageResource(R.drawable.map_america);
                            iSelectedMapAvailability  = SubcategoryData.SELECTED_REGION_AMERICA;
                            onReOpenCheckoutWindow();
                            break;
                        case "europe" :
                            viewMapSelection.setImageResource(R.drawable.map_europe);
                            iSelectedMapAvailability  = SubcategoryData.SELECTED_REGION_EUROPE;
                            onReOpenCheckoutWindow();
                            break;
                        case "asia" :
                            viewMapSelection.setImageResource(R.drawable.map_asia);
                            iSelectedMapAvailability  = SubcategoryData.SELECTED_REGION_ASIA;
                            onReOpenCheckoutWindow();
                            break;
                        case "rest" :
                            viewMapSelection.setImageResource(R.drawable.map_rest);
                            iSelectedMapAvailability  = SubcategoryData.SELECTED_REGION_REST;
                            onReOpenCheckoutWindow();
                            break;

                    }

//?                } else {

//?                }
            }

            @Override
            public void onBubbleClicked(int id)
            {
                // react to info bubble for area being tapped
            }
        });

        root = parent.findViewById(R.id.mapavailability_conainter);
        root.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mListener != null)
                        mListener.mapavailability_onSetOpenHeartMode(false, false);
                }
                return true;
            }
        });
        View container = parent.findViewById(R.id.mapavailability_layout);
        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                return true;
            }
        });
    }

    public void setAppTableDataManager(AppTableDataManager appTableDataManager) {
        mAppTableDataManager = appTableDataManager;
        initLayout(getContext());
    }

    public void onReOpenCheckoutWindow() {
        mSelectedAppAdapter = new SelectedAppAdapter(getContext(), mAppDataManager, mAppTableDataManager, iSelectedMapAvailability);
        mSelectedAppAdapter.setUILOption(options);
        selectedAppGridView.setAdapter(mSelectedAppAdapter);
        selectedAppGridView.setOnNoItemPressListener(new CustomGridView.OnNoItemPressListener() {
            @Override
            public void onNoItemLongPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }

            @Override
            public void onNoItemPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }
        });
        selectedAppGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                return false;
            }
        });
        selectedAppGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int iPassionIndex =  ((ApplicationIndex) mSelectedAppAdapter.getItem(position)).getPassionIndex();
                int iSubcategoryIndex =  ((ApplicationIndex) mSelectedAppAdapter.getItem(position)).getSubcategoryIndex();

                Log.d(TAG, "selectedAppGridView setOnItemClickListener onItemClick ::: " + position + " : " + iPassionIndex + " : " + iSubcategoryIndex);

                if (mListener != null)
                    mListener.mapavailability_onSelectedAppItemClick(iPassionIndex, iSubcategoryIndex);

                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }
        });


    }


    public Animator generateShowAnimator() {
        final MapAvailabilityLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_open);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                myself.setVisibility(VISIBLE);
            }
        });
        return animatorSet;
    }

    public Animator generateDismissAnimator() {
        final MapAvailabilityLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_close);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                myself.setVisibility(View.GONE);
            }
        });
        return animatorSet;
    }

    public void reset() {
        viewMapSelection.setImageResource(R.drawable.map_america);
        iSelectedMapAvailability = SubcategoryData.SELECTED_REGION_AMERICA;
        iMapId = 0;
    }

    public interface OnMapAvailabilityLayoutInteractionListener {
        void mapavailability_onSetOpenHeartMode(boolean isOpen, boolean isGoLv1);

        void mapavailability_onSelectedAppItemClick(int PassionIndex, int SubcategoryIndex);
    }
}


