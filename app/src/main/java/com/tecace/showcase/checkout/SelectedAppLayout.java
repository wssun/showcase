package com.tecace.showcase.checkout;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.InputType;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;
import com.tecace.showcase.ShowcaseApplication;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.ApplicationData;
import com.tecace.showcase.display.SubcategoryDisplayLayout;
import com.tecace.showcase.util.CommonAnimUtil;
import com.tecace.showcase.util.CubicBezierInterpolator;
import com.tecace.showcase.util.GMailSender;
import com.tecace.showcase.util.MixPanelUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

//public class SelectedAppLayout extends FrameLayout implements CheckoutViewPager.OnCheckoutPageChangeListener {
public class SelectedAppLayout extends FrameLayout {
    private final String TAG = this.getClass().getSimpleName();
    //Todo: IS_DEBUG set false before git checkout.
    private final Boolean IS_DEBUG = false;

    private static final int MESSAGE_SHOW_SEC = 3000;      //3sec

//?    private AppTableDataManager mAppTableDataManager = null;
//?    private AppDataManager mAppDataManager = null;

    private KeyboardFragment mKeyboardFragment;

    private FrameLayout parent;

   // private CheckoutViewPager mCheckoutViewPager = null;
//?    private SelectedAppAdapter mSelectedAppAdapter = null;
    private ArrayList<Button> arrayIndicator = new ArrayList<Button>();
    private DisplayImageOptions options;

//?    TextView checkoutTitle;
    private final String DOWN = "DOWN";
    private final String UP = "UP";
//?    CustomGridView selectedAppGridView;
    Button sendButton;
//?    ImageButton moreButton;
    EditText emailEditText;

    Handler completeHandler;
    Runnable completeRunnable;

    TextView txtWrongEmail;
    View arrowWrongEmail;

    TextView txtFavoriteEmpty;
    View arrowFavoriteEmpty;

    TextView txtEditSelection;
    View arrowEditSelection;


    View root;
    private boolean isLongPress = false;
    private boolean isShortPress = false;

    private String selectedapptitle= null;

    private OnSelectedAppLayoutInteractionListener mListener = null;

    private String mReceipentEmail = null; // for MixPanel logging

    public SelectedAppLayout(Context context) {
        super(context);
        init(context);
    }

    public SelectedAppLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SelectedAppLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        parent = (FrameLayout) inflate(getContext(), R.layout.activity_selected_app, this);
        mListener = (OnSelectedAppLayoutInteractionListener) (Activity) (super.getContext());
//?        mAppDataManager = ((ShowcaseApplication) getContext().getApplicationContext()).getAppDataManager();
    }

    public void initLayout(final Context context) {
        options = ((ShowcaseApplication) (getContext().getApplicationContext())).getUILOptions();

        RelativeLayout touchInterceptLayout = (RelativeLayout) parent.findViewById(R.id.touch_intercept_layout);

        sendButton = (Button) parent.findViewById(R.id.send);

/*
        checkoutTitle = (TextView) parent.findViewById(R.id.checkout_layout_title);

        selectedAppGridView = (CustomGridView) parent.findViewById(R.id.selected_app_grid);
        selectedAppGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView absListView, int scrollState) {
            }

            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {
                if (moreButton == null) return;
                ((FullscreenActivity) view.getContext()).startAutoResetHandler();

                int offsetY = selectedAppGridView.computeVerticalScrollOffset();
                int extentY = selectedAppGridView.computeVerticalScrollExtent();
                int rangeY = selectedAppGridView.computeVerticalScrollRange();
                rangeY += 32; //paddingbottom 40px (??)

                if (offsetY + extentY == rangeY) {
                    if (moreButton.getTag() != null && !moreButton.getTag().equals(UP)) {
                        moreButton.setTag(UP);
                        moreButton.setImageResource(R.drawable.ic_more_info_opend);
                    }
                    return;
                }
                if (moreButton.getTag() != null && !moreButton.getTag().equals(DOWN)) {
                    moreButton.setTag(DOWN);
                    moreButton.setImageResource(R.drawable.ic_more_info_closed);
                }
            }
        });
        moreButton = (ImageButton) parent.findViewById(R.id.selected_app_more_button);
        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();

                if (moreButton.getTag() != null && moreButton.getTag().equals(DOWN)) {
                    final int lastPosition = selectedAppGridView.getLastVisiblePosition();
                    selectedAppGridView.smoothScrollToPosition(lastPosition + 9);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(lastPosition);
                        }
                    }, 100);
                } else {
                    selectedAppGridView.smoothScrollToPositionFromTop(0, 0);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(0);
                        }
                    }, 100);
                }
            }
        });
        moreButton.setTag(DOWN);
*/
        emailEditText = (EditText) parent.findViewById(R.id.emailEditText);
        int inType = emailEditText.getInputType(); // backup the input type
        emailEditText.setInputType(InputType.TYPE_NULL);
        emailEditText.setTextIsSelectable(false);
        emailEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isLongPress = false;
                    isShortPress = false;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (emailEditText.getText().length() > 0) {
                        if (!isLongPress && emailEditText.isSelected()) {
                            emailEditText.setSelection((int) (emailEditText.getText().length()));
                            emailEditText.setSelected(false);
                            mKeyboardFragment.selectString(false);
                            mKeyboardFragment.setCursorIndex(emailEditText.getText().length());
                            emailEditText.requestFocus();

                        } else if (!isLongPress) {
                            Layout layout = ((EditText) v).getLayout();
                            float x = event.getX() + emailEditText.getScrollX();
                            int offset = layout.getOffsetForHorizontal(0, x);
                            emailEditText.setSelection(offset);
                            mKeyboardFragment.setCursorIndex(offset);
                            emailEditText.requestFocus();
                            isShortPress = true;
                        }
                    }
                    return true;
                }
                return false; // consume touch even
            }
        });

        emailEditText.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (emailEditText.getText().length() > 0 && !isShortPress) {
                    isLongPress = true;
                    emailEditText.setSelection(0, (int) (emailEditText.getText().length()));
                    emailEditText.setSelected(true);
                    mKeyboardFragment.selectString(true);
                }
                return true;
            }
        });

        sendButton = (Button) parent.findViewById(R.id.send);
        //sendButton.setBackgroundResource(R.drawable.button_outline);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FullscreenActivity)getContext()).startAutoResetHandler();

                mReceipentEmail = emailEditText.getText().toString();
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(mReceipentEmail).matches()) {
                    if (false) {//?getOrderItems() == 0) {
                        showFavoriteEmptyMessage();
                    }
                    else {
                        if (!isNetworkConnected()) {  // NetWork Error
                            if (mListener != null)
                                mListener.onErrorOccured();
                        } else {
                            onClickSend(mReceipentEmail);
                        }
                    }
                }else{
                    showWrongEmailMessage();
                }
            }
        });
        emailEditText.setText("");

        root = parent.findViewById(R.id.openheart_conainter);
        root.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mListener != null)
                        mListener.onSetOpenHeartMode(false, false);
                }
                return true;
            }
        });
        View container = parent.findViewById(R.id.openheart_layout);
        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                return true;
            }
        });

        mKeyboardFragment = (KeyboardFragment) mFragmentManager.findFragmentById(R.id.fragment_keyboard);

        emailEditText.setInputType(inType);

        completeHandler = new Handler();
        completeRunnable = new Runnable() {
            @Override
            public void run() {
//                mAppTableDataManager.emptySelectItems();
                mKeyboardFragment.initKeyboard();
                mKeyboardFragment.emptyCharacter();
                if (mListener != null) {
                    mListener.onCompleteSendEmail();
                }
            }
        };
        mKeyboardFragment.initKeyboard();
        mKeyboardFragment.emptyCharacter();
    }

    public void setSendButtonVisivility (boolean isVisible) {
        if(isVisible)
            sendButton.setVisibility(VISIBLE);
        else
            sendButton.setVisibility(GONE);
    }

    public void changePagerIndicator(int position) {
        for (int i = 0; i < arrayIndicator.size(); i++) {
            if (i == position) {
                ((Button) arrayIndicator.get(i)).setBackgroundResource(R.drawable.circle_indicator_focus);
            } else {
                ((Button) arrayIndicator.get(i)).setBackgroundResource(R.drawable.circle_indicator);
            }
        }
    }

    public void updateEditmodeStatus(boolean status) {
        ((FullscreenActivity) getContext()).startAutoResetHandler();

        LinearLayout viewInputbox = (LinearLayout) parent.findViewById(R.id.checkout_inputbox_container);

        if (status) {
//            editButton.setText(R.string.button_title_confirm);
//            backArrowButton.setClickable(false);
//            backArrowButton.setAlpha(0.5f);
            mKeyboardFragment.setInputKeyboardBlock(true);
            emailEditText.setClickable(false);
            emailEditText.clearFocus();
            sendButton.setClickable(false);
            viewInputbox.setAlpha(0.5f);
        } else {
//            editButton.setText(R.string.button_title_edit);
//            backArrowButton.setClickable(true);
//            backArrowButton.setAlpha(1.0f);
            mKeyboardFragment.setInputKeyboardBlock(false);
            emailEditText.setClickable(true);
            sendButton.setClickable(true);
            viewInputbox.setAlpha(1.0f);
        }
    }

    public void setAppTableDataManager(AppTableDataManager appTableDataManager) {
//?        mAppTableDataManager = appTableDataManager;
        initLayout(getContext());
    }

    private android.app.FragmentManager mFragmentManager = null;

    public void setFragmentManager(android.app.FragmentManager fragmentMgr) {
        mFragmentManager = fragmentMgr;
    }

    private android.support.v4.app.FragmentManager mV4FragmentManager = null;

    public void setV4FragmentManager(android.support.v4.app.FragmentManager fragmentManager) {
        mV4FragmentManager = fragmentManager;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivity = (ConnectivityManager) parent.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void updateString(String email, int cursorIndex) {
        emailEditText.requestFocus();
        emailEditText.setText(email);
        emailEditText.setSelection(cursorIndex);
        mKeyboardFragment.selectString(false);

        if( email.equalsIgnoreCase("sysadmin@tecace.com:launch menu") ||
                email.equalsIgnoreCase("@quit") ||
                email.equalsIgnoreCase("@exit") ) {
//            setSendButtonVisivility(false);
//            sendButton.setVisibility(GONE);
//            quitButton.setVisibility(VISIBLE);
            mListener.quitApplication();
            return;
        }else{
            setSendButtonVisivility(true);
//            sendButton.setVisibility(VISIBLE);
            //quitButton.setVisibility(GONE);
        }
    }

    public void enterPressed() {
        ((FullscreenActivity)getContext()).startAutoResetHandler();

        mReceipentEmail = emailEditText.getText().toString();
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(mReceipentEmail).matches()) {
            if (!isNetworkConnected()) {  // NetWork Error
                if (mListener != null)
                    mListener.onErrorOccured();
            } else {
                onClickSend(mReceipentEmail);
            }
        }else{
            showWrongEmailMessage();
        }
    }

    public void onClickSend(String recipient) {
        ((FullscreenActivity) getContext()).stopAutoResetHandler();
        new SendMailTask().execute(recipient);
        mKeyboardFragment.setinputLock(true);
        if (mListener != null) {
            mListener.onSendingEmail();
        }else{
            mListener.onErrorOccured();
        }
    }

    private void showWrongEmailMessage(){
        txtWrongEmail = (TextView) parent.findViewById(R.id.help_checkout_display_wrong_email_message);
        arrowWrongEmail = (View) parent.findViewById(R.id.help_checkout_display_wrong_email_arrow);

        AnimatorSet animReturned = new AnimatorSet();

        ObjectAnimator anim1 = CommonAnimUtil.getAlphaAnim(txtWrongEmail, 0);
        ObjectAnimator anim2 = CommonAnimUtil.getAlphaAnim(arrowWrongEmail, 0);

        ObjectAnimator anim3 = CommonAnimUtil.getFadeOutAnim(txtWrongEmail, 7);
        ObjectAnimator anim4 = CommonAnimUtil.getFadeOutAnim(arrowWrongEmail, 7);

        animReturned.playTogether(anim1, anim2, anim3, anim4);
        animReturned.start();
    }

    private void showFavoriteEmptyMessage() {
        txtFavoriteEmpty = (TextView) parent.findViewById(R.id.help_checkout_display_favroite_empty_message);
        arrowFavoriteEmpty = (View) parent.findViewById(R.id.help_checkout_display_favroite_empty_arrow);

        AnimatorSet animReturned = new AnimatorSet();

        ObjectAnimator anim1 = CommonAnimUtil.getAlphaAnim(txtFavoriteEmpty, 0);
        ObjectAnimator anim2 = CommonAnimUtil.getAlphaAnim(arrowFavoriteEmpty, 0);

        ObjectAnimator anim3 = CommonAnimUtil.getFadeOutAnim(txtFavoriteEmpty, 7);
        ObjectAnimator anim4 = CommonAnimUtil.getFadeOutAnim(arrowFavoriteEmpty, 7);

        animReturned.playTogether(anim1, anim2, anim3, anim4);
        animReturned.start();
    }

    private void showEditSelectionMessage() {
        txtEditSelection = (TextView) parent.findViewById(R.id.help_checkout_edit_selection_message);
        arrowEditSelection = (View) parent.findViewById(R.id.help_checkout_edit_selection_arrow);

        AnimatorSet animReturned = new AnimatorSet();

        ObjectAnimator anim1 = CommonAnimUtil.getAlphaAnim(txtEditSelection, 0);
        ObjectAnimator anim2 = CommonAnimUtil.getAlphaAnim(arrowEditSelection, 0);

        ObjectAnimator anim3 = CommonAnimUtil.getFadeOutAnim(txtEditSelection, 4);
        ObjectAnimator anim4 = CommonAnimUtil.getFadeOutAnim(arrowEditSelection, 4);

        animReturned.playTogether(anim1, anim2, anim3, anim4);
        animReturned.start();
    }

    public void appendEmailCharacter(char c) {
        emailEditText.append(c + "");
    }

    public void clearEmailEditText() {
        emailEditText.setText("");
    }

    private String makeEmailBody() {
        StringBuffer passionPart = new StringBuffer();
        int prevItemIndex = 0;
        int temp = -1;
/*
        for (ApplicationIndex app : mAppTableDataManager.getOrderdSelectedAppItems()) {
            ApplicationData appInfo = mAppDataManager.getPassionDataAt(app.getPassionIndex()).getSubcategoryDataAt(app.getSubcategoryIndex()).getAppplicationDataAt(app.getApplicationIndex());

            if (temp != app.getPassionIndex()) {
                if(temp != -1) {
                    if(prevItemIndex%4 != 0) {

                        switch (prevItemIndex%4) {
                            case 1 :
                                passionPart.append("<td width='90'><br/></td>");
                                passionPart.append("<td width='63'><br/></td>");
                                passionPart.append("<td width='90'><br/></td>");
                                passionPart.append("<td width='63'><br/></td>");
                                passionPart.append("<td width='90'><br/></td>");
                                break;
                            case 2 :
                                passionPart.append("<td width='90'><br/></td>");
                                passionPart.append("<td width='63'><br/></td>");
                                passionPart.append("<td width='90'><br/></td>");
                                break;
                            case 3 :
                                passionPart.append("<td width='90'><br/></td>");
                                break;
                        }
                        passionPart.append("</tr>");
                        passionPart.append("<tr><td colspan='7' height='44'><br/></td></tr>");
                    }
                    passionPart.append("</tbody>");
                    passionPart.append("</table>");
                    passionPart.append("</td>");
                    passionPart.append("</tr>");
                    prevItemIndex = 0;
                }
                passionPart.append("<tr>");
                passionPart.append("<td bgcolor='#012654' height='20'><br></td>");
                passionPart.append("</tr>");
                passionPart.append("<tr>");
                passionPart.append("<td bgcolor='#011738'>");
                passionPart.append("<table id='intro' class='inner' width='624' border='0' cellpadding='0' cellspacing='0' style='empty-cells:show;'>");//padding:15px;
                passionPart.append("<tbody>");
                passionPart.append("<tr><td colspan='7' height='10'><br></td></tr>");
                passionPart.append("<tr>");
                passionPart.append("<td width='44'><br/></td>");
                passionPart.append("<td colspan='6' id='device-name' style='font-family:Helvetica, Arial, Tahoma, Verdana; font-size:26px; color:white;'>" + mAppDataManager.getPassionsLabels()[app.getPassionIndex()] + "</td>");
                passionPart.append("</tr>");
                passionPart.append("<tr><td colspan='7' height='44'><br/></td></tr>");
                temp = app.getPassionIndex();
            }
            //Log.d(TAG, "App Category prevItemIndex%4 ::: " + prevItemIndex%4 );
            switch (prevItemIndex%4) {
                case 0 : //0 column
                    passionPart.append("<tr style='text-align:left'>");
                    passionPart.append("<td width='44'><br/></td>");
                    passionPart.append("<td width='100' align='center'>");
                    //passionPart.append("<a href='"+ appInfo.getDownloadLink() +"' target='_blank' style='display:inner-block;' >");
                    passionPart.append(makeAppContent(appInfo));
                    passionPart.append("</td>");
                    passionPart.append("<td width='44'><br/></td>");
                    break;
                case 1 :
                case 2 :
                    passionPart.append("<td width='100' align='center'>");
                    //passionPart.append("<a href='"+ appInfo.getDownloadLink() +"' target='_blank' style='display:inner-block;' >");
                    passionPart.append(makeAppContent(appInfo));
                    passionPart.append("</td>");
                    passionPart.append("<td width='44'><br/></td>");
                    break;
                case 3 : //3 Column
                    passionPart.append("<td width='100' align='center'>");
                    passionPart.append(makeAppContent(appInfo));
                    passionPart.append("</td>");
                    passionPart.append("<td width='44'><br/></td>");
                    passionPart.append("</tr>");
                    passionPart.append("<tr><td colspan='7' height='44'><br/></td></tr>");
                    break;
            }
            prevItemIndex++;
        }
*/

        if(prevItemIndex%4 != 0) {
            switch (prevItemIndex%4) {
                case 1 :
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    break;
                case 2 :
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    break;
                case 3 :
                    passionPart.append("<td width='100'><br/></td>");
                    passionPart.append("<td width='44'><br/></td>");
                    break;
            }
            passionPart.append("</tr>");
            passionPart.append("<tr><td colspan='7' height='44'><br/></td></tr>");
        }

        passionPart.append("</tbody>");
        passionPart.append("</table>");
        passionPart.append("</td>");
        passionPart.append("</tr>");

        return passionPart.toString();
    }

    public String makeAppContent(ApplicationData appInfo) {
        String appName;
        if(appInfo.getAppName().length() > 12){
            appName = appInfo.getAppName().substring(0, 11)+"..";
        }else{
            appName = appInfo.getAppName();
        }

        StringBuffer appContent = new StringBuffer();
        appContent.append("<table width='100' border='0' cellpadding='0' cellspacing='0' style='table-layout:fixed;'>");
        appContent.append("<tr><td class='bg_image' style='display:block; border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;'>");
        appContent.append("<a href='"+ appInfo.getDownloadLink() +"' target='_blank' style='display:inner-block;' >");
        appContent.append("<img width='90' height='90' src='" + appInfo.getSharedIconUrl() +"' alt='" + appInfo.getAppName()+"" + " border='0'" +
                " style='display:block; border-style:none; border:none; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;'/>");
        appContent.append("</a></td></tr>");
        appContent.append("<tr>");
        appContent.append("<td style='font-family:Helvetica, Arial, Tahoma, Verdana; text-align:center; font-size:11px; color:white; padding-top:12px; overflow:hidden; text-overflow: ellipsis; white-space:nowrap;" +
                " border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;'>");
        appContent.append(appName);
        appContent.append("</td></tr></table>");
        return appContent.toString();
    }

    public String makeEmailFooter() {
        StringBuffer emailfooter = new StringBuffer();
        emailfooter.append("<tr><td bgcolor='#012654' height='20'><br /></td></tr>");
        emailfooter.append("<tr>"+
                "<td bgcolor='#012654' align='center' style='font-family:Helvetica, Arial, Tahoma, Verdana; font-weight:bold; font-size:11px; text-align:center; color:white;"
                + " -ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;'>Follow US:</td></tr>");
        emailfooter.append("<tr><td bgcolor='#012654' height='15'><br /></td></tr>");
        emailfooter.append("<tr>");
        emailfooter.append("<td bgcolor='#012654' style='text-align:center;'>");
        emailfooter.append("<table id='sns-wrapper' width='auto' align='center' border='0' cellpadding='0' cellspacing='0'>");
        emailfooter.append("<tr>");
        emailfooter.append("<td bgcolor='#012654'>");
        emailfooter.append("<a href='https://www.facebook.com/#!/SamsungMobileUSA' style='order:collapse;'>");
        emailfooter.append("<img src='https://sra-resources.s3.amazonaws.com/public/837Resources/facebook.png' width='24' height='24' style='border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;' />");
        emailfooter.append("</a>");
        emailfooter.append("</td>");
        emailfooter.append("<td bgcolor='#012654' width='15'><br /></td>");
        emailfooter.append("<td bgcolor='#012654'>");
        emailfooter.append("<a href='https://twitter.com/SamsungMobileUS' style='order:collapse;'>");
        emailfooter.append("<img alt='twitter' src='https://sra-resources.s3.amazonaws.com/public/837Resources/twitter.png' width='24' height='24' style='border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;' />");
        emailfooter.append("</a>");
        emailfooter.append("</td>");
        emailfooter.append("<td bgcolor='#012654' width='15'><br /></td>");
        emailfooter.append("<td bgcolor='#012654'>");
        emailfooter.append("<a href='https://instagram.com/samsungmobileusa/?hl=en' style='order:collapse;'>");
        emailfooter.append("<img alt='instagram' src='https://sra-resources.s3.amazonaws.com/public/837Resources/instagram.png' width='24' height='24' style='border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;' />");
        emailfooter.append("</a>");
        emailfooter.append("</td>");
        emailfooter.append("</tr></table></td></tr>");
        emailfooter.append("<tr><td bgcolor='#012654' height='20'><br /></td></tr>");
        emailfooter.append("<tr>");
        emailfooter.append("<td bgcolor='#012654' style='font-family:Helvetica, Arial, Tahoma, Verdana; font-size:11px;" +
                " text-align:center; color:#ffffff; -ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;'>");
        emailfooter.append("&quot;We’re Samsung 837, and we’re open for everything. For art, for music, for sport—and for the magic that happens<br />when culture and technology come together under the same roof.  Our most innovative devices are only part of the story.<br /> We’re here to show off the experiences they unlock, and the people and places they connect us to.&quot;<br /><br />In the heart of the Meatpacking District - 837 Washington Street - New York NY 10014");
        emailfooter.append("</td>");
        emailfooter.append("</tr>");
        emailfooter.append("<tr><td bgcolor='#012654' height='18'><br /></td></tr>");
        emailfooter.append("<tr>");
        emailfooter.append("<td bgcolor='#012654' style='font-family:Helvetica, Arial, Tahoma, Verdana; font-size:11px;" +
                " text-align:center; color:#ffffff; -ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;'>");
        emailfooter.append("2016 &copy; Samsung Electronics");
        emailfooter.append("</td>");
        emailfooter.append("</tr>");
        emailfooter.append("<tr><td bgcolor='#012654' height='18'><br /></td></tr>");
        emailfooter.append("</tbody></table>");
        emailfooter.append("</td></tr>");
        emailfooter.append("</tbody></table>");
        emailfooter.append("</body></html>");
        return emailfooter.toString();

    }

    private String makeNewEmailContent() {
        StringBuffer passionPart = new StringBuffer();
        int sizeSelectedApp = 0;/*mAppTableDataManager.getOrderdSelectedAppItems().size();*/

        if( sizeSelectedApp < 10){
            passionPart.append("<div class='passion' style='max-width:640px; '>");
            passionPart.append("<table border='0' style='border-collapse: collapse;empty-cells:show; margin-bottom:10px; width:100%;'>");
            passionPart.append("<tr style='width:100%;'>");
            passionPart.append("<td style='width:100%; padding-bottom: 5px;' >");
                passionPart.append("<table border='0' style='width:97%; margin:5px 0px 0px 5px; min-width: 313px; border-collapse:collpase; empty-cells:show; align:left; float:left; background-color:#000030; '>");
                passionPart.append("<tr><td colspan='2' style='padding-left:2%;'><br /><span style='color:#FFF;font-size:1.5em;padding-top:10px;font-weight:normal;'>&nbsp;&nbsp;Your Collection</span></td></tr>");
                passionPart.append("<tr>");
                passionPart.append("<td style='vertical-align:top; text-align:center; padding:3%; text-align:center; '>");
                passionPart.append("<div class='application_content'");
                passionPart.append(" style='margin:0px auto; display:inner-block; ' >");

/*
                for (ApplicationIndex app : mAppTableDataManager.getOrderdSelectedAppItems()) {
                    ApplicationData appInfo = mAppDataManager.getPassionDataAt(app.getPassionIndex()).getSubcategoryDataAt(app.getSubcategoryIndex()).getAppplicationDataAt(app.getApplicationIndex());

                    String appName;
                    if(appInfo.getAppName().length() > 12){
                        appName = appInfo.getAppName().substring(0, 11)+"..";
                    }else{
                        appName = appInfo.getAppName();
                    }

                    passionPart.append("<a href='"+appInfo.getDownloadLink()+"'");
                    passionPart.append("style='display:inner-block; min-width:29%; max-width:31%;  color:#FFF; text-decoration:none;");
                    passionPart.append("margin: 1%; margin-bottom:20px; font-size:0.9em;");
                    passionPart.append("float:left; '><img style='width:90px' src='"+appInfo.getSharedIconUrl()+"' alt='"+appInfo.getAppName()+"' /><br /><span style='display:block; margin-top:5px;overflow:hidden; ' >"+appName+"</span></a>");
                }
*/

                passionPart.append("<div style='clear:both;'></div>");
                passionPart.append("</div>");
                passionPart.append("</td>");
                passionPart.append("</tr>");
                passionPart.append("</table>");
            passionPart.append("</td>");
            passionPart.append("</tr>");
            passionPart.append("</table>");
            passionPart.append("</div>");
        } else {
            int prevIndex = -1;
            boolean isOdd = true;

/*
            for (ApplicationIndex app : mAppTableDataManager.getOrderdSelectedAppItems()) {
                ApplicationData appInfo = mAppDataManager.getPassionDataAt(app.getPassionIndex()).getSubcategoryDataAt(app.getSubcategoryIndex()).getAppplicationDataAt(app.getApplicationIndex());

                if (prevIndex != app.getPassionIndex()) {
                    if (prevIndex != -1) {
                        passionPart.append("<div style='clear:both;'></div>");
                        passionPart.append("</div>");
                        passionPart.append("</td>");
                        passionPart.append("</tr>");
                        passionPart.append("</table>");
                        Log.d(TAG, "inner table end");
                        isOdd = !isOdd;
                    }else{

                    }
                    if(isOdd){
                        if(prevIndex != -1) {
                            passionPart.append("</td>");
                            passionPart.append("</tr>");
                            passionPart.append("</table>");
                            passionPart.append("</div>");
                        }

                        passionPart.append("<div class='passion' style='max-width:640px; '>");
                        passionPart.append("<table border='0' style='border-color:red; border-collapse: collapse;empty-cells:show; margin-bottom:10px; width:100%;'>");
                        passionPart.append("<tr style='width:100%;'>");
                        passionPart.append("<td style='width:100%; padding-bottom: 5px;' >");

                        passionPart.append("<table border='0' style='width:47%; min-width: 313px; margin:5px 0px 0px 5px; border-collapse:collpase; empty-cells:show; align:left; float:left; background-color:#030030; '>");
                        passionPart.append("<tr><td colspan='2'><span style='color:#FFF; font-size: 1.3em; line-height:250%;'>&nbsp;&nbsp; "+mAppDataManager.getPassionsLabels()[app.getPassionIndex()]+"</span></td></tr>");
                        passionPart.append("<tr>");
                            passionPart.append("<td style='vertical-align:top; text-align:center; padding:3%; text-align:center; '>");
                                passionPart.append("<div class='application_content'");
                                passionPart.append(" style='margin:0px auto; display:inner-block; ' >");
                    }else{
                        passionPart.append("<table border='0' style='width:47%; min-width: 313px; margin:5px 0px 0px 5px; border-collapse:collpase; empty-cells:show; align:left; float:left; background-color:#030030; '>");
//                        passionPart.append("<table border='0' style='min-width:310px; max-width: 313px; border-collapse:collpase; empty-cells:show; align:right; float:left; background-color:#000030; width:*; '>");
                        passionPart.append("<tr><td colspan='2'><span style='color:#FFF; font-size: 1.3em; line-height:250%;'>&nbsp;&nbsp; "+mAppDataManager.getPassionsLabels()[app.getPassionIndex()]+"</span></td></tr>");
                        passionPart.append("<tr>");
                             passionPart.append("<td style='vertical-align:top; text-align:center; padding:3%; text-align:center; '>");
                                passionPart.append("<div class='application_content'");
                                passionPart.append(" style='margin:0px auto; display:inner-block; ' >");
                    }
                }

                String appName;
                if(appInfo.getAppName().length() > 12){
                    appName = appInfo.getAppName().substring(0, 11)+"..";
                }else{
                    appName = appInfo.getAppName();
                }
                passionPart.append("<a href='"+appInfo.getDownloadLink()+"'");
                passionPart.append(" style='display:inner-block; min-width:28%; max-width:30%; color:#FFF; text-decoration:none; ");
                passionPart.append(" margin: 3px; margin-bottom:20px; font-size:0.9em;");
                passionPart.append(" float:left; '><img style='width:80px; height:80px;' src='"+appInfo.getSharedIconUrl()+"' alt='"+appInfo.getAppName()+"' /><span style='display:block; overflow:hidden; ' >"+appName+"</span></a>");

                prevIndex = app.getPassionIndex();

            }
*/

            passionPart.append("</td>");
            passionPart.append("</tr>");
            passionPart.append("</table>");
            passionPart.append("</div>");
        }
        return passionPart.toString();
    }

    private String makeEmailContent() {
        StringBuffer passionPart = new StringBuffer();
        int prevIndex = -1;

/*
        for (ApplicationIndex app : mAppTableDataManager.getOrderdSelectedAppItems()) {
            ApplicationData appInfo = mAppDataManager.getPassionDataAt(app.getPassionIndex()).getSubcategoryDataAt(app.getSubcategoryIndex()).getAppplicationDataAt(app.getApplicationIndex());

            if (prevIndex != app.getPassionIndex()) {
                if (prevIndex != -1) {
                    passionPart.append("</table>");
                    passionPart.append("</div>");
                }
                passionPart.append("<div class='passion' style='max-width:640px;'>");
                String hexColor = String.format("#%06X", (0xFFFFFF & mAppDataManager.getPassionsColors()[app.getPassionIndex()]));
                passionPart.append("<div style='background-color:" + hexColor + "; width: 100%; font-weight: bold; color: #fff; padding: 6px 0px; vertical-align:text-bottom; line-height:100%;'>&nbsp;&nbsp;&nbsp;" + mAppDataManager.getPassionsLabels()[app.getPassionIndex()] + "</div>");
                passionPart.append("<table style='border-collapse: collapse;empty-cells:show; margin-bottom:10px; width:100%;'>");
                passionPart.append("<tr style='width:100%;'>");
            } else {
                passionPart.append("<tr style='border-top: 1px solid #999; width:100%;'>");
            }

            passionPart.append("<td style='width:100%;' >");
            passionPart.append("<table border='0' style='width:49%; border-collapse:collpase; empty-cells:show; align:left; float:left;'>");
            passionPart.append("<tr>");
            passionPart.append("<td style='vertical-align:top;'>");
            if (null != appInfo.getSharedScreenShotUrls()) {
                passionPart.append("<img style='padding: 5px;' src='" + appInfo.getSharedScreenShotUrls().get(0) + "' alt='" + appInfo.getAppName() + "' />");
            } else {
                passionPart.append("<img style='padding: 5px;' src='http://000.png' alt='" + appInfo.getAppName() + "' />");
            }
            passionPart.append("</td>");
            passionPart.append("</tr>");
            passionPart.append("</table>");

            passionPart.append("<table border='0' style='width:*; max-width: 316px; border-collapse:collpase; empty-cells:show; align:right; float:left;'>");
            passionPart.append("<tr>");
            passionPart.append("<td style='vertical-align:top; color:#000;'>");
            passionPart.append("<span style='color:#000; display:block; min-width:306px; width:100%; font-weight:bold; padding-top:5px; padding-bottom:5px;'>");
            passionPart.append(appInfo.getAppName() + "</span>");
            passionPart.append("<span style='display:block; min-width:306px; width:100%;'>");
            passionPart.append(appInfo.getMobileDescription() + "</span>");
            if (appInfo.getVideoUrls().size() > 0) {
                passionPart.append("<a href='" + appInfo.getVideoUrls().get(0) + "'");
                passionPart.append(" style='display:block; text-decoration:none; border:0px solid black; color: #000; line-height:100%; padding-top:5px; vertical-align: text-bottom;'>");
                passionPart.append("<img src='http://182.162.90.124/stable/images/bot_sns_1_mini.png' ");
                passionPart.append(" style='width:16px; height:16px; padding-right:5px; line-height:100%; vertical-align:text-bottom;' alt='app_video' />");
                passionPart.append("WATCH VIDEO</a>");
            }

            passionPart.append("</td>");
            passionPart.append("</tr>");
            passionPart.append("</table>");

            passionPart.append("</td>");
            passionPart.append("</tr>");
            prevIndex = app.getPassionIndex();
        }
*/

        passionPart.append("</table>");
        passionPart.append("</div>");
        return passionPart.toString();
    }

    private class SendMailTask extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            String recipient = params[0];
            if (IS_DEBUG) {
                recipient = recipient + ",tecacetable@gmail.com";
            }

            boolean success;
            try {
                GMailSender sender = new GMailSender(
                        "iheartappcollection@gmail.com",
                        "Samsung#1");
//                    sender.addAttachment(Environment.getExternalStorageDirectory().getPath()+"/image.jpg");

                success = sender.sendMail("Enjoy Your Apps!",
                        readFile("html/header4.html") + makeEmailBody() + makeEmailFooter(), // + readFile("html/footer3.html"),
                        "iheartappcollection@gmail.com",
                        recipient);
                return success;
            } catch (Exception e) {
                Log.e(TAG, Arrays.toString(e.getStackTrace()));
                return false;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Boolean isSuccess) {
            if (isSuccess) {
                if (mListener != null) {
                    mListener.onSentEmail();
                    completeHandler.postDelayed(completeRunnable, 3000);
                }
                MixPanelUtil.page_view(MixPanelUtil.pageview_Display_confirmation_message);

/*
                for (ApplicationIndex appIndex : mAppTableDataManager.getSelectedAppItems()) {
                    MixPanelUtil.download(mAppDataManager.getPassionDataAt(appIndex.getPassionIndex())
                            .getSubcategoryDataAt(appIndex.getSubcategoryIndex())
                            .getAppplicationDataAt(appIndex.getApplicationIndex())
                            .getAppName());
                }
*/

                MixPanelUtil.confirmexport(mReceipentEmail);
                mReceipentEmail = null;
            } else {
                if (mListener != null) {
                    mListener.onErrorOccured();
                }
            }
            mKeyboardFragment.setinputLock(false);
        }
    }

    private String readFile(String assetName) {
        StringBuilder buf = new StringBuilder();
        BufferedReader in;
        InputStream isAssetFile;
        String str;
        try {
            isAssetFile = (InputStream) (getContext().getAssets().open(assetName));
            in = new BufferedReader(new InputStreamReader(isAssetFile, "UTF-8"));
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
            isAssetFile.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e(TAG, "readFile() = " + buf.toString());
        return buf.toString();
    }

    OnLongClickListener mEditTextLongClickListener = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            Log.d(TAG, "edit text long click");
            if (emailEditText.getText().length() > 0) {
                emailEditText.setSelection(0, (int) (emailEditText.getText().length()));
                emailEditText.setSelected(true);
                mKeyboardFragment.selectString(true);
                isLongPress = true;
            }
            return true;
        }
    };

    private int[] loc = new int[2];

    public Point getCenterOfSendButton() {
        sendButton.getLocationOnScreen(loc);
        return new Point(loc[0] + sendButton.getMeasuredWidth() / 2, loc[1] + sendButton.getMeasuredHeight() / 2);
    }

    public void pressedCenterOfSendButton(boolean pressed) {
        sendButton.setPressed(pressed);
    }

    public void onReOpenCheckoutWindow() {
        updateEditmodeStatus(false);
        mKeyboardFragment.initKeyboard();

/*
        checkoutTitle.setText(getTitle());
        if(mAppTableDataManager.getSelectedAppItems().size() > 18){
            selectedAppGridView.setClipToPadding(false);
            selectedAppGridView.setPadding(0, 0, 0, 40);
            moreButton.setVisibility(VISIBLE);

        }else
            moreButton.setVisibility(GONE);

        mSelectedAppAdapter = new SelectedAppAdapter(getContext(), mAppDataManager, mAppTableDataManager);
        mSelectedAppAdapter.setUILOption(options);
        mSelectedAppAdapter.setListener(this);
        selectedAppGridView.setAdapter(mSelectedAppAdapter);
        selectedAppGridView.setOnNoItemPressListener(new CustomGridView.OnNoItemPressListener() {
            @Override
            public void onNoItemLongPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (mSelectedAppAdapter.getEditMode()) {
                    updateSelectedAppList();
                    checkoutTitle.setText(getTitle());
                    if (mListener != null)
                        mListener.updateSelectedAppList();
                    mSelectedAppAdapter.setEditMode(false);
                    mSelectedAppAdapter.notifyDataSetChanged();
                } else {
                    mSelectedAppAdapter.setEditMode(true);
                    mSelectedAppAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNoItemPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }
        });
        selectedAppGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (mSelectedAppAdapter.getEditMode()) {
                    updateSelectedAppList();
                    checkoutTitle.setText(getTitle());
                    if (mListener != null)
                        mListener.updateSelectedAppList();
                    mSelectedAppAdapter.setEditMode(false);
                    mSelectedAppAdapter.notifyDataSetChanged();
                } else {
                    mSelectedAppAdapter.setEditMode(true);
                    mSelectedAppAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        selectedAppGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (mSelectedAppAdapter.getEditMode()) {
                    if (((ApplicationIndex) mSelectedAppAdapter.getItem(position)).getDeleteMede()) {
                        ((ApplicationIndex) mSelectedAppAdapter.getItem(position)).setDeleteMode(false);
                        checkoutTitle.setText(getTitle());
                        if (mListener != null) {
                            mListener.updateOrderdSelectedAppItems(getOrderItems());
                        }
                        mSelectedAppAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
*/
    }

    public Animator generateShowAnimator() {
        final SelectedAppLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_open);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                myself.setVisibility(VISIBLE);
            }
        });
        return animatorSet;
    }

    public Animator generateDismissAnimator() {
        final SelectedAppLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_close);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                myself.setVisibility(View.GONE);
            }
        });
        return animatorSet;
    }

    public interface OnSelectedAppLayoutInteractionListener {
        void onSetOpenHeartMode(boolean isOpen, boolean isGoLv1);

        void onSendingEmail();

        void onSentEmail();

        void onCompleteSendEmail();

        void onErrorOccured();

        void quitApplication();
    }
}
