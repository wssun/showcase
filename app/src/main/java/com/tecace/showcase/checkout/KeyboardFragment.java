package com.tecace.showcase.checkout;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link KeyboardFragment.OnKeyboardFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link KeyboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KeyboardFragment extends Fragment {
    private final String TAG = this.getClass().getCanonicalName();
    private static final int DELETE_KEY_INTERVAL_TIME = 100;
    private StringBuffer inputStrBuf = new StringBuffer();

    private String mParam1;
    private String mParam2;
    private boolean isAlphabet = true;
    private boolean isShiftOn = false;
    //private boolean isShiftLock = false;        //for check pressed Shift twice sequentially
    private boolean isInputLock = false;

    private boolean isBlocked = false;
    private LinearLayout keyboardContainer4touchintercept;

    private static final int KEYBOARD_ALPHABET_LOWER_MODE = 0;
    private static final int KEYBOARD_ALPHABET_UPPER_MODE = 1;
    private static final int KEYBOARD_NUMBERIC_LOWER_MODE = 2;
    //private static final int KEYBOARD_NUMBERIC_UPPER_MODE = 3;
    private static int kyeboardMode = 0;

    private static boolean isSelecteText = false;

    private RelativeLayout alphabet_keyboard_layout;
    private FrameLayout keyboard_panel;
    Button changeModeButton_alphabet;
    ImageButton shiftKey;
    TextView shiftKeyText;
    private int mCursorIndex = 0;

    private OnKeyboardFragmentInteractionListener mListener;

    public static KeyboardFragment newInstance(String param1, String param2) {
        KeyboardFragment fragment = new KeyboardFragment();
        return fragment;
    }

    public KeyboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void changeText(ViewGroup group, boolean toUppercase) {
        int childCnt = group.getChildCount();
        for (int i = 0; i < childCnt; i++) {
            View child = group.getChildAt(i);
            if (child instanceof Button) {
                if (toUppercase) {
                    ((Button) child).setText(((Button) child).getText().toString().toUpperCase());
                } else {
                    ((Button) child).setText(((Button) child).getText().toString().toLowerCase());
                }
            } else if (child instanceof ViewGroup)
                changeText((ViewGroup) child, toUppercase);
        }
    }

    private View.OnClickListener makeOnClickListener(final String[] keyValue) {
        return new View.OnClickListener() {
            String[] values = keyValue;

            @Override
            public void onClick(View v) {
                ((FullscreenActivity)alphabet_keyboard_layout.getContext()).startAutoResetHandler();
                appendCharacter(values[kyeboardMode], mCursorIndex);

                //if (!isShiftLock && isAlphabet) {
                if(isShiftOn && isAlphabet) {
                    kyeboardMode = KEYBOARD_ALPHABET_LOWER_MODE;
                    isShiftOn = false;
                    changeText(alphabet_keyboard_layout, false);   //false mean isShiftOn
                    offShift();
                }
            }
        };
    }

    public void setInputKeyboardBlock(boolean isBlock){
        isBlocked = isBlock;
        if(isBlock){
            keyboard_panel.setAlpha(0.5f);
            keyboardContainer4touchintercept.setVisibility(View.VISIBLE);
            keyboardContainer4touchintercept.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //for Block Keyboard...
                    return true;
                }
            });
        }else{
            keyboard_panel.setAlpha(1.0f);
            keyboardContainer4touchintercept.setOnTouchListener(null);
            keyboardContainer4touchintercept.setVisibility(View.GONE);
        }
    }

    public void setCursorIndex(int index) {
        mCursorIndex = index;
    }

    public int getCursorIndex() {
        return mCursorIndex;
    }

    Button[][] btnKeyboard = null;
    String[][][] keys = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        isAlphabet = true;
        isShiftOn = false;
        //isShiftLock = false;
        kyeboardMode = 0;

        // Layout Alphabetical
        alphabet_keyboard_layout = (RelativeLayout) inflater.inflate(R.layout.alphabetical_keyboard_layout, null);
        keyboard_panel = (FrameLayout) alphabet_keyboard_layout.findViewById(R.id.alphabet_framelayout);
        keyboardContainer4touchintercept = (LinearLayout) alphabet_keyboard_layout.findViewById(R.id.keyboard_touch_block_container);

        // Add ActionListener on Characters
        //Number keys
        final Button oneKey = (Button) alphabet_keyboard_layout.findViewById(R.id.onekey);
        final Button twoKey = (Button) alphabet_keyboard_layout.findViewById(R.id.twokey);
        final Button threeKey = (Button) alphabet_keyboard_layout.findViewById(R.id.threekey);
        final Button fourKey = (Button) alphabet_keyboard_layout.findViewById(R.id.fourkey);
        final Button fiveKey = (Button) alphabet_keyboard_layout.findViewById(R.id.fivekey);
        final Button sixKey = (Button) alphabet_keyboard_layout.findViewById(R.id.sixkey);
        final Button sevenKey = (Button) alphabet_keyboard_layout.findViewById(R.id.sevenkey);
        final Button eightKey = (Button) alphabet_keyboard_layout.findViewById(R.id.eightkey);
        final Button nineKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ninekey);
        final Button zeroKey = (Button) alphabet_keyboard_layout.findViewById(R.id.zerokey);

        // Alphabet keys
        final Button qKey = (Button) alphabet_keyboard_layout.findViewById(R.id.qkey);
        final Button wKey = (Button) alphabet_keyboard_layout.findViewById(R.id.wkey);
        final Button eKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ekey);
        final Button rKey = (Button) alphabet_keyboard_layout.findViewById(R.id.rkey);
        final Button tKey = (Button) alphabet_keyboard_layout.findViewById(R.id.tkey);
        final Button yKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ykey);
        final Button uKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ukey);
        final Button iKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ikey);
        final Button oKey = (Button) alphabet_keyboard_layout.findViewById(R.id.okey);
        final Button pKey = (Button) alphabet_keyboard_layout.findViewById(R.id.pkey);

        final Button aKey = (Button) alphabet_keyboard_layout.findViewById(R.id.akey);
        final Button sKey = (Button) alphabet_keyboard_layout.findViewById(R.id.skey);
        final Button dKey = (Button) alphabet_keyboard_layout.findViewById(R.id.dkey);
        final Button fKey = (Button) alphabet_keyboard_layout.findViewById(R.id.fkey);
        final Button gKey = (Button) alphabet_keyboard_layout.findViewById(R.id.gkey);
        final Button hKey = (Button) alphabet_keyboard_layout.findViewById(R.id.hkey);
        final Button jKey = (Button) alphabet_keyboard_layout.findViewById(R.id.jkey);
        final Button kKey = (Button) alphabet_keyboard_layout.findViewById(R.id.kkey);
        final Button lKey = (Button) alphabet_keyboard_layout.findViewById(R.id.lkey);
        shiftKey = (ImageButton) alphabet_keyboard_layout.findViewById(R.id.shiftButton);
        shiftKeyText = (TextView) alphabet_keyboard_layout.findViewById(R.id.shiftButtonText);
//        final ImageButton shiftKey = (ImageButton)alphabet_keyboard_layout.findViewById(R.id.shiftButton);

        final Button zKey = (Button) alphabet_keyboard_layout.findViewById(R.id.zkey);
        final Button xKey = (Button) alphabet_keyboard_layout.findViewById(R.id.xkey);
        final Button cKey = (Button) alphabet_keyboard_layout.findViewById(R.id.ckey);
        final Button vKey = (Button) alphabet_keyboard_layout.findViewById(R.id.vkey);
        final Button bKey = (Button) alphabet_keyboard_layout.findViewById(R.id.bkey);
        final Button nKey = (Button) alphabet_keyboard_layout.findViewById(R.id.nkey);
        final Button mKey = (Button) alphabet_keyboard_layout.findViewById(R.id.mkey);
        final Button commaKey = (Button) alphabet_keyboard_layout.findViewById(R.id.comma);
        final Button apostropheKey = (Button) alphabet_keyboard_layout.findViewById(R.id.apostrophe);

        final ImageButton delKey = (ImageButton) alphabet_keyboard_layout.findViewById(R.id.deleteButton);
        final Button atKey = (Button) alphabet_keyboard_layout.findViewById(R.id.at);
        final Button alphabet_spaceKey = (Button) alphabet_keyboard_layout.findViewById(R.id.alphabet_space);
        final Button semicolonKey = (Button) alphabet_keyboard_layout.findViewById(R.id.semicolon);
        final Button dotKey = (Button) alphabet_keyboard_layout.findViewById(R.id.dot);
        final Button dotcomKey = (Button) alphabet_keyboard_layout.findViewById(R.id.dotcom);
        final ImageButton enterKey = (ImageButton) alphabet_keyboard_layout.findViewById(R.id.enter);

        keys = new String[][][]{
                {
                        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
                        {"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"},     //10
                        {"a", "s", "d", "f", "g", "h", "j", "k", "l"},          //9
                        {"z", "x", "c", "v", "b", "n", "m",",","'"},                    //7
                        {"@", " ",";", ".",".com"}        //other
                },
                {
                        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
                        {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"},
                        {"A", "S", "D", "F", "G", "H", "J", "K", "L"},
                        {"Z", "X", "C", "V", "B", "N", "M",",","'"},
                        {"@", " ",";", ".",".com"}
                },
                {
                        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
                        {"~", "`", "^", "&", "[", "]", "÷", "×", "#", "*"},
                        {"_", "-", "#", "$", "%", "&", "+", "(", ")"},
                        {"/", "\"", "'", ":", ";", "!", "?",",","'"},
                        {"@", " ",";", ".",".com"}
                },
//                {
//                        {"~", "`", "|", "·", "√", "π", "÷", "×", "¶", "△"},
//                        {"￡", "￠", "€", "￥", "<", ">", "=", "{", "}"},
//                        {"＼", "©", "®", "™", "^", "[", "]"},
//                        {",", " ", "／"}
//                }
        };
        btnKeyboard = new Button[][]{
                {oneKey, twoKey, threeKey, fourKey, fiveKey, sixKey, sevenKey, eightKey, nineKey, zeroKey},
                {qKey, wKey, eKey, rKey, tKey, yKey, uKey, iKey, oKey, pKey},
                {aKey, sKey, dKey, fKey, gKey, hKey, jKey, kKey, lKey},
                {zKey, xKey, cKey, vKey, bKey, nKey, mKey, commaKey, apostropheKey},
                {atKey, alphabet_spaceKey, semicolonKey, dotKey, dotcomKey}
//                {shiftKey, delKey, atKey, alphabet_spaceKey, dotKey, enterKey}
        };

        for (int i = 0; i < btnKeyboard.length; i++) {
            for (int j = 0; j < btnKeyboard[i].length; j++) {
                String[] strKey = {
                        keys[KEYBOARD_ALPHABET_LOWER_MODE][i][j],
                        keys[KEYBOARD_ALPHABET_UPPER_MODE][i][j],
                        keys[KEYBOARD_NUMBERIC_LOWER_MODE][i][j]
                        //keys[KEYBOARD_NUMBERIC_UPPER_MODE][i][j]
                };
                btnKeyboard[i][j].setOnClickListener(makeOnClickListener(strKey));
            }
        }

        shiftKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FullscreenActivity)alphabet_keyboard_layout.getContext()).startAutoResetHandler();
//                if (isShiftOn && isAlphabet && !isShiftLock) {
//                    isShiftLock = true;
//                    shiftKeyText.setText("");
//                    shiftKey.setImageResource(R.drawable.shift_key);
//                    shiftKey.setBackgroundResource(R.drawable.roundbutton_lock);
//                    shiftKey.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//                    shiftKey.setPadding(19, 19, 19, 19);
//                    kyeboardMode = KEYBOARD_ALPHABET_UPPER_MODE;
//                    return;
//                } else
//                    isShiftLock = false;

                isShiftOn = !isShiftOn;

                if (isAlphabet) {
                    changeText(alphabet_keyboard_layout, isShiftOn);
                    shiftKeyText.setText("");
                    if (isShiftOn) {
                        kyeboardMode = KEYBOARD_ALPHABET_UPPER_MODE;

                        shiftKey.setImageResource(R.drawable.shift_key_press);
                        shiftKey.setBackgroundResource(R.drawable.roundbutton_prs);
                    } else {
                        kyeboardMode = KEYBOARD_ALPHABET_LOWER_MODE;

                        shiftKey.setImageResource(R.drawable.shift_key);
                        shiftKey.setBackgroundResource(R.drawable.roundbutton);
                    }
                    shiftKey.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    shiftKey.setPadding(19, 19, 19, 19);
                } else {
                    shiftKey.setImageResource(android.R.color.transparent);
                    shiftKey.setBackgroundResource(R.drawable.roundbutton);
//                    if (isShiftOn) {
//                        kyeboardMode = KEYBOARD_NUMBERIC_UPPER_MODE;
//                        shiftKeyText.setText("?123");
//                    } else {
//                        kyeboardMode = KEYBOARD_NUMBERIC_LOWER_MODE;
//                        shiftKeyText.setText("=\\<");
//                    }
                }

                for (int i = 0; i < btnKeyboard.length; i++) {
                    for (int j = 0; j < btnKeyboard[i].length; j++) {
                        btnKeyboard[i][j].setText(keys[kyeboardMode][i][j]);
                    }
                }
            }
        });

        enterKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FullscreenActivity)alphabet_keyboard_layout.getContext()).startAutoResetHandler();
                mListener.enterPressed();
            }
        });

        delKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FullscreenActivity)alphabet_keyboard_layout.getContext()).startAutoResetHandler();
                removeCharacter(inputStrBuf);
            }
        });

        final Handler delHandler = new Handler();

        delKey.setOnTouchListener(new View.OnTouchListener() {
            private float width_del;
            private float height_del;

            Runnable delRunnable = new Runnable() {
                @Override
                public void run() {
                    removeCharacter(inputStrBuf);
                    delHandler.postDelayed(delRunnable, DELETE_KEY_INTERVAL_TIME);
                }
            };

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                width_del = view.getMeasuredWidth();
                height_del = view.getMeasuredHeight();

                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    delHandler.postDelayed(delRunnable, DELETE_KEY_INTERVAL_TIME * 5);
                } else if (action == MotionEvent.ACTION_UP) {
                    delHandler.removeCallbacks(delRunnable);
                } else if (action == MotionEvent.ACTION_MOVE) {
                    float posX = event.getX();
                    float posY = event.getY();
                    if ((posX < (width_del) && posX > 0) && (posY < (height_del) && posY > 0)) {
                    } else {
                        delHandler.removeCallbacks(delRunnable);
                    }
                } else {
                    Log.w(TAG, "unhandled case! " + action);
                }
                return false;
            }
        });

        // Add Action Listener on keyboard Buttons
        // Change Mode Button Action Event Add. Alphabet & Numeric
        changeModeButton_alphabet = (Button) alphabet_keyboard_layout.findViewById(R.id.changeMode);
        changeModeButton_alphabet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FullscreenActivity)alphabet_keyboard_layout.getContext()).startAutoResetHandler();
                if (isAlphabet) {
                    kyeboardMode = KEYBOARD_NUMBERIC_LOWER_MODE;
                    shiftKey.setImageResource(android.R.color.transparent);
                    shiftKeyText.setText("=\\<");
                    changeModeButton_alphabet.setText("ABC");
                } else {
                    kyeboardMode = KEYBOARD_ALPHABET_LOWER_MODE;
                    shiftKey.setImageResource(R.drawable.shift_key);
                    shiftKey.setPadding(19, 19, 19, 19);
                    shiftKeyText.setText("");
                    changeModeButton_alphabet.setText("?!/+");
                }
                isShiftOn = false;
                //isShiftLock = false;
                shiftKey.setBackgroundResource(R.drawable.roundbutton);
                isAlphabet = !isAlphabet;
                for (int i = 0; i < btnKeyboard.length; i++) {
                    for (int j = 0; j < btnKeyboard[i].length; j++) {
                        btnKeyboard[i][j].setText(keys[kyeboardMode][i][j]);
                    }
                }
            }
        });
        return alphabet_keyboard_layout;
    }

    public void offShift() {
        shiftKey.setImageResource(R.drawable.shift_key);
        shiftKey.setBackgroundResource(R.drawable.roundbutton);
        shiftKey.setPadding(19, 19, 19, 19);
    }

    public void removeCharacter(StringBuffer edit) {
        if (isInputLock) {
            return;
        }
        Log.d(TAG, "removeCharacter(), isSelectText = " + isSelecteText);
        if (isSelecteText) {
            emptyCharacter();
            isSelecteText = false;
            return;
        }
        if (mCursorIndex >= 1 && mCursorIndex - 1 < edit.length()) {
            edit.deleteCharAt(mCursorIndex - 1);
            mCursorIndex--;
        }
        mListener.updateString(edit.toString(), mCursorIndex);

    }

    public void appendCharacter(String str) {
        if (isInputLock) {
            return;
        }
        if (isSelecteText) {
            emptyCharacter();
        }
        inputStrBuf.append(str);
        mListener.updateString(inputStrBuf.toString(), mCursorIndex);

    }

    public void appendCharacter(String str, int index) {
        if (isInputLock) {
            return;
        }
        if (isSelecteText) {
            emptyCharacter();
            inputStrBuf.append(str);
        } else
            inputStrBuf.insert(index, str);
        mCursorIndex = mCursorIndex + str.length();
        mListener.updateString(inputStrBuf.toString(), mCursorIndex);
    }

    public void emptyCharacter() {
        if (inputStrBuf.length() >= 1)
            inputStrBuf.delete(0, inputStrBuf.length());
        mCursorIndex = 0;
        mListener.updateString(inputStrBuf.toString(), mCursorIndex);
    }

    public void selectString(boolean isSelecteText) {
        this.isSelecteText = isSelecteText;
    }

    public void setinputLock(boolean isInputLock) {
        this.isInputLock = isInputLock;
    }

    public void initKeyboard() {
        isAlphabet = true;
        isShiftOn = false;
        kyeboardMode = KEYBOARD_ALPHABET_LOWER_MODE;
        shiftKey.setImageResource(R.drawable.shift_key);
        shiftKey.setBackgroundResource(R.drawable.roundbutton);
        shiftKey.setPadding(19, 19, 19, 19);
        shiftKeyText.setText("");
        changeModeButton_alphabet.setText("?!/+");
        for(int i = 0; i <btnKeyboard.length; i++) {
            for (int j = 0; j < btnKeyboard[i].length; j++) {
                btnKeyboard[i][j].setText(keys[kyeboardMode][i][j]);
            }
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnKeyboardFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnKeyboardFragmentInteractionListener {
        void updateString(String email, int cursorIndex);
        void enterPressed();
    }
}
