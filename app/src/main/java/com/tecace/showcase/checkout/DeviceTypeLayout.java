package com.tecace.showcase.checkout;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tecace.showcase.CustomGridView;
import com.tecace.showcase.FullscreenActivity;
import com.tecace.showcase.R;
import com.tecace.showcase.ShowcaseApplication;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.ApplicationIndex;
import com.tecace.showcase.data.SubcategoryData;
import com.tecace.showcase.util.CubicBezierInterpolator;

//public class DeviceTypeLayout extends FrameLayout implements CheckoutViewPager.OnCheckoutPageChangeListener {
public class DeviceTypeLayout extends FrameLayout {
    private final String TAG = this.getClass().getSimpleName();
    //Todo: IS_DEBUG set false before git checkout.
    private final Boolean IS_DEBUG = false;

    private static final int MESSAGE_SHOW_SEC = 3000;      //3sec

    private AppTableDataManager mAppTableDataManager = null;
    private AppDataManager mAppDataManager = null;

    private FrameLayout parent;

    private SelectedAppAdapter mSelectedAppAdapter = null;
    private DisplayImageOptions options;

    TextView checkoutTitle;
    private final String DOWN = "DOWN";
    private final String UP = "UP";
    CustomGridView selectedAppGridView;
    ImageButton moreButton;

    ImageView viewPhoneSelection;
    ImageView viewTabSelection;
    ImageView viewGearSelection;

    TextView textPhoneSelection;
    TextView textTabSelection;
    TextView textGearSelection;

    View root;
    View container;

    private OnDeviceTypeLayoutInteractionListener mListener = null;

    private int iSelectedDeviceType = SubcategoryData.SELECTED_DEVICE_PHONE;

    public DeviceTypeLayout(Context context) {
        super(context);
        init(context);
    }

    public DeviceTypeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DeviceTypeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        parent = (FrameLayout) inflate(getContext(), R.layout.activity_device_type, this);
        mListener = (OnDeviceTypeLayoutInteractionListener) (Activity) (super.getContext());
        mAppDataManager = ((ShowcaseApplication) getContext().getApplicationContext()).getAppDataManager();
    }

    public void initLayout(final Context context) {
        options = ((ShowcaseApplication) (getContext().getApplicationContext())).getUILOptions();

        RelativeLayout touchInterceptLayout = (RelativeLayout) parent.findViewById(R.id.devicetype_touch_intercept_layout);

        checkoutTitle = (TextView) parent.findViewById(R.id.devicetype_checkout_layout_title);
//?        sendButton = (Button) parent.findViewById(R.id.devicetype_send);

        selectedAppGridView = (CustomGridView) parent.findViewById(R.id.device_type_grid);
        selectedAppGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView absListView, int scrollState) {
            }

            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {
                if (moreButton == null) return;
                ((FullscreenActivity) view.getContext()).startAutoResetHandler();

                int offsetY = selectedAppGridView.computeVerticalScrollOffset();
                int extentY = selectedAppGridView.computeVerticalScrollExtent();
                int rangeY = selectedAppGridView.computeVerticalScrollRange();
                rangeY += 32; //paddingbottom 40px (??)

                if (offsetY + extentY == rangeY) {
                    if (moreButton.getTag() != null && !moreButton.getTag().equals(UP)) {
                        moreButton.setTag(UP);
                        moreButton.setImageResource(R.drawable.ic_more_info_opend);
                    }
                    return;
                }
                if (moreButton.getTag() != null && !moreButton.getTag().equals(DOWN)) {
                    moreButton.setTag(DOWN);
                    moreButton.setImageResource(R.drawable.ic_more_info_closed);
                }
            }
        });
        moreButton = (ImageButton) parent.findViewById(R.id.device_type_more_button);
        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();

                if (moreButton.getTag() != null && moreButton.getTag().equals(DOWN)) {
                    final int lastPosition = selectedAppGridView.getLastVisiblePosition();
                    selectedAppGridView.smoothScrollToPosition(lastPosition + 9);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(lastPosition);
                        }
                    }, 100);
                } else {
                    selectedAppGridView.smoothScrollToPositionFromTop(0, 0);
                    selectedAppGridView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedAppGridView.setSelection(0);
                        }
                    }, 100);
                }
            }
        });
        moreButton.setTag(DOWN);

        textPhoneSelection = (TextView) parent.findViewById(R.id.devicetype_selection_text_phone);
        textTabSelection = (TextView) parent.findViewById(R.id.devicetype_selection_text_tab);
        textGearSelection = (TextView) parent.findViewById(R.id.devicetype_selection_text_gear);

        viewPhoneSelection = (ImageView) parent.findViewById(R.id.devicetype_selection_view_phone);
        viewPhoneSelection.setImageResource(R.drawable.phone_selected);
        viewPhoneSelection.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//?                checkoutTitle.setText("PHONE");
                viewPhoneSelection.setImageResource(R.drawable.phone_selected);
                viewTabSelection.setImageResource(R.drawable.tap_unselected);
                viewGearSelection.setImageResource(R.drawable.gear_unselected);

                textPhoneSelection.setTextColor(Color.parseColor("#ffffff"));
                textTabSelection.setTextColor(Color.parseColor("#aaaaaa"));
                textGearSelection.setTextColor(Color.parseColor("#aaaaaa"));
                container.setX(110);

                iSelectedDeviceType  = SubcategoryData.SELECTED_DEVICE_PHONE;
                onReOpenCheckoutWindow();

            }
        });

        viewTabSelection = (ImageView) parent.findViewById(R.id.devicetype_selection_view_tab);
        viewTabSelection.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//?                checkoutTitle.setText("TAB");
                viewPhoneSelection.setImageResource(R.drawable.phone_unselected);
                viewTabSelection.setImageResource(R.drawable.tap_selected);
                viewGearSelection.setImageResource(R.drawable.gear_unselected);

                container.setX(640);
                textPhoneSelection.setTextColor(Color.parseColor("#aaaaaa"));
                textTabSelection.setTextColor(Color.parseColor("#ffffff"));
                textGearSelection.setTextColor(Color.parseColor("#aaaaaa"));

                iSelectedDeviceType  = SubcategoryData.SELECTED_DEVICE_TABLET;
                onReOpenCheckoutWindow();

            }
        });

        viewGearSelection = (ImageView) parent.findViewById(R.id.devicetype_selection_view_gear);
        viewGearSelection.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//?                checkoutTitle.setText("GEAR");
                viewPhoneSelection.setImageResource(R.drawable.phone_unselected);
                viewTabSelection.setImageResource(R.drawable.tap_unselected);
                viewGearSelection.setImageResource(R.drawable.gear_selected);

                container.setX(1230);
                textPhoneSelection.setTextColor(Color.parseColor("#aaaaaa"));
                textTabSelection.setTextColor(Color.parseColor("#aaaaaa"));
                textGearSelection.setTextColor(Color.parseColor("#ffffff"));

                iSelectedDeviceType  = SubcategoryData.SELECTED_DEVICE_S3;
                onReOpenCheckoutWindow();

            }
        });

        root = parent.findViewById(R.id.devicetype_conainter);
        root.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mListener != null)
                        mListener.devicetype_onSetOpenHeartMode(false, false);
                }
                return true;
            }
        });

        container = parent.findViewById(R.id.devicetype_layout);
        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                ((MainActivity) getContext()).stopAutoResetHandler();
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                return true;
            }
        });

    }

    public void setAppTableDataManager(AppTableDataManager appTableDataManager) {
        mAppTableDataManager = appTableDataManager;
        initLayout(getContext());
    }

    public void onReOpenCheckoutWindow() {

        mSelectedAppAdapter = new SelectedAppAdapter(getContext(), mAppDataManager, mAppTableDataManager, iSelectedDeviceType);
        mSelectedAppAdapter.setUILOption(options);
        selectedAppGridView.setAdapter(mSelectedAppAdapter);
        selectedAppGridView.setOnNoItemPressListener(new CustomGridView.OnNoItemPressListener() {
            @Override
            public void onNoItemLongPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }

            @Override
            public void onNoItemPress() {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }
        });
        selectedAppGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                ((FullscreenActivity) getContext()).startAutoResetHandler();
                return false;
            }
        });
        selectedAppGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int iPassionIndex =  ((ApplicationIndex) mSelectedAppAdapter.getItem(position)).getPassionIndex();
                int iSubcategoryIndex =  ((ApplicationIndex) mSelectedAppAdapter.getItem(position)).getSubcategoryIndex();

                Log.d(TAG, "selectedAppGridView setOnItemClickListener onItemClick ::: " + position + " : " + iPassionIndex + " : " + iSubcategoryIndex);

                if (mListener != null)
                    mListener.devicetype_onSelectedAppItemClick(iPassionIndex, iSubcategoryIndex);

                ((FullscreenActivity) getContext()).startAutoResetHandler();
            }
        });

    }

    public Animator generateShowAnimator() {
        final DeviceTypeLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_open);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                myself.setVisibility(VISIBLE);
            }
        });
        return animatorSet;
    }

    public Animator generateDismissAnimator() {
        final DeviceTypeLayout myself = this;
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.dialog_sally_close);
        animatorSet.setTarget(myself);
        Interpolator dialogSally = new CubicBezierInterpolator(0.6f, 0, 0.4f, 1f);
        animatorSet.setInterpolator(dialogSally);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                myself.setVisibility(View.GONE);
            }
        });
        return animatorSet;
    }

    public void reset() {
        viewPhoneSelection.setImageResource(R.drawable.phone_selected);
        viewTabSelection.setImageResource(R.drawable.tap_unselected);
        viewGearSelection.setImageResource(R.drawable.gear_unselected);

        iSelectedDeviceType = SubcategoryData.SELECTED_DEVICE_PHONE;

        textPhoneSelection.setTextColor(Color.parseColor("#ffffff"));
        textTabSelection.setTextColor(Color.GRAY);
        textGearSelection.setTextColor(Color.GRAY);
        container.setX(110);
    }

    public interface OnDeviceTypeLayoutInteractionListener {
        void devicetype_onSetOpenHeartMode(boolean isOpen, boolean isGoLv1);

        void devicetype_onSelectedAppItemClick(int PassionIndex, int SubcategoryIndex);
    }
}


