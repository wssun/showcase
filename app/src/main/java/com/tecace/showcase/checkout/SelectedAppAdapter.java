package com.tecace.showcase.checkout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tecace.showcase.R;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.ApplicationData;
import com.tecace.showcase.data.ApplicationIndex;
import com.tecace.showcase.data.SubcategoryData;
import com.tecace.showcase.util.FontTypeface;

import java.util.ArrayList;

/**
 * Created by veronica on 2015-09-22.
 */
public class SelectedAppAdapter extends BaseAdapter {
    private String TAG = this.getClass().getSimpleName().toString();

    private Context mContext;

    private boolean isEditState = false;
    AppDataManager mAppDataManager = null;
    AppTableDataManager mAppTableDataManager = null;
    private int iSelected = 0;
    private ApplicationIndex targetIndex = null;

    private LayoutInflater vi;
    private Typeface tfEtextRoman = null;
    private Shader textShader;
    private DisplayImageOptions options;

    public SelectedAppAdapter(Context context,AppDataManager appDataManager, AppTableDataManager appTableDataManager, int selected) {
        mContext = context;
        this.mAppDataManager = appDataManager;
        this.mAppTableDataManager = appTableDataManager;
        this.iSelected = selected;
        vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tfEtextRoman = FontTypeface.getInstance().getEtextFontRoman();
        textShader = new LinearGradient(mContext.getResources().getDimensionPixelSize(R.dimen.subcategory_display_app_size) - 20, 0,
                mContext.getResources().getDimensionPixelSize(R.dimen.subcategory_display_app_size) - 2, 0,
                new int[]{Color.WHITE, Color.TRANSPARENT},
                new float[]{0, 1}, Shader.TileMode.CLAMP);
    }

    public void setUILOption(DisplayImageOptions options) {
        this.options = options;
    }

    @Override
    public int getCount() {

        switch (iSelected) {
            case SubcategoryData.SELECTED_REGION_AMERICA:
                return mAppTableDataManager.getmapavailabilityAmericaAppItems().size();
            case SubcategoryData.SELECTED_REGION_EUROPE:
                return mAppTableDataManager.getmapavailabilityEuropeAppItems().size();
            case SubcategoryData.SELECTED_REGION_ASIA:
                return mAppTableDataManager.getmapavailabilityAsiaItems().size();
            case SubcategoryData.SELECTED_REGION_REST:
                return mAppTableDataManager.getmapavailabilityRestAppItems().size();
            case SubcategoryData.SELECTED_DEVICE_PHONE:
                return mAppTableDataManager.getdevicetypePhoneAppItems().size();
            case SubcategoryData.SELECTED_DEVICE_TABLET:
                return mAppTableDataManager.getdevicetypeTabAppItems().size();
            case SubcategoryData.SELECTED_DEVICE_S3:
                return mAppTableDataManager.getdevicetypeGearItems().size();

            default:
                return 0;
        }
    }

    @Override
    public Object getItem(int position) {

        switch (iSelected) {
            case SubcategoryData.SELECTED_REGION_AMERICA:
                return mAppTableDataManager.getmapavailabilityAmericaAppItems().get(position);
            case SubcategoryData.SELECTED_REGION_EUROPE:
                return mAppTableDataManager.getmapavailabilityEuropeAppItems().get(position);
            case SubcategoryData.SELECTED_REGION_ASIA:
                return mAppTableDataManager.getmapavailabilityAsiaItems().get(position);
            case SubcategoryData.SELECTED_REGION_REST:
                return mAppTableDataManager.getmapavailabilityRestAppItems().get(position);
            case SubcategoryData.SELECTED_DEVICE_PHONE:
                return mAppTableDataManager.getdevicetypePhoneAppItems().get(position);
            case SubcategoryData.SELECTED_DEVICE_TABLET:
                return mAppTableDataManager.getdevicetypeTabAppItems().get(position);
            case SubcategoryData.SELECTED_DEVICE_S3:
                return mAppTableDataManager.getdevicetypeGearItems().get(position);

            default:
                return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<ApplicationIndex> getItemList() {

        switch (iSelected) {
            case SubcategoryData.SELECTED_REGION_AMERICA:
                return mAppTableDataManager.getmapavailabilityAmericaAppItems();
            case SubcategoryData.SELECTED_REGION_EUROPE:
                return mAppTableDataManager.getmapavailabilityEuropeAppItems();
            case SubcategoryData.SELECTED_REGION_ASIA:
                return mAppTableDataManager.getmapavailabilityAsiaItems();
            case SubcategoryData.SELECTED_REGION_REST:
                return mAppTableDataManager.getmapavailabilityRestAppItems();
            case SubcategoryData.SELECTED_DEVICE_PHONE:
                return mAppTableDataManager.getdevicetypePhoneAppItems();
            case SubcategoryData.SELECTED_DEVICE_TABLET:
                return mAppTableDataManager.getdevicetypeTabAppItems();
            case SubcategoryData.SELECTED_DEVICE_S3:
                return mAppTableDataManager.getdevicetypeGearItems();

            default:
                return null;
        }
   }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View v = convertView;

        final SelectedAppGridItemHolder selectedAppHolder;
//?        final ApplicationIndex targetIndex; //? = mAppTableDataManager.getSelectedAppItems().get(position);
        switch (iSelected) {
            case SubcategoryData.SELECTED_REGION_AMERICA:
                targetIndex = mAppTableDataManager.getmapavailabilityAmericaAppItems().get(position);
                break;
            case SubcategoryData.SELECTED_REGION_EUROPE:
                targetIndex = mAppTableDataManager.getmapavailabilityEuropeAppItems().get(position);
                break;
            case SubcategoryData.SELECTED_REGION_ASIA:
                targetIndex = mAppTableDataManager.getmapavailabilityAsiaItems().get(position);
                break;
            case SubcategoryData.SELECTED_REGION_REST:
                targetIndex = mAppTableDataManager.getmapavailabilityRestAppItems().get(position);
                break;
            case SubcategoryData.SELECTED_DEVICE_PHONE:
                targetIndex = mAppTableDataManager.getdevicetypePhoneAppItems().get(position);
                break;
            case SubcategoryData.SELECTED_DEVICE_TABLET:
                targetIndex = mAppTableDataManager.getdevicetypeTabAppItems().get(position);
                break;
            case SubcategoryData.SELECTED_DEVICE_S3:
                targetIndex = mAppTableDataManager.getdevicetypeGearItems().get(position);
                break;
        }

//?        final ApplicationData targetApp = (ApplicationData) mAppDataManager.getPassionDataAt(targetIndex.getPassionIndex())
//?                .getSubcategoryDataAt(targetIndex.getSubcategoryIndex())
//?                .getAppplicationDataAt(targetIndex.getApplicationIndex());
        final SubcategoryData targetApp = (SubcategoryData) mAppDataManager.getPassionDataAt(targetIndex.getPassionIndex())
                    .getSubcategoryDataAt(targetIndex.getSubcategoryIndex());

        if(v == null)
        {
            v = vi.inflate(R.layout.selected_app_grid_item, null);
            selectedAppHolder = new SelectedAppGridItemHolder(v);
            v.setTag(selectedAppHolder);
        } else
            selectedAppHolder = (SelectedAppGridItemHolder) v.getTag();

        if(targetApp != null) {
            String appName = generateShorterAppName(targetApp.getSubcategoryName());
            selectedAppHolder.appName.setText(appName);
            selectedAppHolder.appName.measure(0, 0);
            if (selectedAppHolder.appName.getMeasuredWidth()
                    > mContext.getResources().getDimensionPixelSize(R.dimen.subcategory_display_app_size)) {
                selectedAppHolder.appName.setGravity(Gravity.LEFT);
                ((RelativeLayout.LayoutParams) selectedAppHolder.appName.getLayoutParams()).addRule(RelativeLayout.ALIGN_LEFT, R.id.expand_app_item_icon);
                ((RelativeLayout.LayoutParams) selectedAppHolder.appName.getLayoutParams()).width =
                        mContext.getResources().getDimensionPixelSize(R.dimen.subcategory_display_app_size);
                selectedAppHolder.appName.getPaint().setShader(textShader);
            } else {
                ((RelativeLayout.LayoutParams) selectedAppHolder.appName.getLayoutParams()).addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                selectedAppHolder.appName.setGravity(Gravity.CENTER);
                selectedAppHolder.appName.getPaint().setShader(null);
            }
            selectedAppHolder.appName.requestLayout();

            ImageLoader.getInstance().displayImage("file://" + targetApp.getIcon(), selectedAppHolder.appIconImage, options);
        }

        return v;
    }

    class SelectedAppGridItemHolder {
        ImageView appIconImage;
        TextView appName;
        boolean isDelete = false;

        public SelectedAppGridItemHolder(View gridviewitem) {
            appIconImage = (ImageView)gridviewitem.findViewById(R.id.checkout_app_item_icon);
            appName = (TextView)gridviewitem.findViewById(R.id.checkout_app_item_title);
            appName.setTypeface(tfEtextRoman);
        }
    }

    private String generateShorterAppName(String appName) {
        int barIndex = appName.indexOf(" - ");
        if (barIndex != -1) {
            appName = appName.substring(0, barIndex);
        }
        int colonIndex = appName.indexOf(": ");
        if (colonIndex != -1) {
            appName = appName.substring(0, colonIndex);
        }
        
        //remove "Samsung" title from appName
        if (appName.indexOf("Samsung ") == 0) {
            return appName.replaceFirst("Samsung ", "");
        }
        if (appName.indexOf("samsung ") == 0) {
            return appName.replaceFirst("samsung ", "");
        }

        return appName;
    }
}
