package com.tecace.showcase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.tecace.showcase.checkout.DeviceTypeLayout;
import com.tecace.showcase.checkout.KeyboardFragment;
import com.tecace.showcase.checkout.MapAvailabilityLayout;
import com.tecace.showcase.checkout.SelectedAppLayout;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.data.AppTableDataManager;
import com.tecace.showcase.data.RecoveryData;
import com.tecace.showcase.display.SubcategoryDisplayViewPager;
import com.tecace.showcase.navigation.AttractorLoopView;
import com.tecace.showcase.preview.TransitionBackgroundView;
import com.tecace.showcase.util.AppConst;
import com.tecace.showcase.util.MixPanelUtil;
import com.tecace.showcase.youtube.VideoLayout;

import java.lang.ref.WeakReference;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends FragmentActivity
    implements AttractorLoopView.OnAttractorLoopEventListener,
        SelectedAppLayout.OnSelectedAppLayoutInteractionListener,
        DeviceTypeLayout.OnDeviceTypeLayoutInteractionListener,
        MapAvailabilityLayout.OnMapAvailabilityLayoutInteractionListener,
        TransitionBackgroundView.OnBackgroundGestureListener,
        ControllersLayout.OnControllersLayoutInteractionListener,
        SubcategoryDisplayViewPager.OnDisplayPagerEventListener,
        KeyboardFragment.OnKeyboardFragmentInteractionListener,
        VideoLayout.OnVideoLayoutInteractionListener,
        CallToActionView.OnCallToActionViewClickListener {
    private final String TAG = this.getClass().getSimpleName();

    private final String GHOST_HAND_FINISH = "GHOST_HAND_FINISH";
    private static final int AUTO_RESET_TIMER = 60000;
    private static final int AUTO_RESET_TIMER_15SEC = 15000;
    private static final int AUTO_RESET_TIMER_25SEC = 25000;
    private static final int AUTO_RESET_TIMER_35SEC = 35000;

    private static DisplayLevel mLevel = DisplayLevel.LEVEL_1;

    private static enum DisplayLevel {
        LEVEL_1,    //PASSION LEVEL
        LEVEL_2,    //SUBCATEGORY DISPLAY
        LEVEL_3,    //MAIN WINDOW DISPLAY
        LEVEL_4     //FAVORITES CHECKOUT DISPLAY
    };

    private AttractorLoopView mAttractorLoopView = null;
    private TransitionBackgroundView mTransitionBackgroundView = null;

    private CallToActionView mGhostAnimationDefenceView = null;

    private PowerManager.WakeLock mWakeLock = null;

    private android.app.FragmentManager mFragmentManager = null;
    private AppDataManager mAppDatamanager = null;

    private Handler mResetHandler = null;
    private Runnable mResetRunnable = null;

    private ControllersLayout mControllersLayout = null;
    private WaveMessageView mWaveMessageView = null;
    private SelectedAppLayout mEmailLayout = null;
    private DeviceTypeLayout mDeviceTypeLayout = null;
    private MapAvailabilityLayout mMapAvailabilityLayout = null;
    private SubcategoryDisplayViewPager mPartnerDisplayPager = null;
    private VideoLayout mVideoLayout = null;
    private AppTableDataManager mAppTableDataManager;
    private RecoveryData mRecoveryData = null;
    private static int mLvBeforeOpenHeart = RecoveryData.LEVEL_ATTRACTOR_LOOP;

    // ANIMATORS
    private AnimatorSet mDisplayToLoopAnimator = null;
    private AnimatorSet mLoopToDisplayAnimator = null;
    private AnimatorSet mMainToEmailAnimator = null;
    private AnimatorSet mEmailToMainAnimator = null;
    private AnimatorSet mMainToDevicetypeAnimator = null;
    private AnimatorSet mDevicetypeToMainAnimator = null;
    private AnimatorSet mMainToMapavailabilityAnimator = null;
    private AnimatorSet mMapavailabilityToMainAnimator = null;
    private boolean isFirstCategoryDisplay = true;

    private ProfileHandler handler;
    private Dialog dialog;

    private int currentApiVersion;

    class ProfileHandler extends Handler {
        private FullscreenActivity parent;

        public ProfileHandler(FullscreenActivity parent) {
            this.parent = parent;
        }

        public void handleMessage(Message msg) {
            parent.handleMessage(msg);
        }
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                mTransitionBackgroundView.setDefaultImageSet();
                dialog.dismiss();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        if(currentApiVersion == Build.VERSION_CODES.LOLLIPOP_MR1) {// Galaxy View
            final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            getWindow().getDecorView().setSystemUiVisibility(flags);
            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        } else {
            View decorView = this.getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");

        mFragmentManager = (android.app.FragmentManager) getFragmentManager();

        mRecoveryData = new RecoveryData();

        // GENERATE SAMPLE DATA
        mAppDatamanager = ((ShowcaseApplication) getApplicationContext()).getAppDataManager();
        if (null == mAppDatamanager) {
            Toast.makeText(this, "Application data is not ready", Toast.LENGTH_LONG).show();
//            sendBroadcast(new Intent(AppConst.ACTION_ROLLBACK_DATA_FOLDER));
//            Exit(false);
            return;
        }

        mAppTableDataManager = new AppTableDataManager(this, mAppDatamanager);

        // ATTRACTOR LOOP
        mAttractorLoopView = (AttractorLoopView) findViewById(R.id.main_attractor_view);
        mAttractorLoopView.setPassionData(mAppDatamanager, mAppDatamanager.getSamsungIndex());
        mAttractorLoopView.addOnLoopListener(this);


        // SUBCATEGORY DISPLAY PAGER
        mPartnerDisplayPager = (SubcategoryDisplayViewPager) findViewById(R.id.main_subcategory_display_viewpager);
        mPartnerDisplayPager.setAppTableDataManager(mAppTableDataManager);
        mPartnerDisplayPager.setOnDisplayPagerEventListener(this);
        mPartnerDisplayPager.setRecoveryData(mRecoveryData);

        mTransitionBackgroundView = (TransitionBackgroundView) findViewById(R.id.main_subcategory_transition_background);

        dialog = new Dialog(this, R.style.dialog);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        handler = new ProfileHandler(this);
        Thread networkThread = new Thread() {
            public void run() {
                mTransitionBackgroundView.setAppDataManager(mAppDatamanager, mAttractorLoopView.getCenterPassionIndex());
                FullscreenActivity.this.handler.sendEmptyMessage(0);
            }
        };
        networkThread.start();

        mTransitionBackgroundView.setOnBackgroundGestureListener(this);

        //EMAIL
        mEmailLayout = (SelectedAppLayout) findViewById(R.id.open_heart_layout);
        mEmailLayout.setFragmentManager(mFragmentManager);
        mEmailLayout.setV4FragmentManager(getSupportFragmentManager());
        mEmailLayout.setAppTableDataManager(mAppTableDataManager);
        mEmailLayout.setVisibility(View.GONE);

        //TYPE
        mDeviceTypeLayout = (DeviceTypeLayout) findViewById(R.id.device_type_layout);
//        mDeviceTypeLayout.setFragmentManager(mFragmentManager);
//        mDeviceTypeLayout.setV4FragmentManager(getSupportFragmentManager());
        mDeviceTypeLayout.setAppTableDataManager(mAppTableDataManager);
        mDeviceTypeLayout.setVisibility(View.GONE);


        //AVAILABILITY
        mMapAvailabilityLayout = (MapAvailabilityLayout) findViewById(R.id.map_availability_layout);
//        mMapAvailabilityLayout.setFragmentManager(mFragmentManager);
//        mMapAvailabilityLayout.setV4FragmentManager(getSupportFragmentManager());
        mMapAvailabilityLayout.setAppTableDataManager(mAppTableDataManager);
        mMapAvailabilityLayout.setVisibility(View.GONE);


        // CONTROLLER ( HEART, HELP, RESET )
        mControllersLayout = (ControllersLayout) findViewById(R.id.main_controller_layout);
        //mControllersLayout.setAppTableDataManager(mAppTableDataManager);

        mWaveMessageView = (WaveMessageView) findViewById(R.id.main_message_view);

        // YOUTUBR VIDEO PLAYER
        mVideoLayout = (VideoLayout) findViewById(R.id.main_video_layout);
        mVideoLayout.setFragmentManager(mFragmentManager);

        // call to action view
        mGhostAnimationDefenceView = (CallToActionView) findViewById(R.id.main_ghost_layout);
        mGhostAnimationDefenceView.setCallToActionViewClickListener(this);

        mResetHandler = new Handler();
        mResetRunnable = new Runnable() {
            @Override
            public void run() {
                resetAppTable(null);
            }
        };

        mAutoResetHandler = new AutoResetHandler(this);
//?        registerStopReceiver();

        MixPanelUtil.initialize(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mWakeLock.acquire();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWakeLock.release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Exit(true);
    }

    private void Exit(boolean notifyAppStopped) {
        if (notifyAppStopped) {
            sendBroadcast(new Intent(AppConst.ACTION_APP_STOPPED));
        }

        if (null != mTransitionBackgroundView)
            mTransitionBackgroundView.onDestroy();
        if (null != mAttractorLoopView)
            mAttractorLoopView.onDestroy();
        stopAutoLoopHandler();

        moveTaskToBack(true);
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onDestroy() {
        if (null != mTransitionBackgroundView)
            mTransitionBackgroundView.onDestroy();
        if (null != mAttractorLoopView)
            mAttractorLoopView.onDestroy();
        stopAutoLoopHandler();
        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(currentApiVersion == Build.VERSION_CODES.LOLLIPOP_MR1) { // Galaxy View
            if (hasFocus) {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }

        if (hasFocus) {
            startAutoResetHandler(DisplayLevel.LEVEL_1);
        } else {
            stopAutoLoopHandler();
        }
    }

    public void resetAppTable(final String cause) {

//        mWaveMessageView.dismissMessage();
        mEmailLayout.clearEmailEditText();

        if (null != cause && cause.length() > 0 && cause.equals(GHOST_HAND_FINISH) == false)
            MixPanelUtil.app_closed(cause);

        if (mEmailLayout.isShown()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mEmailLayout, "alpha", 0f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mEmailLayout.setVisibility(View.GONE);
                }
            });
            animator.setDuration(500);
            animator.start();

            Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
            appTitleDismiss.start();
            mTransitionBackgroundView.stopAnimators();
            mTransitionBackgroundView.generateClearBackgroundAnimator().start();
        }

        if (mDeviceTypeLayout.isShown()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mDeviceTypeLayout, "alpha", 0f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mDeviceTypeLayout.setVisibility(View.GONE);

                    Log.d(TAG, "resetAppTable mDeviceTypeLayout.isShown");
                    mDeviceTypeLayout.reset();
                }
            });
            animator.setDuration(500);
            animator.start();

            Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
            appTitleDismiss.start();
            mTransitionBackgroundView.stopAnimators();
            mTransitionBackgroundView.generateClearBackgroundAnimator().start();
        }

        if (mMapAvailabilityLayout.isShown()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mMapAvailabilityLayout, "alpha", 0f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMapAvailabilityLayout.setVisibility(View.GONE);
                }
            });
            animator.setDuration(500);
            animator.start();

            Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
            appTitleDismiss.start();
            mTransitionBackgroundView.stopAnimators();
            mTransitionBackgroundView.generateClearBackgroundAnimator().start();
        }

        if (mPartnerDisplayPager.isShown()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 0f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPartnerDisplayPager.setVisibility(View.GONE);

                    mPartnerDisplayPager.setAlpha(1f);
                }
            });
            animator.setDuration(500);
            animator.start();
//            Animator staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
//            staticMessageShow.setDuration(500);
//            staticMessageShow.start();
            Animator appTitleDismiss = mControllersLayout.getAppTitleDismissAnimator();
            appTitleDismiss.setDuration(500);
            appTitleDismiss.start();
            mTransitionBackgroundView.stopAnimators();
            mTransitionBackgroundView.generateClearBackgroundAnimator().start();
        }

        if (mAttractorLoopView.isShown()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 1f);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mAttractorLoopView.setVisibility(View.VISIBLE);
                }
            });
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(true);
                } else
                    mControllersLayout.showAccessButtons(false);
                mControllersLayout.showHomeButton(false);

                mTransitionBackgroundView.setTransitionRatio(mAttractorLoopView.getCenterPassionIndex(), 1f, true, false);
                }
            });
            animator.setDuration(500);
            animator.start();
        }

        mControllersLayout.resetAccessButtons();

        //Ghost hand finished
        if (cause != null && cause.length() > 0 && cause.equals(GHOST_HAND_FINISH)) {
            startAutoResetHandler(DisplayLevel.LEVEL_1);
        }
        else {  //reset finished
//            if (mGhostAnimationDefenceView.isGhostHandRunning() == false)
//                mControllersLayout.showResetMessage();
            startAutoLoopHandler(1000);
        }
    }

    private void removeChildAnimator(AnimatorSet animatorSet) {
        for (Animator animator : animatorSet.getChildAnimations()) {
            if (animator.getClass().equals(AnimatorSet.class)) {
                removeChildAnimator((AnimatorSet) animator);
            }
            animator.removeAllListeners();
            animator.cancel();
        }
    }

    public void stopAutoLoopHandler() {
        if (mAutoLoopAnimator != null && (mAutoLoopAnimator.isRunning() || mAutoLoopAnimator.isStarted())) {
            removeChildAnimator(mAutoLoopAnimator);
            mAutoLoopAnimator.removeAllListeners();
            mAutoLoopAnimator.cancel();
            mAutoLoopAnimator = null;
        }

        if (mResetHandler != null) {
            mResetHandler.removeCallbacksAndMessages(null);
        }
    }

    // OnAttractorLoopEventListener
    @Override
    public void onAttactTouchDuringYoutubeLoading() {
    }

    @Override
    public void onArriving(int oldPassionIndex, int newPassionIndex, float ratio, boolean isLandArriving) {
        if (mPartnerDisplayPager.isShown() && ((mDisplayToLoopAnimator != null)
                && (mDisplayToLoopAnimator.isRunning() || mDisplayToLoopAnimator.isStarted()))) {
            if (mPartnerDisplayPager.getDisplayCount() == 0)
                mPartnerDisplayPager.setVisibility(View.GONE);
            //#307 부자연스러운 UI 동작 수정함.
//            else {
//                startDisplayToLoopAnimation(false);
//            }
        }
        mTransitionBackgroundView.setTransitionRatio(newPassionIndex, ratio, isLandArriving, mPartnerDisplayPager.isShown());

        if (ratio > 0.95f) {
            if (!mPartnerDisplayPager.isShown()) {
                if (newPassionIndex == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(true);
                } else
                    mControllersLayout.showAccessButtons(false);
            } else {
                if (newPassionIndex == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showHomeButton(false);
                } else
                    mControllersLayout.showHomeButton(true);

                mControllersLayout.setAppTitle( mAppDatamanager.getPassionDataAt(newPassionIndex).getName() );
            }
        }
    }

    @Override
    public void onArrived(int passionIndex, boolean isLand) {
        mTransitionBackgroundView.setTransitionRatio(passionIndex, 1f, isLand, false);
        if (!mPartnerDisplayPager.isShown() && isLand) {
            mPartnerDisplayPager.setCurrentPage(passionIndex, 0);

            startLoopToDisplayAnimation(passionIndex, 0, 0);
        } else if (mPartnerDisplayPager.isShown()) {
            mControllersLayout.setAppTitle( mAppDatamanager.getPassionDataAt(passionIndex).getName() );

            startDisplayToLoopAnimation(true);

            startAutoResetHandler();
        }

        mTransitionBackgroundView.setTransitionRatio(passionIndex, 1f, isLand, false);
        if (!mPartnerDisplayPager.isShown() && isLand) {
            if (passionIndex == mAppDatamanager.getSamsungIndex()) {
                mControllersLayout.showAccessButtons(true);
            } else
                mControllersLayout.showAccessButtons(false);

            mPartnerDisplayPager.setCurrentPage(passionIndex, 0);

            startLoopToDisplayAnimation(passionIndex, 0, 0);
        } else if (mPartnerDisplayPager.isShown() && isLand) {
            mControllersLayout.setAppTitle( mAppDatamanager.getPassionDataAt(passionIndex).getName() );
        } else {
            startAutoResetHandler();
        }
    }

    @Override
    public void onStartOfFling() {
        if (mPartnerDisplayPager.isShown()) {
            mPartnerDisplayPager.setVisibility(View.GONE);
            mControllersLayout.hideTitleNHome();
        }
    }

    @Override
    public void onArrivedFromFling(int passionIndex) {
        if (mPartnerDisplayPager.isShown()) {
            startDisplayToLoopAnimation(false);
        }
        else {
            startLoopToDisplayAnimation(passionIndex, 0, 0);

            if (passionIndex == mAppDatamanager.getSamsungIndex()) {
                mControllersLayout.showAccessButtons(true);
            } else
                mControllersLayout.showAccessButtons(false);
        }

        mTransitionBackgroundView.setTransitionRatio(passionIndex, 1f, true, false);

        startAutoResetHandler();
    }

    @Override
    public void onArrivedForAppDetail(int passionIndex, int subcategoryIndex, int appIndex) {
        mTransitionBackgroundView.setTransitionRatio(passionIndex, 1f, true, false);
        startLoopToDisplayAnimation(passionIndex, subcategoryIndex, appIndex);
    }

    @Override
    public void onTap(View view, final int passionIndex) {
        if (mPartnerDisplayPager.isShown()) {
            if (passionIndex != mAttractorLoopView.getCenterPassionIndex()) {
                mControllersLayout.showAccessButtons(false);

                getDisplayToLoopAnimation(true);

                mDisplayToLoopAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mPartnerDisplayPager.setVisibility(View.GONE);
                        mPartnerDisplayPager.destroyChildren();

                        // show buttons at M4S category
                        if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                            mControllersLayout.showAccessButtons(true);
                        } else
                            mControllersLayout.showAccessButtons(false);

                        mAttractorLoopView.startSmoothLoopWithDisplayAnimation(passionIndex, 0, 0);
                    }
                });
//                mDisplayToLoopAnimator.setStartDelay(1);

                mDisplayToLoopAnimator.start();
            } else {
                startDisplayToLoopAnimation(true);
            }
        } else {
            // CLICK PASSION ITEM
            if (mLoopToDisplayAnimator != null && (mLoopToDisplayAnimator.isRunning() || mLoopToDisplayAnimator.isStarted())) {
                return;
            }

            mControllersLayout.showAccessButtons(false);

            if (passionIndex == mAttractorLoopView.getCenterPassionIndex()) {
                mPartnerDisplayPager.setCurrentPage(passionIndex, 0);
                startLoopToDisplayAnimation(passionIndex, 0, 0);
            } else {
                mAttractorLoopView.startSmoothLoop(view, passionIndex);
            }
        }
    }

    @Override
    public void onAutoLoopArrived(int passionIndex) {
        mTransitionBackgroundView.setTransitionRatio(passionIndex, 1f, true, false);
        mPartnerDisplayPager.setCurrentPage(passionIndex, 0);
    }

    // OnBackgroundGestureListener
    @Override
    public void onBackgroundTap() {
        if (mAttractorLoopView.isLoopAnimationRunning()) {
            return;
        }

        if ( isSwitchingAnimationRunning() )
            return;

        if (!mPartnerDisplayPager.isShown() ) {
            mPartnerDisplayPager.setCurrentCenterPage(mAttractorLoopView.getCenterPassionIndex(), 0);
            startLoopToDisplayAnimation(mAttractorLoopView.getCenterPassionIndex(), 0, 0);
        } else
            startDisplayToLoopAnimation(true);
    }

    @Override
    public void onFirstTouch(View view) {
        if (mAttractorLoopView.isLoopAnimationRunning()) {
            mAttractorLoopView.stopAttractorLoop();
        }
        if (mTransitionBackgroundView.isTranslateAnimationRunning())
            mTransitionBackgroundView.stopTranslateAnimation();

        stopAutoLoopHandler();
        stopAutoResetHandler();
    }

    @Override
    public void onBackgroundArriving(int targetIndex, float ratio) {
        mAttractorLoopView.loopWithBackground(targetIndex, ratio);

        if (ratio > 0.95f) {
            if (targetIndex == mAppDatamanager.getSamsungIndex()) {
                mControllersLayout.showAccessButtons(true);
            } else {
                mControllersLayout.showAccessButtons(false);
            }
        }
    }

    @Override
    public void onBackgroundArrived(int targetIndex) {
        startAutoResetHandler();
    }
    // OnBackgroundGestureListener

    // CallToActionView.OnCallToActionViewClickListener
    @Override
    public void onCallToActionClicked() {
        stopAutoLoopHandler();
        resetAppTable(GHOST_HAND_FINISH);
    }
    // CallToActionView.OnCallToActionViewClickListener

    /*
     * Reset Message Timing & Inactivity
     */
    public int getResetTimer() {
        switch(mLevel) {
            case LEVEL_1:
                return AUTO_RESET_TIMER_15SEC;

            case LEVEL_2:
                return AUTO_RESET_TIMER_25SEC;

            case LEVEL_3:
                return AUTO_RESET_TIMER_35SEC;

            case LEVEL_4:
                return AUTO_RESET_TIMER_25SEC;
        }
        return AUTO_RESET_TIMER;
    }

    public void startAutoResetHandler() {
        if (mGhostAnimationDefenceView.isGhostHandRunning()) return;
//        if (mVideoLayout.isVideoPlaying()) return;
        if (mAutoResetHandler == null) return;

        mAutoResetHandler.removeCallbacksAndMessages(null);
        mAutoResetHandler.sendEmptyMessageDelayed(0, getResetTimer());
    }

    public void startAutoResetHandler(DisplayLevel level) {
        mLevel = level;

        if (mGhostAnimationDefenceView.isGhostHandRunning()) return;
//        if (mVideoLayout.isVideoPlaying()) return;
        if (mAutoResetHandler == null) return;

        mAutoResetHandler.removeCallbacksAndMessages(null);
        mAutoResetHandler.sendEmptyMessageDelayed(0, getResetTimer());
    }

    public void stopAutoResetHandler() {
        if (mAutoResetHandler == null) return;
        mAutoResetHandler.removeCallbacksAndMessages(null);
    }

    private AnimatorSet mAutoLoopAnimator = null;
    public void startAutoLoopHandler(int delay) {
        if (mAutoLoopAnimator != null && (mAutoLoopAnimator.isRunning() || mAutoLoopAnimator.isStarted())) {
            Log.e(TAG, "startAutoLoopHandler is running");
            return;
        }

        int stackedDelay = 1000;

        final int attractorLoopY = getResources().getDimensionPixelSize(R.dimen.screen_height)
                - (mAttractorLoopView.getMarginBottom() + mAttractorLoopView.getFontL());
        final int attractorLoopStartX = getResources().getDimensionPixelSize(R.dimen.screen_width) / 2;
        final int attractorLoopEndX = getResources().getDimensionPixelSize(R.dimen.screen_width) / 3;
        mAutoLoopAnimator = new AnimatorSet();
        ValueAnimator stepMoveAnimator1 = ValueAnimator.ofInt(attractorLoopStartX, attractorLoopEndX);
        stepMoveAnimator1.setDuration(600);
        stepMoveAnimator1.setStartDelay(stackedDelay);
        stepMoveAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int moveX = (int) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchView(moveX, attractorLoopY);
            }
        });
        stepMoveAnimator1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(false);
                }

                mGhostAnimationDefenceView.show();
                mGhostAnimationDefenceView.setTouchView(attractorLoopStartX, attractorLoopY);
                mAttractorLoopView.startAutoLoopAnimation(mAttractorLoopView.getCenterPassionIndex() + 1);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        // delay 1000ms
        stackedDelay += 2000;
        ValueAnimator stepMoveAnimator2 = ValueAnimator.ofInt(attractorLoopStartX, attractorLoopEndX);
        stepMoveAnimator2.setDuration(600);
        stepMoveAnimator2.setStartDelay(stackedDelay);
        stepMoveAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int moveX = (int) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchView(moveX, attractorLoopY);
            }
        });
        stepMoveAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(attractorLoopStartX, attractorLoopY);
                mAttractorLoopView.startAutoLoopAnimation(mAttractorLoopView.getCenterPassionIndex() + 1);
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });

        // delay 1000ms
        stackedDelay += 2000;
        ValueAnimator backgroundTouchAnimator = ValueAnimator.ofFloat(1f, 2f);
        backgroundTouchAnimator.setDuration(500);
        backgroundTouchAnimator.setStartDelay(stackedDelay);
        backgroundTouchAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        backgroundTouchAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mGhostAnimationDefenceView.show();
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) / 2,
                        getResources().getDimensionPixelSize(R.dimen.screen_height) / 2);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();

                startLoopToDisplayAnimation(mAttractorLoopView.getCenterPassionIndex(), 0, 0); // 1700ms
            }
        });

        stackedDelay += 4000;
        ValueAnimator rightMoveTouchAnimator = ValueAnimator.ofFloat(1f, 2f);
        rightMoveTouchAnimator.setDuration(500);
        rightMoveTouchAnimator.setStartDelay(stackedDelay);
        rightMoveTouchAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        rightMoveTouchAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) - 200, 400);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        ValueAnimator rightMoveAnimator = ValueAnimator.ofFloat(1f, 2f);
        rightMoveAnimator.setDuration(500);
        rightMoveAnimator.setStartDelay(stackedDelay);
        rightMoveAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.startNextPageAnimation(SubcategoryDisplayViewPager.DIRECTION_RIGHT);
            }
        });

        stackedDelay += 2000;
        ValueAnimator rightMoveTouchAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        rightMoveTouchAnimator2.setDuration(500);
        rightMoveTouchAnimator2.setStartDelay(stackedDelay);
        rightMoveTouchAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        rightMoveTouchAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) - 200, 400);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();
            }
        });
        ValueAnimator rightMoveAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        rightMoveAnimator2.setDuration(500);
        rightMoveAnimator2.setStartDelay(stackedDelay);
        rightMoveAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.startNextPageAnimation(SubcategoryDisplayViewPager.DIRECTION_RIGHT);
            }
        });

        // delay 1000ms
        stackedDelay += 2000;
        ValueAnimator backgroundTouchAnimator2 = ValueAnimator.ofFloat(1f, 2f);
        backgroundTouchAnimator2.setDuration(500);
        backgroundTouchAnimator2.setStartDelay(stackedDelay);
        backgroundTouchAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                mGhostAnimationDefenceView.setTouchViewScale(scale, scale);
            }
        });
        backgroundTouchAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mGhostAnimationDefenceView.setTouchView(getResources().getDimensionPixelSize(R.dimen.screen_width) / 2,
//                        getResources().getDimensionPixelSize(R.dimen.screen_height) / 4);
                mGhostAnimationDefenceView.setTouchView(1300, 125); // position of home button
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mGhostAnimationDefenceView.hideTouchView();

                startDisplayToLoopAnimation(true);
            }
        });

        stackedDelay += 2000;
        ValueAnimator sentAnimator = ValueAnimator.ofInt(0, 1);
        sentAnimator.setDuration(500);
        sentAnimator.setStartDelay(stackedDelay);
        sentAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mResetHandler.postDelayed(mResetRunnable, 3000);
            }
        });


        mAutoLoopAnimator.play(stepMoveAnimator1).with(stepMoveAnimator2).with(backgroundTouchAnimator)
                .with(rightMoveTouchAnimator).with(rightMoveAnimator)
                .with(rightMoveTouchAnimator2).with(rightMoveAnimator2)
                .with(backgroundTouchAnimator2).with(sentAnimator);

        mAutoLoopAnimator.setStartDelay(delay);
        mAutoLoopAnimator.start();
    }

    private AutoResetHandler mAutoResetHandler = null;

    private static class AutoResetHandler extends Handler {
        private final WeakReference<FullscreenActivity> mActivity;

        public AutoResetHandler(FullscreenActivity parent) {
            mActivity = new WeakReference<>(parent);
        }

        public void handleMessage(Message msg) {
            FullscreenActivity activity = mActivity.get();
            if (activity != null) {
                activity.handlerResetMessage(msg);
            }
        }
    }

    public void handlerResetMessage(Message msg) {
        //mControllersLayout.showReset();

        //#177 "Paused Youtube video will not trigger application reset prompt" resolved
//        mWaveMessageView.showResetMessageWithAnimation(MixPanelUtil.cause_timeout, getResetMessage());
        resetAppTable(null);
    }


    public boolean isAttractorLoop() {
        return (!mEmailLayout.isShown() && !mPartnerDisplayPager.isShown());
    }

    public void onAvailabilityPressed(boolean withAnimation) {
        if (isSwitchingAnimationRunning())
            return;

        if (mAttractorLoopView.isLoopAnimationRunning()) {
            mAttractorLoopView.stopAttractorLoop();
        }
        if (mTransitionBackgroundView.isTranslateAnimationRunning())
            mTransitionBackgroundView.stopTranslateAnimation();

        stopAutoLoopHandler();
        stopAutoResetHandler();

        Log.d(TAG,"onAvailabilityPressed");

        mControllersLayout.resetAccessButtons();

        if (mMapAvailabilityLayout.isShown()) {
            startMapavailabilityToMain();
        }
        else {
            if (mDeviceTypeLayout.isShown()) {
                startDevicetypeToMapavailability();
            }
            else if (mEmailLayout.isShown()) {
                startEmailToMapavailability();
            }
            else {
                startMainToMapavailability();
            }

            onEnterMapAvailabilityDisplayState();
        }

    }

    public void onDeviceTypePressed(boolean withAnimation) {
        if (isSwitchingAnimationRunning())
            return;

        if (mAttractorLoopView.isLoopAnimationRunning()) {
            mAttractorLoopView.stopAttractorLoop();
        }
        if (mTransitionBackgroundView.isTranslateAnimationRunning())
            mTransitionBackgroundView.stopTranslateAnimation();

        stopAutoLoopHandler();
        stopAutoResetHandler();

        Log.d(TAG,"onDeviceTypePressed");

        mControllersLayout.resetAccessButtons();

        if (!mDeviceTypeLayout.isShown()) {
            if (mMapAvailabilityLayout.isShown()) {
                startMapavailabilityToDevicetype();
            }
            else if (mEmailLayout.isShown()) {
                startEmailToDevicetype();
            }
            else {
                startMainToDevicetype();
            }

            onEnterDeviceTypeDisplayState();
        } else {
            startDevicetypeToMain();
        }


    }

    public void onEmailTypePressed(boolean withAnimation) {
        if (isSwitchingAnimationRunning())
            return;

        if (mAttractorLoopView.isLoopAnimationRunning()) {
            mAttractorLoopView.stopAttractorLoop();
        }
        if (mTransitionBackgroundView.isTranslateAnimationRunning())
            mTransitionBackgroundView.stopTranslateAnimation();

        stopAutoLoopHandler();
        stopAutoResetHandler();

        Log.d(TAG,"onEmailTypePressed");

        mControllersLayout.resetAccessButtons();

        if (!mEmailLayout.isShown()) {
            if (mMapAvailabilityLayout.isShown()) {
                startMapavailabilityToEmail();
            }
            else if (mDeviceTypeLayout.isShown()) {
                startDevicetypeToEmail();
            }
            else {
                startMainToEmail();
            }

            onEnterCheckOutDisplayState();
        } else {
            startEmailToMain();
        }

    }

    public void onHomePressed(boolean withAnimation) {
        if (isSwitchingAnimationRunning())
            return;

        if (mAttractorLoopView.isLoopAnimationRunning()) {
            mAttractorLoopView.stopAttractorLoop();
        }
        if (mTransitionBackgroundView.isTranslateAnimationRunning())
            mTransitionBackgroundView.stopTranslateAnimation();

        stopAutoLoopHandler();
        stopAutoResetHandler();

        Log.d(TAG, "onHomePressed");

        mControllersLayout.resetAccessButtons();

        if (mPartnerDisplayPager.isShown()) {
            startDisplayToLoopAnimation(true);
        }

        getDisplayToLoopAnimation(true);

        mDisplayToLoopAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.setVisibility(View.GONE);
                mPartnerDisplayPager.destroyChildren();

                // show buttons at M4S category
                if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(true);
                } else
                    mControllersLayout.showAccessButtons(false);

                mAttractorLoopView.startSmoothLoopWithDisplayAnimation(mAppDatamanager.getSamsungIndex(), 0, 0);
            }
        });
        mDisplayToLoopAnimator.setStartDelay(10);

        mDisplayToLoopAnimator.start();
    }

    public void onBackPressed(boolean withAnimation) {
        if (mMapAvailabilityLayout.isShown())
            startMapavailabilityToMain();
        else if (mDeviceTypeLayout.isShown())
            startDevicetypeToMain();
        else if (mEmailLayout.isShown())
            startEmailToMain();

        mControllersLayout.resetAccessButtons();
    }

    public boolean isSwitchingAnimationRunning() {
        //#162 Reset/Hekp 아이콘 터치안되는 현상 수정
        return (   (mLoopToDisplayAnimator != null && (mLoopToDisplayAnimator.isRunning() || mLoopToDisplayAnimator.isStarted()))
                || (mDisplayToLoopAnimator != null && (mDisplayToLoopAnimator.isRunning() || mDisplayToLoopAnimator.isStarted()))
                || (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning()))
                || (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning()))
                || (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning()))
                || (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning()))
                || (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning()))
                || (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning())));
    }

    @Override
    public void onDisplayArrived(int finalPassionIndex) {
//        mTransitionBackgroundView.setTransitionRatio(finalPassionIndex, 1, true, mPartnerDisplayPager.isShown());
        Log.d(TAG, "onDisplayArrived (" + finalPassionIndex + ")");
    }

    @Override
    public void onDisplayMovingNeighbourPassion(int toPassionIndex, float ratio) {
        mTransitionBackgroundView.setTransitionRatio(toPassionIndex, ratio, true, true);
        mAttractorLoopView.loopWithBackground(toPassionIndex, ratio);
        if (ratio == 1f) {
            mControllersLayout.setAppTitle(mAppDatamanager.getPassionDataAt(toPassionIndex).getName());

            if (toPassionIndex != mAppDatamanager.getSamsungIndex()) {
                mControllersLayout.showHomeButton(true);
            } else
                mControllersLayout.showHomeButton(false);
        }
    }

    @Override
    public void onDisplayBackgroundTapped() {
        if (mDisplayToLoopAnimator != null && (mDisplayToLoopAnimator.isRunning() || mDisplayToLoopAnimator.isStarted()))
            return;
        if (mLoopToDisplayAnimator != null && (mLoopToDisplayAnimator.isRunning() || mLoopToDisplayAnimator.isStarted()))
            return;

        // 02/22/17 wssun
        // issue #6. Image remains blurred
        // partner display view 상태에서 약간의 swipe 이후에 배경 이미지가 정상으로 되돌아가지 않은 상태에서
        // attractor loop view로 올 경우에 화면이 blur 된 상태로 남는 이슈 수정
        // loop view로 돌아올 때 강제로 clear 이미지로 변경하도록 함
        mTransitionBackgroundView.setTransitionRatio(mAttractorLoopView.getCenterPassionIndex(), 1f, true, true);

        startDisplayToLoopAnimation(true);
    }

    // ===================================================
    // ANIMATORS : DISPLAY > LOOP
    // ===================================================
    private void getDisplayToLoopAnimation(boolean restartAutoLoop) {
        mDisplayToLoopAnimator = new AnimatorSet();
        Animator displayMainAnimator = mPartnerDisplayPager.generateDisplayToLoopAnimator();
        displayMainAnimator.setDuration(700);
        Animator backgroundAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
        backgroundAnimator.setDuration(700);
//        Animator dismissLabelAnimator = mPartnerDisplayPager.generateTitleDismissAnimator();
//        dismissLabelAnimator.setDuration(500);
//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(500);
//        Animator staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
//        staticMessageShow.setDuration(500);
        Animator staticMessageDismiss = mControllersLayout.getAppTitleDismissAnimator();
        staticMessageDismiss.setDuration(500);

        if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
            Animator dismissM4SAnimator = mPartnerDisplayPager.generateM4SDismissAnimator();
            if (dismissM4SAnimator != null) {
                dismissM4SAnimator.setDuration(300);
                mDisplayToLoopAnimator.play(displayMainAnimator).with(backgroundAnimator).after(staticMessageDismiss).after(dismissM4SAnimator);
            } else
                mDisplayToLoopAnimator.play(displayMainAnimator).with(backgroundAnimator).after(staticMessageDismiss);
        }
        else
            mDisplayToLoopAnimator.play(displayMainAnimator).with(backgroundAnimator).after(staticMessageDismiss);
    }

    private void startDisplayToLoopAnimation(final boolean restartAutoLoop) {
        if (mDisplayToLoopAnimator != null && (mDisplayToLoopAnimator.isRunning())) {
            return;
        }

        if (mLoopToDisplayAnimator != null && (mLoopToDisplayAnimator.isRunning() || mLoopToDisplayAnimator.isStarted()))
            mLoopToDisplayAnimator.end();

        getDisplayToLoopAnimation(restartAutoLoop);

        mDisplayToLoopAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.setVisibility(View.GONE);
                mPartnerDisplayPager.destroyChildren();

                // show buttons at M4S category
                if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(true);
                } else
                    mControllersLayout.showAccessButtons(false);

                startAutoResetHandler(DisplayLevel.LEVEL_1);
                onEnterAttractorLoopState();
            }
        });
        mDisplayToLoopAnimator.setStartDelay(10);
        mDisplayToLoopAnimator.start();
    }

    private void setMainToEmailAnimation() {
        mMainToEmailAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 1f, 0f);

        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown()) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 1f, 0f);
        } else {
            mTransitionBackgroundView.stopAnimators();
            viewAnimator = mTransitionBackgroundView.generateBlurBackgroundAnimator();
        }

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
//        Animator staticMessageShow = mControllersLayout.getStaticMessageCheckOutDisplay();
//        staticMessageShow.setDuration(400);
        Animator appTitleShow = mControllersLayout.getM4STitleDisplay("EMAIL");
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mEmailLayout.generateShowAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mEmailLayout.setVisibility(View.VISIBLE);
                mEmailLayout.onReOpenCheckoutWindow();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setOpenHeartMode(true, false);
                startAutoResetHandler(DisplayLevel.LEVEL_4);
            }
        });
        mEmailLayout.setAlpha(0f);
        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mMainToEmailAnimator.play(checkoutAnimator).after(viewAnimator).after(loopDismissAnimator).before(appTitleShow);
        } else
            mMainToEmailAnimator.play(checkoutAnimator).after(loopDismissAnimator).before(appTitleShow);
    }

    private void setEmailToMainAnimation() {
        mEmailToMainAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 0f, 1f);

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
        Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
        appTitleDismiss.setDuration(400);
//        Animator staticMessageShow = null;
        Animator appTitleShow = null;
        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown() && mPartnerDisplayPager.getDisplayCount() > 0) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 0f, 1f);
//            staticMessageShow = mControllersLayout.getStaticMessageSubcategoryDisplay();
            appTitleShow = mControllersLayout.getAppsTitleSubcategoryDisplay(mAppDatamanager.getPassionDataAt(mAttractorLoopView.getCenterPassionIndex()).getName());
        } else {
            viewAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
//            staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
            appTitleShow = mControllersLayout.getAppsTitleAttractorLoop();
        }
//        staticMessageShow.setDuration(400);
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mEmailLayout.generateDismissAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                setOpenHeartMode(false, false);
                if (mPartnerDisplayPager.isShown())
                    startAutoResetHandler(DisplayLevel.LEVEL_3);
//                startAutoLoopHandler(0);
            }
        });

        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mEmailToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(viewAnimator).before(loopDismissAnimator).before(appTitleShow);
        } else
            mEmailToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(loopDismissAnimator).before(appTitleShow);
    }

    private void setMainToDevicetypeAnimation() {
        mMainToDevicetypeAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 1f, 0f);

        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown()) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 1f, 0f);
        } else {
            mTransitionBackgroundView.stopAnimators();
            viewAnimator = mTransitionBackgroundView.generateBlurBackgroundAnimator();
        }

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
//        Animator staticMessageShow = mControllersLayout.getStaticMessageCheckOutDisplay();
//        staticMessageShow.setDuration(400);
        Animator appTitleShow = mControllersLayout.getM4STitleDisplay("Device Type");
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mDeviceTypeLayout.generateShowAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mDeviceTypeLayout.setVisibility(View.VISIBLE);
                mDeviceTypeLayout.onReOpenCheckoutWindow();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setDeviceTypeMode(true, false);
                startAutoResetHandler(DisplayLevel.LEVEL_4);
            }
        });
        mDeviceTypeLayout.setAlpha(0f);
        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mMainToDevicetypeAnimator.play(checkoutAnimator).after(viewAnimator).after(loopDismissAnimator).before(appTitleShow);
        } else
            mMainToDevicetypeAnimator.play(checkoutAnimator).after(loopDismissAnimator).before(appTitleShow);
    }

    private void setDevicetypeToMainAnimation() {
        mDevicetypeToMainAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 0f, 1f);

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
        Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
        appTitleDismiss.setDuration(400);
//        Animator staticMessageShow = null;
        Animator appTitleShow = null;
        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown() && mPartnerDisplayPager.getDisplayCount() > 0) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 0f, 1f);
//            staticMessageShow = mControllersLayout.getStaticMessageSubcategoryDisplay();
            appTitleShow = mControllersLayout.getAppsTitleSubcategoryDisplay(mAppDatamanager.getPassionDataAt(mAttractorLoopView.getCenterPassionIndex()).getName());
        } else {
            viewAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
//            staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
            appTitleShow = mControllersLayout.getAppsTitleAttractorLoop();
        }
//        staticMessageShow.setDuration(400);
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mDeviceTypeLayout.generateDismissAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                setDeviceTypeMode(false, false);
                if (mPartnerDisplayPager.isShown())
                    startAutoResetHandler(DisplayLevel.LEVEL_3);
//                startAutoLoopHandler(0);

                Log.d(TAG, "setDevicetypeToMainAnimation mDeviceTypeLayout.reset");
                mDeviceTypeLayout.reset();
            }
        });

        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mDevicetypeToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(viewAnimator).before(loopDismissAnimator).before(appTitleShow);
        } else
            mDevicetypeToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(loopDismissAnimator).before(appTitleShow);
    }

    private void setMainToMapavailabilityAnimation() {
        mMainToMapavailabilityAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 1f, 0f);

        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown()) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 1f, 0f);
        } else {
            mTransitionBackgroundView.stopAnimators();
            viewAnimator = mTransitionBackgroundView.generateBlurBackgroundAnimator();
        }

        Animator appTitleShow = mControllersLayout.getM4STitleDisplay("Availability");
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mMapAvailabilityLayout.generateShowAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mMapAvailabilityLayout.setVisibility(View.VISIBLE);
                mMapAvailabilityLayout.onReOpenCheckoutWindow();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setMapAvailabilityMode(true, false);
                startAutoResetHandler(DisplayLevel.LEVEL_4);
            }
        });
        mMapAvailabilityLayout.setAlpha(0f);
        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mMainToMapavailabilityAnimator.play(checkoutAnimator).after(viewAnimator).after(loopDismissAnimator).before(appTitleShow);
        } else
            mMainToMapavailabilityAnimator.play(checkoutAnimator).after(loopDismissAnimator).before(appTitleShow);
    }

    private void setMapavailabilityToMainAnimation() {
        mMapavailabilityToMainAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 0f, 1f);

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
        Animator appTitleDismiss = mControllersLayout.getM4STitleDismissAnimator();
        appTitleDismiss.setDuration(400);
//        Animator staticMessageShow = null;
        Animator appTitleShow = null;
        Animator viewAnimator = null;
        if (mPartnerDisplayPager.isShown() && mPartnerDisplayPager.getDisplayCount() > 0) {
            viewAnimator = ObjectAnimator.ofFloat(mPartnerDisplayPager, "alpha", 0f, 1f);
//            staticMessageShow = mControllersLayout.getStaticMessageSubcategoryDisplay();
            appTitleShow = mControllersLayout.getAppsTitleSubcategoryDisplay(mAppDatamanager.getPassionDataAt(mAttractorLoopView.getCenterPassionIndex()).getName());
        } else {
            viewAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
//            staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
            appTitleShow = mControllersLayout.getAppsTitleAttractorLoop();
        }
//        staticMessageShow.setDuration(400);
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mMapAvailabilityLayout.generateDismissAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                setMapAvailabilityMode(false, false);
                if (mPartnerDisplayPager.isShown())
                    startAutoResetHandler(DisplayLevel.LEVEL_3);
//                startAutoLoopHandler(0);

                // show buttons at M4S category
                if (mAttractorLoopView.getCenterPassionIndex() == mAppDatamanager.getSamsungIndex()) {
                    mControllersLayout.showAccessButtons(true);
                } else
                    mControllersLayout.showAccessButtons(false);

                mMapAvailabilityLayout.reset();
            }
        });

        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
            mMapavailabilityToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(viewAnimator).before(loopDismissAnimator).before(appTitleShow);
        } else
            mMapavailabilityToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(loopDismissAnimator).before(appTitleShow);
    }

    public void startMainToEmail() {
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        setMainToEmailAnimation();

        mMainToEmailAnimator.start();
    }

    public void startEmailToMain() {
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        setEmailToMainAnimation();

        mEmailToMainAnimator.start();
    }

    public void startMainToDevicetype() {
        // device show animation
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        setMainToDevicetypeAnimation();

        mMainToDevicetypeAnimator.start();
    }

    public void startDevicetypeToMain() {
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        setDevicetypeToMainAnimation();

        mDevicetypeToMainAnimator.start();
    }

    public void startMainToMapavailability() {
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning()))// || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        setMainToMapavailabilityAnimation();

        mMainToMapavailabilityAnimator.start();
    }

    public void startMapavailabilityToMain() {
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        setMapavailabilityToMainAnimation();

        mMapavailabilityToMainAnimator.start();
    }



    public void startMapavailabilityToDevicetype() {
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        // device show animation
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        setMapavailabilityToMainAnimation();
        setMainToDevicetypeAnimation();

        mMapavailabilityToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToDevicetypeAnimator.start();
            }
        });

        mMapavailabilityToMainAnimator.start();
    }

    public void startMapavailabilityToEmail() {
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        // device show animation
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        setMapavailabilityToMainAnimation();
        setMainToEmailAnimation();

        mMapavailabilityToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToEmailAnimator.start();
            }
        });

        mMapavailabilityToMainAnimator.start();
    }

    public void startDevicetypeToMapavailability() {
        // device
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        // map availability
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        setDevicetypeToMainAnimation();
        setMainToMapavailabilityAnimation();

        mDevicetypeToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToMapavailabilityAnimator.start();
            }
        });

        mDevicetypeToMainAnimator.start();
    }

    public void startDevicetypeToEmail() {
        // device show animation
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        // email
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        setDevicetypeToMainAnimation();
        setMainToEmailAnimation();

        mDevicetypeToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToEmailAnimator.start();
            }
        });

        mDevicetypeToMainAnimator.start();
    }

    public void startEmailToMapavailability() {
        // email
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        // map availability
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        setEmailToMainAnimation();
        setMainToMapavailabilityAnimation();

        mEmailToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToMapavailabilityAnimator.start();
            }
        });

        mEmailToMainAnimator.start();
    }

    public void startEmailToDevicetype() {
        // email
        if (mMainToEmailAnimator != null && (mMainToEmailAnimator.isRunning() || mMainToEmailAnimator.isStarted()))
            return;
        if (mEmailToMainAnimator != null && (mEmailToMainAnimator.isRunning() || mEmailToMainAnimator.isStarted()))
            return;

        // device show animation
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        setEmailToMainAnimation();
        setMainToDevicetypeAnimation();

        mEmailToMainAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainToDevicetypeAnimator.start();
            }
        });

        mEmailToMainAnimator.start();
    }

    @Override
    public void forwardEvent2Parent(MotionEvent event) {
        mPartnerDisplayPager.forwardEvent2SubDispPager(event);
    }

    @Override
    public boolean checkDisplayLayoutBeforeYoutubePlay() {
        if (mDisplayToLoopAnimator != null && mDisplayToLoopAnimator.isRunning()) {
            return false;
        } else if (mPartnerDisplayPager.getVisibility() == View.VISIBLE) {
            return true;
        }
        return false;
    }

    //ICANMOBILE - 20160116
    @Override
    public void onStartVideo() {
        Log.d(TAG, "##### onStartVideo : stopAutoResetHandler ");
        stopAutoResetHandler();
    }

    //#177 "Paused Youtube video will not trigger application reset prompt" resolved
    @Override
    public void onPauseVideo() {
        startAutoResetHandler();
    }

    @Override
    public void onStopVideo() {
        //set Visibility Thumbnail
        startAutoResetHandler();
    }

    /*
        @Override
        public void updateString(String email, int cursorIndex) {
            mEmailLayout.updateString(email, cursorIndex);
        }

        @Override
        public void enterPressed() {
            mEmailLayout.enterPressed();
        }

        @Override
        public void onSetOpenHeartMode(boolean isOpen, boolean isReseting) {
            if (!isOpen && !isReseting) {
                startEmailToMain();
                mControllersLayout.updateSavedAppCount(mAppTableDataManager.getSelectedAppItems().size());
            }
        }
    */
    public void setOpenHeartMode(boolean isOpen, boolean isReseting) {
        if (isOpen) {
            // WILL OPEN HEART
            mEmailLayout.setAppTableDataManager(mAppTableDataManager);
            if (mPartnerDisplayPager.getVisibility() == View.VISIBLE) {
                mLvBeforeOpenHeart = RecoveryData.LEVEL_SUB_DISPLAY;
            } else {
                if (!isReseting) {
                    mLvBeforeOpenHeart = RecoveryData.LEVEL_ATTRACTOR_LOOP;
                }
            }
            MixPanelUtil.page_view(MixPanelUtil.pageview_Display_saved_app);
        } else {
            mEmailLayout.clearEmailEditText();
            mEmailLayout.setVisibility(View.GONE);
            if (isReseting) {
                mPartnerDisplayPager.setVisibility(View.GONE);
                mTransitionBackgroundView.setBlurBackground(false);
            } else {
                if (mLvBeforeOpenHeart == RecoveryData.LEVEL_SUB_DISPLAY) {
                    mPartnerDisplayPager.setVisibility(View.VISIBLE);
                } else {
//                    startAutoLoopHandler(0);
                    startAutoResetHandler();
                    mTransitionBackgroundView.setBlurBackground(false);
                }
            }
        }
    }

    public void setDeviceTypeMode(boolean isOpen, boolean isReseting) {
        if (isOpen) {
            // WILL OPEN HEART
            mDeviceTypeLayout.setAppTableDataManager(mAppTableDataManager);
            if (mPartnerDisplayPager.getVisibility() == View.VISIBLE) {
                mLvBeforeOpenHeart = RecoveryData.LEVEL_SUB_DISPLAY;
            } else {
                if (!isReseting) {
                    mLvBeforeOpenHeart = RecoveryData.LEVEL_ATTRACTOR_LOOP;
                }
            }
            MixPanelUtil.page_view(MixPanelUtil.pageview_Display_saved_app);
        } else {
//?            mDeviceTypeLayout.clearEmailEditText();

            Log.d(TAG, "setDeviceTypeMode isOpen isReseting");

            mDeviceTypeLayout.setVisibility(View.GONE);
            if (isReseting) {
                mPartnerDisplayPager.setVisibility(View.GONE);
                mTransitionBackgroundView.setBlurBackground(false);
            } else {
                if (mLvBeforeOpenHeart == RecoveryData.LEVEL_SUB_DISPLAY) {
                    mPartnerDisplayPager.setVisibility(View.VISIBLE);
                } else {
//                    startAutoLoopHandler(0);
                    startAutoResetHandler();
                    mTransitionBackgroundView.setBlurBackground(false);
                }
            }
        }
    }

    public void setMapAvailabilityMode(boolean isOpen, boolean isReseting) {
        if (isOpen) {
            // WILL OPEN HEART
            mMapAvailabilityLayout.setAppTableDataManager(mAppTableDataManager);
            if (mPartnerDisplayPager.getVisibility() == View.VISIBLE) {
                mLvBeforeOpenHeart = RecoveryData.LEVEL_SUB_DISPLAY;
            } else {
                if (!isReseting) {
                    mLvBeforeOpenHeart = RecoveryData.LEVEL_ATTRACTOR_LOOP;
                }
            }
            MixPanelUtil.page_view(MixPanelUtil.pageview_Display_saved_app);
        } else {
//?            mMapAvailabilityLayout.clearEmailEditText();
            mMapAvailabilityLayout.setVisibility(View.GONE);
            if (isReseting) {
                mPartnerDisplayPager.setVisibility(View.GONE);
                mTransitionBackgroundView.setBlurBackground(false);
            } else {
                if (mLvBeforeOpenHeart == RecoveryData.LEVEL_SUB_DISPLAY) {
                    mPartnerDisplayPager.setVisibility(View.VISIBLE);
                } else {
//                    startAutoLoopHandler(0);
                    startAutoResetHandler();
                    mTransitionBackgroundView.setBlurBackground(false);
                }
            }
        }
    }

    private void onEnterAttractorLoopState() {
        MixPanelUtil.page_view(MixPanelUtil.pageview_Attractor_Loop);
    }

    private void onEnterSubcategoryDisplayState() {
        MixPanelUtil.page_view(MixPanelUtil.pageview_Display_selected_subcategory);
    }

    private void onEnterCheckOutDisplayState() {
        MixPanelUtil.page_view(MixPanelUtil.pageview_Display_selected_subcategory);
    }

    private void onEnterDeviceTypeDisplayState() {
        MixPanelUtil.page_view(MixPanelUtil.pageview_Display_selected_subcategory);
    }

    private void onEnterMapAvailabilityDisplayState() {
        MixPanelUtil.page_view(MixPanelUtil.pageview_Display_selected_subcategory);
    }

    @Override
    public void quitApplication() {
        sendBroadcast(new Intent(AppConst.ACTION_ENABLE_ADMIN_MODE));
        Exit(false);
    }

    @Override
    public void onSendingEmail() {
        mWaveMessageView.showSendingMessageWithAnimation();
    }

    @Override
    public void onCompleteSendEmail() {

        SpannableStringBuilder ss = mWaveMessageView.getHelpBoldMessage(getString(R.string.message_session_end_reset_undo_clear_favorites), "App Table");
        mWaveMessageView.showResetMessageWithAnimation(MixPanelUtil.cause_timeout, ss);
    }

    public void startMapAvailabilityToAppDetail(final int passionIndex, final int subcategoryIndex, final int appIndex) {
        if (mMainToMapavailabilityAnimator != null && (mMainToMapavailabilityAnimator.isRunning() || mMainToMapavailabilityAnimator.isStarted()))
            return;
        if (mMapavailabilityToMainAnimator != null && (mMapavailabilityToMainAnimator.isRunning() || mMapavailabilityToMainAnimator.isStarted()))
            return;

        mMapavailabilityToMainAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 0f, 1f);

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
        Animator appTitleDismiss = mControllersLayout.getAppTitleDismissAnimator();
        appTitleDismiss.setDuration(400);

        Animator viewAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
//        Animator staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
//        staticMessageShow.setDuration(400);
        Animator appTitleShow = mControllersLayout.getM4STitleDismissAnimator();
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mEmailLayout.generateDismissAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mPartnerDisplayPager.setVisibility(View.GONE);
                mMapAvailabilityLayout.setVisibility(View.GONE);
                mMapAvailabilityLayout.reset();
                mControllersLayout.resetAccessButtons();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setOpenHeartMode(false, false);
                if (passionIndex == mAttractorLoopView.getCenterPassionIndex()) {
                    startLoopToDisplayAnimation(passionIndex, subcategoryIndex, appIndex);
                } else {
                    mAttractorLoopView.startSmoothLoopWithDisplayAnimation(passionIndex, subcategoryIndex, appIndex);
                }
                startAutoResetHandler(DisplayLevel.LEVEL_3);
            }
        });

        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
//            mEmailToMainAnimator.play(checkoutAnimator).with(staticMessageDismiss).before(viewAnimator).before(loopDismissAnimator).before(staticMessageShow);
            mMapavailabilityToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(viewAnimator).before(loopDismissAnimator).before(appTitleShow);
        } else
//            mEmailToMainAnimator.play(checkoutAnimator).with(staticMessageDismiss).before(loopDismissAnimator).before(staticMessageShow);
            mMapavailabilityToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(loopDismissAnimator).before(appTitleShow);
        mMapavailabilityToMainAnimator.start();
    }


    public void startDeviceTypeToAppDetail(final int passionIndex, final int subcategoryIndex, final int appIndex) {
        if (mMainToDevicetypeAnimator != null && (mMainToDevicetypeAnimator.isRunning() || mMainToDevicetypeAnimator.isStarted()))
            return;
        if (mDevicetypeToMainAnimator != null && (mDevicetypeToMainAnimator.isRunning() || mDevicetypeToMainAnimator.isStarted()))
            return;

        mDevicetypeToMainAnimator = new AnimatorSet();
        ObjectAnimator loopDismissAnimator = ObjectAnimator.ofFloat(mAttractorLoopView, "alpha", 0f, 1f);

//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(400);
        Animator appTitleDismiss = mControllersLayout.getAppTitleDismissAnimator();
        appTitleDismiss.setDuration(400);

        Animator viewAnimator = mTransitionBackgroundView.generateClearBackgroundAnimator();
//        Animator staticMessageShow = mControllersLayout.getStaticMessageAttractorLoop();
//        staticMessageShow.setDuration(400);
        Animator appTitleShow = mControllersLayout.getM4STitleDismissAnimator();
        appTitleShow.setDuration(400);
        Animator checkoutAnimator = mEmailLayout.generateDismissAnimator();
        checkoutAnimator.setDuration(400);
        checkoutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mPartnerDisplayPager.setVisibility(View.GONE);
                mDeviceTypeLayout.setVisibility(View.GONE);
                mDeviceTypeLayout.reset();
                mControllersLayout.resetAccessButtons();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setOpenHeartMode(false, false);
                if (passionIndex == mAttractorLoopView.getCenterPassionIndex()) {
                    startLoopToDisplayAnimation(passionIndex, subcategoryIndex, appIndex);
                } else {
                    mAttractorLoopView.startSmoothLoopWithDisplayAnimation(passionIndex, subcategoryIndex, appIndex);
                }
                startAutoResetHandler(DisplayLevel.LEVEL_3);
            }
        });

        if (viewAnimator != null) {
            viewAnimator.setDuration(400);
//            mEmailToMainAnimator.play(checkoutAnimator).with(staticMessageDismiss).before(viewAnimator).before(loopDismissAnimator).before(staticMessageShow);
            mDevicetypeToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(viewAnimator).before(loopDismissAnimator).before(appTitleShow);
        } else
//            mEmailToMainAnimator.play(checkoutAnimator).with(staticMessageDismiss).before(loopDismissAnimator).before(staticMessageShow);
            mDevicetypeToMainAnimator.play(checkoutAnimator).with(appTitleDismiss).before(loopDismissAnimator).before(appTitleShow);
        mDevicetypeToMainAnimator.start();
    }

    private void startLoopToDisplayAnimation(final int passion, final int subcategory, final int appIndex) {
        if (isSwitchingAnimationRunning())
            return;

        if (mLoopToDisplayAnimator != null && (mLoopToDisplayAnimator.isRunning())) {
            return;
        }

        if (mDisplayToLoopAnimator != null && (mDisplayToLoopAnimator.isRunning() || mDisplayToLoopAnimator.isStarted()))
            mDisplayToLoopAnimator.end();

        mPartnerDisplayPager.setCurrentCenterPage(passion, subcategory);
        mPartnerDisplayPager.setAppItem(appIndex);
        mLoopToDisplayAnimator = new AnimatorSet();
        Animator displayMainAnimator = mPartnerDisplayPager.generateLoopToDisplayAnimator();
        displayMainAnimator.setDuration(300);
//        Animator displayLabelAnimator = mPartnerDisplayPager.generateTitleShowAnimator();
//        displayLabelAnimator.setDuration(300);
        mTransitionBackgroundView.stopAnimators();
        Animator backgroundAnimator = mTransitionBackgroundView.generateBlurBackgroundAnimator();
        backgroundAnimator.setDuration(300);
//        Animator staticMessageDismiss = mControllersLayout.getStaticMessageDismissAnimator();
//        staticMessageDismiss.setDuration(300);
//        Animator staticMessageShow = mControllersLayout.getStaticMessageSubcategoryDisplay();
//        staticMessageShow.setDuration(300);
//        mLoopToDisplayAnimator.play(displayMainAnimator).with(backgroundAnimator).after(staticMessageDismiss).before(staticMessageShow).before(displayLabelAnimator);
        Animator appTitleShow = mControllersLayout.getAppsTitleSubcategoryDisplay(mAppDatamanager.getPassionDataAt(mAttractorLoopView.getCenterPassionIndex()).getName());
        appTitleShow.setDuration(300);

        if (passion == mAppDatamanager.getSamsungIndex()) {
            mControllersLayout.showAccessButtons(false); // hide button

            Animator displayM4SAnimator = mPartnerDisplayPager.generateM4SShowAnimator();
            displayM4SAnimator.setDuration(1000);
            mLoopToDisplayAnimator.play(displayMainAnimator).with(backgroundAnimator).before(appTitleShow).before(displayM4SAnimator);
        } else
            mLoopToDisplayAnimator.play(displayMainAnimator).with(backgroundAnimator).before(appTitleShow);

        mLoopToDisplayAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                stopAutoLoopHandler();
                mPartnerDisplayPager.setVisibility(View.VISIBLE);
                mPartnerDisplayPager.setAlpha(0f);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mPartnerDisplayPager.setCurrentNeighbourPage(passion, subcategory);
                mPartnerDisplayPager.getRecoveryData().setRecoveryAllIndex(passion, subcategory, appIndex);
                onEnterSubcategoryDisplayState();
                MixPanelUtil.content_view(mAppDatamanager.getPassionDataAt(passion).getSubcategoryDataAt(subcategory).getSubcategoryName());
                startAutoResetHandler(DisplayLevel.LEVEL_3);
            }
        });
        mLoopToDisplayAnimator.setStartDelay(10);
        mLoopToDisplayAnimator.start();
    }

    @Override
    public void onSetOpenHeartMode(boolean isOpen, boolean isReseting) {
        if (!isOpen && !isReseting) {
            mControllersLayout.resetAccessButtons();

            startEmailToMain();
        }
    }

    @Override
    public void onSentEmail() {
        mWaveMessageView.showSentMessage();
    }

    @Override
    public void onErrorOccured() {
        mWaveMessageView.showErrorMessageWithAnimation();
    }


    @Override
    public void updateString(String email, int cursorIndex) {
        mEmailLayout.updateString(email, cursorIndex);
    }

    @Override
    public void enterPressed() {
        mEmailLayout.enterPressed();
    }


    //DEVICE TYPE CALLBACK
    @Override
    public void devicetype_onSetOpenHeartMode(boolean isOpen, boolean isReseting){
        if (!isOpen && !isReseting) {
            mControllersLayout.resetAccessButtons();

            startDevicetypeToMain();
        }
    }

    @Override
    public void devicetype_onSelectedAppItemClick(int PassionIndex, int SubcategoryIndex){

        Log.d(TAG, "devicetype_onSelectedAppItemClick ::: " + PassionIndex + " : " + SubcategoryIndex);

        startDeviceTypeToAppDetail (PassionIndex, SubcategoryIndex, 0);
    }

    //MAP AVAILABILITY CALLBACK
    @Override
    public void mapavailability_onSetOpenHeartMode(boolean isOpen, boolean isReseting){
        if (!isOpen && !isReseting) {
            mControllersLayout.resetAccessButtons();

            startMapavailabilityToMain();
        }
    }

    @Override
    public void mapavailability_onSelectedAppItemClick(int PassionIndex, int SubcategoryIndex){

        Log.d(TAG, "mapavailability_onSelectedAppItemClick ::: " + PassionIndex + " : " + SubcategoryIndex);

        startMapAvailabilityToAppDetail (PassionIndex, SubcategoryIndex, 0);
    }
}
