package com.tecace.showcase.data;

import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

/**
 * Created by swkim on 2015-03-19.
 */
public class AppTableDataManager implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String TAG = this.getClass().getSimpleName();

//?    private AppDataManager mAppDataManager = null;

    private LinkedHashMap<String, ApplicationIndex> devicetypePhoneItems;
    private LinkedHashMap<String, ApplicationIndex> devicetypeTabItems;
    private LinkedHashMap<String, ApplicationIndex> devicetypeGearItems;
    private LinkedHashMap<String, ApplicationIndex> mapavailabilityAmericaItems;
    private LinkedHashMap<String, ApplicationIndex> mapavailabilityEuropeItems;
    private LinkedHashMap<String, ApplicationIndex> mapavailabilityAsiaItems;
    private LinkedHashMap<String, ApplicationIndex> mapavailabilityRestItems;

    public AppTableDataManager(Context context, AppDataManager mAppDatamanager){

        devicetypePhoneItems = new LinkedHashMap<String, ApplicationIndex>();
        devicetypeTabItems = new LinkedHashMap<String, ApplicationIndex>();
        devicetypeGearItems = new LinkedHashMap<String, ApplicationIndex>();
        mapavailabilityAmericaItems = new LinkedHashMap<String, ApplicationIndex>();
        mapavailabilityEuropeItems = new LinkedHashMap<String, ApplicationIndex>();
        mapavailabilityAsiaItems = new LinkedHashMap<String, ApplicationIndex>();
        mapavailabilityRestItems = new LinkedHashMap<String, ApplicationIndex>();

        int subcategoryIndex = 0;
        int subcategorySize = 0;
        int numberofPassion = mAppDatamanager.getNumberOfPassions();
        SubcategoryData subcategoryData;
        int deviceType;
        int mapAvailability;

        for (int i = 0; i < numberofPassion; i++) {

            subcategorySize = mAppDatamanager.getPassionDataAt(i).getSubcategories().size();

            Log.d(TAG, "AppTableDataManager numberofPassion : " + numberofPassion + " subcategorySize : " + subcategorySize + " subcategorySize : " + subcategorySize);

            for (int j = 0 ; j < subcategorySize; j++) {
                subcategoryData = mAppDatamanager.getPassionDataAt(i).getSubcategoryDataAt(j);

                deviceType = subcategoryData.getDeviceInfo();
                if ((deviceType & SubcategoryData.DEVICE_PHONE) > 0) {
                    devcietypePhoneItem(subcategoryData, i, j);
                }
                if ((deviceType & SubcategoryData.DEVICE_TABLET) > 0) {
                    devcietypeTabItem(subcategoryData, i, j);
                }
                if ((deviceType & SubcategoryData.DEVICE_S3) > 0) {
                    devcietypeGearItem(subcategoryData, i, j);
                }

                mapAvailability = subcategoryData.getRegionInfo();
                if ((mapAvailability & SubcategoryData.REGION_AMERICA) > 0) {
                    mapavailabilityAmericaItem(subcategoryData, i, j);
                }
                if ((mapAvailability & SubcategoryData.REGION_EUROPE) > 0) {
                    mapavailabilityEuropeItem(subcategoryData, i, j);
                }
                if ((mapAvailability & SubcategoryData.REGION_ASIA) > 0) {
                    mapavailabilityAsiaItem(subcategoryData, i, j);
                }
                if ((mapAvailability & SubcategoryData.REGION_REST) > 0) {
                    mapavailabilityRestItem(subcategoryData, i, j);
                }
            }
        }

        Log.d(TAG, "AppTableDataManager getdevicetypePhoneAppItems size : " + getdevicetypePhoneAppItems().size());
        Log.d(TAG, "AppTableDataManager getdevicetypeTabAppItems size : " + getdevicetypeTabAppItems().size());
        Log.d(TAG, "AppTableDataManager getdevicetypeGearItems size : " + getdevicetypeGearItems().size());
        Log.d(TAG, "AppTableDataManager getmapavailabilityAmericaAppItems size : " + getmapavailabilityAmericaAppItems().size());
        Log.d(TAG, "AppTableDataManager getmapavailabilityEuropeAppItems size : " + getmapavailabilityEuropeAppItems().size());
        Log.d(TAG, "AppTableDataManager getmapavailabilityAsiaItems size : " + getmapavailabilityAsiaItems().size());
        Log.d(TAG, "AppTableDataManager getmapavailabilityRestAppItems size : " + getmapavailabilityRestAppItems().size());

    }


   // ADD ITEM TO DEVCIE TYPE PHONE
    public void devcietypePhoneItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        devicetypePhoneItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO DEVCIE TYPE TAB
    public void devcietypeTabItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        devicetypeTabItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO DEVCIE TYPE GEAR
    public void devcietypeGearItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        devicetypeGearItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO MAP AVAILABILITY AMERICA
    public void mapavailabilityAmericaItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        mapavailabilityAmericaItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO MAP AVAILABILITY EUROPE
    public void mapavailabilityEuropeItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        mapavailabilityEuropeItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO MAP AVAILABILITY ASIA
    public void mapavailabilityAsiaItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        mapavailabilityAsiaItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    // ADD ITEM TO MAP AVAILABILITY REST
    public void mapavailabilityRestItem(SubcategoryData item, int PassionIndex, int SubcategoryIndex){

        mapavailabilityRestItems.put(item.getSubcategoryName(), new ApplicationIndex(PassionIndex, SubcategoryIndex));

    }

    public ArrayList<ApplicationIndex> getdevicetypePhoneAppItems(){
        return new ArrayList<ApplicationIndex>(devicetypePhoneItems.values());
    }
    public ArrayList<ApplicationIndex> getdevicetypeTabAppItems(){
        return new ArrayList<ApplicationIndex>(devicetypeTabItems.values());
    }
    public ArrayList<ApplicationIndex> getdevicetypeGearItems(){
        return new ArrayList<ApplicationIndex>(devicetypeGearItems.values());
    }
    public ArrayList<ApplicationIndex> getmapavailabilityAmericaAppItems(){
        return new ArrayList<ApplicationIndex>(mapavailabilityAmericaItems.values());
    }
    public ArrayList<ApplicationIndex> getmapavailabilityEuropeAppItems(){
        return new ArrayList<ApplicationIndex>(mapavailabilityEuropeItems.values());
    }
    public ArrayList<ApplicationIndex> getmapavailabilityAsiaItems(){
        return new ArrayList<ApplicationIndex>(mapavailabilityAsiaItems.values());
    }
    public ArrayList<ApplicationIndex> getmapavailabilityRestAppItems(){
        return new ArrayList<ApplicationIndex>(mapavailabilityRestItems.values());
    }
}
