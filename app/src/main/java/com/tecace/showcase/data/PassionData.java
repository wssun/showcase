package com.tecace.showcase.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wssun on 2015-05-13.
 */
public class PassionData implements Serializable {
    private static final String TAG = PassionData.class.getName();

    @SerializedName("name")
    private String name;

    @SerializedName("backgroundImage1")
    private String backgroundImage1;

    @SerializedName("backgroundImage2")
    private String backgroundImage2;

    @SerializedName("subCategoryDataSet")
    private List<SubcategoryData> subCategoryDataSet;

    @SerializedName("priority")
    private int priority;

    private int mPassionColor; // color index. calculated by passion count

    public PassionData(){
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getClearBgImage() {
        return this.backgroundImage1;
    }
    public void setClearBgImage(String backgroundImage1) {
        this.backgroundImage1 = backgroundImage1;
    }

    public String getBlurBgImage() {
        return this.backgroundImage2;
    }
    public void setBlurBgImage(String backgroundImage2) {
        this.backgroundImage2 = backgroundImage2;
    }

    public int getPassionColor() {
        return this.mPassionColor;
    }
    public void setPassionColor(int passionColor) {
        this.mPassionColor = passionColor;
    }

    public int getPassionIndex() { // index starts at 0
        return this.priority - 1;
    }

    public List<SubcategoryData> getSubcategories() {
        return subCategoryDataSet;
    }

    public void setSubcategories(List<SubcategoryData> subcategories) {
        this.subCategoryDataSet = subcategories;
    }

    public SubcategoryData getSubcategoryDataAt(int index){
        return this.subCategoryDataSet.get(index);
    }

    public void addSubcategory(SubcategoryData item){
        subCategoryDataSet.add(item);
    }
    public void removeSubcategoryItem(SubcategoryData item){
        this.subCategoryDataSet.remove(item);
    }

    public void removeSubcategoryDataAt(int index){
        this.subCategoryDataSet.remove(index);
    }

    public int getPriority() {
        return this.priority;
    }
    public void setPriority(int priority) {
        this.priority = priority;
    }
}
