package com.tecace.showcase.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class SubPageData implements Serializable {
    private static final String TAG = SubPageData.class.getName();

    public static final int ACTION_CLICK = 1;
    public static final int ACTION_TIMER = 2;
    public static final int ACTION_ALL = 4;
    public static final int ACTION_PHONE = 8;

    private int actionInfo = -1;

    @SerializedName("index")
    private String index;

    @SerializedName("pageView")
    private String pageView;

    @SerializedName("nextAction")
    private String nextAction;

    @SerializedName("leftTopX")
    private String leftTopX;

    @SerializedName("leftTopY")
    private String leftTopY;

    @SerializedName("rightBottomX")
    private String rightBottomX;

    @SerializedName("rightBottomY")
    private String rightBottomY;

    public SubPageData() {
    }

    public SubPageData(String index,
                           String pageView,
                           String nextAction,
                           String leftTopX,
                           String leftTopY,
                           String rightBottomX,
                           String rightBottomY) {
        this.index = index;
        this.pageView = pageView;
        this.nextAction = nextAction;
        this.leftTopX = leftTopX;
        this.leftTopY = leftTopY;
        this.rightBottomX = rightBottomX;
        this.rightBottomY = rightBottomY;
    }

    public String getindex() {
        return this.index;
    }
    public void setindex(String passionName) {
        this.index = index;
    }

    public String getpageView() {
        return this.pageView;
    }
    public void setpageView(String pageView) {
        this.pageView = pageView;
    }

    public int getnextAction() {
        if (actionInfo == -1) { // not initialized by this.devices
            actionInfo = 0;

            if (this.nextAction == null)
                return actionInfo;

            if (this.nextAction.contains("click"))
                actionInfo += ACTION_CLICK;

            if (this.nextAction.contains("timer"))
                actionInfo += ACTION_TIMER;

            if (this.nextAction.contains("all"))
                actionInfo += ACTION_ALL;

            if (this.nextAction.contains("phone"))
                actionInfo += ACTION_PHONE;
        }

        return actionInfo;
    }
//?    public void setnextAction(String nextAction) {
//?        this.nextAction = nextAction;
//?    }

    public String getleftTopX() {
        return this.leftTopX;
    }
    public void setleftTopX(String leftTopX) {
        this.leftTopX = leftTopX;
    }

    public String getleftTopY() {
        return this.leftTopY;
    }
    public void setleftTopY(String leftTopY) {
        this.leftTopY = leftTopY;
    }

    public String getrightBottomX() {
        return this.rightBottomX;
    }
    public void setrightBottomX(String rightBottomX) {
        this.rightBottomX = rightBottomX;
    }

    public String getrightBottomY() {
        return this.rightBottomY;
    }
    public void setrightBottomY(String rightBottomY) {
        this.rightBottomY = rightBottomY;
    }

}
