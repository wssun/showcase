package com.tecace.showcase.data;

import java.io.Serializable;

/**
 * Created by wssun on 2015-05-13.
 */
public class ApplicationIndex implements Serializable {
    private static final String TAG = ApplicationIndex.class.getName();

    private int mPassionIndex;
    private int mSubcategoryIndex;

    private boolean isDelete = false;

    public ApplicationIndex() {
    }

    public ApplicationIndex(int passion,
                            int subcategory) {
        this.mPassionIndex      = passion;
        this.mSubcategoryIndex = subcategory;
    }

    public int getPassionIndex() {
        return this.mPassionIndex;
    }
    public void setPassionIndex(int passionIndex) {
        this.mPassionIndex = passionIndex;
    }

    public int getSubcategoryIndex() {
        return this.mSubcategoryIndex;
    }
    public void setSubcategoryIndex(int subcategoryIndex) {
        this.mSubcategoryIndex = subcategoryIndex;
    }
}
