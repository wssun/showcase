package com.tecace.showcase.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wssun on 2015-05-13.
 */
public class ApplicationData implements Serializable, Cloneable {
    private static final String TAG = ApplicationData.class.getName();

    @SerializedName("packageName")
    private String packageName;

    @SerializedName("subCategory")
    private String subCategory;

    @SerializedName("contentRating")
    private String contentRating;

    @SerializedName("appName")
    private String appName;

    @SerializedName("price")
    private String price;

    @SerializedName("videoUrls")
    private List<String> videoUrls;

    @SerializedName("publisherName")
    private String publisherName;

    @SerializedName("allRating")
    private float allRating;

    @SerializedName("appType")
    private String appType;

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("sharedIconUrl")
    private String sharedIconUrl;

    @SerializedName("version")
    private String version;

    @SerializedName("publisherUrl")
    private String publisherUrl;

    @SerializedName("publishId")
    private String publishId;

    @SerializedName("downloads")
    private String downloads;

    @SerializedName("screenShotUrls")
    private List<String> screenShotUrls;

    @SerializedName("largeScreenShotUrls")
    private List<String> largeScreenShotUrls;

    @SerializedName("sharedScreenShotUrls")
    private List<String> sharedScreenShotUrls;

    @SerializedName("description")
    private String description;

    @SerializedName("genre")
    private String genre;

    @SerializedName("allRatingCount")
    private int allRatingCount;

    @SerializedName("allHistogram")
    private RatingData allHistogram;

    @SerializedName("requiresOS")
    private String requiresOS;

    @SerializedName("fileSize")
    private String fileSize;

    @SerializedName("statusDate")
    private String statusDate;

    @SerializedName("mobileDescription")
    private String mobileDescription;

    @SerializedName("smartPhoneCompatible")
    private String smartPhoneCompatible;

    @SerializedName("tabletCompatiable")
    private String tabletCompatiable;

    @SerializedName("priority")
    private int priority;

    @SerializedName("downloadLink")
    private String downloadLink;

    private int subcategoryIndex;
    private int passionIndex;

    public String getPackageName() {
        return this.packageName;
    }
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSubCategory() {
        return this.subCategory;
    }
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getContentRating() {
        return this.contentRating;
    }
    public void setContentRating(String contentRating) {
        this.contentRating = contentRating;
    }

    public String getAppName() {
        return this.appName;
    }
    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public List<String> getVideoUrls() {
        return this.videoUrls;
    }
    public void setVideoUrls(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public String getPublisherName() {
        return this.publisherName;
    }
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public float getAllRating() {
        return this.allRating;
    }
    public void setAllRating(float allRating) {
        this.allRating = allRating;
    }

    public String getAppType() {
        return this.appType;
    }
    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getSharedIconUrl() {
        return this.sharedIconUrl;
    }
    public void setSharedIconUrl(String sharedIconUrl) {
        this.sharedIconUrl = sharedIconUrl;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getVersion() {
        return this.version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getPublisherUrl() {
        return this.publisherUrl;
    }
    public void setPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
    }

    public String getPublishId() {
        return this.publishId;
    }
    public void setPublishId(String publishId) {
        this.publishId = publishId;
    }

    public String getDownloads() {
        return this.downloads;
    }
    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public List<String> getScreenShotUrls() {
        return this.screenShotUrls;
    }
    public void setScreenShotUrls(List<String> screenShotUrls) {
        this.screenShotUrls = screenShotUrls;
    }

    public List<String> getLargeScreenShotUrls() {
        return this.largeScreenShotUrls;
    }
    public void setLargeScreenShotUrls(List<String> largeScreenShotUrls) {
        this.largeScreenShotUrls = largeScreenShotUrls;
    }

    public List<String> getSharedScreenShotUrls() {
        return this.sharedScreenShotUrls;
    }
    public void setSharedScreenShotUrls(List<String> sharedScreenShotUrls) {
        this.sharedScreenShotUrls = sharedScreenShotUrls;
    }

    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return this.genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getAllRatingCount() {
        return this.allRatingCount;
    }
    public void setAllRatingCount(int allRatingCount) {
        this.allRatingCount = allRatingCount;
    }

    public RatingData getAllHistogram() {
        return this.allHistogram;
    }
    public void setAllHistogram(RatingData allHistogram) {
        this.allHistogram = allHistogram;
    }

    public String getRequiresOS() {
        return this.requiresOS;
    }
    public void setRequiresOS(String requiresOS) {
        this.requiresOS = requiresOS;
    }

    public String getFileSize() {
        return this.fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getStatusDate() {
        return this.statusDate;
    }
    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getMobileDescription() {
        return this.mobileDescription;
    }
    public void setMobileDescription(String mobileDescription) {
        this.mobileDescription = mobileDescription;
    }

    public String getSmartPhoneCompatible() {
        return this.smartPhoneCompatible;
    }
    public void setSmartPhoneCompatible(String smartPhoneCompatible) {
        this.smartPhoneCompatible = smartPhoneCompatible;
    }

    public String getTabletCompatiable() {
        return "" + this.tabletCompatiable;
    }
    public void setTabletCompatiablen(String tabletCompatiable) {
        this.tabletCompatiable = tabletCompatiable;
    }

    public void setPassionIndex(int passionIndex) {
        this.passionIndex = passionIndex;
    }
    public int getPassionIndex() {
        return passionIndex;
    }

    public void setSubcategory(int subcategoryIndex) {
        this.subcategoryIndex = subcategoryIndex;
    }
    public int getSubcategoryIndex(){
        return subcategoryIndex;
    }

    public String getDownloadLink() {
        return this.downloadLink;
    }
    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public int getApplicationIndex(){
        return this.priority - 1;
    }
    public void setApplicationIndex(int applicationIndex){
        this.priority = applicationIndex + 1;
    }

    public void genCategoryIndex() {
        String[] catData = subCategory.split("\\.");

        this.passionIndex = Integer.valueOf(catData[0].replace("Passion", "")).intValue() - 1;
        this.subcategoryIndex = Integer.valueOf(catData[1].replace("SubCategory", "")).intValue() - 1;
    }

    protected ApplicationData clone(){
        ApplicationData app = null;
        try {
            app = (ApplicationData)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return app;
    }
}
