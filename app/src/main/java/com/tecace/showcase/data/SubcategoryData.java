package com.tecace.showcase.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wssun on 2015-05-13.
 */
public class SubcategoryData implements Serializable {
    private static final String TAG = SubcategoryData.class.getName();

    public static final int SELECTED_REGION_AMERICA = 1;
    public static final int SELECTED_REGION_EUROPE = 2;
    public static final int SELECTED_REGION_ASIA = 3;
    public static final int SELECTED_REGION_REST = 4;
    public static final int SELECTED_DEVICE_PHONE = 5;
    public static final int SELECTED_DEVICE_TABLET = 6;
    public static final int SELECTED_DEVICE_S3 = 7;

    public static final int REGION_AMERICA = 1;
    public static final int REGION_EUROPE = 2;
    public static final int REGION_ASIA = 4;
    public static final int REGION_REST = 8;

    public static final int DEVICE_PHONE = 1;
    public static final int DEVICE_TABLET = 2;
    public static final int DEVICE_S3 = 4;

    public static final int ACTION_CLICK = 1;
    public static final int ACTION_TIMER = 2;
    public static final int ACTION_ALL = 4;
    public static final int ACTION_PHONE = 8;

    @SerializedName("passionName")
    private String passionName;

    @SerializedName("subCategoryName")
    private String subCategoryName;

    @SerializedName("previewImage1")
    private String previewImage1;

    @SerializedName("previewImage2")
    private String previewImage2;

    @SerializedName("icon")
    private String icon;

    @SerializedName("introduction")
    private String introduction;

    @SerializedName("benefits")
    private String benefits;

    @SerializedName("regions")
    private String regions;

    private int regionInfo = -1;

    @SerializedName("devices")
    private String devices;

    private int deviceInfo = -1;

    @SerializedName("introImage")
    private String introImage;

    @SerializedName("benefitsImage")
    private String benefitsImage;

    @SerializedName("subPagePhone")
    private List<SubPageData> subPagePhone;

    @SerializedName("subPageTab")
    private List<SubPageData> subPageTab;

    @SerializedName("subPageGear")
    private List<SubPageData> subPageGear;

    public SubcategoryData() {
    }

    public SubcategoryData(String passionName,
                           String subCategoryName,
                           String previewImage1,
                           String previewImage2,
                           String icon,
                           String introduction,
                           String benefits,
                           String regions,
                           String devices) {
        this.passionName = passionName;
        this.subCategoryName = subCategoryName;
        this.previewImage1 = previewImage1;
        this.previewImage2 = previewImage2;
        this.icon = icon;
        this.introduction = introduction;
        this.benefits = benefits;
        this.regions = regions;
        this.devices = devices;
    }

    public String getPassionName() {
        return this.passionName;
    }
    public void setPassionName(String passionName) {
        this.passionName = passionName;
    }

    public String getSubcategoryName() {
        return this.subCategoryName;
    }
    public void setSubcategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getPreviewImage1() {
        return this.previewImage1;
    }
    public void setPreviewImage1(String previewImage1) {
        this.previewImage1 = previewImage1;
    }

    public String getPreviewImage2() {
        return this.previewImage2;
    }
    public void setPreviewImage2(String previewImage2) {
        this.previewImage2 = previewImage2;
    }

    public String getIcon() {
        return this.icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIntroImage() {
        return this.introImage;
    }
    public void setIntroImage(String introImage) {
        this.introImage = introImage;
    }

    public String getBenefitsImage() {
        return this.benefitsImage;
    }
    public void setBenefitsImage(String benefitsImage) {
        this.benefitsImage = benefitsImage;
    }

    public String getIntroduction() {
        return this.introduction;
    }
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getBenefits() {
        return this.benefits;
    }
    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    public int getRegionInfo() {
        if (regionInfo == -1) { // not initialized by this.regions
            regionInfo = 0;

            if (this.regions == null)
                return deviceInfo;

            if (this.regions.contains("America"))
                regionInfo += REGION_AMERICA;

            if (this.regions.contains("Europe"))
                regionInfo += REGION_EUROPE;

            if (this.regions.contains("Asia"))
                regionInfo += REGION_ASIA;

            if (this.regions.contains("Rest"))
                regionInfo += REGION_REST;
        }

        return regionInfo;
    }

    public int getDeviceInfo() {
        if (deviceInfo == -1) { // not initialized by this.devices
            deviceInfo = 0;

            if (this.devices == null)
                return deviceInfo;

            if (this.devices.contains("Phone"))
                deviceInfo += DEVICE_PHONE;

            if (this.devices.contains("Tablet"))
                deviceInfo += DEVICE_TABLET;

            if (this.devices.contains("S3"))
                deviceInfo += DEVICE_S3;
        }

        return deviceInfo;
    }


    public List<SubPageData> getSubPagePhone() {
        return subPagePhone;
    }

    public void setSubPagePhones(List<SubPageData> SubPagePhone) {
        this.subPagePhone = SubPagePhone;
    }

    public SubPageData getSubPagePhoneAt(int index){
        return this.subPagePhone.get(index);
    }

    public void addSubPagePhoneItem(SubPageData item){
        subPagePhone.add(item);
    }
    public void removeSubPagePhoneItem(SubPageData item){
        this.subPagePhone.remove(item);
    }

    public void removeSubPagePhoneAt(int index){
        this.subPagePhone.remove(index);
    }


    public List<SubPageData> getSubPageTab() {
        return subPageTab;
    }

    public void setSubPageTabs(List<SubPageData> SubPageTab) {
        this.subPageTab = SubPageTab;
    }

    public SubPageData getSubPageTabAt(int index){
        return this.subPageTab.get(index);
    }

    public void addSubPageTabItem(SubPageData item){
        subPageTab.add(item);
    }
    public void removeSubPageTabItem(SubPageData item){
        this.subPageTab.remove(item);
    }

    public void removeSubPageTabAt(int index){
        this.subPageTab.remove(index);
    }


    public List<SubPageData> getSubPageGear() {
        return subPageGear;
    }

    public void setSubPageGear(List<SubPageData> SubPageGear) {
        this.subPageGear = SubPageGear;
    }

    public SubPageData getSubPageGearAt(int index){
        return this.subPageGear.get(index);
    }

    public void addSubPageGearItem(SubPageData item){
        subPageGear.add(item);
    }
    public void removeSubPageGearItem(SubPageData item){
        this.subPageGear.remove(item);
    }

    public void removeSubPageGearAt(int index){
        this.subPageGear.remove(index);
    }

}
