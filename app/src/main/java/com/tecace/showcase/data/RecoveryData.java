package com.tecace.showcase.data;

/**
 * Created by tecace119 on 5/26/15.
 */
public class RecoveryData {
    private int level = 0;
    private int indexPassion = -1;
    private int indexSubCategory = -1;
    private int indexApp = -1;

    public static final int LEVEL_ATTRACTOR_LOOP = 1;
    public static final int LEVEL_SUB_PREVIEW = 2;
    public static final int LEVEL_SUB_DISPLAY = 3;
    public static final int LEVEL_OPEN_HEART = 4;

    public RecoveryData() {
        level = LEVEL_ATTRACTOR_LOOP;
        indexPassion = 0;
        indexSubCategory = 0;
        indexApp = 0;
    }

    public void setRecoveryStatus(int level){
        this.level = level;
    }

    public int getRecoveryStatus(){
        return level;
    }

    public int getRecoveryPassionIndex(){
        return indexPassion;
    }

    public int getRecoverySubcategoryIndex(){
        return indexSubCategory;
    }

    public int getRecoveryAppIndex(){
        return indexApp;
    }

    public void setRecoveryPassionIndex(int index){
        indexPassion = index;
    }

    public void setRecoverySubcategoryIndex(int index){
        indexSubCategory = index;
    }

    public void setRecoveryAppIndex(int index){
        indexApp = index;
    }

    public void setRecoveryAllIndex(int indexPassion, int indexSubCategory, int indexApp){
        this.indexPassion = indexPassion;
        this.indexSubCategory = indexSubCategory;
        this.indexApp = indexApp;
    }
}
