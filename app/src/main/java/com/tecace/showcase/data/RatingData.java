package com.tecace.showcase.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wssun on 2015-05-13.
 */
public class RatingData implements Serializable {
    private static final String TAG = RatingData.class.getName();

    @SerializedName("1")
    private String star1;

    @SerializedName("2")
    private String star2;

    @SerializedName("3")
    private String star3;

    @SerializedName("4")
    private String star4;

    @SerializedName("5")
    private String star5;

    public RatingData() {
    }

    public String getStar1() {
        return this.star1;
    }

    public String getStar2() {
        return this.star2;
    }

    public String getStar3() {
        return this.star3;
    }

    public String getStar4() {
        return this.star4;
    }

    public String getStar5() {
        return this.star5;
    }
}
