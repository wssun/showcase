package com.tecace.showcase.data;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.tecace.showcase.navigation.AttractLoopColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wssun on 2015-05-13.
 */
public class AppDataManager implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("resultSet")
    private List<PassionData> passions;

    public AppDataManager() {
    }

    public List<PassionData> getPassions() {
        return this.passions;
    }

    public void setPassions(List<PassionData> passions) {
        this.passions = passions;
    }

    public PassionData getPassionDataAt(int index) {
        return passions.get(index % getNumberOfPassions());
    }

    public String[] getPassionsLabels() {
        String[] labels = new String[passions.size()];

        for (int i = 0; i < passions.size(); i++)
            labels[i] = passions.get(i).getName();

        return labels;
    }

    public int[] getPassionsColors() {
        int[] colors = new int[passions.size()];

        for (int i = 0; i < passions.size(); i++)
            colors[i] = passions.get(i).getPassionColor();

        return colors;
    }

    private final static Comparator<PassionData> myComparator = new Comparator<PassionData>() {
        @Override
        public int compare(PassionData object1, PassionData object2) {
            return object1.getPassionIndex() < object2.getPassionIndex() ? -1 : object1.getPassionIndex() >= object2.getPassionIndex() ? 1 : 0;
        }
    };

    private final static Comparator<ApplicationData> myApplicationComparator = new Comparator<ApplicationData>() {
        @Override
        public int compare(ApplicationData object1, ApplicationData object2) {
            return object1.getApplicationIndex() < object2.getApplicationIndex() ? -1 : object1.getApplicationIndex() >= object2.getApplicationIndex() ? 1 : 0;
        }
    };

    private final static Comparator<ApplicationData> myApplicationAlphaNumbericComparator = new Comparator<ApplicationData>() {
        @Override
        public int compare(ApplicationData object1, ApplicationData object2) {
            return object1.getAppName().compareTo(object2.getAppName());
        }
    };

    public void generatePassionColors() {
        Collections.sort(passions, myComparator);

        AttractLoopColor attractLoopColor = new AttractLoopColor(passions.size(), getSamsungIndex());

        for (int i = 0; i < passions.size(); i++) {
            passions.get(i).setPassionColor(attractLoopColor.getColorAt(i));
        }
    }

    public int getNumberOfPassions() {
        return passions.size();
    }

    public int getSamsungIndex() {
        for (int i = 0; i < passions.size(); i++) {
            if (passions.get(i).getName().compareTo("Made for Samsung") == 0) {
                return i;
            }
        }

        return 0;
    }
}
