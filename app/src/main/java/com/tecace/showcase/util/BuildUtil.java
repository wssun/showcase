package com.tecace.showcase.util;

import android.os.Build;

/**
 * Created by icanmobile on 9/4/15.
 */
public class BuildUtil {
    private static final String TAG = BuildUtil.class.getName();

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
}
