package com.tecace.showcase.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by wssun on 2015-04-29.
 */
public class BitmapUtil {

    public static Bitmap getBlurImage(Context context, String imagePath, float radius) {
        RenderScript rs = RenderScript.create(context);

        Bitmap main_bg = loadBitmap(imagePath, null);

        //this will blur the bitmapOriginal with a radius of 8 and save it in bitmapOriginal
        final Allocation input = Allocation.createFromBitmap(rs, main_bg); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(radius);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(main_bg);

        return main_bg;
    }

    public static Bitmap loadBitmap(String imgPath, BitmapFactory.Options opts) {
        BitmapFactory.Options options = null;

        if (null == opts) {
            options = new BitmapFactory.Options();

            options.inDither = true;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 2;
        } else
            options = opts;

        return BitmapFactory.decodeFile(imgPath, options);
    }

    public static void setBitmap(ImageView view, String imgPath, BitmapFactory.Options opts) {
        Bitmap bm = loadBitmap(imgPath, opts);
        if(bm == null){
            Log.e("BitmapUtils", "cannot load bitmap : " + imgPath);
            return;
        }
        if (bm.isRecycled())
            Log.e("BitmapUtils", "try to use recycled image");
        view.setImageBitmap(bm);
    }

    public static Point getBitmapSize(String imgPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath, options);

        return new Point(options.outWidth, options.outHeight);
    }

    public static void setBitmap(ImageView view, String imgPath) {
        setBitmap(view, imgPath, null);
    }

    public static void recyleBitmap(ImageView view) {

        if (view == null) return;
//        if (view.getDrawable() != null
//                && view.getDrawable() instanceof BitmapDrawable
//                && (((BitmapDrawable) view.getDrawable())).getBitmap() != null
//                && !(((BitmapDrawable) view.getDrawable())).getBitmap().isRecycled()) {
        if (view.getDrawable() != null
                && view.getDrawable() instanceof BitmapDrawable
                && (((BitmapDrawable) view.getDrawable())).getBitmap() != null) {
            (((BitmapDrawable) view.getDrawable())).getBitmap().recycle();
        }
    }
}
