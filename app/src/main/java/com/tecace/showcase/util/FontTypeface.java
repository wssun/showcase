package com.tecace.showcase.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by icanmobile on 10/2/15.
 */
public class FontTypeface {
    private static FontTypeface sInstance = null;
    public static FontTypeface getInstance() {
        if (sInstance == null)
            sInstance = new FontTypeface();
        return sInstance;
    }

    private Context mAppContext = null;
    public Context getAppContext() {
        return this.mAppContext;
    }
    public void setAppContext(Context context) {
        this.mAppContext = context;
    }

    public Typeface mFontLight;
    public Typeface getFontLight() {
        mFontLight = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_FONT_LIGHT);
        return mFontLight;
    }

    public Typeface mFontThin;
    public Typeface getFontThin() {
        mFontThin = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_FONT_THIN);
        return mFontThin;
    }

    public Typeface mFontRoman;
    public Typeface getFontRoman() {
        mFontRoman = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_FONT_ROMAN);
        return mFontRoman;
    }

    public Typeface mFontMidium;
    public Typeface getFontMidium() {
        mFontMidium = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_FONT_MEDIUM);
        return mFontMidium;
    }

    public Typeface mFontUltrathin;
    public Typeface getFontUltrathin() {
        mFontUltrathin = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PAHT_FONT_ULTRATHIN);
        return mFontUltrathin;
    }

    public Typeface mFontBold;
    public Typeface getFontBold() {
        mFontBold = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PAHT_FONT_BOLD);
        return mFontBold;
    }


    public Typeface mEtextFontLight;
    public Typeface getFontEtextLight() {
        mEtextFontLight = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_ETEXT_FONT_LIGHT);
        return mEtextFontLight;
    }

    public Typeface mEtextFontRoman;
    public Typeface getEtextFontRoman() {
        mEtextFontRoman = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_ETEXT_FONT_ROMAN);
        return mEtextFontRoman;
    }

    public Typeface mEtextFontMedium;
    public Typeface getEtextFontMedium() {
        mEtextFontMedium = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_ETEXT_FONT_MEDIUM);
        return mEtextFontMedium;
    }

    public Typeface mEtextFontBold;
    public Typeface getEtextFontBold() {
        mEtextFontBold = Typeface.createFromAsset(getAppContext().getAssets(), AppConst.PATH_ETEXT_FONT_BOLD);
        return mEtextFontBold;
    }
}
