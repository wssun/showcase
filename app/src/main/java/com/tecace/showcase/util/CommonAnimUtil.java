package com.tecace.showcase.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Created by tecace119 on 8/5/15.
 */
public class CommonAnimUtil {
    private static final int FADE_IN_TIME = 200;
    private static final int LATENCY_BETWEEN_ANIM = 500;
    private static final int AUTO_FADE_OUT_TIME = 15000;

    public CommonAnimUtil(){

    }

    public static ObjectAnimator getAlphaAnim(final View view, int delayCount) {
        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
        animAlpha.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animAlpha.setDuration(FADE_IN_TIME);
        animAlpha.setStartDelay(LATENCY_BETWEEN_ANIM * delayCount);

        return animAlpha;
    }

    public static ObjectAnimator getFadeOutAnim(final View view, int delayCount) {
        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
        animAlpha.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animAlpha.setDuration(FADE_IN_TIME);
        animAlpha.setStartDelay(LATENCY_BETWEEN_ANIM * delayCount);

        return animAlpha;
    }
}
