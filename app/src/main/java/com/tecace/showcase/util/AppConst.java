package com.tecace.showcase.util;

import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Created by wssun on 2015-04-29.
 */
public class AppConst {
    static public float BLUR_RADIUS = 20f;

    public static final String MIXPANEL_TOKEN               = "60d3c95c80a0f5eb30a1d95df3270457";

    public static final String APPTABLE                     = "AppTable";
    public static final String APPDATA_JSON_PATH            = Environment.getExternalStorageDirectory() + File.separator + "ShowcaseData" + File.separator + "appdata.json";
    public static final String APPTABLE_LOG_PATH            = Environment.getExternalStorageDirectory() + File.separator + APPTABLE;

    // Helvetica Font
    public static final String PATH_FONT_LIGHT              = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-Lt.otf";
    public static final String PATH_FONT_THIN               = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-Th.otf";
    public static final String PATH_FONT_ROMAN              = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-Roman.otf";
    public static final String PATH_FONT_MEDIUM             = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-Md.otf";
    public static final String PAHT_FONT_ULTRATHIN          = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-UltLt.otf";
    public static final String PAHT_FONT_BOLD               = "fonts/helvetica_neue_lt_std/HelveticaNeueLTStd-Bd.otf";

    // Helvetica eText Pro Font
    public static final String PATH_ETEXT_FONT_LIGHT        = "fonts/helvetica_neue_etext_pro/HelveticaNeueeTextProLight.ttf";
    public static final String PATH_ETEXT_FONT_ROMAN        = "fonts/helvetica_neue_etext_pro/HelveticaNeueeTextPro.ttf";
    public static final String PATH_ETEXT_FONT_MEDIUM       = "fonts/helvetica_neue_etext_pro/HelveticaNeueeTextProMedium.ttf";
    public static final String PATH_ETEXT_FONT_BOLD         = "fonts/helvetica_neue_etext_pro/HelveticaNeueeTextProBold.ttf";



    public static final String ACTION_STOP_APP              = "com.tecace.stableservice.action.STOP_APP";

    public static final String ACTION_ENABLE_ADMIN_MODE     = "com.tecace.stableservice.action.ENABLE_ADMIN_MODE";
    public static final String ACTION_APP_STARTED           = "com.tecace.stableservice.action.APP_STARTED";
    public static final String ACTION_APP_CRASHED           = "com.tecace.stableservice.action.APP_CRASHED";
    public static final String ACTION_APP_STOPPED           = "com.tecace.stableservice.action.APP_STOPPED";
    public static final String ACTION_ROLLBACK_DATA_FOLDER  = "com.tecace.stableservice.action.ROLLBACK_DATA_FOLDER";


    /*
     * for Lynx 4K device Test
     */
    public static String getAppDataPath() {
        String ext = Environment.getExternalStorageState();
        if (ext.equals(Environment.MEDIA_MOUNTED)) {
            return new StringBuilder()
                    .append(Environment.getExternalStorageDirectory())
                    .append(File.separator)
                    .append("STableData")
                    .append(File.separator)
                    .append("AppTable")
                    .append(File.separator)
                    .append("appdata.json")
                    .toString();
        }
        else {
            File dir = new File("/mnt/usb_storage/USB_DISK2/");
            if (!dir.exists())
            {
                Log.i("INFO", "##### \"/mnt/usb_storage/USB_DISK2/\" does not exist !!!");

                dir = new File("/mnt/usb_storage/USB_DISK3/");
                if (!dir.exists()) {
                    Log.i("INFO", "##### \"/mnt/usb_storage/USB_DISK3/\" does not exist !!!");
                    return new StringBuilder()
                            .append(Environment.getExternalStorageDirectory())
                            .append(File.separator)
                            .append("STableData")
                            .append(File.separator)
                            .append("AppTable")
                            .append(File.separator)
                            .append("appdata.json")
                            .toString();
                }
                else {
                    Log.i("INFO", "##### \"/mnt/usb_storage/USB_DISK3/\" exists !!!");
                    return new StringBuilder()
                            .append("/mnt/usb_storage/USB_DISK3")
                            .append(File.separator)
                            .append("STableData")
                            .append(File.separator)
                            .append("AppTable")
                            .append(File.separator)
                            .append("appdata.json")
                            .toString();
                }
            }
            else {
                Log.i("INFO", "##### \"/mnt/usb_storage/USB_DISK2/\" exists !!!");
                return new StringBuilder()
                        .append("/mnt/usb_storage/USB_DISK2")
                        .append(File.separator)
                        .append("STableData")
                        .append(File.separator)
                        .append("AppTable")
                        .append(File.separator)
                        .append("appdata.json")
                        .toString();
            }
        }
    }
}
