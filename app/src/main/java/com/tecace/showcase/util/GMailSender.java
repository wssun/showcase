package com.tecace.showcase.util;

/**
 * Created by tecace119 on 4/6/15.
 */
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.Properties;

import javax.activation.DataSource;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class GMailSender extends javax.mail.Authenticator {
    private String TAG = this.getClass().getCanonicalName();
    private String mailhost = "smtp.gmail.com";
    private String user;
    private String password;
    private Session session;

    private Multipart _multipart = new MimeMultipart();
    static {
        Security.addProvider(new JSSEProvider());
    }

    public GMailSender(String user, String password) {
        this.user = user;
        this.password = password;

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", mailhost);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.quitwait", "false");

        session = Session.getDefaultInstance(props, this);
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, password);
    }

    public synchronized boolean sendMail(String subject, String body,
                                      String sender, String recipients) throws Exception {

        String strEmailTempleteHeader =
                "<meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>" +
                        "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>" +
                        "<title>Wearable UI</title>" +
                        "<!-- load theme file for your application -->" +
                        "<link rel='stylesheet'  href='http://182.162.90.124/stable/style.css' />" +
                        "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' ></script>";

        try {
            MimeMessage message = new MimeMessage(session);
            DataHandler handlerHead = new DataHandler(new ByteArrayDataSource(
                    strEmailTempleteHeader.getBytes() , "text/html"));
            DataHandler handlerBody = new DataHandler(new ByteArrayDataSource(
                   body.getBytes() , "text/html"));
            message.setSender(new InternetAddress(sender));
            message.setSubject(subject);
            message.setDataHandler(handlerHead);

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(handlerBody);
//            messageBodyPart.setText(body);
            _multipart.addBodyPart(messageBodyPart);
            // Put parts in message
            message.setContent(_multipart);
            if (recipients.indexOf(',') > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
            }else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
            }
            Transport.send(message);
            return true;
        } catch (Exception e) {
            Log.e(TAG, e.getStackTrace().toString());
            return false;
        }
    }

    public void addAttachment(String filename) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(filename);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("download image");

        _multipart.addBodyPart(messageBodyPart);
    }

    public class ByteArrayDataSource implements DataSource {
        private byte[] data;
        private String type;

        public ByteArrayDataSource(byte[] data, String type) {
            super();
            this.data = data;
            this.type = type;
        }

        public ByteArrayDataSource(byte[] data) {
            super();
            this.data = data;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getContentType() {
            if (type == null)
                return "application/octet-stream";
            else
                return type;
        }

        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(data);
        }

        public String getName() {
            return "ByteArrayDataSource";
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("Not Supported");
        }
    }
}