package com.tecace.showcase.util;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by wssun on 2015-05-22.
 */
public class MixPanelUtil {
    private static MixpanelAPI mMixpanel = null;
    private static Date logTime = null;
    private static Date sessionTime = null;

    public static final String pageview_Attractor_Loop	= "categories";
    public static final String pageview_Display_subcategories	= "subcategories";
    public static final String pageview_Display_selected_subcategory	= "subcategorydetail";
    public static final String pageview_Feature_selected_app	= "featuredapp";
    public static final String pageview_Display_saved_app	= "savedapps";
    public static final String pageview_Display_confirmation_message	= "download";
    public static final String pageview_Display_guide_messages	= "help";
    public static final String pageview_Display_error_messages	= "globalerror";

    public static final String cause_timeout = "timeout";
    public static final String cause_user_exit = "user_exit";

    public static void initialize(Context context) {
//        if (mMixpanel == null)
//            mMixpanel=MixpanelAPI.getInstance(context,AppConst.MIXPANEL_TOKEN);

        long now = System.currentTimeMillis();
        logTime = new Date(now);
        sessionTime = new Date(now);
    }

    public static void flush() {
        if (null != mMixpanel)
            mMixpanel.flush();
    }

    public static void identify() {
        long now = System.currentTimeMillis();
        sessionTime = new Date(now);

        if (null != mMixpanel)
            mMixpanel.identify(String.valueOf(sessionTime.getTime()));
    }

    public static void page_view(String contentId) {
        try {
            JSONObject props = new JSONObject();

            long now = System.currentTimeMillis();
            logTime = new Date(now);
            props.put("timestamp", logTime.getTime());
            props.put("content_id", contentId);

            if (null != mMixpanel)
                mMixpanel.track("page_view", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void content_view(String passionOrSubcategory) {
        try {
            JSONObject props = new JSONObject();

            long now = System.currentTimeMillis();
            logTime = new Date(now);
            props.put("timestamp", logTime.getTime());
            props.put("content_id", passionOrSubcategory);

            if (null != mMixpanel)
                mMixpanel.track("content_view", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void download(String savedApp) {
        try {
            JSONObject props = new JSONObject();

            long now = System.currentTimeMillis();
            logTime = new Date(now);
            props.put("timestamp", logTime.getTime());
            props.put("app", savedApp);

            if (null != mMixpanel)
                mMixpanel.track("download", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmexport(String email) {
        try {
            JSONObject props = new JSONObject();

            long now = System.currentTimeMillis();
            logTime = new Date(now);
            props.put("timestamp", logTime.getTime());
            props.put("email", email);

            if (null != mMixpanel)
                mMixpanel.track("confirmexport", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void app_closed(String cause) {
        try {
            JSONObject props = new JSONObject();

            long now = System.currentTimeMillis();
            logTime = new Date(now);
            props.put("timestamp", logTime.getTime());
            props.put("duration", logTime.getTime() - sessionTime.getTime());
            props.put("cause", cause);

            sessionTime = new Date(now);

            if (null != mMixpanel)
                mMixpanel.track("app_closed", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
