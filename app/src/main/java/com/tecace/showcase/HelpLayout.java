package com.tecace.showcase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tecace.showcase.util.CustomTypeFaceSpan;

import static com.tecace.showcase.R.layout;

/**
 * Created by wssun on 2015-05-08.
 */
public class HelpLayout extends RelativeLayout {
    private String TAG = this.getClass().getCanonicalName();

    private static final int FADE_IN_TIME = 200;
    private static final int LATENCY_BETWEEN_ANIM = 500;
    private static final int AUTO_FADE_OUT_TIME = 15000;

    private TextView mHelpAttractorMessage;
    private TextView mHelpSubcategoryPreview;
    private TextView mHelpSubcategoryPreviewCenter;
    private TextView mHelpSubcategoryDisplayMoreInfo;
    private TextView mHelpSubcategoryDisplaySaveWhole;
    private TextView mHelpSubcategoryDisplaySaveApp;
    private TextView mHelpSubcategoryDisplayFavorites;
    private TextView mHelpCheckoutDisplayGoBack;
    private TextView mHelpCheckoutDisplayEdit;
    private TextView mHelpCheckoutDisplaySend;
    //ICANMBILE - 20160116
    private TextView mHelpCheckoutEditSelection;

    private View mArrowAttractorMessage;
    private View mArrowSubcategoryPreview;
    private View mArrowSubcategoryPreviewCenter;
    private View mArrowSubcategoryDisplayMoreInfo;
    private View mArrowSubcategoryDisplaySaveWhole;
    private View mArrowSubcategoryDisplaySaveApp;
    private View mArrowSubcategoryDisplayFavorites;
    private View mArrowCheckoutDisplayGoBack;
    private View mArrowCheckoutDisplayEdit;
    private View mArrowCheckoutDisplaySend;
    //ICANMBILE - 20160116
    private View mArrowCheckoutEditSelection;

    private Handler mAutoFadeOutHandler;
    private Runnable mAutoFadeOutRunnable;

    private View mSelf = null;
    private AnimatorSet animReturned = null;

    public HelpLayout(Context context) {
        super(context);
        init(context);
    }

    public HelpLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HelpLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(final Context context) {
        RelativeLayout parent = (RelativeLayout) inflate(getContext(), layout.help_layout, this);
        mSelf = parent;

        parent.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (animReturned != null) {
                    animReturned.cancel();
                    animReturned = null;
                }

                hideAllHelpMessage();
                mAutoFadeOutHandler.removeCallbacks(mAutoFadeOutRunnable);
                view.setVisibility(View.GONE); // hide help view
                actionGoingToAttractorLoop();
                return true;
            }
        });

        if (!isInEditMode())
            initHelpMessage(parent);

        prepareAutoFadeoutHandler();
    }

    private void initHelpMessage(final RelativeLayout parent) {
        mHelpAttractorMessage = (TextView) parent.findViewById(R.id.help_attractor_view_message);
        mHelpSubcategoryPreview = (TextView) parent.findViewById(R.id.help_subcategory_preview_message);
        mHelpSubcategoryPreviewCenter = (TextView) parent.findViewById(R.id.help_subcategory_preview_message_center);
        mHelpSubcategoryDisplayMoreInfo = (TextView) parent.findViewById(R.id.help_subcategory_display_more_info_message);
        mHelpSubcategoryDisplaySaveWhole = (TextView) parent.findViewById(R.id.help_subcategory_display_save_whole_message);
        mHelpSubcategoryDisplaySaveApp = (TextView) parent.findViewById(R.id.help_subcategory_display_save_app_message);
        mHelpSubcategoryDisplayFavorites = (TextView) parent.findViewById(R.id.help_subcategory_display_favorites_message);
        mHelpCheckoutDisplayGoBack = (TextView) parent.findViewById(R.id.help_checkout_display_goback_message);
        mHelpCheckoutDisplayEdit = (TextView) parent.findViewById(R.id.help_checkout_display_edit_message);
        mHelpCheckoutDisplaySend = (TextView) parent.findViewById(R.id.help_checkout_display_send_message);
        //ICANMBILE - 20160116
        mHelpCheckoutEditSelection = (TextView) parent.findViewById(R.id.help_checkout_edit_selection_message);

        mArrowAttractorMessage = parent.findViewById(R.id.help_attractor_view_arrow);
        mArrowSubcategoryPreview = parent.findViewById(R.id.help_subcategory_preview_arrow);
        mArrowSubcategoryPreviewCenter = parent.findViewById(R.id.help_subcategory_preview_arrow_center);
        mArrowSubcategoryDisplayMoreInfo = parent.findViewById(R.id.help_subcategory_display_more_info_arrow);
        mArrowSubcategoryDisplaySaveWhole = parent.findViewById(R.id.help_subcategory_display_save_whole_arrow);
        mArrowSubcategoryDisplaySaveApp = parent.findViewById(R.id.help_subcategory_display_save_app_arrow);
        mArrowSubcategoryDisplayFavorites = parent.findViewById(R.id.help_subcategory_display_favorites_arrow);
        mArrowCheckoutDisplayGoBack = parent.findViewById(R.id.help_checkout_display_goback_arrow);
        mArrowCheckoutDisplayEdit = parent.findViewById(R.id.help_checkout_display_edit_arrow);
        mArrowCheckoutDisplaySend = parent.findViewById(R.id.help_checkout_display_send_arrow);
        //ICANMBILE - 20160116
        mArrowCheckoutEditSelection = parent.findViewById(R.id.help_checkout_edit_selection_arrow);

//        Typeface fontBold = Typeface.createFromAsset(super.getContext().getAssets(), AppConst.PATH_FONT_NORMAL);
//
//        setHelpMessage(mHelpAttractorMessage, fontBold, super.getContext().getString(R.string.help_attractor_loop), super.getContext().getString(R.string.bold_help_attractor_loop));
//        setHelpMessage(mHelpSubcategoryPreview, fontBold, super.getContext().getString(R.string.help_open_app_set), super.getContext().getString(R.string.bold_help_open_app_set));
//        setHelpMessage(mHelpSubcategoryDisplayMoreInfo, fontBold, super.getContext().getString(R.string.help_display_more_info),
//                super.getContext().getString(R.string.bold_help_more), super.getContext().getString(R.string.bold_help_information));
//        setHelpMessage(mHelpSubcategoryDisplaySaveWhole, fontBold, super.getContext().getString(R.string.help_display_save_whole),
//                super.getContext().getString(R.string.bold_help_save_the_whole), super.getContext().getString(R.string.bold_help_collection));
//        setHelpMessage(mHelpSubcategoryDisplaySaveApp, fontBold, super.getContext().getString(R.string.help_display_save_app),
//                super.getContext().getString(R.string.bold_help_save_app), super.getContext().getString(R.string.bold_help_favorites));
//        setHelpMessage(mHelpSubcategoryDisplayFavorites, fontBold, super.getContext().getString(R.string.help_display_favorites),
//                super.getContext().getString(R.string.bold_help_favorites));
    }

    private void setHelpMessage(TextView tv, Typeface tf, String original, String bold) {
        int location = original.indexOf(bold);
        SpannableStringBuilder SS = new SpannableStringBuilder(original);
        //SS.setSpan (new CustomTypeFaceSpan("", fontThin), 0, location-1,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tv.setText(SS);
    }

    private void setHelpMessage(TextView tv, Typeface tf, String original, String bold, String bold2) {
        SpannableStringBuilder SS = new SpannableStringBuilder(original);

        int location = original.indexOf(bold);
        //SS.setSpan (new CustomTypeFaceSpan("", fontThin), 0, location-1,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        location = original.indexOf(bold2);
        SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tv.setText(SS);
    }

    public void prepareAutoFadeoutHandler() {
        mAutoFadeOutHandler = new Handler();
        mAutoFadeOutRunnable = new Runnable() {
            @Override
            public void run() {
                mAutoFadeOutHandler.removeCallbacks(mAutoFadeOutRunnable);

                getAutoFadeOutAnim().start();
                mSelf.setVisibility(View.GONE);

            }
        };
    }

    public AnimatorSet getFirstSubcategoryDisplayAnim() {
        animReturned = new AnimatorSet();

        return animReturned;
    }

    public AnimatorSet getAttractorLoopHelp() {
        animReturned = new AnimatorSet();

        ObjectAnimator anim1 = getAlphaAnim(mHelpAttractorMessage, 0);
        ObjectAnimator anim2 = getAlphaAnim(mArrowAttractorMessage, 0);

        animReturned.playTogether(anim1, anim2);
        mAutoFadeOutHandler.postDelayed(mAutoFadeOutRunnable, AUTO_FADE_OUT_TIME);
        animReturned.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                //actionGoingToAttractorLoop();
            }
        });
        return animReturned;
    }

    public AnimatorSet getSubcategoryPreviewHelp() {
        animReturned = new AnimatorSet();

        ObjectAnimator anim1 = getAlphaAnim(mHelpSubcategoryPreview, 0);
        ObjectAnimator anim2 = getAlphaAnim(mArrowSubcategoryPreview, 0);

        animReturned.playTogether(anim1, anim2);
        mAutoFadeOutHandler.postDelayed(mAutoFadeOutRunnable, AUTO_FADE_OUT_TIME);

        return animReturned;
    }

    public AnimatorSet getSubcategoryPreviewCenterHelp() {
        animReturned = new AnimatorSet();

        ObjectAnimator anim1 = getAlphaAnim(mHelpSubcategoryPreviewCenter, 0);
        ObjectAnimator anim2 = getAlphaAnim(mArrowSubcategoryPreviewCenter, 0);

        animReturned.playTogether(anim1, anim2);
        mAutoFadeOutHandler.postDelayed(mAutoFadeOutRunnable, AUTO_FADE_OUT_TIME);

        return animReturned;
    }

    public AnimatorSet getSubcategoryDisplayHelp() {
        animReturned = new AnimatorSet();

        ObjectAnimator anim1 = getAlphaAnim(mHelpSubcategoryDisplayMoreInfo, 0);
        ObjectAnimator anim2 = getAlphaAnim(mArrowSubcategoryDisplayMoreInfo, 0);

        ObjectAnimator anim3 = getAlphaAnim(mHelpSubcategoryDisplaySaveWhole, 1);
        ObjectAnimator anim4 = getAlphaAnim(mArrowSubcategoryDisplaySaveWhole, 1);

        ObjectAnimator anim5 = getAlphaAnim(mHelpSubcategoryDisplaySaveApp, 2);
        ObjectAnimator anim6 = getAlphaAnim(mArrowSubcategoryDisplaySaveApp, 2);

        ObjectAnimator anim7 = getAlphaAnim(mHelpSubcategoryDisplayFavorites, 3);
        ObjectAnimator anim8 = getAlphaAnim(mArrowSubcategoryDisplayFavorites, 3);

        animReturned.playTogether(anim1, anim2, anim3, anim4, anim5, anim6, anim7, anim8);
        mAutoFadeOutHandler.postDelayed(mAutoFadeOutRunnable, AUTO_FADE_OUT_TIME);

        return animReturned;
    }

    public AnimatorSet getCheckoutDisplayHelp() {
        animReturned = new AnimatorSet();
//
//        ObjectAnimator anim1 = getAlphaAnim(mHelpCheckoutDisplayGoBack, 0);
//        ObjectAnimator anim2 = getAlphaAnim(mArrowCheckoutDisplayGoBack, 0);
//
//        ObjectAnimator anim3 = getAlphaAnim(mHelpCheckoutDisplayEdit, 1);
//        ObjectAnimator anim4 = getAlphaAnim(mArrowCheckoutDisplayEdit, 1);

        //ICANMBILE - 20160116
        ObjectAnimator anim1 = getAlphaAnim(mHelpCheckoutEditSelection, 0);
        ObjectAnimator anim2 = getAlphaAnim(mArrowCheckoutEditSelection, 0);

        ObjectAnimator anim3 = getAlphaAnim(mHelpCheckoutDisplaySend, 1);
        ObjectAnimator anim4 = getAlphaAnim(mArrowCheckoutDisplaySend, 1);

//        animReturned.playTogether(anim1, anim2, anim3, anim4, anim5, anim6);
        animReturned.playTogether(anim1, anim2, anim3, anim4);
        mAutoFadeOutHandler.postDelayed(mAutoFadeOutRunnable, AUTO_FADE_OUT_TIME);

        return animReturned;
    }

    public AnimatorSet getAutoFadeOutAnim() {
        animReturned = new AnimatorSet();

        // Attractor Loop
        ObjectAnimator anim1 = getFadeOutAnim(mHelpAttractorMessage, 0);
        ObjectAnimator anim2 = getFadeOutAnim(mArrowAttractorMessage, 0);

        // Subcategory Preview
        ObjectAnimator anim3 = getFadeOutAnim(mHelpSubcategoryPreview, 0);
        ObjectAnimator anim4 = getFadeOutAnim(mArrowSubcategoryPreview, 0);

        // Cenber Subcategory Preview
        ObjectAnimator anim5 = getFadeOutAnim(mHelpSubcategoryPreviewCenter, 0);
        ObjectAnimator anim6 = getFadeOutAnim(mArrowSubcategoryPreviewCenter, 0);

        // Subcategory Display
        ObjectAnimator anim7 = getFadeOutAnim(mHelpSubcategoryDisplayMoreInfo, 0);
        ObjectAnimator anim8 = getFadeOutAnim(mArrowSubcategoryDisplayMoreInfo, 0);

        ObjectAnimator anim9 = getFadeOutAnim(mHelpSubcategoryDisplaySaveWhole, 0);
        ObjectAnimator anim10 = getFadeOutAnim(mArrowSubcategoryDisplaySaveWhole, 0);

        ObjectAnimator anim11 = getFadeOutAnim(mHelpSubcategoryDisplaySaveApp, 0);
        ObjectAnimator anim12 = getFadeOutAnim(mArrowSubcategoryDisplaySaveApp, 0);

        ObjectAnimator anim13 = getFadeOutAnim(mHelpSubcategoryDisplayFavorites, 0);
        ObjectAnimator anim14 = getFadeOutAnim(mArrowSubcategoryDisplayFavorites, 0);

        // Checkout Display
        ObjectAnimator anim15 = getFadeOutAnim(mHelpCheckoutDisplayGoBack, 0);
        ObjectAnimator anim16 = getFadeOutAnim(mArrowCheckoutDisplayGoBack, 0);

        ObjectAnimator anim17 = getFadeOutAnim(mHelpCheckoutDisplayEdit, 0);
        ObjectAnimator anim18 = getFadeOutAnim(mArrowCheckoutDisplayEdit, 0);

        ObjectAnimator anim19 = getFadeOutAnim(mHelpCheckoutDisplaySend, 0);
        ObjectAnimator anim20 = getFadeOutAnim(mArrowCheckoutDisplaySend, 0);

        //ICANMBILE - 20160116
        ObjectAnimator anim21 = getFadeOutAnim(mHelpCheckoutEditSelection, 0);
        ObjectAnimator anim22 = getFadeOutAnim(mArrowCheckoutEditSelection, 0);

        animReturned.playTogether(anim1, anim2, anim3, anim4, anim5, anim6, anim7, anim8, anim9, anim10, anim11, anim12, anim13, anim14, anim15, anim16, anim17, anim18, anim19, anim20, anim21, anim22);
        animReturned.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                actionGoingToAttractorLoop();
            }
        });
        return animReturned;
    }

    private ObjectAnimator getAlphaAnim(final View view, int delayCount) {
        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
        animAlpha.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animAlpha.setDuration(FADE_IN_TIME);
        animAlpha.setStartDelay(LATENCY_BETWEEN_ANIM * delayCount);

        return animAlpha;
    }

    private ObjectAnimator getFadeOutAnim(final View view, int delayCount) {
        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
        animAlpha.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animAlpha.setDuration(FADE_IN_TIME);
        animAlpha.setStartDelay(LATENCY_BETWEEN_ANIM * delayCount);

        return animAlpha;
    }

    private void hideAllHelpMessage() {
        mHelpAttractorMessage.setVisibility(View.GONE);
        mHelpSubcategoryPreview.setVisibility(View.GONE);
        mHelpSubcategoryPreviewCenter.setVisibility(View.GONE);
        mHelpSubcategoryDisplayMoreInfo.setVisibility(View.GONE);
        mHelpSubcategoryDisplaySaveWhole.setVisibility(View.GONE);
        mHelpSubcategoryDisplaySaveApp.setVisibility(View.GONE);
        mHelpSubcategoryDisplayFavorites.setVisibility(View.GONE);
        mHelpCheckoutDisplayGoBack.setVisibility(View.GONE);
        mHelpCheckoutDisplayEdit.setVisibility(View.GONE);
        mHelpCheckoutDisplaySend.setVisibility(View.GONE);
        mHelpCheckoutEditSelection.setVisibility(View.GONE);

        mArrowAttractorMessage.setVisibility(View.GONE);
        mArrowSubcategoryPreview.setVisibility(View.GONE);
        mArrowSubcategoryPreviewCenter.setVisibility(View.GONE);
        mArrowSubcategoryDisplayMoreInfo.setVisibility(View.GONE);
        mArrowSubcategoryDisplaySaveWhole.setVisibility(View.GONE);
        mArrowSubcategoryDisplaySaveApp.setVisibility(View.GONE);
        mArrowSubcategoryDisplayFavorites.setVisibility(View.GONE);
        mArrowCheckoutDisplayGoBack.setVisibility(View.GONE);
        mArrowCheckoutDisplayEdit.setVisibility(View.GONE);
        mArrowCheckoutDisplaySend.setVisibility(View.GONE);
        mArrowCheckoutEditSelection.setVisibility(View.GONE);
    }

    private void actionGoingToAttractorLoop() {
//        if (((MainActivity) mSelf.getContext()).isCheckOutDisplayOpened() == false)
//            ((MainActivity) mSelf.getContext()).startAutoLoopHandler(0);
//        if (!((MainActivity) mSelf.getContext()).isAttractorLoop())
            ((FullscreenActivity) mSelf.getContext()).startAutoResetHandler();
    }
}
