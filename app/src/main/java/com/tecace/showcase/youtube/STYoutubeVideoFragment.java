package com.tecace.showcase.youtube;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

public final class STYoutubeVideoFragment extends YouTubePlayerFragment implements YouTubePlayer.OnInitializedListener
{
    private final String TAG = this.getClass().getSimpleName();
    public YouTubePlayer player = null;
    private String videoId;
    private View view;
    private YouTubePlayer.PlaybackEventListener playbackEventListener= null;

    public static STYoutubeVideoFragment newInstance() {
        return new STYoutubeVideoFragment();
    }

    public void initialize() {
        initialize(DeveloperKey.DEVELOPER_KEY, this);
    }

    public boolean isActive() {
        return (player == null) ? false : true;
    }

    public void releasePlayer() {
        if(player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(DeveloperKey.DEVELOPER_KEY, this);

//        this.getView().setBackgroundColor(Color.parseColor("#FF000000"));
    }

    public boolean isIgnoreStopPlayer(){
        if(player.getCurrentTimeMillis() < 1000)
            return true;   //IGNORE
        else
            return false;
    }

    @Override
    public void onDestroy() {
        if (player != null) {
            player.release();
        }
        super.onDestroy();
    }

    public void onStop() {
        if (player != null) {
            player.seekToMillis(player.getDurationMillis());
//            player.play();
        }
        super.onStop();
    }

    public void setPlaybackEventListener(YouTubePlayer.PlaybackEventListener listener){
        Log.d(TAG, "setPlaybackEventListener");
        playbackEventListener = listener;
        if(player!=null){
            player.setPlaybackEventListener(playbackEventListener);
        }
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;

        if (videoId != null && !videoId.equals(this.videoId)) {
            if (player != null) {
                player.loadVideo(videoId);
            }
        }
        else {
            if (player != null) {
                //player.play();
				player.loadVideo(videoId);
            }
        }
    }

    public void pause() {
        if (player != null) {
            player.pause();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean restored) {
        this.player = player;

        if(playbackEventListener != null) {
            player.setPlaybackEventListener(playbackEventListener);
        }

        YouTubePlayer.PlayerStyle style = YouTubePlayer.PlayerStyle.MINIMAL;
        player.setPlayerStyle(style);

        if (!restored && videoId != null) {
            player.loadVideo(videoId);
        }
//        if(!videoId.isEmpty())
//            player.loadVideo(videoId);

        player.setShowFullscreenButton(false);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        this.player = null;
    }

}
