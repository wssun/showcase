package com.tecace.showcase.youtube;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.youtube.player.YouTubePlayer;
import com.tecace.showcase.R;

public class VideoLayout extends RelativeLayout implements YouTubePlayer.PlaybackEventListener{
    private String TAG = this.getClass().getSimpleName();
    private STYoutubeVideoFragment mSTYoutubeVideoFragment = null;
    private FragmentManager mFragmentManager = null;

    private RelativeLayout parent;

    private boolean isPlayingVideo = false;
    private boolean isStopVideoByUser = false;

    private OnVideoLayoutInteractionListener mListener = null;

    public VideoLayout(Context context) {
        super(context);
        init(context);
    }

    public VideoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VideoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context){
        if(!isInEditMode()) {
            mListener = (OnVideoLayoutInteractionListener) (Activity)(super.getContext());
        }

        parent = (RelativeLayout) inflate(getContext(), R.layout.video_layout, this);
        parent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        if (mListener != null) {
                            mListener.forwardEvent2Parent(event);
                        }
                        return true;
                    case MotionEvent.ACTION_DOWN:
                        isStopVideoByUser = true;
                        if (mSTYoutubeVideoFragment.getView().getVisibility() == VISIBLE) {
                            if (isPlayingVideo) {
                                Log.d(TAG, "[ARDEN] TOUCH : isStopVideoByUser = true");
                                isStopVideoByUser = true;
                            }
                            stopVideo();
                            return true;
                        } else
                            return false;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    default:
                        //
                }
                return false;
            }
        });
    }

    public void setFragmentManager(FragmentManager mFragmentManager){
        this.mFragmentManager = mFragmentManager;
        if(mSTYoutubeVideoFragment == null) {
            mSTYoutubeVideoFragment = (STYoutubeVideoFragment) mFragmentManager.findFragmentById(R.id.youtubeplayer);
            mSTYoutubeVideoFragment.initialize();
            mSTYoutubeVideoFragment.setPlaybackEventListener(this);
        }
    }

    public void showVideo(){
        this.setVisibility(VISIBLE);
        mSTYoutubeVideoFragment.getView().setVisibility(View.VISIBLE);
    }

    public void hideVideo(){
        this.setVisibility(GONE);
    }

    public void playVideo(String videoId) {
        Log.d(TAG, "playVideo(" + videoId + ")");
        if(mSTYoutubeVideoFragment.isActive() == false) {
            mSTYoutubeVideoFragment.initialize();
            mSTYoutubeVideoFragment.setPlaybackEventListener(this);
        }
        if(mListener != null) {
            if(mListener.checkDisplayLayoutBeforeYoutubePlay()) {
                mSTYoutubeVideoFragment.setVideoId(videoId);
            }else{
                stopVideo();
            }
        }
    }

    public void setStopVideoMode(boolean byUser){
        isStopVideoByUser = byUser;
    }

    public void stopVideo() {
        isPlayingVideo = false;
        if(mSTYoutubeVideoFragment.isActive()) {
            //#165 Youtube video sound leaks and Youtube play 시작할때 이전 영상이 잠깐 보이는 문제 수정
//            mSTYoutubeVideoFragment.pause();
            mSTYoutubeVideoFragment.releasePlayer();
        }
        if(mListener != null){
            mListener.onStopVideo();
        }
        hideVideo();
    }

    public boolean isVideoPlaying() {
        return this.isPlayingVideo;
    }

    ///* YouTubePlayer.PlaybackEventListener Override Method *///

    @Override
    public void onPlaying() {
        Log.d(TAG, "onPlaying...");
        if(isStopVideoByUser==true)
            return;
        showVideo();
        isPlayingVideo = true;
        if (mListener != null)
            mListener.onStartVideo();
    }

    @Override
    public void onPaused() {
        Log.d(TAG, "onPaused...");
        isPlayingVideo = false;
        //#177 "Paused Youtube video will not trigger application reset prompt" resolved
        if (mListener != null) {
            mListener.onPauseVideo();
        }
    }

    @Override
    public void onStopped() {
        isPlayingVideo = false;
        if(mSTYoutubeVideoFragment.isIgnoreStopPlayer() && isStopVideoByUser == false){
            Log.d(TAG, "IGNORE STOP...");
            showVideo();
            return;
        }else {
            if (mListener != null) {
                mListener.onStopVideo();
            }
            hideVideo();
        }
    }

    @Override
    public void onBuffering(boolean b) {
        Log.d(TAG, "onBuffering");
    }

    @Override
    public void onSeekTo(int i) {
        Log.d(TAG, "onSeekTo");
    }

    public interface OnVideoLayoutInteractionListener {
        void forwardEvent2Parent(MotionEvent event);
        boolean checkDisplayLayoutBeforeYoutubePlay();
        void onStartVideo();
        //#177 "Paused Youtube video will not trigger application reset prompt" resolved
        void onPauseVideo();
        void onStopVideo();
    }
}
