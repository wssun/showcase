package com.tecace.showcase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tecace.showcase.util.CustomTypeFaceSpan;
import com.tecace.showcase.util.FontTypeface;
import com.tecace.showcase.util.MixPanelUtil;

import java.lang.ref.WeakReference;

public class WaveMessageView extends RelativeLayout implements View.OnClickListener {
    private String TAG = WaveMessageView.class.getSimpleName().toString();

    private RelativeLayout mParent;
    private ImageView mResetIcon = null;
    private TextView mResetCounter = null;
    private Button mResetConfirmButton = null;
    private Button mResetCancelButton = null;
    private ImageView mProgressIcon = null;
    private RelativeLayout mSendingContainer = null;
    private RelativeLayout mSentContainer = null;
    private RelativeLayout mErrorContainer = null;
    private RelativeLayout mResetContainer = null;
    private TextView mResetText = null;

    final private String mReset_start_time = "10";

    private String resetCause = MixPanelUtil.cause_timeout;

    public WaveMessageView(Context context) {
        super(context);
        init();
    }

    public WaveMessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WaveMessageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mParent = this;
        mParent.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        RelativeLayout parent = (RelativeLayout) inflate(getContext(), R.layout.wave_message_view, this);
        mSentContainer = (RelativeLayout) parent.findViewById(R.id.message_sent_container);
        mSendingContainer = (RelativeLayout) parent.findViewById(R.id.message_sending_container);
        mErrorContainer = (RelativeLayout) parent.findViewById(R.id.message_error_container);
        mResetContainer = (RelativeLayout) parent.findViewById(R.id.message_reset_container);

        //#191 Reset 화면에서 cancel 동작 수정함.
//        mResetContainer.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                dismissMessage();
//                return false;
//            }
//        });

        mResetIcon = (ImageView) parent.findViewById(R.id.message_reset_icon);
        mResetCounter = (TextView) parent.findViewById(R.id.message_reset_counter);
        mResetConfirmButton = (Button) parent.findViewById(R.id.message_confirm_button);
        mResetConfirmButton.setOnClickListener(this);
        mResetCancelButton = (Button) parent.findViewById(R.id.message_cancel_button);
        mResetCancelButton.setOnClickListener(this);
        mProgressIcon = (ImageView) parent.findViewById(R.id.message_sending_icon);

        mResetText = (TextView) parent.findViewById(R.id.message_reset_text);

        Typeface fontRoman = FontTypeface.getInstance().getFontRoman();
        setHelpMessage((TextView) parent.findViewById(R.id.message_error_text), fontRoman, super.getContext().getString(R.string.message_reset_error), "Sorry");
        setHelpMessage((TextView) parent.findViewById(R.id.message_reset_text), fontRoman, super.getContext().getString(R.string.message_reset_undo), "Reset");
        setHelpMessage((TextView) parent.findViewById(R.id.message_sending_text), fontRoman, super.getContext().getString(R.string.message_sending_email), "sending");
        setHelpMessage((TextView) parent.findViewById(R.id.message_sent_text), fontRoman, super.getContext().getString(R.string.message_success_email), "sent");

        generateWaveAnimator(parent);
    }

    private void setHelpMessage(TextView tv, Typeface tf, String original, String bold) {
        try {
            int location = original.indexOf(bold);
            SpannableStringBuilder SS = new SpannableStringBuilder(original);
            //SS.setSpan (new CustomTypeFaceSpan("", fontThin), 0, location-1,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            tv.setText(SS);
        }catch(Exception e) {
            Log.d(TAG, "setHelpMessage Exception ::: " + e);
        }
    }

    public SpannableStringBuilder getHelpBoldMessage(String original, String bold) {
        try {
            Typeface tf = FontTypeface.getInstance().getFontRoman();
            int location = original.indexOf(bold);
            SpannableStringBuilder SS = new SpannableStringBuilder(original);
            SS.setSpan(new CustomTypeFaceSpan("", tf), location, location + bold.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            return SS;
        }catch(Exception e) {
            Log.d(TAG, "setHelpMessage Exception ::: " + e);
        }
        return null;
    }

    private ObjectAnimator mResetRotateAnimator = null;
    private ObjectAnimator mShowAnimator = null;

    public void showResetMessageWithAnimation(final String cause) {
        if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
            return;

//        mResetText.setText(super.getContext().getString(R.string.message_reset_undo));
        mResetText.setText(getHelpBoldMessage(super.getContext().getString(R.string.message_reset_undo), "reset"));

        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();
        mShowAnimator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
        mShowAnimator.setDuration(500);
        mShowAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mParent.setVisibility(VISIBLE);
                resetCause = cause;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startResetCounter();
            }
        });
        mShowAnimator.start();

        if (mResetRotateAnimator != null && (mResetRotateAnimator.isRunning() || mResetRotateAnimator.isStarted()))
            mResetRotateAnimator.cancel();
        mResetRotateAnimator = ObjectAnimator.ofFloat(mResetIcon, "rotation", 0f, 360f);

        mResetRotateAnimator.setInterpolator(new LinearInterpolator());
        mResetRotateAnimator.setRepeatMode(ObjectAnimator.RESTART);
        mResetRotateAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        mResetRotateAnimator.setDuration(3000);
        mResetRotateAnimator.start();

        mSendingContainer.setVisibility(GONE);
        mSentContainer.setVisibility(GONE);
        mErrorContainer.setVisibility(GONE);
        mResetContainer.setVisibility(VISIBLE);
    }

    public void showResetMessageWithAnimation(final String cause, final SpannableStringBuilder ss) {

        if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
            return;

        if (ss == null || ss.length() == 0) {
//            mResetText.setText(super.getContext().getString(R.string.message_reset_undo));
            mResetText.setText(getHelpBoldMessage(super.getContext().getString(R.string.message_reset_undo), "reset"));
        }
        else {
            mResetText.setText(ss);
        }

        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();
        mShowAnimator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
        mShowAnimator.setDuration(500);
        mShowAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mParent.setVisibility(VISIBLE);
                resetCause = cause;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startResetCounter();
            }
        });
        mShowAnimator.start();

        if (mResetRotateAnimator != null && (mResetRotateAnimator.isRunning() || mResetRotateAnimator.isStarted()))
            mResetRotateAnimator.cancel();
        mResetRotateAnimator = ObjectAnimator.ofFloat(mResetIcon, "rotation", 0f, 360f);

        mResetRotateAnimator.setInterpolator(new LinearInterpolator());
        mResetRotateAnimator.setRepeatMode(ObjectAnimator.RESTART);
        mResetRotateAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        mResetRotateAnimator.setDuration(3000);
        mResetRotateAnimator.start();

        mSendingContainer.setVisibility(GONE);
        mSentContainer.setVisibility(GONE);
        mErrorContainer.setVisibility(GONE);
        mResetContainer.setVisibility(VISIBLE);
    }

    public void showErrorMessageWithAnimation() {
        if (!mSendingContainer.isShown()) {
            if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
                return;
            mShowAnimator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
            mShowAnimator.setDuration(500);
            mShowAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mParent.setVisibility(VISIBLE);
                }
            });
            mShowAnimator.start();

            ValueAnimator secTimerAnimator = ValueAnimator.ofFloat(0f, 1f);
            secTimerAnimator.setDuration(1000);
            secTimerAnimator.start();
            secTimerAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    dismissMessage();
                }
            });
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(mShowAnimator).before(secTimerAnimator);
            animatorSet.start();
        }
        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();

        mSendingContainer.setVisibility(GONE);
        mSentContainer.setVisibility(GONE);
        mErrorContainer.setVisibility(VISIBLE);
        mResetContainer.setVisibility(GONE);

        runDismissMessage(5000);
    }


    public void showCrashErrorMessageWithAnimation(final String errorMsg) {
        if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
            return;

        mResetText.setText(errorMsg);

        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();
        mShowAnimator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
        mShowAnimator.setDuration(500);
        mShowAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mParent.setVisibility(VISIBLE);
                resetCause = errorMsg;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startCrashResetCounter();
            }
        });
        mShowAnimator.start();

        if (mResetRotateAnimator != null && (mResetRotateAnimator.isRunning() || mResetRotateAnimator.isStarted()))
            mResetRotateAnimator.cancel();
        mResetRotateAnimator = ObjectAnimator.ofFloat(mResetIcon, "rotation", 0f, 360f);

        mResetRotateAnimator.setInterpolator(new LinearInterpolator());
        mResetRotateAnimator.setRepeatMode(ObjectAnimator.RESTART);
        mResetRotateAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        mResetRotateAnimator.setDuration(3000);
        mResetRotateAnimator.start();

        mSendingContainer.setVisibility(GONE);
        mSentContainer.setVisibility(GONE);
        mErrorContainer.setVisibility(GONE);
        mResetContainer.setVisibility(VISIBLE);
    }

    public void showSendingMessageWithAnimation() {
        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();
        if (!mParent.isShown()) {
            if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
                return;
            mShowAnimator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f);
            mShowAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mParent.setVisibility(VISIBLE);
                }
            });
            mShowAnimator.setDuration(500);
            mShowAnimator.start();
        }

        mSendingContainer.setVisibility(VISIBLE);
        mSentContainer.setVisibility(GONE);
        mErrorContainer.setVisibility(GONE);
        mResetContainer.setVisibility(GONE);
        if (mWaveValueAnimator != null)
            mWaveValueAnimator.start();

    }

    public void showSentMessage() {
        mSendingContainer.setVisibility(GONE);
        mSentContainer.setVisibility(VISIBLE);
        mErrorContainer.setVisibility(GONE);
        mResetContainer.setVisibility(GONE);
    }

    private ObjectAnimator mDismissAnimator;

    public void dismissMessage() {
        if (mDismissAnimator != null && (mDismissAnimator.isRunning() || mDismissAnimator.isStarted()))
            return;
        if (mShowAnimator != null && (mShowAnimator.isRunning() || mShowAnimator.isStarted()))
            return;
        mDismissAnimator = ObjectAnimator.ofFloat(this, "alpha", 1f, 0f);
        mDismissAnimator.setDuration(500);
        mDismissAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (mWaveValueAnimator != null && (mWaveValueAnimator.isRunning() || mWaveValueAnimator.isStarted()))
                    mWaveValueAnimator.cancel();
                if (mResetRotateAnimator != null && (mResetRotateAnimator.isRunning() || mResetRotateAnimator.isStarted()))
                    mResetRotateAnimator.cancel();
                if (mCounterHandler != null)
                    mCounterHandler.removeCallbacksAndMessages(null);

                mResetCounter.setText(mReset_start_time);
                mParent.setVisibility(GONE);
                if (mParent.getContext() instanceof FullscreenActivity)
                    ((FullscreenActivity) mParent.getContext()).startAutoResetHandler();
                else
                    ((CrashActivity) mParent.getContext()).finish();
//                ((MainActivity) mParent.getContext()).startAutoLoopHandler(0);
            }
        });
        mDismissAnimator.start();
    }

    // RESET COUNTER
    private CounterHandler mCounterHandler = null;

    private static final int MSG_HANDLE_COUNTER = 0;
    private static final int MSG_DISMISS_MESSAGE = 1;
    private static class CounterHandler extends Handler {
        private final WeakReference<WaveMessageView> parent;

        public CounterHandler(WaveMessageView parent) {
            this.parent = new WeakReference<>(parent);
        }

        @Override
        public void handleMessage(Message msg) {
            WaveMessageView parent = this.parent.get();
            switch (msg.what) {
                case MSG_HANDLE_COUNTER:
                    {
                        if (parent != null) {
                            parent.handleCounterMessage(msg);
                        }
                    }
                    break;

                case MSG_DISMISS_MESSAGE:
                    {
                        if (parent != null) {
                            parent.handleDismissMessage();
                        }
                    }
                    break;
            }
        }
    }

    private void handleCounterMessage(Message msg) {
        if (!mResetContainer.isShown()) return;
        int time = Integer.parseInt(mResetCounter.getText().toString());
        time--;
        if (time >= 0) {
            mResetCounter.setText(Integer.toString(time));
            mCounterHandler.sendEmptyMessageDelayed(MSG_HANDLE_COUNTER, 1000);
        } else {
            if (mParent.getContext() instanceof FullscreenActivity)
                ((FullscreenActivity) mParent.getContext()).resetAppTable(resetCause);
            else
                ((CrashActivity) mParent.getContext()).finish();
        }
    }

    private void stopResetCounter() {
        if (mCounterHandler != null)
            mCounterHandler.removeCallbacksAndMessages(null);
    }

    private void startResetCounter() {
        if (mCounterHandler != null)
            mCounterHandler.removeCallbacksAndMessages(null);
        else
            mCounterHandler = new CounterHandler(this);
        mResetCounter.setText(mReset_start_time);
        mCounterHandler.sendEmptyMessageDelayed(MSG_HANDLE_COUNTER, 1000);
    }

    private void startCrashResetCounter() {
        if (mCounterHandler != null)
            mCounterHandler.removeCallbacksAndMessages(null);
        else
            mCounterHandler = new CounterHandler(this);
        mResetCounter.setText("30");
        mResetCancelButton.setVisibility(View.GONE);

        RelativeLayout.LayoutParams labelLayoutParams = (RelativeLayout.LayoutParams) mResetConfirmButton.getLayoutParams();
        labelLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mResetConfirmButton.setLayoutParams(labelLayoutParams);
        mCounterHandler.sendEmptyMessageDelayed(MSG_HANDLE_COUNTER, 1000);
    }

    public void runDismissMessage(int millisec) {
        if (mCounterHandler != null)
            mCounterHandler.removeCallbacksAndMessages(null);
        else
            mCounterHandler = new CounterHandler(this);
        mCounterHandler.sendEmptyMessageDelayed(MSG_DISMISS_MESSAGE, millisec);
    }
    private void handleDismissMessage() {
        dismissMessage();
        if (mCounterHandler != null)
            mCounterHandler.removeCallbacksAndMessages(null);
    }

    private int red1, red2;
    private int green1, green2;
    private int blue1, blue2;
    private ValueAnimator mWaveValueAnimator = null;
    private PaintDrawable mPaintDrawable = null;
    private RectShape rectShape = null;

    private void generateWaveAnimator(final View view) {
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                LinearGradient lg = new LinearGradient(0, view.getHeight(), view.getWidth(), 0,
                        new int[]{Color.argb(229, red1, green1, blue1), Color.argb(229, red2, green2, blue2)}, //substitute the correct colors for these
                        new float[]{0.2f, 0.8f}, Shader.TileMode.REPEAT);
                return lg;
            }
        };
        mPaintDrawable = new PaintDrawable();
        rectShape = new RectShape();
        mPaintDrawable.setShape(rectShape);
        mPaintDrawable.setShaderFactory(sf);
        //view.setBackgroundDrawable((Drawable) mPaintDrawable);
        view.setBackground(mPaintDrawable);
        mWaveValueAnimator = ValueAnimator.ofFloat(0.0f, 10.0f);
        mWaveValueAnimator.setDuration(30000);
        mWaveValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float fValue = (float) animation.getAnimatedValue();

                red1 = (int) (Math.sin(Math.PI * (fValue + 0.5f) + 4.0f) * 23) + 24;
                green1 = (int) (Math.sin(Math.PI * (fValue + 0.5f) + 2.0f) * 127) + 128;
                blue1 = 255;//(int)(Math.sin(Math.PI*fValue + 0.0f) * 127) + 128;

                red2 = (int) (Math.sin(Math.PI * (fValue + 0.0f) + 4.0f) * 23) + 24;
                green2 = (int) (Math.sin(Math.PI * (fValue + 0.0f) + 2.0f) * 127) + 128;
                blue2 = 255;//(int)(Math.sin(Math.PI*(fValue+0.5f) + 0.0f) * 127) + 128;

                mPaintDrawable.setShape(rectShape); // this makes the shader recreate the lineargradient
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.equals(mResetConfirmButton)) {
            stopResetCounter();
            if (getContext() instanceof FullscreenActivity)
                ((FullscreenActivity) getContext()).resetAppTable(resetCause);
            else
                ((CrashActivity) getContext()).finish();
        } else if (view.equals(mResetCancelButton)) {
            dismissMessage();
        }
    }
}
