package com.tecace.showcase;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.tecace.showcase.data.AppDataManager;
import com.tecace.showcase.util.AppConst;
import com.tecace.showcase.util.FontTypeface;
import com.tecace.showcase.util.FontsOverride;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by wssun on 6/03/15.
 */
public class ShowcaseApplication extends Application {
    private final String TAG = this.getClass().getCanonicalName();

    private static ShowcaseApplication singleton;

    private AppDataManager appDataManager;
    private static DisplayImageOptions options;

    @Override
    public void onCreate() {
        super.onCreate();

        singleton = this;

        Gson gson = new Gson();
        BufferedReader br = null;
        try {
            br = new BufferedReader( new FileReader(AppConst.APPDATA_JSON_PATH) );

            /*
             * for Lynx 4K device test
             */
//            String appDataJsonPath = AppConst.getAppDataPath();
//            br = new BufferedReader( new FileReader(appDataJsonPath) );

            appDataManager = gson.fromJson(br, AppDataManager.class);
            appDataManager.generatePassionColors();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }

        // font override
        FontsOverride.setDefaultFont(this, "DEFAULT", AppConst.PATH_FONT_ROMAN);
        FontsOverride.setDefaultFont(this, "MONOSPACE", AppConst.PATH_FONT_THIN);
        FontsOverride.setDefaultFont(this, "SERIF", AppConst.PATH_FONT_LIGHT);
        FontsOverride.setDefaultFont(this, "SANS_SERIF", AppConst.PATH_FONT_MEDIUM);

        // font typeface
        FontTypeface.getInstance().setAppContext(this);

        initImageLoader(getApplicationContext());
    }

    public static ShowcaseApplication getInstance(){
        return singleton;
    }

    public AppDataManager getAppDataManager() {
        return appDataManager;
    }

    public DisplayImageOptions getUILOptions() {
        if (null == options) {
            BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
            resizeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            resizeOptions.inDither = true;
            resizeOptions.inSampleSize = 2;
            options = new DisplayImageOptions.Builder()
                    //.showImageOnLoading(R.drawable.in_progress)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .cacheOnDisk(true)
                    .cacheInMemory(false)
                    .considerExifParams(true)
                    .decodingOptions(resizeOptions)
                    .build();
        }

        return options;
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
