package com.tecace.showcase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by icanmobile on 9/21/15.
 */
public class CallToActionView extends RelativeLayout {
    private static final String TAG = CallToActionView.class.getName();

    private boolean mGhostHandRunning = false;
    private RelativeLayout mParent = null;
    private ImageView mGhostTouchView = null;
    private RelativeLayout mGhostAnimationContainner = null;
    private RelativeLayout mGhostAnimationLayout = null;

    public CallToActionView(Context context) {
        super(context);
        init();
    }

    public CallToActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CallToActionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mParent = this;

        RelativeLayout view = (RelativeLayout) inflate(getContext(), R.layout.call_to_action_view, this);
        mGhostTouchView = (ImageView) view.findViewById(R.id.ghost_touch_view);

        mGhostAnimationContainner = (RelativeLayout) view.findViewById(R.id.ghost_animation_containner);
        mGhostAnimationLayout = (RelativeLayout) view.findViewById(R.id.ghost_animation_layout);
        mGhostAnimationLayout.post(new Runnable() {
            @Override
            public void run() {
                generateGhostAnimator(mGhostAnimationLayout);
            }
        });

        mParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();
                mListener.onCallToActionClicked();
            }
        });
    }

    public boolean isGhostHandRunning() {
        return this.mGhostHandRunning;
    }

    public void show() {
        mGhostHandRunning = true;
        ((FullscreenActivity) getContext()).stopAutoResetHandler();
        startGhostAnimation();
    }

    public void hide() {
        mGhostHandRunning = false;
        hideTouchView();
        stopGhostAnimation();
    }

    private OnCallToActionViewClickListener mListener = null;
    public void setCallToActionViewClickListener(OnCallToActionViewClickListener listener) {
        mListener = listener;
    }
    public interface OnCallToActionViewClickListener {
        void onCallToActionClicked();
    }

    /*
     * Ghost Touch View
     */
    public void setTouchView(int x, int y) {
        if (!mParent.isShown()) return;

        showTouchView();
        mGhostTouchView.setScaleX(1f);
        mGhostTouchView.setScaleY(1f);
        ((RelativeLayout.LayoutParams) mGhostTouchView.getLayoutParams()).leftMargin = x - mGhostTouchView.getMeasuredWidth() / 2;
        ((RelativeLayout.LayoutParams) mGhostTouchView.getLayoutParams()).topMargin = y - mGhostTouchView.getMeasuredHeight() / 2;
        mGhostTouchView.requestLayout();
    }

    float mScaleX = 1f;
    float mScaleY = 1f;
    public void setTouchViewScale(float scaleX, float scaleY) {
        if (!mParent.isShown()) return;

        if (mScaleX != scaleX) {
            mGhostTouchView.setScaleX(scaleX);
            mScaleX = scaleX;
        }

        if (mScaleY != scaleY) {
            mGhostTouchView.setScaleY(scaleY);
            mScaleY = scaleY;
        }
    }

    public void setTouchViewScale(float scale) {
        if (!mParent.isShown()) return;
        if (mScaleX == scale || mScaleY == scale) return;

        mGhostTouchView.setScaleX(scale);
        mGhostTouchView.setScaleY(scale);
    }

    public void showTouchView() {
        mGhostTouchView.setVisibility(View.VISIBLE);
    }

    public void hideTouchView() {
        mGhostTouchView.setVisibility(View.GONE);
    }


    /*
     * Wave animation
     */
    private final int FADE_ANIMATION_TIME = 500;
    public void startGhostAnimation() {
        if (mGhostAnimator.isRunning()) return;

        mParent.setVisibility(View.VISIBLE);
        mGhostAnimationContainner.setAlpha(0f);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(mGhostAnimationContainner, "alpha",  0f, 1f);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(FADE_ANIMATION_TIME);
        fadeIn.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationEnd(animation);
                mGhostAnimator.start();
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mGhostAnimationContainner.setAlpha(1f);
            }
        });
        fadeIn.start();
    }

    public void stopGhostAnimation() {
        if (mGhostAnimator.isRunning()) {
            ObjectAnimator fadeOut = ObjectAnimator.ofFloat(mGhostAnimationContainner, "alpha",  1f, 0f);
            fadeOut.setInterpolator(new DecelerateInterpolator()); //add this
            fadeOut.setDuration(FADE_ANIMATION_TIME);

            fadeOut.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mGhostAnimationContainner.setAlpha(0f);
                    mGhostAnimator.cancel();
                    mParent.setVisibility(View.GONE);
                }
            });
            fadeOut.start();
        }
    }

    private int red1, red2;
    private int green1, green2;
    private int blue1, blue2;
    private ValueAnimator mGhostAnimator = null;
    private PaintDrawable mPaintDrawable = null;
    private OvalShape ovalShape = null;

    private void generateGhostAnimator(final View view) {
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {

            @Override
            public Shader resize(int width, int height) {
                float radius = (view.getWidth() + 2) / 2; //remove view border
                RadialGradient rg = new RadialGradient(view.getWidth() / 2, view.getWidth() / 2, radius,
                        Color.argb(255, red1, green1, blue1),
                        Color.argb(0, red2, green2, blue2),
                        Shader.TileMode.REPEAT);
                return rg;
            }
        };

        mPaintDrawable = new PaintDrawable();
        ovalShape = new OvalShape();
        mPaintDrawable.setShape(ovalShape);
        mPaintDrawable.setShaderFactory(sf);
        view.setBackground(mPaintDrawable);
        mGhostAnimator = ValueAnimator.ofFloat(0.0f, 10.0f);
        mGhostAnimator.setDuration(30000);
        mGhostAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mGhostAnimator.setRepeatMode(ValueAnimator.RESTART);
        mGhostAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float fValue = (float) animation.getAnimatedValue();

                red1 = (int) (Math.sin(Math.PI * (fValue + 0.5f) + 4.0f) * 23) + 24;
                green1 = (int) (Math.sin(Math.PI * (fValue + 0.5f) + 2.0f) * 127) + 128;
                blue1 = 255;//(int)(Math.sin(Math.PI*fValue + 0.0f) * 127) + 128;

                red2 = (int) (Math.sin(Math.PI * (fValue + 0.0f) + 4.0f) * 23) + 24;
                green2 = (int) (Math.sin(Math.PI * (fValue + 0.0f) + 2.0f) * 127) + 128;
                blue2 = 255;//(int)(Math.sin(Math.PI*(fValue+0.5f) + 0.0f) * 127) + 128;

                mPaintDrawable.setShape(ovalShape); // this makes the shader recreate the linear-gradient
            }
        });
    }
}
