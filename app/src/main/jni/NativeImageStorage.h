#include <jni.h>
#include <stdio.h>
#include <cstring>
#include <pthread.h>
#include <time.h>
#include <android/log.h>
#include <android/bitmap.h>

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, "ShowcaseJNI", __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "ShowcaseJNI", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , "ShowcaseJNI", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , "ShowcaseJNI", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , "ShowcaseJNI", __VA_ARGS__)

#define STATE_NEW 0
#define STATE_OLD 1
#define MODE_CLEAR 0
#define MODE_BLUR 1
#define MAXIMUM_SIZE 30
extern "C"{
	void fillPixelsInBitmapObject(JNIEnv * env, jobject &bitmap, uint32_t* pixels);
	JNIEXPORT jobject JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_init(JNIEnv * env, jobject obj, jint width, jint height);
	JNIEXPORT jobject JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_pullImageFromNativeHeap(JNIEnv * env, jobject obj, jint index, jint state, jint mode);
	JNIEXPORT void JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_storeImageInNativeHeap(JNIEnv * env, jobject obj, jobject bitmap, jint index, jint mode);
	JNIEXPORT void JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_releaseImageStorage(JNIEnv * env, jobject obj);
}

