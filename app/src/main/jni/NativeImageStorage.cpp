#include "NativeImageStorage.h"

extern "C"{

uint32_t* gClearBitmapPixels[MAXIMUM_SIZE];
uint32_t* gBlurBitmapPixels[MAXIMUM_SIZE];
jobject gNewClearBitmap, gOldClearBitmap;
jobject gNewBlurBitmap, gOldBlurBitmap;

JNIEXPORT jobject JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_init(JNIEnv * env, jobject obj, jint width, jint height){
	
	// CREATE BITMAP CLASS
	jclass bitmapCls = env->FindClass("android/graphics/Bitmap");
	jmethodID createBitmapFunction = env->GetStaticMethodID(bitmapCls,
		"createBitmap",
		"(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
	jstring configName = env->NewStringUTF("ARGB_8888");
	jclass bitmapConfigClass = env->FindClass("android/graphics/Bitmap$Config");
	jmethodID valueOfBitmapConfigFunction = env->GetStaticMethodID(
		bitmapConfigClass, "valueOf",
		"(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;");
	jobject bitmapConfig = env->CallStaticObjectMethod(bitmapConfigClass,
		valueOfBitmapConfigFunction, configName);
	
	// CREATE GLOBAL NEW BLUR BITMAP
	jobject newBlurBitmap = env->CallStaticObjectMethod(bitmapCls,createBitmapFunction, width,height, bitmapConfig);
	gNewBlurBitmap = env->NewGlobalRef(newBlurBitmap);
	
	// CREATE GLOBAL NEW CLEAR BITMAP
	jobject newClearBitmap = env->CallStaticObjectMethod(bitmapCls,createBitmapFunction, width,height, bitmapConfig);
	gNewClearBitmap = env->NewGlobalRef(newClearBitmap);	
	
	// CREATE GLOBAL OLD BLUR BITMAP
	jobject oldBlurBitmap = env->CallStaticObjectMethod(bitmapCls,createBitmapFunction, width,height, bitmapConfig);
	gOldBlurBitmap = env->NewGlobalRef(oldBlurBitmap);
	
	// CREATE GLOBAL OLD CLEAR BITMAP
	jobject oldClearBitmap = env->CallStaticObjectMethod(bitmapCls,createBitmapFunction, width,height, bitmapConfig);
	gOldClearBitmap = env->NewGlobalRef(oldClearBitmap);	
	LOGI("Native Image Storage is initailized");
}

// GET OLD BLUR IMAGE
JNIEXPORT jobject JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_pullImageFromNativeHeap(JNIEnv * env, jobject obj, jint index, jint state, jint mode){
	if(state==STATE_NEW && mode == MODE_CLEAR){
		fillPixelsInBitmapObject(env, gNewClearBitmap, gClearBitmapPixels[index]);
		return gNewClearBitmap;
	}else if(state==STATE_NEW && mode == MODE_BLUR){
		fillPixelsInBitmapObject(env, gNewBlurBitmap, gBlurBitmapPixels[index]);
		return gNewBlurBitmap;
	}else if(state==STATE_OLD && mode == MODE_CLEAR){
		fillPixelsInBitmapObject(env, gOldClearBitmap, gClearBitmapPixels[index]);
		return gOldClearBitmap;
	}else if(state==STATE_OLD && mode == MODE_BLUR){
		fillPixelsInBitmapObject(env, gOldBlurBitmap, gBlurBitmapPixels[index]);
		return gOldBlurBitmap;
	}else{
		return NULL;
	}
}

void fillPixelsInBitmapObject(JNIEnv * env, jobject &bitmap, uint32_t* pixels){
	int ret;
	void* copybitmapPixels;
	AndroidBitmapInfo bitmapInfo;
	if ((ret = AndroidBitmap_getInfo(env, bitmap, &bitmapInfo)) < 0){
		LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
		return;
	}
	
    if ((ret = AndroidBitmap_lockPixels(env, bitmap, &copybitmapPixels)) < 0){
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
	}
	
    uint32_t* newBitmapPixels = (uint32_t*) copybitmapPixels;
    int pixelsCount = bitmapInfo.height * bitmapInfo.width;
    memcpy(newBitmapPixels, pixels,sizeof(uint32_t) * pixelsCount);
    AndroidBitmap_unlockPixels(env, bitmap);
}

// PUT CLEAR IMAGE
JNIEXPORT void JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_storeImageInNativeHeap(JNIEnv * env, jobject obj, jobject bitmap, jint index, jint mode){
	AndroidBitmapInfo bitmapInfo,gbitmapInfo;
	uint32_t* storedBitmapPixels = NULL;
	
	int ret;
	if ((ret = AndroidBitmap_getInfo(env, bitmap, &bitmapInfo)) < 0){
		LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
		return;
	}
	if ((ret = AndroidBitmap_getInfo(env, gOldClearBitmap, &gbitmapInfo)) < 0){
		LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
		return;
	}
	if (bitmapInfo.format != ANDROID_BITMAP_FORMAT_RGBA_8888){
		LOGE("Bitmap format is not RGBA_8888");
		return;
	}
	if (bitmapInfo.width != gbitmapInfo.width || bitmapInfo.height != gbitmapInfo.height){
		LOGE("The background is difference with initialized size : %dx%d (init:%d%d)",bitmapInfo.width,bitmapInfo.height,gbitmapInfo.width,gbitmapInfo.height);
		return;
	}
	void* bitmapPixels;
    if ((ret = AndroidBitmap_lockPixels(env, bitmap, &bitmapPixels)) < 0){
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
    }
    uint32_t* src = (uint32_t*) bitmapPixels;
	if(mode == MODE_CLEAR)
		gClearBitmapPixels[index] = new uint32_t[bitmapInfo.height * bitmapInfo.width];
	else
		gBlurBitmapPixels[index] = new uint32_t[bitmapInfo.height * bitmapInfo.width];
    int pixelsCount = bitmapInfo.height * bitmapInfo.width;
	if(mode == MODE_CLEAR){
		memcpy(gClearBitmapPixels[index], src, sizeof(uint32_t) * pixelsCount);
		LOGI("%d background(blur) bitmap is saved in native heap",index);
	}else{
		memcpy(gBlurBitmapPixels[index], src, sizeof(uint32_t) * pixelsCount);
		LOGI("%d background(clear) bitmap is saved in native heap",index);
	}
	
    return;
}

JNIEXPORT void JNICALL Java_com_tecace_showcase_preview_NativeImageStorage_releaseImageStorage(JNIEnv * env, jobject obj){
    for(int i = 0; i < MAXIMUM_SIZE; i++){
		if(gBlurBitmapPixels[i] != NULL){
			free(gBlurBitmapPixels[i]);
			LOGI("%d background(blur) bitmap is released", i);
		}
		if(gClearBitmapPixels[i] != NULL){
			free(gClearBitmapPixels[i]);
			LOGI("%d background(clear) bitmap is released", i);
		}
    }
}

} // END OF EXTERN "C"